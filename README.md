This is the prototype app for **Data Analytics as a Service**!

To run the app:

1. Clone the repo
2. Use `npm install` to install dependencies
3. Run `npm start` to run the app
4. Connect to [http://localhost:3000](http://localhost:3000) to view the app

---

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run storybook`

Launches the Storybook UI.<br>
Open [http://localhost:6006/](http://localhost:6006/) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm install --global surge`

Installs the Surge plugin

### `surge` or `npx surge`

To initiate login to Surge for future updates of the repository

### `npm run build-storybook` then...

### `node_modules/.bin/surge ./storybook-static https://uxdc-daaas-storybook.surge.sh`

### OR alternatively - `npx surge ./storybook-static https://uxdc-daaas-storybook.surge.sh`

Deploys the repo to 'https://uxdc-daaas-storybook.surge.sh' for external use
Change this link to something more specific when deploying a branch for testing (eg. https://uxdc-daaas-storybook-dialog.surge.sh)

### `node_modules/.bin/surge teardown https://uxdc-daaas-storybook-dialog.surge.sh`

Used for deleting an existing deployed repo
Make sure to specify the corrent link to be torn down that relates to your branch

Any MRs that have changes in `src/Components/CommonComponents` or `src/Theme` need to have the `DESIGN SYSTEM UPDATED` label added to them before merge.

## Using Global State

[Official documentation](https://www.npmjs.com/package/react-hooks-global-state)

1. Import the global state library into page 1 (the page you will be exporting the state from) using: `import {createGlobalState} from 'react-hooks-global-state';`
2. Within page 1, create an object to reference for access to state properties: `export const {useGlobalState, setGlobalState} = createGlobalState({ example: 0 });` (in this example, 'example: 0' is added and will be targeted in step 4)
3. In page 2, import the Global State from page 1, but re-label it in case Global State is imported from multiple different pages: `import {useGlobalState as exampleStates}, from './Example`
4. In page 2, within any components using the Global State, define a new variable: `const [newVariableName, setNewVariable] = exampleStates('example')` (the variable will currently have a value of 0 from step 2)
5. To modify the state from page 1: `setGlobalState('example', 999);`
6. To modify the state from page 2: `setNewVariable(999);` (setNewVariable function is created and step 4, and can be called something else)

### Page 1 example

```
import {createGlobalState} from 'react-hooks-global-state';

export const {useGlobalState, setGlobalState} = createGlobalState({
  example: 0,
});

export const Page1Component = () => {
  function modifyGlobalState() {
    setGlobalState('example', 999);
  }

  return (
    <>
      <button onClick={modifyGlobalState}>Modify from page 1</button>
    </>
  );
};
```

### Page 2 example

```
import {useGlobalState as exampleStates} from './ExamplePage1';

export const Page2Component = () => {
  const [newVariableName, setNewVariable] = exampleStates('example');

  function modifyGlobalState() {
    setNewVariable(999);
  }
  return (
    <>
      <button onClick={modifyGlobalState}>Modify from page 2</button>
    </>
  );
};
```
