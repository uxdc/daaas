import React from 'react';
import {styled} from '@mui/material/styles';
import {Alert} from '../Components/CommonComponents/Alert';
import {Button} from '@mui/material';

const PREFIX = 'Default';

const classes = {
  marginFix: `${PREFIX}-marginFix`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.marginFix}`]: {
    marginLeft: theme.spacing(-2),
    marginRight: theme.spacing(2),
  },
}));

export default {
  title: 'Molecules/Alert',
  component: Alert,
};

export const Default = (args) => {
  const [state, setState] = React.useState({
    error: true,
    warning: true,
    info: true,
    success: true,
  });

  function handleCloseAlert(value) {
    setState({...state, [value]: false});
  }

  const handleReopen = () => {
    setState({error: true, info: true, success: true, warning: true});
  };

  return (
    <Root>
      <Alert
        open={state.error}
        severity="error"
        message="Example alert message (Error)"
        handleClose={() => handleCloseAlert('error')}
        sx={{mb: 3}}
      />
      <Alert
        open={state.warning}
        severity="warning"
        message="Example alert message (Warning)"
        handleClose={() => handleCloseAlert('warning')}
        sx={{mb: 3}}
      />
      <Alert
        open={state.info}
        severity="info"
        message="Example alert message (Info)"
        handleClose={() => handleCloseAlert('info')}
        sx={{mb: 3}}
      />
      <Alert
        open={state.success}
        severity="success"
        message="Example alert message (Success)"
        handleClose={() => handleCloseAlert('success')}
        sx={{mb: 3}}
      />
      <Button color="primary" variant="contained" onClick={handleReopen}>
        Reopen
      </Button>
    </Root>
  );
};

export const Banners = (args) => {
  const [state, setState] = React.useState({
    error: true,
    warning: true,
    info: true,
    success: true,
  });

  function handleCloseAlert(value) {
    setState({...state, [value]: false});
  }

  const handleReopen = () => {
    setState({error: true, info: true, success: true, warning: true});
  };

  return (
    <>
      <Alert
        open={state.error}
        severity="error"
        message="Example banner message (Error)"
        handleClose={() => handleCloseAlert('error')}
        banner={true}
        className={classes.marginFix}
      />
      <Alert
        open={state.warning}
        severity="warning"
        message="Example banner message (Warning)"
        handleClose={() => handleCloseAlert('warning')}
        banner={true}
        className={classes.marginFix}
      />
      <Alert
        open={state.info}
        severity="info"
        message="Example banner message (Info)"
        handleClose={() => handleCloseAlert('info')}
        banner={true}
        className={classes.marginFix}
      />
      <Alert
        open={state.success}
        severity="success"
        message="Example banner message (Success)"
        handleClose={() => handleCloseAlert('success')}
        banner={true}
        className={classes.marginFix}
      />
      <Button
        sx={{mt: 3}}
        color="primary"
        variant="contained"
        onClick={handleReopen}
      >
        Reopen
      </Button>
    </>
  );
};
