import React from 'react';
import {
  Typography,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  Divider,
  Box,
} from '@mui/material';
import TextField from '../Components/CommonComponents/TextField';

export default {
  title: 'Lab/Custom TextField',
  component: TextField,
};

export const Readonly = (args) => {
  const [state, setState] = React.useState('readonly');

  const handleChange = (event) => {
    setState(event.target.value);
  };

  return (
    <>
      <Typography component="h1" variant="h5" sx={{mb: 3}}>
        Custom TextField
      </Typography>
      <Divider sx={{mb: 3}} />
      <Box sx={{mb: 2}}>
        <FormControl component="fieldset">
          <FormLabel component="legend">States</FormLabel>
          <RadioGroup
            aria-label="helper text readonly"
            name="helper-text-read-only"
            value={state.toString()}
            onChange={handleChange}
          >
            <FormControlLabel
              value="default"
              control={<Radio color="primary" />}
              label="Default"
            />
            <FormControlLabel
              value="error"
              control={<Radio color="primary" />}
              label="Error"
            />
            <FormControlLabel
              value="readonly"
              control={<Radio color="primary" />}
              label="Read-only"
            />
            <FormControlLabel
              value="disabled"
              control={<Radio color="primary" />}
              label="Disabled"
            />
          </RadioGroup>
        </FormControl>
      </Box>
      {/* <Divider sx={{mb: 3}} /> */}
      {/* <Typography component="h2" variant="h6" sx={{mb: 3}}>
        Basic
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Empty
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Filled
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="h2" variant="h6" sx={{mb: 3}}>
        Required
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Empty
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Filled
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography> */}
      {/* <Typography component="h2" variant="h6" sx={{mb: 3}}>
        Helper text
      </Typography> */}
      <Typography component="p" variant="subtitle2" sx={{mb: 2}}>
        Empty
      </Typography>
      <Box sx={{mb: 3}}>
        <TextField
          label="Label text"
          readOnly={state === 'readonly'}
          error={state === 'error'}
          disabled={state === 'disabled'}
          defaultValue=""
          helperText="Helper text"
        />
      </Box>
      <Typography component="p" variant="subtitle2" sx={{mb: 2}}>
        Filled
      </Typography>
      <Box sx={{mb: 3}}>
        <TextField
          label="Label text"
          readOnly={state === 'readonly'}
          error={state === 'error'}
          disabled={state === 'disabled'}
          defaultValue="Input text"
          helperText="Helper text"
        />
      </Box>
      {/* <Typography component="h2" variant="h6" sx={{mb: 3}}>
        Placeholder text
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Empty
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Filled
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="h2" variant="h6" sx={{mb: 3}}>
        Adornment - Text start
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Empty
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Filled
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="h2" variant="h6" sx={{mb: 3}}>
        Adornment - Text end
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Empty
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Filled
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="h2" variant="h6" sx={{mb: 3}}>
        Adornment - Icon start
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Empty
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Filled
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="h2" variant="h6" sx={{mb: 3}}>
        Adornment - Icon end
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Empty
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Filled
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="h2" variant="h6" sx={{mb: 3}}>
        Adornment - Button
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Empty
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Filled
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="h2" variant="h6" sx={{mb: 3}}>
        Multiline - Single row
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Empty
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Filled
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="h2" variant="h6" sx={{mb: 3}}>
        Multiline - Multiple row
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Empty
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography>
      <Typography component="p" variant="subtitle2" sx={{mb: 3}}>
        Filled
      </Typography>
      <Typography component="p" variant="body2" sx={{mb: 3}}>
        --
      </Typography> */}
    </>
  );
};
