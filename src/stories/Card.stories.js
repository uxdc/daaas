import React from 'react';
import {styled} from '@mui/material/styles';
import {Card} from '../Components/CommonComponents/Card';
import {Paper, Typography, Button} from '@mui/material';
import Icon from '@mdi/react';
import {mdiPlus} from '@mdi/js';
import {alpha} from '@mui/material/styles';

const PREFIX = 'Default';

const classes = {
  addBtn: `${PREFIX}-addBtn`,
  maxWidth: `${PREFIX}-maxWidth`,
  cardContainer: `${PREFIX}-cardContainer`,
  errorText: `${PREFIX}-errorText`,
};

const Root = styled('div')(({theme}) => ({
  [`& .${classes.addBtn}`]: {
    'marginTop': theme.spacing(-1),
    'borderStyle': 'dashed',
    'width': '100%',
    'borderColor': alpha(theme.palette.common.black, 0.23),
    '&.MuiButton-outlinedPrimary:hover': {
      borderStyle: 'dashed',
    },
  },

  [`&.${classes.maxWidth}`]: {
    maxWidth: '640px',
  },

  [`& .${classes.cardContainer}`]: {
    padding: theme.spacing(2),
    backgroundColor: theme.palette.grey[100],
    borderColor: alpha(theme.palette.common.black, 0.08),
    marginBottom: theme.spacing(3),
  },

  [`& .${classes.errorText}`]: {
    color: theme.palette.error.main,
  },
}));

export default {
  title: 'Molecules/Card',
  component: Card,
};

export const Default = (args) => {
  return (
    <Root className={classes.maxWidth}>
      <Typography id="add-card-header" variant="body2" component="p">
        Add card... <span aria-hidden="true">*</span>
        <span className="screen-reader-text">required</span>
      </Typography>
      <Typography
        variant="caption"
        color="textSecondary"
        component="p"
        sx={{mb: 2}}
      >
        At least one card must be added
      </Typography>
      <Paper className={classes.cardContainer} variant="outlined">
        <Card
          title="Card title"
          error={false}
          primaryButton="Edit"
          secondaryButton="Delete"
          content={
            <>
              <Typography variant="body2" component="p">
                This is the content for the card.
              </Typography>
            </>
          }
        />
      </Paper>
      <Button
        aria-describedby="add-card-header"
        variant="outlined"
        color="primary"
        startIcon={<Icon path={mdiPlus} size={1} />}
        fullWidth={true}
        className={classes.addBtn}
      >
        Add card
      </Button>
    </Root>
  );
};

export const Errors = (args) => {
  return (
    <Root className={classes.maxWidth}>
      <Typography id="add-card-header" variant="body2" component="p">
        Add card... <span aria-hidden="true">*</span>
        <span className="screen-reader-text">required</span>
      </Typography>
      <Typography
        variant="caption"
        color="textSecondary"
        component="p"
        sx={{mb: 2}}
      >
        At least one card must be added
      </Typography>
      <Paper className={classes.cardContainer} variant="outlined">
        <Card
          title="Card title"
          error={true}
          totalErrors={200}
          primaryButton="Edit"
          secondaryButton="Delete"
          content={
            <>
              <Typography variant="body2" component="p">
                This is the content for the card.
              </Typography>
            </>
          }
        />
      </Paper>
      <Button
        aria-describedby="add-card-header"
        variant="outlined"
        color="primary"
        startIcon={<Icon path={mdiPlus} size={1} />}
        fullWidth={true}
        className={classes.addBtn}
      >
        Add card
      </Button>
    </Root>
  );
};

export const NoCard = (args) => {
  return (
    <Root className={classes.maxWidth}>
      <Typography id="add-card-header" variant="body2" component="p">
        Add card... <span aria-hidden="true">*</span>
        <span className="screen-reader-text">required</span>
      </Typography>
      <Typography
        variant="caption"
        color="textSecondary"
        component="p"
        sx={{mb: 2}}
      >
        At least one card must be added
      </Typography>
      <Paper className={classes.cardContainer} variant="outlined">
        <Typography variant="caption" component="p" color="textSecondary">
          No cards added
        </Typography>
      </Paper>
      <Button
        aria-describedby="add-card-header"
        variant="outlined"
        color="primary"
        startIcon={<Icon path={mdiPlus} size={1} />}
        fullWidth={true}
        className={classes.addBtn}
      >
        Add card
      </Button>
    </Root>
  );
};

export const NoCardError = (args) => {
  return (
    <Root className={classes.maxWidth}>
      <Typography
        id="add-card-header"
        variant="body2"
        component="p"
        className={classes.errorText}
      >
        Add card... <span aria-hidden="true">*</span>
        <span className="screen-reader-text">required</span>
      </Typography>
      <Typography
        variant="caption"
        color="textSecondary"
        component="p"
        className={classes.errorText}
        sx={{mb: 2}}
      >
        At least one card must be added
      </Typography>
      <Paper className={classes.cardContainer} variant="outlined">
        <Typography variant="caption" component="p" color="textSecondary">
          No cards added
        </Typography>
      </Paper>
      <Button
        aria-describedby="add-card-header"
        variant="outlined"
        color="primary"
        startIcon={<Icon path={mdiPlus} size={1} />}
        fullWidth={true}
        className={classes.addBtn}
      >
        Add card
      </Button>
    </Root>
  );
};

export const ReadOnly = (args) => {
  return (
    <Root className={classes.maxWidth}>
      <Typography id="add-card-header" variant="body2" component="p">
        Add card... <span aria-hidden="true">*</span>
        <span className="screen-reader-text">required</span>
      </Typography>
      <Typography
        variant="caption"
        color="textSecondary"
        component="p"
        sx={{mb: 2}}
      >
        At least one card must be added
      </Typography>
      <Paper className={classes.cardContainer} variant="outlined">
        <Card
          title="Card title"
          error={false}
          primaryButton="View details"
          content={
            <>
              <Typography variant="body2" component="p">
                This is the content for the card.
              </Typography>
            </>
          }
        />
      </Paper>
    </Root>
  );
};

export const NoCardReadOnly = (args) => {
  return (
    <Root className={classes.maxWidth}>
      <Typography id="add-card-header" variant="body2" component="p">
        Add card... <span aria-hidden="true">*</span>
        <span className="screen-reader-text">required</span>
      </Typography>
      <Typography
        variant="caption"
        color="textSecondary"
        component="p"
        sx={{mb: 2}}
      >
        At least one card must be added
      </Typography>
      <Paper className={classes.cardContainer} variant="outlined">
        <Typography variant="caption" component="p" color="textSecondary">
          No cards added
        </Typography>
      </Paper>
    </Root>
  );
};
