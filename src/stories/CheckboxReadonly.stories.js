import React from 'react';
import {
  Checkbox,
  FormGroup,
  FormControl,
  FormLabel,
  FormControlLabel,
  FormHelperText,
  Typography,
} from '@mui/material';

export default {
  title: 'Lab/Read-only Checkbox',
  component: FormGroup,
  argTypes: {},
};

export const Group = (args) => {
  const [state, setState] = React.useState({
    readonly: false,
  });

  const handleChbxChange = (event) => {
    if (event.target.dataset.readonly) {
      return false;
    } else {
      setState({...state, [event.target.name]: event.target.checked});
    }
  };

  return (
    <>
      <FormControl component="fieldset" aria-describedby="helperText">
        <FormLabel component="legend">Read-only Checkboxes</FormLabel>
        <FormHelperText id="helperText">Select all that apply</FormHelperText>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={true}
                onChange={handleChbxChange}
                inputProps={{
                  'data-readonly': true,
                }}
                name="readonly"
                aria-label="read only"
              />
            }
            label={
              <>
                <Typography variant="body2">Non editable</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Read only
                </Typography>
              </>
            }
          />
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={false}
                onChange={handleChbxChange}
                inputProps={{
                  'data-readonly': true,
                }}
                name="readonly"
                aria-label="read only"
              />
            }
            label={
              <>
                <Typography variant="body2">Non editable</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Read only
                </Typography>
              </>
            }
          />
          <FormControlLabel
            disabled
            control={<Checkbox color="primary" name="disabled" />}
            label="(Disabled option)"
          />
        </FormGroup>
      </FormControl>
    </>
  );
};
