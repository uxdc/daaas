import React from 'react';
import {ThemeProvider, StyledEngineProvider} from '@mui/material/styles';
import {darkTheme} from '../Theme/theme';

import {Link, Typography, Paper} from '@mui/material';

export default {
  title: 'Atoms/Link',
  component: Link,
};

export const LightThemeLinks = (args) => {
  return (
    <>
      <Typography>
        <Link underline="always" color="inherit" href="#">
          Default
        </Link>
      </Typography>
      <br />
      <Typography>
        <Link underline="always" color="primary" href="#">
          Primary
        </Link>
      </Typography>
    </>
  );
};

export const DarkThemeLinks = (args) => {
  return (
    <>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={darkTheme}>
          <Paper sx={{p: 3}}>
            <Typography>
              <Link underline="always" color="inherit" href="#">
                Default
              </Link>
            </Typography>
            <br />
            <Typography>
              <Link underline="always" color="primary" href="#">
                Primary
              </Link>
            </Typography>
          </Paper>
        </ThemeProvider>
      </StyledEngineProvider>
    </>
  );
};
