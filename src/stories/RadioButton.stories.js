import React from 'react';
import {
  Radio,
  RadioGroup,
  FormControl,
  FormLabel,
  FormControlLabel,
  FormHelperText,
  Typography,
  Link,
} from '@mui/material';

export default {
  title: 'Molecules/RadioButtons',
  component: RadioGroup,
  argTypes: {},
};

export const Group = (args) => {
  const [state, setState] = React.useState({
    gender: null,
  });

  const handleRadioChange = (event) => {
    const name = event.target.name;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  return (
    <>
      <FormControl component="fieldset" aria-describedby="helperText">
        <FormLabel component="legend">Gender</FormLabel>
        <FormHelperText id="helperText">Helper text</FormHelperText>
        <RadioGroup
          name="gender"
          value={state.gender}
          onChange={handleRadioChange}
        >
          <FormControlLabel
            value="female"
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">Female</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="male"
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">Male</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="other"
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">Other</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="disabled"
            disabled
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">(Disabled option)</Typography>
              </>
            }
          />
        </RadioGroup>
      </FormControl>
    </>
  );
};

export const GroupError = (args) => {
  const [state, setState] = React.useState({
    gender: null,
  });

  const handleRadioChange = (event) => {
    const name = event.target.name;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  return (
    <>
      <FormControl
        component="fieldset"
        error={!state.gender}
        aria-describedby="errorText"
      >
        <FormLabel component="legend">Gender</FormLabel>
        <FormHelperText id="errorText">Error text</FormHelperText>
        <RadioGroup
          name="gender"
          value={state.gender}
          onChange={handleRadioChange}
        >
          <FormControlLabel
            value="female"
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">Female</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="male"
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">Male</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="other"
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">Other</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="disabled"
            disabled
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">(Disabled option)</Typography>
              </>
            }
          />
        </RadioGroup>
      </FormControl>
    </>
  );
};
GroupError.storyName = 'Group - Error';

export const GroupRequired = (args) => {
  const [state, setState] = React.useState({
    gender: null,
  });

  const handleRadioChange = (event) => {
    const name = event.target.name;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  return (
    <>
      <FormControl component="fieldset" aria-describedby="helperText" required>
        <FormLabel component="legend">
          Gender <span className="screen-reader-text">required</span>
        </FormLabel>
        <FormHelperText id="helperText">Helper text</FormHelperText>
        <RadioGroup
          name="gender"
          value={state.gender}
          onChange={handleRadioChange}
        >
          <FormControlLabel
            value="female"
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">Female</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="male"
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">Male</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="other"
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">Other</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="disabled"
            disabled
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">(Disabled option)</Typography>
              </>
            }
          />
        </RadioGroup>
      </FormControl>
    </>
  );
};
GroupRequired.storyName = 'Group - Required';

export const GroupLink = (args) => {
  const [state, setState] = React.useState({
    gender: null,
  });

  const handleRadioChange = (event) => {
    const name = event.target.name;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  return (
    <>
      <FormControl component="fieldset" aria-describedby="helperText" required>
        <FormLabel component="legend" id="legend">
          Gender <span className="screen-reader-text">required</span>
        </FormLabel>
        <FormHelperText id="helperText">
          Helper text{' '}
          <Link href="#" color="textSecondary" underline="always">
            Some link
          </Link>
        </FormHelperText>
        <RadioGroup
          name="gender"
          value={state.gender}
          onChange={handleRadioChange}
          aria-labelledby="legend"
        >
          <FormControlLabel
            value="female"
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">Female</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="male"
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">Male</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="other"
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">Other</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="disabled"
            disabled
            control={<Radio color="primary" />}
            label={
              <>
                <Typography variant="body2">(Disabled option)</Typography>
              </>
            }
          />
        </RadioGroup>
      </FormControl>
    </>
  );
};
GroupLink.storyName = 'Group - Link';
