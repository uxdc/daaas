import React from 'react';
import {styled} from '@mui/material/styles';
import {InputAdornment, TextField, Typography, Box} from '@mui/material';
import AccountCircle from '@mui/icons-material/AccountCircle';
import PhoneField from '../Components/CommonComponents/PhoneField';

const PREFIX = 'Default';

const classes = {
  container: `${PREFIX}-container`,
};

const StyledPhoneField = styled(PhoneField)(({theme}) => ({
  [`& .${classes.container}`]: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
}));

export default {
  title: 'Molecules/TextField',
  component: TextField,
};

export const Default = (args) => <TextField {...args} />;
Default.args = {
  label: 'Default',
};

export const OutlinedTextField = (args) => {
  return (
    <div className={classes.container}>
      <Box sx={{mb: 3, width: '100%'}}>
        <TextField
          id="full-width"
          label="Full width"
          fullWidth
          autoComplete="off"
        />
      </Box>
      <Box sx={{mb: 3}}>
        <TextField
          id="outlined-basic"
          label="Required"
          required
          autoComplete="off"
        />
      </Box>
      <Box sx={{mb: 3}}>
        <TextField id="disabled" label="Disabled" disabled autoComplete="off" />
      </Box>
      <Box sx={{mb: 3}}>
        <TextField
          id="readonly"
          label="Read only"
          defaultValue="Read only"
          autoComplete="off"
          InputProps={{
            readOnly: true,
          }}
        />
      </Box>
      <Box sx={{mb: 3}}>
        <TextField
          id="placeholder"
          label="Placeholder"
          placeholder="Placeholder text"
          autoComplete="off"
        />
      </Box>
      <Box sx={{mb: 3}}>
        <TextField
          id="help-text"
          label="Helper text"
          helperText="Helper text"
          autoComplete="off"
        />
      </Box>
      <Box sx={{mb: 3}}>
        <TextField
          id="default-val"
          label="Default value"
          defaultValue="Default value"
          autoComplete="off"
        />
      </Box>
      <Box sx={{mb: 3}}>
        <TextField
          label="Error"
          id="error"
          error={true}
          helperText="Error text"
          autoComplete="off"
        />
      </Box>
      <Box sx={{mb: 3}}>
        <TextField
          label="Adornment"
          id="standard-start-adornment"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <AccountCircle />
              </InputAdornment>
            ),
          }}
          autoComplete="off"
        />
      </Box>
      <Box sx={{mb: 3}}>
        <TextField
          label="Text start adornment"
          id="text-start-adornment"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Typography variant="body2">Kg</Typography>
              </InputAdornment>
            ),
          }}
          autoComplete="off"
        />
      </Box>
      <Box sx={{mb: 3}}>
        <TextField
          label="Text end adornment"
          id="text-end-adornment"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <Typography variant="body2">Kg</Typography>
              </InputAdornment>
            ),
          }}
          autoComplete="off"
        />
      </Box>
      <Box sx={{mb: 3}}>
        <TextField
          id="multiline-1"
          label="Multiline"
          multiline
          minRows="3"
          autoComplete="off"
        />
      </Box>
      <TextField
        id="multiline-2"
        label="Multiline"
        multiline
        minRows="1"
        autoComplete="off"
      />
    </div>
  );
};

export const MaskedPhone = (args) => {
  return <StyledPhoneField label="Phone number" id="phone-input" />;
};
