import React from 'react';
import {styled} from '@mui/material/styles';
import {
  Select,
  FormControl,
  InputLabel,
  MenuItem,
  TextField,
  Box,
} from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import {countries} from '../Data/storybook';

const PREFIX = 'OutlinedSelect';

const classes = {
  container: `${PREFIX}-container`,
  width: `${PREFIX}-width`,
};

const Root = styled('div')(({theme}) => ({
  [`&.${classes.container}`]: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },

  [`& .${classes.width}`]: {
    width: 200,
  },
}));

export default {
  title: 'Lab/Selects',
  component: Select,
};

export const OutlinedSelect = (args) => {
  const [state, setState] = React.useState({
    fullWidth: '',
    reg: '',
    req: '',
    ro: 10,
  });

  const handleChange = (event) => {
    setState({...state, [event.target.name]: event.target.value});
  };

  return (
    <Root className={classes.container}>
      <Box sx={{mb: 3, width: '100%'}}>
        <FormControl fullWidth>
          <InputLabel id="fullWidth-label">Full width</InputLabel>
          <Select
            labelId="fullWidth-label"
            id="fullWidth"
            value={state.fullWidth}
            onChange={handleChange}
            label="Full width"
            name="fullWidth"
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={10}>Option 1</MenuItem>
            <MenuItem value={20}>Option 2</MenuItem>
            <MenuItem value={30}>Option 3</MenuItem>
          </Select>
        </FormControl>
      </Box>
      <Box sx={{mb: 3}}>
        <FormControl fullWidth={false} className={classes.width}>
          <InputLabel id="reg-label">Set width</InputLabel>
          <Select
            labelId="reg-label"
            id="reg"
            value={state.reg}
            onChange={handleChange}
            label="Set width"
            name="reg"
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={10}>Option 1</MenuItem>
            <MenuItem value={20}>Option 2</MenuItem>
            <MenuItem value={30}>Option 3</MenuItem>
          </Select>
        </FormControl>
      </Box>
      <Box sx={{mb: 3}}>
        <FormControl fullWidth={false} className={classes.width} required>
          <InputLabel id="req-label">
            Required <span className="screen-reader-text">required</span>
          </InputLabel>
          <Select
            labelId="req-label"
            id="req"
            value={state.req}
            onChange={handleChange}
            label="Required *"
            name="req"
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={10}>Option 1</MenuItem>
            <MenuItem value={20}>Option 2</MenuItem>
            <MenuItem value={30}>Option 3</MenuItem>
          </Select>
        </FormControl>
      </Box>
      <FormControl fullWidth={false} className={classes.width}>
        <InputLabel id="ro-label">Read only</InputLabel>
        <Select
          labelId="ro-label"
          id="ro"
          value={state.ro}
          onChange={handleChange}
          label="Read only"
          name="ro"
          inputProps={{readOnly: true}}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={10}>Option 1</MenuItem>
          <MenuItem value={20}>Option 2</MenuItem>
          <MenuItem value={30}>Option 3</MenuItem>
        </Select>
      </FormControl>
    </Root>
  );
};

export const OutlinedAutocomplete = (args) => {
  const [state, setState] = React.useState({
    fullWidth: null,
    reg: null,
    req: null,
    ro: 'Canada',
    wrap: 'Bosnia & Herzegovina',
  });

  return (
    <Root className={classes.container}>
      <Autocomplete
        sx={{mb: 3}}
        componentsProps={{
          clearIndicator: {tabIndex: 0},
        }}
        fullWidth={true}
        id="autocomplete-full"
        options={countries}
        value={state.fullWidth}
        onChange={(event, newValue) => {
          setState({...state, fullWidth: newValue});
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            name="autocompleteFull"
            label="Autocomplete full width"
          />
        )}
      />
      <Autocomplete
        sx={{mb: 3}}
        componentsProps={{
          clearIndicator: {tabIndex: 0},
        }}
        className={classes.width}
        fullWidth={false}
        id="autocomplete-set"
        options={countries}
        value={state.reg}
        onChange={(event, newValue) => {
          setState({...state, reg: newValue});
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            name="autocompleteSet"
            label="Autocomplete set width"
          />
        )}
      />
      <Autocomplete
        sx={{mb: 3}}
        componentsProps={{
          clearIndicator: {tabIndex: 0},
        }}
        className={classes.width}
        fullWidth={false}
        id="autocomplete-req"
        options={countries}
        value={state.req}
        onChange={(event, newValue) => {
          setState({...state, req: newValue});
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            name="autocompleteReq"
            label="Autocomplete required"
            required
          />
        )}
      />
      <Autocomplete
        sx={{mb: 3}}
        componentsProps={{
          clearIndicator: {tabIndex: 0},
        }}
        className={classes.width}
        fullWidth={false}
        id="autocomplete-wrap"
        options={countries}
        value={state.wrap}
        onChange={(event, newValue) => {
          setState({...state, wrap: newValue});
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            name="autocompleteWrap"
            label="Autocomplete text wrap"
            required
            multiline
            minRows="1"
          />
        )}
      />
      <Autocomplete
        componentsProps={{
          clearIndicator: {tabIndex: 0},
        }}
        className={classes.width}
        fullWidth={false}
        id="autocomplete-ro"
        options={countries}
        value={state.ro}
        onChange={(event, newValue) => {
          setState({...state, ro: newValue});
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            name="autocompleteRo"
            label="Autocomplete read only"
            InputProps={{
              readOnly: true,
            }}
          />
        )}
      />
    </Root>
  );
};
