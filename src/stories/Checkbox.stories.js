import React from 'react';
import {
  Checkbox,
  FormGroup,
  FormControl,
  FormLabel,
  FormControlLabel,
  FormHelperText,
  Typography,
  Link,
} from '@mui/material';

export default {
  title: 'Molecules/Checkboxes',
  component: FormGroup,
  argTypes: {},
};

export const Group = (args) => {
  const [state, setState] = React.useState({
    gilad: false,
    jason: false,
    antoine: false,
    readonly: false,
  });

  const handleChbxChange = (event) => {
    if (event.target.dataset.readonly) {
      return false;
    } else {
      setState({...state, [event.target.name]: event.target.checked});
    }
  };

  return (
    <>
      <FormControl component="fieldset" aria-describedby="helperText">
        <FormLabel component="legend">Assign responsibility</FormLabel>
        <FormHelperText id="helperText">Select all that apply</FormHelperText>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={state.gilad}
                onChange={handleChbxChange}
                name="gilad"
              />
            }
            label={
              <>
                <Typography variant="body2">Gilad Gray</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={state.jason}
                onChange={handleChbxChange}
                name="jason"
              />
            }
            label={
              <>
                <Typography variant="body2">Jason Killian</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={state.antoine}
                onChange={handleChbxChange}
                name="antoine"
              />
            }
            label={
              <>
                <Typography variant="body2">Antoine Llorca</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={state.readonly}
                onChange={handleChbxChange}
                inputProps={{
                  'data-readonly': true,
                }}
                name="readonly"
                aria-label="read only"
              />
            }
            label={
              <>
                <Typography variant="body2">Non editable</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Read only
                </Typography>
              </>
            }
          />
          <FormControlLabel
            disabled
            control={<Checkbox color="primary" name="disabled" />}
            label="(Disabled option)"
          />
        </FormGroup>
      </FormControl>
    </>
  );
};

export const GroupRequired = (args) => {
  const [state, setState] = React.useState({
    gilad: false,
    jason: false,
    antoine: false,
  });

  const handleChbxChange = (event) => {
    setState({...state, [event.target.name]: event.target.checked});
  };

  return (
    <>
      <FormControl component="fieldset" aria-describedby="helperText" required>
        <FormLabel component="legend">
          Assign responsibility
          <span className="screen-reader-text">required</span>
        </FormLabel>
        <FormHelperText id="helperText">Select all that apply</FormHelperText>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={state.gilad}
                onChange={handleChbxChange}
                name="gilad"
              />
            }
            label={
              <>
                <Typography variant="body2">Gilad Gray</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={state.jason}
                onChange={handleChbxChange}
                name="jason"
              />
            }
            label={
              <>
                <Typography variant="body2">Jason Killian</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={state.antoine}
                onChange={handleChbxChange}
                name="antoine"
              />
            }
            label={
              <>
                <Typography variant="body2">Antoine Llorca</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            disabled
            control={<Checkbox color="primary" name="disabled" />}
            label="(Disabled option)"
          />
        </FormGroup>
      </FormControl>
    </>
  );
};
GroupRequired.storyName = 'Group - Required';

export const GroupLink = (args) => {
  const [state, setState] = React.useState({
    gilad: false,
    jason: false,
    antoine: false,
  });

  const handleChbxChange = (event) => {
    setState({...state, [event.target.name]: event.target.checked});
  };

  return (
    <>
      <FormControl component="fieldset" aria-describedby="helperText" required>
        <FormLabel component="legend" id="legend">
          Assign responsibility
          <span className="screen-reader-text">required</span>
        </FormLabel>
        <FormHelperText id="helperText">
          Select all that apply.{' '}
          <Link href="#" color="textSecondary" underline="always">
            Some link
          </Link>
        </FormHelperText>
        <FormGroup role="group" aria-labelledby="legend">
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={state.gilad}
                onChange={handleChbxChange}
                name="gilad"
              />
            }
            label={
              <>
                <Typography variant="body2">Gilad Gray</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={state.jason}
                onChange={handleChbxChange}
                name="jason"
              />
            }
            label={
              <>
                <Typography variant="body2">Jason Killian</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={state.antoine}
                onChange={handleChbxChange}
                name="antoine"
              />
            }
            label={
              <>
                <Typography variant="body2">Antoine Llorca</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            disabled
            control={<Checkbox color="primary" name="disabled" />}
            label="(Disabled option)"
          />
        </FormGroup>
      </FormControl>
    </>
  );
};
GroupLink.storyName = 'Group - Link';

export const GroupError = (args) => {
  const [state, setState] = React.useState({
    gilad: false,
    jason: false,
    antoine: false,
  });

  const {gilad, jason, antoine} = state;
  const error = [gilad, jason, antoine].filter((v) => v).length === 0;

  const handleChbxChange = (event) => {
    setState({...state, [event.target.name]: event.target.checked});
  };

  return (
    <>
      <FormControl
        component="fieldset"
        aria-describedby="helperText"
        error={error}
      >
        <FormLabel component="legend">Assign responsibility</FormLabel>
        <FormHelperText id="helperText">Error text</FormHelperText>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={state.gilad}
                onChange={handleChbxChange}
                name="gilad"
              />
            }
            label={
              <>
                <Typography variant="body2">Gilad Gray</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={state.jason}
                onChange={handleChbxChange}
                name="jason"
              />
            }
            label={
              <>
                <Typography variant="body2">Jason Killian</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={state.antoine}
                onChange={handleChbxChange}
                name="antoine"
              />
            }
            label={
              <>
                <Typography variant="body2">Antoine Llorca</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
          <FormControlLabel
            disabled
            control={<Checkbox color="primary" name="disabled" />}
            label="(Disabled option)"
          />
        </FormGroup>
      </FormControl>
    </>
  );
};
GroupError.storyName = 'Group - Error';

export const Individual = (args) => {
  const [state, setState] = React.useState({
    terms: false,
  });

  const handleChbxChange = (event) => {
    setState({...state, [event.target.name]: event.target.checked});
  };
  return (
    <>
      <FormControl component="fieldset">
        <FormLabel component="legend" className="screen-reader-text">
          Select if you agree
        </FormLabel>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                checked={state.terms}
                onChange={handleChbxChange}
                name="terms"
                color="primary"
              />
            }
            label={
              <>
                <Typography variant="body2">
                  I would like to participate in future research activities to
                  help Statistics Canada improve their tools and services.
                </Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
        </FormGroup>
      </FormControl>
    </>
  );
};

export const IndividualRequired = (args) => {
  const [state, setState] = React.useState({
    terms: false,
  });

  const handleChbxChange = (event) => {
    setState({...state, [event.target.name]: event.target.checked});
  };
  return (
    <>
      <FormControl component="fieldset">
        <FormLabel component="legend" className="screen-reader-text">
          Select if you agree
        </FormLabel>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                checked={state.terms}
                onChange={handleChbxChange}
                name="terms"
                color="primary"
                required
              />
            }
            label={
              <>
                <Typography variant="body2">
                  I agree to the Terms and conditions and Privacy policy{' '}
                  <span aria-hidden="true">*</span>
                </Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Helper text
                </Typography>
              </>
            }
          />
        </FormGroup>
      </FormControl>
    </>
  );
};
IndividualRequired.storyName = 'Individual - Required';

export const IndividualError = (args) => {
  const [state, setState] = React.useState({
    terms: false,
  });

  const handleChbxChange = (event) => {
    setState({...state, [event.target.name]: event.target.checked});
  };

  return (
    <>
      <FormControl component="fieldset" error={!state.terms}>
        <FormLabel component="legend" className="screen-reader-text">
          Select if you agree
        </FormLabel>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                checked={state.terms}
                onChange={handleChbxChange}
                name="terms"
                color="primary"
                required
              />
            }
            label={
              <>
                <Typography variant="body2">
                  I agree to the Terms and conditions and Privacy policy{' '}
                  <span aria-hidden="true">*</span>
                </Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <FormHelperText>Error text</FormHelperText>
              </>
            }
          />
        </FormGroup>
      </FormControl>
    </>
  );
};
IndividualError.storyName = 'Individual - Error';
