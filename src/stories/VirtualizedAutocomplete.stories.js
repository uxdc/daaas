import React from 'react';
import {styled} from '@mui/material/styles';

import {VirtualizedAutocomplete} from '../Components/CommonComponents/VirtualizedAutocomplete';
import {generatefilePaths} from '../Data/storybook';
const PREFIX = 'BasicField';

const classes = {
  container: `${PREFIX}-container`,
};

const Root = styled('div')((
    {
      theme,
    },
) => ({
  [`&.${classes.container}`]: {
    maxWidth: '500px',
  },
}));

// import {filePaths} from '../Data/storybook';

export default {
  title: 'Lab/VirtualizedAutocomplete',
  component: VirtualizedAutocomplete,
};

export const BasicField = (args) => {
  const [value, setValue] = React.useState(null);

  return (
    <Root className={classes.container}>
      <VirtualizedAutocomplete
        {...args}
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      />
    </Root>
  );
};

BasicField.args = {
  id: 'file-path',
  label: 'File path',
  required: true,
  options: generatefilePaths(),
  // options: filePaths,
  variant: 'outlined',
};
