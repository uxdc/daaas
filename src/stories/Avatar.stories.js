import React from 'react';

import {Avatar} from '../Components/CommonComponents/Avatar';
import avatarImg from './assets/avatar.jpg';

export default {
  title: 'Atoms/Avatar',
  component: Avatar,
};

const img = {
  src: avatarImg,
  alt: 'Joe',
};

export const AllAvatars = (args) => {
  return (
    <>
      <Avatar sx={{m: 1}} content="AZ" color="purple" />
      <Avatar sx={{m: 1}} src={img.src} alt={img.alt} />
    </>
  );
};

export const AvatarColours = (args) => {
  return (
    <>
      <Avatar sx={{m: 1}} content="AZ" />
      <Avatar sx={{m: 1}} content="AZ" color="purple" />
      <Avatar sx={{m: 1}} content="AZ" color="green" />
      <Avatar sx={{m: 1}} content="AZ" color="orange" />
    </>
  );
};
