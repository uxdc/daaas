import React from 'react';

import {AvatarButton} from '../Components/CommonComponents/AvatarButton';
import avatarImg from './assets/avatar.jpg';

export default {
  title: 'Atoms/Buttons/AvatarButton',
  component: AvatarButton,
};

const img = {
  src: avatarImg,
  alt: 'Joe',
};

export const AllButtons = (args) => {
  return (
    <>
      <AvatarButton sx={{m: 1}} content="AZ" color="purple" />
      <AvatarButton sx={{m: 1}} content="AZ" disabled />
      <br />
      <AvatarButton sx={{m: 1}} src={img.src} alt={img.alt} />
      <AvatarButton sx={{m: 1}} src={img.src} alt={img.alt} disabled />
    </>
  );
};

export const AvatarColours = (args) => {
  return (
    <>
      <AvatarButton sx={{m: 1}} content="AZ" />
      <AvatarButton sx={{m: 1}} content="AZ" color="purple" />
      <AvatarButton sx={{m: 1}} content="AZ" color="green" />
      <AvatarButton sx={{m: 1}} content="AZ" color="orange" />
    </>
  );
};
