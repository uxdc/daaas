import React from 'react';
import {
  Radio,
  RadioGroup,
  FormControl,
  FormLabel,
  FormControlLabel,
  FormHelperText,
  Typography,
} from '@mui/material';

export default {
  title: 'Lab/Read-only RadioButtons',
  component: RadioGroup,
  argTypes: {},
};

export const Group = (args) => {
  const [state, setState] = React.useState({
    gender: 'male',
  });

  const handleRadioChange = (event) => {
    const name = event.target.name;
    if (event.target.dataset.readonly) {
      return false;
    } else {
      setState({
        ...state,
        [name]: event.target.value,
      });
    }
  };

  return (
    <>
      <FormControl component="fieldset">
        <FormLabel component="legend">Read-only group</FormLabel>
        <FormHelperText>Helper text</FormHelperText>
        <RadioGroup
          aria-label="gender"
          name="gender"
          value={state.gender}
          onChange={handleRadioChange}
        >
          <FormControlLabel
            value="female"
            aria-label="readonly"
            control={
              <Radio
                color="primary"
                inputProps={{
                  'data-readonly': true,
                }}
              />
            }
            label={
              <>
                <Typography variant="body2">Female</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Read only
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="male"
            aria-label="readonly"
            control={
              <Radio
                color="primary"
                inputProps={{
                  'data-readonly': true,
                }}
              />
            }
            label={
              <>
                <Typography variant="body2">Male</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Read only
                </Typography>
              </>
            }
          />
          <FormControlLabel
            value="other"
            aria-label="readonly"
            control={
              <Radio
                color="primary"
                inputProps={{
                  'data-readonly': true,
                }}
              />
            }
            label={
              <>
                <Typography variant="body2">Other</Typography>
                <Typography variant="caption" component="p">
                  Secondary label text
                </Typography>
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  Read only
                </Typography>
              </>
            }
          />
        </RadioGroup>
      </FormControl>
    </>
  );
};
