import React from 'react';
import {styled} from '@mui/material/styles';
import {
  Button,
  Step,
  Stepper,
  StepButton,
  Typography,
  StepLabel,
  Grid,
  FormControl,
  TextField,
  AppBar,
  Toolbar,
  IconButton,
} from '@mui/material';
import Icon from '@mdi/react';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import {mdiInboxArrowDown} from '@mdi/js';
import {SnackbarSubmitRequestError} from '../../Components/VettingApp/CommonComponents/Snackbars';
import Header from '../../Components/VettingApp/CommonComponents/Header';

const PREFIX = 'StepperErrors';

const classes = {
  appBar: `${PREFIX}-appBar`,
  gridContainer: `${PREFIX}-gridContainer`,
  stepperContainer: `${PREFIX}-stepperContainer`,
  stepButton: `${PREFIX}-stepButton`,
  stepperNextBtn: `${PREFIX}-stepperNextBtn`,
  stepperBackBtn: `${PREFIX}-stepperBackBtn`,
  navButtons: `${PREFIX}-navButtons`,
};

const Root = styled('div')(({theme}) => ({
  [`& .${classes.appBar}`]: {
    margin: theme.spacing(0, -3),
    width: 'auto',
    backgroundColor: theme.palette.common.white,
    boxShadow: 'none',
    borderBottom: '1px solid',
    borderBottomColor: theme.palette.divider,
    top: theme.spacing(8),
    left: 0,
    [theme.breakpoints.down('sm')]: {
      top: theme.spacing(7),
    },
  },

  [`& .${classes.gridContainer}`]: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
  },

  [`& .${classes.stepperContainer}`]: {
    'display': 'flex',
    '& .MuiStepper-root': {
      flexGrow: 1,
      padding: 0,
    },
    'paddingTop': theme.spacing(8),
    'marginTop': theme.spacing(8),
  },

  [`& .${classes.stepButton}`]: {
    textAlign: 'left',
  },

  [`& .${classes.stepperNextBtn}`]: {
    marginLeft: theme.spacing(6),
  },

  [`& .${classes.stepperBackBtn}`]: {
    marginRight: theme.spacing(6),
  },

  [`& .${classes.navButtons}`]: {
    paddingTop: theme.spacing(3),
    borderTop: 'solid 1px',
    borderTopColor: theme.palette.divider,
  },
}));

function getSteps() {
  return [
    'Request details',
    'Output files for vetting',
    'Residual disclosure',
    'Additional information',
  ];
}

export default {
  title: 'Organisms/Vetting/Errors',
  component: Stepper,
};

export const StepperErrors = (args) => {
  const [state, setState] = React.useState({
    activeStep: 0,
    stepperErrors: [0, 0, 0, 0],
    errors: {
      step0: '',
      step1: '',
      step2: '',
      step3: '',
    },
    snackbarErrors: false,
  });

  const refStep0 = React.useRef(null);
  const refStep1 = React.useRef(null);
  const refStep2 = React.useRef(null);
  const refStep3 = React.useRef(null);
  const steps = getSteps();

  const getStepRef = (step) => {
    switch (step) {
      case 0:
        return refStep0;
      case 1:
        return refStep1;
      case 2:
        return refStep2;
      case 3:
        return refStep3;
      default:
        return null;
    }
  };

  const handleNext = () => {
    window.scrollTo(0, 0);
    const prevActiveStep = state.activeStep;
    setState({...state, activeStep: prevActiveStep + 1});
    getStepRef(prevActiveStep + 1).current.focus();
  };

  const handleBack = () => {
    window.scrollTo(0, 0);
    const prevActiveStep = state.activeStep;
    setState({...state, activeStep: prevActiveStep - 1});
    getStepRef(prevActiveStep - 1).current.focus();
  };

  const handleStep = (step) => {
    window.scrollTo(0, 0);
    setState({...state, activeStep: step});
  };

  const isStepFailed = (step) => {
    return state.stepperErrors[step] !== 0;
  };

  const handleClose = (element) => {
    setState({...state, [element]: false});
  };

  const triggerErrors = () => {
    const firstError = 1;
    setState({
      ...state,
      activeStep: firstError,
      stepperErrors: [0, 1, 1, 0],
      errors: {
        ...state.errors,
        step1: 'This field is required',
        step2: 'This field is required',
      },
      snackbarErrors: true,
    });
    getStepRef(firstError).current.focus();
  };

  const getStepContent = (step) => {
    switch (step) {
      case 0:
        return (
          <>
            <Typography component="h2" variant="h6" sx={{mb: 3}}>
              Request details
            </Typography>
            <FormControl variant="outlined" fullWidth required margin="none">
              <TextField
                id="example"
                name="example"
                variant="outlined"
                fullWidth
                label="Example"
                error={Boolean(state.errors.step0)}
                helperText={state.errors.step0}
                margin="dense"
              />
            </FormControl>
          </>
        );
      case 1:
        return (
          <>
            <Typography component="h2" variant="h6" sx={{mb: 3}}>
              Output files for vetting
            </Typography>
            <FormControl variant="outlined" fullWidth required margin="none">
              <TextField
                id="example"
                name="example"
                variant="outlined"
                fullWidth
                label="Example"
                required
                error={Boolean(state.errors.step1)}
                helperText={state.errors.step1}
                margin="dense"
              />
            </FormControl>
          </>
        );
      case 2:
        return (
          <>
            <Typography component="h2" variant="h6" sx={{mb: 3}}>
              Residual disclosure
            </Typography>
            <FormControl variant="outlined" fullWidth required margin="none">
              <TextField
                id="example"
                name="example"
                variant="outlined"
                fullWidth
                label="Example"
                required
                error={Boolean(state.errors.step2)}
                helperText={state.errors.step2}
                margin="dense"
              />
            </FormControl>
          </>
        );
      case 3:
        return (
          <>
            <Typography component="h2" variant="h6" sx={{mb: 3}}>
              Additional information
            </Typography>
            <FormControl variant="outlined" fullWidth required margin="none">
              <TextField
                id="example"
                name="example"
                variant="outlined"
                fullWidth
                label="Example"
                error={Boolean(state.errors.step3)}
                helperText={state.errors.step3}
                margin="dense"
              />
            </FormControl>
          </>
        );
      default:
        return 'Unknown step';
    }
  };

  function srErrorLabel(val) {
    if (state.stepperErrors[val] === 1) {
      return '1 error';
    } else if (state.stepperErrors[val] > 1) {
      return `${state.stepperErrors[val]} errors`;
    }
  }

  return (
    <Root>
      <Header />
      <AppBar position="fixed" className={classes.appBar} color="default">
        <Toolbar>
          <Grid
            container
            justifyContent="space-between"
            className={classes.gridContainer}
          >
            <Grid item>
              <IconButton
                edge="start"
                className={classes.menuButton}
                aria-label="Back to dashboard"
                size="large"
              >
                <ArrowBackIcon />
              </IconButton>
              <Typography
                variant="body2"
                component="span"
                className={classes.title}
              >
                Dashboard
              </Typography>
            </Grid>
            <Grid item className={classes.actions}>
              <Button
                className={classes.headerBtn}
                variant="contained"
                color="primary"
                startIcon={<Icon path={mdiInboxArrowDown} size={1} />}
                onClick={triggerErrors}
              >
                Submit
              </Button>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>

      <div className={classes.stepperContainer}>
        {state.activeStep !== 0 && (
          <Button
            onClick={handleBack}
            className={classes.stepperBackBtn}
            startIcon={<ArrowBackIosIcon />}
          >
            Back
          </Button>
        )}
        <Stepper nonLinear activeStep={state.activeStep}>
          {steps.map((label, index) => {
            const labelProps = {};
            const buttonProps = {};
            if (isStepFailed(index)) {
              labelProps.error = true;
              buttonProps.optional = (
                <Typography
                  className={classes.errorMsg}
                  variant="body2"
                  color="error"
                  component="span"
                >
                  {srErrorLabel(index)}
                </Typography>
              );
            }
            return (
              <Step key={label} tabIndex="-1">
                <StepButton
                  {...buttonProps}
                  ref={getStepRef(index)}
                  onClick={() => {
                    handleStep(index);
                  }}
                  className={classes.stepButton}
                >
                  <StepLabel {...labelProps}>
                    <span className="screen-reader-text">{`Step ${index +
                      1}: `}</span>
                    {label}
                  </StepLabel>
                </StepButton>
              </Step>
            );
          })}
        </Stepper>
        {state.activeStep !== getSteps().length - 1 && (
          <Button
            onClick={handleNext}
            className={classes.stepperNextBtn}
            endIcon={<ArrowForwardIosIcon />}
          >
            Next
          </Button>
        )}
      </div>
      <div>
        <Grid container justifyContent="center" sx={{mb: 4, mt: 4}}>
          <Grid item xs={6}>
            {getStepContent(state.activeStep)}
          </Grid>
        </Grid>
      </div>
      <Grid
        container
        justifyContent={state.activeStep === 0 ? 'flex-end' : 'space-between'}
        className={classes.navButtons}
      >
        {state.activeStep !== 0 && (
          <Grid item>
            <Button variant="contained" color="primary" onClick={handleBack}>
              Back
            </Button>
          </Grid>
        )}
        {state.activeStep === getSteps().length - 1 ? (
          <Grid item>
            <Button variant="contained" color="primary" onClick={triggerErrors}>
              Submit
            </Button>
          </Grid>
        ) : (
          <Grid item>
            <Button variant="contained" color="primary" onClick={handleNext}>
              Next
            </Button>
          </Grid>
        )}
      </Grid>
      {/* Errors snackbar */}
      <SnackbarSubmitRequestError
        open={state.snackbarErrors}
        handleClose={() => handleClose('snackbarErrors')}
        errors={2}
      />
    </Root>
  );
};
StepperErrors.storyName = 'Stepper - Errors';
