import React from 'react';
import {useTranslation} from 'react-i18next';
import {Menu} from '../Components/CommonComponents/Menu';
import {
  IconButton,
  Link,
  ListItemText,
  ListItemIcon,
  Typography,
  MenuItem,
  Button,
  Avatar,
} from '@mui/material';
import Icon from '@mdi/react';
import {mdiHeart, mdiMenuDown, mdiOpenInNew} from '@mdi/js';
import MoreVertIcon from '@mui/icons-material/MoreVert';

export default {
  title: 'Molecules/Menu',
  component: Menu,
};

export const ButtonMenu = (args) => {
  const {t} = useTranslation();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  function handleBlur(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setAnchorEl(null);
    }
  }

  // ////////////////////// MENU ITEMS
  const menuItem1 = () => {
    return (
      <MenuItem onClick={handleClose} key="1">
        <ListItemText
          primary={<Typography variant="body2">{t('Option 1')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItem2 = () => {
    return (
      <MenuItem onClick={handleClose} key="2">
        <ListItemText
          primary={<Typography variant="body2">{t('Option 2')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItem3 = () => {
    return (
      <MenuItem onClick={handleClose} key="3">
        <ListItemText
          primary={<Typography variant="body2">{t('Option 3')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItems = () => {
    return [menuItem1(), menuItem2(), menuItem3()];
  };

  return (
    <>
      <Button
        onClick={handleClick}
        aria-controls="button-menu"
        aria-haspopup="true"
        variant="contained"
        color="primary"
        endIcon={<Icon path={mdiMenuDown} size={1} />}
      >
        Open menu
      </Button>
      <Menu
        id="button-menu"
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        handleClose={handleClose}
        handleBlur={handleBlur}
      >
        {menuItems()}
      </Menu>
    </>
  );
};
ButtonMenu.storyName = 'Menu - Button';

export const StartIconMenu = (args) => {
  const {t} = useTranslation();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  function handleBlur(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setAnchorEl(null);
    }
  }

  // ////////////////////// MENU ITEMS
  const menuItem1 = () => {
    return (
      <MenuItem onClick={handleClose} key="1">
        <ListItemIcon>
          <Icon path={mdiHeart} size={1} />
        </ListItemIcon>
        <ListItemText
          primary={<Typography variant="body2">{t('Option 1')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItem2 = () => {
    return (
      <MenuItem onClick={handleClose} key="2">
        <ListItemIcon>
          <Icon path={mdiHeart} size={1} />
        </ListItemIcon>
        <ListItemText
          primary={<Typography variant="body2">{t('Option 2')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItem3 = () => {
    return (
      <MenuItem onClick={handleClose} key="3">
        <ListItemIcon>
          <Icon path={mdiHeart} size={1} />
        </ListItemIcon>
        <ListItemText
          primary={<Typography variant="body2">{t('Option 3')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItems = () => {
    return [menuItem1(), menuItem2(), menuItem3()];
  };

  return (
    <>
      <Button
        onClick={handleClick}
        aria-controls="button-menu"
        aria-haspopup="true"
        variant="contained"
        color="primary"
        endIcon={<Icon path={mdiMenuDown} size={1} />}
      >
        Open menu
      </Button>
      <Menu
        id="button-menu"
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        handleClose={handleClose}
        handleBlur={handleBlur}
      >
        {menuItems()}
      </Menu>
    </>
  );
};
StartIconMenu.storyName = 'Menu - Button - Start icons';

export const EndIconMenu = (args) => {
  const {t} = useTranslation();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  function handleBlur(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setAnchorEl(null);
    }
  }

  // ////////////////////// MENU ITEMS
  const menuItem1 = () => {
    return (
      <MenuItem onClick={handleClose} key="1">
        <ListItemText
          primary={<Typography variant="body2">{t('Option 1')}</Typography>}
        />
        <ListItemIcon>
          <Icon path={mdiHeart} size={1} />
        </ListItemIcon>
      </MenuItem>
    );
  };

  const menuItem2 = () => {
    return (
      <MenuItem onClick={handleClose} key="2">
        <ListItemText
          primary={<Typography variant="body2">{t('Option 2')}</Typography>}
        />
        <ListItemIcon>
          <Icon path={mdiHeart} size={1} />
        </ListItemIcon>
      </MenuItem>
    );
  };

  const menuItem3 = () => {
    return (
      <MenuItem onClick={handleClose} key="3">
        <ListItemText
          primary={<Typography variant="body2">{t('Option 3')}</Typography>}
        />
        <ListItemIcon>
          <Icon path={mdiHeart} size={1} />
        </ListItemIcon>
      </MenuItem>
    );
  };

  const menuItems = () => {
    return [menuItem1(), menuItem2(), menuItem3()];
  };

  return (
    <>
      <Button
        onClick={handleClick}
        aria-controls="button-menu"
        aria-haspopup="true"
        variant="contained"
        color="primary"
        endIcon={<Icon path={mdiMenuDown} size={1} />}
      >
        Open menu
      </Button>
      <Menu
        id="button-menu"
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        handleClose={handleClose}
        handleBlur={handleBlur}
      >
        {menuItems()}
      </Menu>
    </>
  );
};
EndIconMenu.storyName = 'Menu - Button - End icons';

export const ExternalLinkMenu = (args) => {
  const {t} = useTranslation();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  function handleBlur(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setAnchorEl(null);
    }
  }

  // ////////////////////// MENU ITEMS
  const menuItem1 = () => {
    const ref = React.createRef();
    return (
      <MenuItem
        onClick={() => {
          handleClose();
          ref.current.click();
        }}
        key="1"
      >
        <Link ref={ref} href="#" target="_blank" underline="hover">
          <ListItemText
            primary={<Typography variant="body2">{t('Option 1')}</Typography>}
          />
          <ListItemIcon>
            <Icon path={mdiOpenInNew} size={1} />
            <span className="screen-reader-text">{t('Opens in new tab')}</span>
          </ListItemIcon>
        </Link>
      </MenuItem>
    );
  };

  const menuItem2 = () => {
    const ref = React.createRef();
    return (
      <MenuItem
        onClick={() => {
          handleClose();
          ref.current.click();
        }}
        key="2"
      >
        <Link ref={ref} href="#" target="_blank" underline="hover">
          <ListItemText
            primary={<Typography variant="body2">{t('Option 2')}</Typography>}
          />
          <ListItemIcon>
            <Icon path={mdiOpenInNew} size={1} />
            <span className="screen-reader-text">{t('Opens in new tab')}</span>
          </ListItemIcon>
        </Link>
      </MenuItem>
    );
  };

  const menuItem3 = () => {
    const ref = React.createRef();
    return (
      <MenuItem
        onClick={() => {
          handleClose();
          ref.current.click();
        }}
        key="3"
      >
        <Link ref={ref} href="#" target="_blank" underline="hover">
          <ListItemText
            primary={<Typography variant="body2">{t('Option 3')}</Typography>}
          />
          <ListItemIcon>
            <Icon path={mdiOpenInNew} size={1} />
            <span className="screen-reader-text">{t('Opens in new tab')}</span>
          </ListItemIcon>
        </Link>
      </MenuItem>
    );
  };

  const menuItems = () => {
    return [menuItem1(), menuItem2(), menuItem3()];
  };

  return (
    <>
      <Button
        onClick={handleClick}
        aria-controls="button-menu"
        aria-haspopup="true"
        variant="contained"
        color="primary"
        endIcon={<Icon path={mdiMenuDown} size={1} />}
      >
        Open menu
      </Button>
      <Menu
        id="button-menu"
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        handleClose={handleClose}
        handleBlur={handleBlur}
      >
        {menuItems()}
      </Menu>
    </>
  );
};
ExternalLinkMenu.storyName = 'Menu - Button - External links';

export const IconButtonMenu = (args) => {
  const {t} = useTranslation();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  function handleBlur(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setAnchorEl(null);
    }
  }

  // ////////////////////// MENU ITEMS
  const menuItem1 = () => {
    return (
      <MenuItem onClick={handleClose} key="1">
        <ListItemText
          primary={<Typography variant="body2">{t('Option 1')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItem2 = () => {
    return (
      <MenuItem onClick={handleClose} key="2">
        <ListItemText
          primary={<Typography variant="body2">{t('Option 2')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItem3 = () => {
    return (
      <MenuItem onClick={handleClose} key="3">
        <ListItemText
          primary={<Typography variant="body2">{t('Option 3')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItems = () => {
    return [menuItem1(), menuItem2(), menuItem3()];
  };

  return (
    <>
      <IconButton
        onClick={handleClick}
        aria-controls="icon-button-menu"
        aria-haspopup="true"
        aria-label="Icon button menu"
        size="large"
      >
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="icon-button-menu"
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        handleClose={handleClose}
        handleBlur={handleBlur}
        placement="bottom-start"
      >
        {menuItems()}
      </Menu>
    </>
  );
};
IconButtonMenu.storyName = 'Menu - Icon button';

export const AvatarButtonMenu = (args) => {
  const {t} = useTranslation();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClick = (event) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  function handleBlur(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setAnchorEl(null);
    }
  }

  // ////////////////////// MENU ITEMS
  const menuItem1 = () => {
    return (
      <MenuItem onClick={handleClose} key="1">
        <ListItemText
          primary={<Typography variant="body2">{t('Option 1')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItem2 = () => {
    return (
      <MenuItem onClick={handleClose} key="2">
        <ListItemText
          primary={<Typography variant="body2">{t('Option 2')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItem3 = () => {
    return (
      <MenuItem onClick={handleClose} key="3">
        <ListItemText
          primary={<Typography variant="body2">{t('Option 3')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItems = () => {
    return [menuItem1(), menuItem2(), menuItem3()];
  };

  return (
    <>
      <IconButton
        onClick={handleClick}
        aria-controls="avatar-button-menu"
        aria-haspopup="true"
        aria-label="Avatar button menu"
        size="large"
      >
        <Avatar className="avatar-purple">AZ</Avatar>
      </IconButton>
      <Menu
        id="avatar-button-menu"
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        handleClose={handleClose}
        handleBlur={handleBlur}
        placement="bottom-start"
      >
        {menuItems()}
      </Menu>
    </>
  );
};
AvatarButtonMenu.storyName = 'Menu - Avatar button';
