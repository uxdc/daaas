import React from 'react';
import {styled} from '@mui/material/styles';

import {HoursMinsField} from '../Components/CommonComponents/HoursMinsField';
import {Button} from '@mui/material';

const PREFIX = 'BasicField';

const classes = {
  displayFlex: `${PREFIX}-displayFlex`,
};

const Root = styled('div')(({theme}) => ({
  [`&.${classes.displayFlex}`]: {
    display: 'flex',
    alignItems: 'start',
  },
}));

export default {
  title: 'Lab/HoursMinsField',
  component: HoursMinsField,
};

export const BasicField = (args) => {
  const [state, setState] = React.useState({
    hours: '',
    minutes: '',
    error: '',
  });

  const handleHoursChange = (event) => {
    setState({...state, hours: event.target.value.trim()});
  };

  const handleMinsChange = (event) => {
    setState({...state, minutes: event.target.value.trim()});
  };

  const handleValidation = (event) => {
    const hours = parseInt(state.hours, 10);
    const mins = parseInt(state.minutes, 10);
    if (hours > 0 || mins > 0) {
      setState({...state, error: ''});
    } else if (hours === 0 || mins === 0) {
      setState({...state, error: 'The minimum accepted value is 1 minute.'});
    } else {
      setState({...state, error: 'Enter an estimated time.'});
    }
  };

  return (
    <Root className={classes.displayFlex}>
      <HoursMinsField
        {...args}
        handleHoursChange={handleHoursChange}
        handleMinsChange={handleMinsChange}
        error={state.error}
      />
      <Button
        sx={{ml: 1}}
        className="input-group-btn"
        variant="contained"
        color="primary"
        onClick={handleValidation}
      >
        Submit
      </Button>
    </Root>
  );
};
BasicField.args = {
  id: 'estimated-time',
  label: 'Estimated time',
  required: true,
};
