import React, {useState} from 'react';

import {styled} from '@mui/material/styles';
import {TextField, Box} from '@mui/material';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import {LocalizationProvider, DatePicker} from '@mui/lab';

import frLocale from 'date-fns/locale/fr';

const PREFIX = 'Default';

const classes = {
  container: `${PREFIX}-container`,
  hidden: `${PREFIX}-hidden`,
};

const Root = styled('div')(({theme}) => ({
  [`&.${classes.container}`]: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },

  [`& .${classes.hidden}`]: {
    visibility: 'hidden',
  },
}));

// class LocalizedUtils extends DateFnsUtils {
//   getDatePickerHeaderText(date) {
//     return format(date, 'd MMM yyyy', {locale: this.locale});
//   }
// }

export default {
  title: 'Lab/Date fields',
  component: DatePicker,
  argTypes: {
    disabled: {
      description: 'Disable picker and text field',
      control: {
        type: 'boolean',
      },
      table: {
        type: {summary: 'boolean'},
      },
    },
    disableToolbar: {
      description: 'Hide toolbar and show only date/time views',
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: {summary: 'false'},
        type: {summary: 'boolean'},
      },
    },
    disableFuture: {
      description: 'Disable future dates',
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: {summary: 'false'},
        type: {summary: 'boolean'},
      },
    },
    disablePast: {
      description: 'Disable past dates',
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: {summary: 'false'},
        type: {summary: 'boolean'},
      },
    },
    maxDate: {
      description: 'Max selectable date',
      control: {
        type: 'text',
      },
      table: {
        type: {summary: 'ParsableDate'},
        defaultValue: {summary: 'Date(2100-01-01)'},
      },
    },
    minDate: {
      description: 'Min selectable date',
      control: {
        type: 'text',
      },
      table: {
        type: {summary: 'ParsableDate'},
        defaultValue: {summary: 'Date(1900-01-01)'},
      },
    },
    variant: {
      description: 'The variant to use.',
      control: {
        type: 'radio',
      },
      options: ['dialog', 'inline', 'static'],
      table: {
        defaultValue: {summary: 'dialog'},
      },
    },
    readOnly: {
      description: 'Make picker read only',
      table: {
        type: {summary: 'boolean'},
      },
      control: {
        type: 'boolean',
      },
    },
    orientation: {
      description: 'Force rendering in particular orientation',
      control: {
        type: 'radio',
      },
      options: ['portrait', 'landscape'],
      table: {
        type: {summary: 'string'},
        defaultValue: {summary: 'portrait'},
      },
    },
  },
};

export const Default = (args) => {
  const [state, setState] = React.useState({
    selectedInline: null,
    selectedInlineFrench: null,
    selectedDefault: null,
  });

  const handleInline = (date) => {
    setState({...state, selectedInline: date});
  };

  const handleInlineFrench = (date) => {
    setState({...state, selectedInlineFrench: date});
  };

  const [selectedDate, handleDateChange] = useState(new Date());

  return (
    <Root className={classes.container}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <Box sx={{mb: 3}}>
          <DatePicker
            value={state.selectedInline}
            variant="dialog"
            format="MMM d, yyyy"
            clearable
            autoOk
            DialogProps={{maxWidth: false}}
            onChange={handleInline}
            renderInput={(props) => (
              <TextField
                {...props}
                label="English date"
                placeholder="Select date"
              />
            )}
            {...args}
          />
        </Box>
      </LocalizationProvider>
      <LocalizationProvider dateAdapter={AdapterDateFns} locale={frLocale}>
        <Box sx={{mb: 3}}>
          <DatePicker
            format="d MMM yyyy"
            variant="dialog"
            clearable
            sx={{mb: 3}}
            autoOk
            value={state.selectedInlineFrench}
            onChange={handleInlineFrench}
            DialogProps={{maxWidth: false}}
            renderInput={(props) => (
              <TextField
                {...props}
                label="Date française"
                placeholder="Sélectionner une date"
              />
            )}
            {...args}
          />
        </Box>
      </LocalizationProvider>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <Box sx={{mb: 3}}>
          <DatePicker
            value={selectedDate}
            clearable
            onChange={handleDateChange}
            variant="dialog"
            format="MMM d, yyyy"
            sx={{mb: 3}}
            autoOk
            renderInput={(props) => (
              <TextField
                {...props}
                label="Default value"
                placeholder="Select date"
              />
            )}
            DialogProps={{maxWidth: false}}
            {...args}
          />
        </Box>
        <Box sx={{mb: 3}}>
          <DatePicker
            onChange={handleDateChange}
            disabled
            sx={{mb: 3}}
            format="MMM d, yyyy"
            renderInput={(props) => <TextField {...props} label="Disabled" />}
          />
        </Box>
        <Box sx={{mb: 3}}>
          <DatePicker
            onChange={handleDateChange}
            readOnly
            sx={{mb: 3}}
            format="MMM d, yyyy"
            renderInput={(props) => <TextField {...props} label="Read only" />}
          />
        </Box>
      </LocalizationProvider>
    </Root>
  );
};
