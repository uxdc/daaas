import React from 'react';
import {IconButton} from '@mui/material';
import Tooltip from '@mui/material/Tooltip';
import Icon from '@mdi/react';
import {mdiInformationOutline} from '@mdi/js';

export default {
  title: 'Molecules/Tooltip',
  component: Tooltip,
  argTypes: {},
};

export function InformationTooltip(args) {
  return (
    <Tooltip
      describeChild
      title="Additional information about item"
      arrow={true}
    >
      <IconButton aria-label="Label text" size="large">
        <Icon path={mdiInformationOutline} size={1} />
      </IconButton>
    </Tooltip>
  );
}
InformationTooltip.storyName = 'Tooltip - More information';

export function LabelTooltip(args) {
  return (
    <Tooltip title="Label text" arrow={true}>
      <IconButton aria-label="Label text" size="large">
        <Icon path={mdiInformationOutline} size={1} />
      </IconButton>
    </Tooltip>
  );
}
LabelTooltip.storyName = 'Tooltip - Label';
