import {InputAdornment, TextField} from '@mui/material';
import {styled} from '@mui/material/styles';
import SearchIcon from '@mui/icons-material/Search';
import {Autocomplete} from '@mui/material';
import queryString from 'query-string';
import React from 'react';
import {withRouter} from 'react-router-dom';

import {suggestions} from '../Data/fakeData';

const PREFIX = 'SearchBar';

const classes = {
  root: `${PREFIX}-root`,
  searchIcon: `${PREFIX}-searchIcon`,
  textfield: `${PREFIX}-textfield`,
};

const Root = styled('div')((
    {
      theme,
    },
) => ({
  [`&.${classes.root}`]: {
    flexGrow: 1,
  },

  [`& .${classes.searchIcon}`]: {
    marginLeft: theme.spacing(1),
  },

  [`& .${classes.textfield}`]: {
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        '& legend': {
          display: 'inline',
        },
      },
    },
  },
}));

function SearchBar(props) {
  const [value, setValue] = React.useState('');
  const searchQuery = queryString.parse(props.location.search).search;

  const handleEnterKey = (e) => {
    if (e.keyCode === 13) {
      props.history.push({pathname: '/results', search: `?search=${value}`});
    }
  };

  return (
    <Root className={classes.root}>
      <Autocomplete
        id="search-bar"
        freeSolo
        // disableOpenOnFocus
        onInputChange={(e, inputValue) => {
          setValue(inputValue);
        }}
        options={suggestions.map((option) => option.subject)}
        defaultValue={searchQuery}
        renderInput={(params) => {
          params.InputProps = {
            ...params.InputProps,
          };
          return (
            <TextField
              {...params}
              className={classes.textfield}
              margin="dense"
              variant="outlined"
              fullWidth
              placeholder={props.placeholder}
              onKeyDown={handleEnterKey}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon className={classes.searchIcon} />
                  </InputAdornment>
                ),
                inputProps: {
                  'aria-label': props.label,
                },
              }}
            />
          );
        }}
      />
    </Root>
  );
}

export default withRouter(SearchBar);
