import React from 'react';
import {styled} from '@mui/material/styles';
import PropTypes from 'prop-types';
import {
  Popper,
  Grow,
  Paper,
  MenuList,
  ClickAwayListener,
} from '@mui/material';
import {ConditionalWrapper} from '../Utilities/HOCs';
import clsx from 'clsx';

const PREFIX = 'Menu';

const classes = {
  popper: `${PREFIX}-popper`,
  hidden: `${PREFIX}-hidden`,
  paper: `${PREFIX}-paper`,
  menuList: `${PREFIX}-menuList`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.popper}`]: {
    zIndex: 1000,
  },

  [`& .${classes.hidden}`]: {
    display: 'none',
  },

  [`& .${classes.paper}`]: {
    border: '1px solid',
    borderColor: theme.palette.divider,
  },

  [`& .${classes.menuList}`]: {
    textAlign: 'left',
  },
}));

export function Menu(props) {
  const {
    children,
    id,
    open,
    anchorEl,
    handleClose,
    handleBlur,
    placement,
  } = props;

  return (
    <Root>
      <Popper
        className={clsx(classes.popper, {
          [classes.hidden]: open === false,
        })}
        open={open}
        anchorEl={anchorEl}
        role={undefined}
        placement={placement}
        transition
        disablePortal
        keepMounted
      >
        {({TransitionProps, placement}) => (
          <Grow
            // TODO: add switch for other placements
            {...TransitionProps}
            style={{
              transformOrigin: 'center top',
            }}
          >
            <Paper className={classes.paper} elevation={4}>
              <ConditionalWrapper
                condition={open}
                wrapper={(children) => (
                  <ClickAwayListener onClickAway={handleClose}>
                    {children}
                  </ClickAwayListener>
                )}
              >
                <MenuList
                  className={classes.menuList}
                  autoFocusItem={open}
                  id={id}
                  onKeyDown={handleBlur}
                >
                  {children}
                </MenuList>
              </ConditionalWrapper>
            </Paper>
          </Grow>
        )}
      </Popper>
    </Root>
  );
}

const PLACEMENT = {
  BOTTOMEND: 'bottom-end',
  BOTTOMSTART: 'bottom-start',
  BOTTOM: 'bottom',
  LEFTEND: 'left-end',
  LEFTSTART: 'left-start',
  LEFT: 'left',
  RIGHTEND: 'right-end',
  RIGHTSTART: 'right-start',
  RIGHT: 'right',
  TOPEND: 'top-end',
  TOPSTART: 'top-start',
  TOP: 'top',
};

Menu.propTypes = {
  /**
   Menu id used for a11y
  */
  id: PropTypes.string.isRequired,
  /**
   Menu default state, true = open, false = closed
  */
  open: PropTypes.bool,
  /**
   Placement of the menu popper
   */
  placement: PropTypes.oneOf(Object.values(PLACEMENT)),
};

Menu.defaultProps = {
  open: false,
  placement: PLACEMENT.BOTTOMEND,
};
