import React from 'react';
import {styled} from '@mui/material/styles';
import {useTranslation} from 'react-i18next';
import {Button} from '@mui/material';
import {withRouterAndRef} from '../Utilities/HOCs';

const PREFIX = 'BypassBlocks';

const classes = {
  root: `${PREFIX}-root`,
  block: `${PREFIX}-block`,
};

const Root = styled('div')((
    {
      theme,
    },
) => ({
  [`& .${classes.root}`]: {
    listStyleType: 'none',
  },

  [`& .${classes.block}`]: {
    left: 0,
    position: 'absolute',
    textAlign: 'center',
    top: '10px',
    width: '100%',
    zIndex: 1300,
  },
}));

const BypassBlocks = React.forwardRef((props, ref) => {
  const {t} = useTranslation();

  const pathname = props.history.location.pathname;
  const [state, setState] = React.useState({
    mainFocused: null,
    footerFocused: null,
  });

  const handleMainFocus = () => {
    setState({...state, mainFocused: true});
  };

  const handleMainBlur = () => {
    setState({...state, mainFocused: false});
  };

  const handleFooterFocus = () => {
    setState({...state, footerFocused: true});
  };

  const handleFooterBlur = () => {
    setState({...state, footerFocused: false});
  };

  const handleMainClick = () => {
    ref.main.current.focus();
  };

  const handleAboutClick = () => {
    ref.footer.current.focus();
  };

  if (pathname === '/') {
    return null;
  } else {
    return (
      <Root>
        <ul className={classes.root}>
          <li className={classes.block}>
            <Button
              className={state.mainFocused ? '' : 'screen-reader-text'}
              color="primary"
              variant="contained"
              onFocus={handleMainFocus}
              onBlur={handleMainBlur}
              onClick={handleMainClick}
            >
              {t('Skip to main content')}
            </Button>
          </li>
          <li className={classes.block}>
            <Button
              className={state.footerFocused ? '' : 'screen-reader-text'}
              color="primary"
              variant="contained"
              onFocus={handleFooterFocus}
              onBlur={handleFooterBlur}
              onClick={handleAboutClick}
            >
              {t('Skip to footer')}
            </Button>
          </li>
        </ul>
      </Root>
    );
  }
});

export default withRouterAndRef(BypassBlocks);
