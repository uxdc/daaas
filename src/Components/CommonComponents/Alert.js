import React from 'react';
import {styled} from '@mui/material/styles';
import PropTypes from 'prop-types';
import {IconButton, Collapse} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import MuiAlert from '@mui/material/Alert';
import clsx from 'clsx';

const PREFIX = 'Alert';

const classes = {
  root: `${PREFIX}-root`,
  bannerAlert: `${PREFIX}-bannerAlert`,
  placeholder: `${PREFIX}-placeholder`,
};

const Root = styled('div')(({theme}) => ({
  [`& .${classes.root}`]: {
    width: '100%',
  },
  [`& .${classes.bannerAlert}`]: {
    borderRadius: 0,
    borderBottom: '1px solid',
    borderBottomColor: theme.palette.divider,
  },
  [`& .${classes.placeholder}`]: {
    minHeight: theme.spacing(6),
    padding: theme.spacing(1.75, 7.5, 1.75, 6.25),
    boxSizing: 'border-box',
    visibility: 'hidden',
  },
}));

export function Alert(props) {
  const ref = React.useRef(null);

  return (
    <Root>
      <Collapse in={props.open} className={classes.root} ref={ref}>
        <MuiAlert
          sx={props.sx}
          severity={props.severity}
          className={clsx(props.className, {
            [classes.bannerAlert]: props.banner === true,
          })}
          role={props.role ? props.role : 'alert'}
          action={
            <IconButton
              aria-label="close"
              onClick={(e) => {
                e.stopPropagation();
                props.handleClose();
              }}
              size="large"
            >
              <CloseIcon fontSize="small" />
            </IconButton>
          }
        >
          {props.message}
        </MuiAlert>
      </Collapse>
    </Root>
  );
}

const SEVERITY = {
  ERROR: 'error',
  INFO: 'info',
  SUCCESS: 'success',
  WARNING: 'warning',
};

Alert.propTypes = {
  /**
   * The message to display.
   */
  message: PropTypes.node.isRequired,
  /**
    If true, Snackbar is open.
  */
  open: PropTypes.bool.isRequired,
  /**
    The severity of the alert. This defines the color and icon used.
  */
  severity: PropTypes.oneOf(Object.values(SEVERITY)).isRequired,
  /**
   Determines whether this alert is displayed as a banner or standard alert.
   */
  banner: PropTypes.bool,
  /**
    Adds an external class to the alert.
  */
  className: PropTypes.string,
};

Alert.defaultProps = {
  open: true,
  banner: false,
};
