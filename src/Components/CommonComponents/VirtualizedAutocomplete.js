import React, {useRef} from 'react';
import {styled} from '@mui/material/styles';
import {alpha} from '@mui/material/styles';
import {useTranslation} from 'react-i18next';
import DynamicList, {createCache} from 'react-window-dynamic-list';
import {useTheme} from '@mui/material/styles';
import {TextField} from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';

const PREFIX = 'VirtualizedAutocomplete';

const classes = {
  option: `${PREFIX}-option`,
  listbox: `${PREFIX}-listbox`,
};

const Root = styled('div')(({theme}) => ({
  [`& .${classes.option}`]: {
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.black, 0.04),
    },
  },

  [`& .${classes.listbox}`]: {
    height: ['auto', '!important'],
  },
}));

const cache = createCache();
const OuterElementContext = React.createContext({});
const OuterElementType = React.forwardRef((props, ref) => {
  const outerProps = React.useContext(OuterElementContext);
  return <div ref={ref} {...props} {...outerProps} />;
});

const DynamicListbox = React.forwardRef(function DynamicListbox(props, ref) {
  const {children, filter, options, width, ...other} = props;
  const itemData = React.Children.toArray(children);
  const dynamicListRef = useRef();
  const theme = useTheme();

  const filteredOptions = options.filter((option) => {
    return option.text
        .toLowerCase()
        .includes(filter ? filter.toLowerCase() : '');
  });

  return (
    <OuterElementContext.Provider value={other}>
      <DynamicList
        cache={cache}
        ref={dynamicListRef}
        data={filteredOptions}
        width={width}
        height={500}
        outerElementType={OuterElementType}
      >
        {({index, style}) => {
          style = {
            ...style,
            fontSize: theme.typography.body1.fontSize,
            fontFamily: theme.typography.body1.fontFamily,
            fontWeight: theme.typography.body1.fontWeight,
            lineHeight: theme.typography.body1.lineHeight,
            letterSpacing: theme.typography.body1.letterSpacing,
          };
          return itemData[index] ?
            React.cloneElement(itemData[index], {style: style}) :
            '';
        }}
      </DynamicList>
    </OuterElementContext.Provider>
  );
});

export function VirtualizedAutocomplete(props) {
  const {t} = useTranslation();
  const ref = React.useRef(null);
  const [state, setState] = React.useState({
    filter: '',
    componentWidth: '',
  });

  React.useEffect(() => {
    const handleResize = () =>
      setState({...state, componentWidth: ref.current.offsetWidth});
    handleResize();
    window.addEventListener('load', handleResize);
    window.addEventListener('resize', handleResize);
    window.addEventListener('orientationchange', handleResize);
    return () => {
      window.removeEventListener('load', handleResize);
      window.removeEventListener('resize', handleResize);
      window.removeEventListener('orientationchange', handleResize);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Root>
      <Autocomplete
        ref={ref}
        componentsProps={{
          clearIndicator: {tabIndex: 0},
        }}
        onInputChange={(e) => setState({...state, filter: e.target.value})}
        onBlur={() => setState({...state, filter: ''})}
        className={props.className}
        classes={{option: classes.option, listbox: classes.listbox}}
        id={props.id}
        ListboxComponent={DynamicListbox}
        options={props.options}
        value={props.value}
        onChange={props.onChange}
        renderInput={(params) => (
          <TextField
            {...params}
            multiline
            minRows="1"
            variant={props.variant}
            label={props.label}
            required={props.required}
          />
        )}
        disabled={props.disabled}
        getOptionLabel={(option) => option.text}
        openOnFocus
        clearText={t('Clear')}
        closeText={t('Close')}
        openText={t('Open')}
        noOptionsText={t('No options')}
        ListboxProps={{
          filter: state.filter,
          options: props.options,
          width: state.componentWidth,
        }}
      />
    </Root>
  );
}
