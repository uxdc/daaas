import React from 'react';
import PropTypes from 'prop-types';
// import {useTranslation} from 'react-i18next';
import {Snackbar as MUISnackbar} from '@mui/material';
import MuiAlert from '@mui/material/Alert';
// import {makeStyles} from '@mui/material/styles';

// const useStyles = makeStyles((theme) => ({
//   root: {
//     'minWidth': theme.spacing(49),
//     '& .MuiAlert-root': {
//       width: '100%',
//     },
//     [theme.breakpoints.down('xs')]: {
//       'bottom': theme.spacing(10),
//       'minWidth': '0',
//       '& .MuiAlert-root': {
//         width: '100%',
//       },
//     },
//   },
// }));

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export function Snackbar(props) {
  // const classes = useStyles();

  return (
    <MUISnackbar
      role="none"
      open={props.open}
      autoHideDuration={6000}
      onClose={props.handleClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      // className={classes.root}
    >
      <Alert role="none" onClose={props.handleClose} severity={props.severity}>
        {<span role="alert">{props.message}</span>}
      </Alert>
    </MUISnackbar>
  );
}

const SEVERITY = {
  ERROR: 'error',
  INFO: 'info',
  SUCCESS: 'success',
  WARNING: 'warning',
};

Snackbar.propTypes = {
  /**
   * The message to display.
   */
  message: PropTypes.node.isRequired,
  /**
    If true, Snackbar is open.
  */
  open: PropTypes.bool,
  /**
    The severity of the alert. This defines the color and icon used.
  */
  severity: PropTypes.oneOf(Object.values(SEVERITY)).isRequired,
  /**
    Click handler to close snackbar
  */
  handleClose: PropTypes.func.isRequired,
};

Snackbar.defaultProps = {
  open: false,
};
