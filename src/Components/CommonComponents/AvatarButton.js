import React from 'react';
import {styled} from '@mui/material/styles';
import PropTypes from 'prop-types';
import {IconButton} from '@mui/material';
import {alpha} from '@mui/material/styles';
import {Avatar} from './Avatar';

const PREFIX = 'AvatarButton';

const classes = {
  root: `${PREFIX}-root`,
};

const StyledIconButton = styled(IconButton)(({theme}) => ({
  [`& .${classes.root}`]: {
    'padding': theme.spacing(0.5),
    'color': alpha(theme.palette.common.black, 0.6),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.black, 0.12),
    },
    '&:focus-visible': {
      backgroundColor: alpha(theme.palette.common.black, 0.12),
    },
  },
}));

export function AvatarButton(props) {
  const {className, disabled, edge, onClick, ...avatarProps} = props;
  return (
    <StyledIconButton
      sx={props.sx}
      className={className}
      disabled={disabled}
      edge={edge}
      onClick={onClick}
      classes={{
        root: classes.root,
      }}
      size="large"
    >
      <Avatar {...avatarProps} disabled={disabled} />
    </StyledIconButton>
  );
}

const COLOR = {
  DEFAULT: 'default',
  PURPLE: 'purple',
  GREEN: 'green',
  ORANGE: 'orange',
};

AvatarButton.propTypes = {
  /**
    Used to render icon or text elements inside the Avatar if src is not set. This can be an element, or just a string.
   */
  content: PropTypes.node,
  /**
    The src attribute for the img element.
   */
  src: PropTypes.string,
  /**
    Used in combination with src to provide an alt attribute for the rendered img element.
   */
  alt: PropTypes.string,
  /**
    Color of avatar if text or icon content.
   */
  color: PropTypes.oneOf(Object.values(COLOR)),
  /**
    If true, the button will be disabled.
  */
  disabled: PropTypes.bool,
  /**
   * If given, uses a negative margin to counteract the padding on one side (this is often helpful for aligning the left or right side of the icon with content above or below, without ruining the border size and shape).
   */
  edge: PropTypes.bool,
  /**
   * If given, uses a negative margin to counteract the padding on one side (this is often helpful for aligning the left or right side of the icon with content above or below, without ruining the border size and shape).
   */
  component: PropTypes.elementType,
};

AvatarButton.defaultProps = {
  color: COLOR.DEFAULT,
  disabled: false,
  edge: false,
};
