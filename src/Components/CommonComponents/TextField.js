import React from 'react';
import {styled} from '@mui/material/styles';
import {TextField as MUITextField} from '@mui/material';
import PropTypes from 'prop-types';

const PREFIX = 'TextField';

const classes = {
  displayBlock: `${PREFIX}-displayBlock`,
};

const StyledMUITextField = styled(MUITextField)(({theme}) => ({
  [`& .${classes.displayBlock}`]: {
    display: 'block',
  },
}));

export default function TextField(props) {
  const [state, setState] = React.useState({
    text: props.defaultValue,
  });

  function handleChange(e) {
    const val = e.target.value;
    setState({...state, text: val});
  }

  return (
    <StyledMUITextField
      {...props}
      className={`${props.className} ${classes.displayBlock}`}
      InputProps={{
        readOnly: props.readOnly,
        startAdornment: props.startAdornment,
        endAdornment: props.endAdornment,
      }}
      InputLabelProps={{
        shrink: props.readOnly ? true : undefined,
      }}
      onChange={handleChange}
      helperText={props.readOnly ? 'Read-only' : props.helperText}
      placeholder={!state.value && props.readOnly ? ' --' : props.placeholder}
      value={state.text}
    ></StyledMUITextField>
  );
}

TextField.propTypes = {
  /**
   Determines the label of the field.
   */
  label: PropTypes.string.isRequired,
  /**
   If true, the input elements will be required.
   */
  required: PropTypes.bool,
  /**
   Determines if the field will feature autocomplete or not.
   */
  autoComplete: PropTypes.string,
  /**
    If true, the input elements will be disabled.
   */
  disabled: PropTypes.bool,
  /**
   It prevents the user from changing the value of the field (not from interacting with the field).
   */
  readOnly: PropTypes.bool,
  /**
    Determines if the field displays error styles and error helper text.
  */
  error: PropTypes.bool,
  /**
   If true, the input will take up the full width of its container.
   */
  fullWidth: PropTypes.bool,
  /**
   Determines the border styles of the field.
   */
  variant: PropTypes.string,
  /**
   Determines the color styles of the field.
   */
  color: PropTypes.string,
  /**
   Determines the placeholder text when the field is empty.
   */
  placeholder: PropTypes.string,
  /**
   Determines the value of the field when the page is loaded.
   */
  defaultValue: PropTypes.string,
  /**
    If true, a textarea element will be rendered instead of an input.
   */
  multiline: PropTypes.bool,
  /**
   'Number of rows to display when multiline option is set to true.'
   */
  rows: PropTypes.number,
  /**
   Maximum number of rows to display when multiline option is set to true.
   */
  maxRows: PropTypes.number,
  /**
   Inserts an adornment to at the beginning of the field.
   */
  startAdornment: PropTypes.element,
  /**
   Inserts an adornment to at the end of the field.
   */
  endAdornment: PropTypes.element,
};

TextField.defaultProps = {
  autoComplete: 'off',
  color: 'primary',
  variant: 'outlined',
  required: false,
  disabled: false,
  readOnly: false,
  multiline: false,
  fullWidth: false,
  select: false,
  placeholder: '',
  defaultValue: '',
  error: false,
  margin: 'dense',
};
