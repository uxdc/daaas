import React from 'react';
import {useTranslation} from 'react-i18next';
import {Button} from '@mui/material';

export default function Language() {
  const {t} = useTranslation();
  return (
    <React.Fragment>
      <span className="screen-reader-text">{t('Language selection')}</span>
      <Button color="default">
        <span lang="fr">Français</span>
      </Button>
    </React.Fragment>
  );
}
