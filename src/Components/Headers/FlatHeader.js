import React from 'react';
import {styled} from '@mui/material/styles';
import {useTranslation} from 'react-i18next';
import {AppBar, Button, Toolbar} from '@mui/material';
import {Link as RouterLink} from 'react-router-dom';
import {mdiAccountCircle} from '@mdi/js';
import {Icon} from '@mdi/react';

import SearchBar from '../SearchBar';
import Branding from './Branding';
import Language from './Language';

const PREFIX = 'FlatHeader';

const classes = {
  accountOptions: `${PREFIX}-accountOptions`,
  appBar: `${PREFIX}-appBar`,
  branding: `${PREFIX}-branding`,
  lang: `${PREFIX}-lang`,
  navMenu: `${PREFIX}-navMenu`,
  search: `${PREFIX}-search`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.accountOptions}`]: {
    [theme.breakpoints.down('md')]: {
      order: 5,
    },
    [theme.breakpoints.down('sm')]: {
      order: 4,
    },
  },

  [`& .${classes.appBar}`]: {
    'zIndex': 1200,
    'boxShadow': 'none',
    'backgroundColor': theme.palette.common.white,
    'color': theme.palette.text.primary,
    '& .MuiToolbar-root': {
      display: 'flex',
      flexWrap: 'wrap',
    },
  },

  [`& .${classes.branding}`]: {
    [theme.breakpoints.down('md')]: {
      order: 1,
      flexGrow: 1,
      width: 'calc(100vw - 112px)',
    },
    '& img': {
      height: theme.spacing(3),
      display: 'block',
      [theme.breakpoints.down('md')]: {
        height: theme.spacing(2.5),
        marginTop: theme.spacing(1),
      },
    },
  },

  [`& .${classes.lang}`]: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.down('md')]: {
      'order': 2,
      'textAlign': 'right',
      '& button': {
        paddingRight: 0,
      },
    },
  },

  [`& .${classes.navMenu}`]: {
    [theme.breakpoints.down('md')]: {
      order: 3,
    },
    [theme.breakpoints.down('sm')]: {
      flexGrow: 1,
    },
  },

  [`& .${classes.search}`]: {
    'flexGrow': 1,
    'textAlign': 'center',
    [theme.breakpoints.down('md')]: {
      order: 4,
    },
    [theme.breakpoints.down('sm')]: {
      order: 5,
      flexBasis: '100%',
    },
    '& > div': {
      width: '60%',
      display: 'inline-block',
      [theme.breakpoints.down('lg')]: {
        width: '90%',
      },
      [theme.breakpoints.down('sm')]: {
        width: '100%',
      },
    },
  },
}));

export default function DefaultHeader(props) {
  const {t} = useTranslation();

  return (
    <Root>
      <AppBar className={classes.appBar}>
        <Toolbar>
          <div className={classes.branding}>
            <Branding />
          </div>
          <div className={classes.search}>
            <SearchBar label={t('Search')} placeholder={t('Start searching')} />
          </div>
          <div className={classes.lang}>
            <Language />
          </div>
          <div className={classes.accountOptions}>
            <Button
              component={RouterLink}
              to="/sign-in"
              variant="outlined"
              color="primary"
              startIcon={<Icon path={mdiAccountCircle} size={1} />}
            >
              {t('Sign in')}
            </Button>
          </div>
        </Toolbar>
      </AppBar>
    </Root>
  );
}
