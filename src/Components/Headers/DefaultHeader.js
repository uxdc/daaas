import React from 'react';
import {styled} from '@mui/material/styles';
import {useTranslation} from 'react-i18next';
import {AppBar, Button, Toolbar} from '@mui/material';
import {Link as RouterLink} from 'react-router-dom';
import {mdiAccountCircle} from '@mdi/js';
import {Icon} from '@mdi/react';
import {SM_SCREEN} from '../../Theme/constants';

import SearchBar from '../SearchBar';
import Branding from './Branding';
import Language from './Language';

const PREFIX = 'DefaultHeader';

const classes = {
  appBar: `${PREFIX}-appBar`,
  flatHeader: `${PREFIX}-flatHeader`,
  toolbar: `${PREFIX}-toolbar`,
  branding: `${PREFIX}-branding`,
  lang: `${PREFIX}-lang`,
  search: `${PREFIX}-search`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.appBar}`]: {
    zIndex: 1200,
    backgroundColor: theme.palette.common.white,
    color: theme.palette.text.primary,
  },

  [`& .${classes.flatHeader}`]: {
    boxShadow: 'none',
  },

  [`& .${classes.toolbar}`]: {
    display: 'flex',
    flexWrap: 'wrap',
    [theme.breakpoints.down('md')]: {
      minHeight: theme.spacing(16),
    },
  },

  [`& .${classes.branding}`]: {
    [theme.breakpoints.down('md')]: {
      flexGrow: 1,
    },
    '& img': {
      height: theme.spacing(3),
    },
  },

  [`& .${classes.lang}`]: {
    marginRight: theme.spacing(1),
  },

  [`& .${classes.search}`]: {
    'flexGrow': 1,
    'textAlign': 'center',
    [theme.breakpoints.down('md')]: {
      flexBasis: '100%',
    },
    '& > div': {
      width: '60%',
      display: 'inline-block',
      [theme.breakpoints.down('md')]: {
        width: '100%',
      },
    },
  },
}));

export default function DefaultHeader(props) {
  const {t} = useTranslation();

  const [state, setState] = React.useState({
    windowWidth: window.innerWidth,
  });

  const isSmScreen = state.windowWidth < SM_SCREEN;

  React.useEffect(() => {
    // Detect screen size
    const handleResize = () =>
      setState({...state, windowWidth: window.innerWidth});
    window.addEventListener('resize', handleResize);
    window.addEventListener('orientationchange', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
      window.removeEventListener('orientationchange', handleResize);
    };
  }, [state]);

  return (
    <Root>
      <AppBar
        className={`${classes.appBar} ${props.flat && classes.flatHeader}`}
      >
        <Toolbar className={classes.toolbar}>
          <div className={classes.branding}>
            <Branding />
          </div>
          {!isSmScreen && (
            <div className={classes.search}>
              <SearchBar
                label={t('Search')}
                placeholder={t('Start searching')}
              />
            </div>
          )}
          <div className={classes.lang}>
            <Language />
          </div>
          <Button
            component={RouterLink}
            to="/sign-in"
            variant="outlined"
            color="primary"
            startIcon={<Icon path={mdiAccountCircle} size={1} />}
          >
            {t('Sign in')}
          </Button>
          {isSmScreen && (
            <div className={classes.search}>
              <SearchBar
                label={t('Search')}
                placeholder={t('Start searching')}
              />
            </div>
          )}
        </Toolbar>
      </AppBar>
    </Root>
  );
}
