import React from 'react';
import {styled} from '@mui/material/styles';
import {SM_SCREEN} from '../../Theme/constants';

const PREFIX = 'Branding';

const classes = {
  brandLink: `${PREFIX}-brandLink`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.brandLink}`]: {
    '&:focus img': {
      border: '2px solid #0049b3',
      borderRadius: '2px',
    },
  },
}));

export default function Branding() {
  const [state, setState] = React.useState({
    windowWidth: window.innerWidth,
  });

  const isSmScreen = state.windowWidth < SM_SCREEN;

  React.useEffect(() => {
    // Detect screen size
    const handleResize = () =>
      setState({...state, windowWidth: window.innerWidth});
    window.addEventListener('resize', handleResize);
    window.addEventListener('orientationchange', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
      window.removeEventListener('orientationchange', handleResize);
    };
  }, [state]);

  return (
    <Root>
      {isSmScreen ? (
        <a href="https://www.canada.ca/en.html" className={classes.brandLink}>
          <img
            src={process.env.PUBLIC_URL + '/images/flag.svg'}
            alt=""
            className={classes.brandImage}
          />
          <span className="screen-reader-text">
            Government of Canada / <span lang="fr">Gouvernement du Canada</span>
          </span>
        </a>
      ) : (
        <a href="https://www.canada.ca/en.html" className={classes.brandLink}>
          <img
            src={process.env.PUBLIC_URL + '/images/sig-blk-en.svg'}
            alt=""
            className={classes.brandImage}
          />
          <span className="screen-reader-text">
            Government of Canada / <span lang="fr">Gouvernement du Canada</span>
          </span>
        </a>
      )}
    </Root>
  );
}
