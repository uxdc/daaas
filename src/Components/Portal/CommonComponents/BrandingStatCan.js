import React from 'react';
import {styled} from '@mui/material/styles';
import {Link} from '@mui/material';
const PREFIX = 'BrandingStatCan';

const classes = {
  brandLink: `${PREFIX}-brandLink`,
  brandImage: `${PREFIX}-brandImage`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('span')(({theme}) => ({
  [`& .${classes.brandLink}`]: {
    display: 'block',
    height: '100%',
  },

  [`& .${classes.brandImage}`]: {
    height: '100%',
    width: '100%',
  },
}));

export function BrandingStatCanEn() {
  return (
    <Root>
      <Link
        href="https://www.statcan.gc.ca/eng/start"
        className={classes.brandLink}
        underline="hover"
      >
        <img
          src={process.env.PUBLIC_URL + '/images/sig-stats-can-blk-en.svg'}
          alt="Statistics Canada"
          className={classes.brandImage}
        />
      </Link>
    </Root>
  );
}

export function BrandingStatCanFr() {
  return (
    <React.Fragment>
      <Link
        href="https://www.statcan.gc.ca/fra/debut"
        className={classes.brandLink}
        underline="hover"
      >
        <img
          src={process.env.PUBLIC_URL + '/images/sig-blk-fr.svg'}
          alt="Statistique Canada"
          className={classes.brandImage}
        />
      </Link>
    </React.Fragment>
  );
}
