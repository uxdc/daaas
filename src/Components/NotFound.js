import React from 'react';
import {styled} from '@mui/material/styles';
import {Typography, Container} from '@mui/material';
const PREFIX = 'NotFound';

const classes = {
  root: `${PREFIX}-root`,
};

const StyledContainer = styled(Container)((
    {
      theme,
    },
) => ({
  [`&.${classes.root}`]: {
    marginTop: theme.spacing(4),
  },
}));

export default function NotFound() {
  return (
    <StyledContainer maxWidth="xl" className={classes.root}>
      <Typography variant="h1">404 Not Found</Typography>
    </StyledContainer>
  );
}
