import React, {useState} from 'react';
import {styled} from '@mui/material/styles';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Autocomplete from '@mui/material/Autocomplete';
import {DatePicker, LocalizationProvider} from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import {
  TextField,
  FormLabel,
  FormControl,
  RadioGroup,
  Radio,
  Typography,
  Grid,
  Divider,
} from '@mui/material';
import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import {useTranslation} from 'react-i18next';

const PREFIX = 'ProjectDetails';

const classes = {
  fullWidth: `${PREFIX}-fullWidth`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.fullWidth}`]: {
    width: '100%',
  },
}));

function ProjectDetails(props) {
  const handleFromDateChange = (date) => {
    setState({...state, selectedFromDate: date});
  };
  const handleToDateChange = (date) => {
    setState({...state, selectedToDate: date});
  };
  const handleExpiryDateChange = (date) => {
    setState({...state, selectedExpiryDate: date});
  };

  const [isOpenStartDate, setIsOpenStartDate] = useState(false);
  const [isOpenEndDate, setIsOpenEndDate] = useState(false);
  const [isExpiryEndDate, setIsOpenExpiryDate] = useState(false);

  const {t} = useTranslation();

  const [state, setState] = React.useState({
    selectedFromDate: null,
    selectedToDate: null,
    selectedExpiryDate: null,
  });

  const Datasets = [
    {title: 'Dataset'},
    {title: 'Dataset'},
    {title: 'Dataset'},
    {title: 'Dataset'},
    {title: 'Dataset'},
    {title: 'Dataset'},
    {title: 'Dataset'},
    {title: 'Build'},
    {title: 'Couch'},
    {title: 'Dataset'},
    {title: 'Echo'},
    {title: 'Foxtrot'},
    {title: 'Gred'},
    {title: 'Hugh'},
    {title: 'Iglooo'},
    {title: 'Jag'},
    {title: 'Kevin'},
    {title: 'Lol'},
    {title: 'Mine'},
    {title: 'Nancy'},
    {title: 'October'},
    {title: 'Pancakes'},
    {title: 'Quorom'},
    {title: 'Rabbit'},
    {title: 'Steve'},
    {title: 'Tables'},
    {title: 'Uranus'},
    {title: 'Violin'},
    {title: 'WHat'},
    {title: 'Xylophone'},
    {title: 'Years'},
    {title: 'ZZ Top'},
    {
      title:
        'Dataset Dataset Dataset Dataset Dataset Dataset Dataset Dataset Dataset Dataset Dataset Dataset Dataset Dataset',
    },
  ];

  return (
    <Root>
      <Grid container sx={{pb: 3, pt: 3}}>
        <Box>
          <Typography variant="h5" sx={{mb: 3}}>
            Project details
          </Typography>
          <Divider sx={{mb: 3}} />
          <Typography variant="body1" sx={{mb: 3}}>
            All the information required on this section needs to exist
            previously in a Service Request (VDL project and researcher
            information). Re-open the Service Request associated with this
            project if any information is missing.
          </Typography>
          <Alert severity="error" sx={{mb: 3}}>
            {t('Complete all required fields to advance to the next step')}
          </Alert>
          <Box sx={{mb: 3}}>
            <TextField
              variant="outlined"
              id="contractnumber"
              fullWidth
              margin="dense"
              label={t('Contract number')}
              required
            />
          </Box>
          <Grid item>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <Box sx={{mb: 3}}>
                <DatePicker
                  id="start-date"
                  variant="inline"
                  format="MM/dd/yyyy"
                  value={state.selectedFromDate}
                  maxDate={state.selectedToDate}
                  autoOk
                  open={isOpenStartDate}
                  onClick={() => setIsOpenStartDate(true)}
                  onOpen={() => setIsOpenStartDate(true)}
                  onClose={() => setIsOpenStartDate(false)}
                  disablePast
                  required
                  onChange={handleFromDateChange}
                  renderInput={(props) => (
                    <TextField
                      {...props}
                      label={t('Contract start date')}
                      margin="dense"
                      fullWidth
                      variant="outlined"
                    />
                  )}
                  PopoverProps={{
                    'aria-modal': t('true'),
                  }}
                  KeyboardButtonProps={{
                    'aria-label': t('Select start date'),
                  }}
                />
              </Box>
              <Box sx={{mb: 3}}>
                <DatePicker
                  open={isOpenEndDate}
                  onClick={() => setIsOpenEndDate(true)}
                  onOpen={() => setIsOpenEndDate(true)}
                  onClose={() => setIsOpenEndDate(false)}
                  id="end-date"
                  variant="inline"
                  format="MM/dd/yyyy"
                  value={state.selectedToDate}
                  minDate={state.selectedFromDate}
                  onChange={handleToDateChange}
                  autoOk
                  required
                  renderInput={(props) => (
                    <TextField
                      {...props}
                      label={t('Contract end date')}
                      margin="dense"
                      fullWidth
                      variant="outlined"
                    />
                  )}
                  KeyboardButtonProps={{
                    'aria-label': t('Select end date'),
                  }}
                />
              </Box>
              <Box sx={{mb: 3}}>
                <DatePicker
                  open={isExpiryEndDate}
                  onClick={() => setIsOpenExpiryDate(true)}
                  onOpen={() => setIsOpenExpiryDate(true)}
                  onClose={() => setIsOpenExpiryDate(false)}
                  id="end-date"
                  variant="inline"
                  format="MM/dd/yyyy"
                  value={state.selectedExpiryDate}
                  minDate={state.selectedFromDate}
                  onChange={handleExpiryDateChange}
                  autoOk
                  required
                  renderInput={(props) => (
                    <TextField
                      {...props}
                      label={t('Archive expiry')}
                      margin="dense"
                      fullWidth
                      variant="outlined"
                    />
                  )}
                  KeyboardButtonProps={{
                    'aria-label': t('Select archive expiry'),
                  }}
                />
              </Box>
            </LocalizationProvider>
          </Grid>
          <Box sx={{mb: 3}}>
            <TextField
              variant="outlined"
              id="primaryinvestigator"
              fullWidth
              label={t('Primary investigator')}
              required
              margin="dense"
              error
              helperText="This field is required."
            />
          </Box>
          <Grid item sx={{mb: 3}}>
            <FormControl component="fieldset" className={classes.fullWidth}>
              <FormLabel component="legend" required>
                {t('Environment')}
              </FormLabel>
              <RadioGroup
                aria-label={t('environment')}
                name={t('radio-buttons-group')}
                color="primary"
              >
                <FormControlLabel
                  value={t('VDL')}
                  control={<Radio color="primary" />}
                  label={t('VDL')}
                />
                <FormControlLabel
                  value={t('Prerelease')}
                  control={<Radio color="primary" />}
                  label={t('Prerelease')}
                />
              </RadioGroup>
            </FormControl>
          </Grid>

          <Grid item sx={{mb: 3}}>
            <FormControl component="fieldset" className={classes.fullWidth}>
              <FormLabel component="legend" required>
                {t('Access location')}
              </FormLabel>
              <RadioGroup
                aria-label={t('Access location')}
                name={t('radio-buttons-group')}
                color="primary"
              >
                <FormControlLabel
                  value={t('Secure room')}
                  control={<Radio color="primary" />}
                  label={t('Secure room')}
                />
                <FormControlLabel
                  value={t('Authorized workspace')}
                  control={<Radio color="primary" />}
                  label={t('Authorized workspace')}
                />
              </RadioGroup>
            </FormControl>
          </Grid>
          <Grid item sx={{mb: 3}}>
            <Autocomplete
              multiple
              id="dataset-folder"
              options={Datasets}
              disableCloseOnSelect
              getOptionLabel={(option) => option.title}
              renderOption={(props, option, {selected}) => (
                <li {...props}>
                  <Checkbox
                    style={{marginRight: 8}}
                    Checkbox
                    color="primary"
                    checked={selected}
                  />
                  {option.title}
                </li>
              )}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="outlined"
                  label="Dataset folders"
                  required
                />
              )}
            />
          </Grid>

          <Grid item>
            <FormControl component="fieldset" className={classes.fullWidth}>
              <FormLabel component="legend" required>
                {t('Required tools')}
              </FormLabel>
              <FormGroup>
                <FormControlLabel
                  control={
                    <Checkbox
                      name="Default tools"
                      color="primary"
                      checked
                      InputProps={{readOnly: true}}
                    />
                  }
                  label={
                    <>
                      <Typography variant="body2">
                        {t('Default tools')}
                      </Typography>
                      <Typography variant="caption" component="p">
                        {t(
                            'Adobe Reader DC, Java, LibreOffice, Office 2019, Power BI, ProjectLibre, Python, R, RStudio, RTools, VSCode',
                        )}
                      </Typography>
                    </>
                  }
                />

                <FormControlLabel
                  control={<Checkbox name="SAS" color="primary" />}
                  label={
                    <>
                      <Typography variant="body2">{t('SAS')}</Typography>
                      <Typography variant="caption" component="p">
                        {t('SAS 9.4, SAS Enterprise Guide')}
                      </Typography>
                    </>
                  }
                />

                <FormControlLabel
                  control={<Checkbox color="primary" />}
                  label={t('SPSS')}
                  color="primary"
                  className={classes.fullWidth}
                />
                <FormControlLabel
                  control={<Checkbox color="primary" />}
                  label={t('STATA')}
                />
              </FormGroup>
            </FormControl>
          </Grid>
        </Box>
      </Grid>
    </Root>
  );
}

export default ProjectDetails;
