import React, {useState} from 'react';
import {styled} from '@mui/material/styles';
import Link from '@mui/material/Link';
import PropTypes from 'prop-types';
import useMediaQuery from '@mui/material/useMediaQuery';
import {Drawer} from '../../CommonComponents/Drawer';
import {DatePicker, LocalizationProvider} from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import {useTranslation} from 'react-i18next';
import {useTheme} from '@mui/material/styles';
import {
  FormControl,
  TextField,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  Typography,
  Checkbox,
  Grid,
  FormGroup,
  Box,
} from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import ListSubheader from '@mui/material/ListSubheader';
import {VariableSizeList} from 'react-window';

const PREFIX = 'MachineDetailsDrawer';

const classes = {
  fullWidth: `${PREFIX}-fullWidth`,
  listbox: `${PREFIX}-listbox`,
};

const Root = styled('div')(({theme}) => ({
  [`& .${classes.fullWidth}`]: {
    width: '100%',
  },

  [`& .${classes.listbox}`]: {
    'boxSizing': 'border-box',
    '& ul': {
      padding: 0,
      margin: 0,
    },
  },
}));

const LISTBOX_PADDING = 8; // px

function renderRow(props) {
  const {data, index, style} = props;
  return React.cloneElement(data[index], {
    style: {
      ...style,
      top: style.top + LISTBOX_PADDING,
    },
  });
}

const OuterElementContext = React.createContext({});

const OuterElementType = React.forwardRef((props, ref) => {
  const outerProps = React.useContext(OuterElementContext);
  return <div ref={ref} {...props} {...outerProps} />;
});

function useResetCache(data) {
  const ref = React.useRef(null);
  React.useEffect(() => {
    if (ref.current != null) {
      ref.current.resetAfterIndex(0, true);
    }
  }, [data]);
  return ref;
}

// Adapter for react-window
const ListboxComponent = React.forwardRef(function ListboxComponent(
    props,
    ref,
) {
  const {children, ...other} = props;
  const itemData = React.Children.toArray(children);
  const theme = useTheme();
  const smUp = useMediaQuery(theme.breakpoints.up('sm'), {noSsr: true});
  const itemCount = itemData.length;
  const itemSize = smUp ? 36 : 48;

  const getChildSize = (child) => {
    if (React.isValidElement(child) && child.type === ListSubheader) {
      return 48;
    }

    return itemSize;
  };

  const getHeight = () => {
    if (itemCount > 8) {
      return 8 * itemSize;
    }
    return itemData.map(getChildSize).reduce((a, b) => a + b, 0);
  };

  const gridRef = useResetCache(itemCount);

  return (
    <Root ref={ref}>
      <OuterElementContext.Provider value={other}>
        <VariableSizeList
          itemData={itemData}
          height={getHeight() + 2 * LISTBOX_PADDING}
          ref={gridRef}
          outerElementType={OuterElementType}
          innerElementType="ul"
          itemSize={(index) => getChildSize(itemData[index])}
          overscanCount={5}
          itemCount={itemCount}
        >
          {renderRow}
        </VariableSizeList>
      </OuterElementContext.Provider>
    </Root>
  );
});

ListboxComponent.propTypes = {
  children: PropTypes.node,
};

function random(length) {
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let result = '';

  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(Math.floor(Math.random() * characters.length));
  }

  return result;
}

const OPTIONS = Array.from(new Array(10000))
    .map(() => random(10 + Math.ceil(Math.random() * 20)))
    .sort((a, b) => a.toUpperCase().localeCompare(b.toUpperCase()));

const renderGroup = (params) => [
  <ListSubheader key={params.key} component="div">
    {params.group}
  </ListSubheader>,
  params.children,
];

export function AddVirtualMachine(props) {
  const {t} = useTranslation();

  return (
    <>
      <Drawer
        open={props.open}
        title={t('Add virtual machine')}
        content={<MachineDetailsContent />}
        primaryButton={t('Add')}
        secondaryButton={t('Cancel')}
        handlePrimaryClick={props.addVirtualMachine}
        handleSecondaryClick={props.closeDrawer}
        toggleDrawer={props.closeDrawer}
      />
    </>
  );
}

export function EditVirtualMachine(props) {
  const {t} = useTranslation();

  return (
    <>
      <Drawer
        open={props.open}
        title={t('Edit virtual machine')}
        content={<MachineDetailsContent />}
        primaryButton={t('Update')}
        secondaryButton={t('Cancel')}
        handlePrimaryClick={props.editVirtualMachine}
        handleSecondaryClick={props.closeDrawer}
        toggleDrawer={props.closeDrawer}
      />
    </>
  );
}

function MachineDetailsContent(props) {
  const {t} = useTranslation();

  const [state, setState] = React.useState({
    snackbarAddVirtualMachine: false,
    addVirtualMachine: false,
    showCard: false,
  });

  const security = [
    {label: 'Statistics Canada'},
    {label: 'Canada Revenue Agency'},
  ];

  const handleFromDateChange = (date) => {
    setState({...state, selectedFromDate: date});
  };

  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <div className={classes.paddingTopBottom}>
        <Typography variant="subtitle1" sx={{mb: 3}}>
          User details
        </Typography>
        <Typography variant="body2" sx={{mb: 3}}>
          If the cloud account email cannot be found it means it does not exist
          in Azure Active Directory. Submit a Jira ticket to the{' '}
          <Link
            href="https://jirab.statcan.ca/projects/DAZSUPP/summary"
            underline="always"
          >
            Cloud Jira project
          </Link>{' '}
          for assistance.
        </Typography>
        <Grid item sx={{mb: 3}}>
          <Autocomplete
            id="cloudaccountemail"
            disableListWrap
            classes={classes}
            ListboxComponent={ListboxComponent}
            renderGroup={renderGroup}
            options={OPTIONS}
            fullWidth
            groupBy={(option) => option[0].toUpperCase()}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="outlined"
                label="Cloud account email"
                required
              />
            )}
            renderOption={(props, option) => (
              <li {...props}>
                <Typography noWrap>{option}</Typography>
              </li>
            )}
          />
        </Grid>
        <Box sx={{mb: 3}}>
          <TextField
            margin="dense"
            id="firstname"
            label="First name"
            variant="outlined"
            fullWidth
            required
          />
        </Box>
        <Box sx={{mb: 3}}>
          <TextField
            margin="dense"
            id="lastName"
            label="Last name"
            variant="outlined"
            fullWidth
            required
          />
        </Box>
        <Box sx={{mb: 3}}>
          <TextField
            margin="dense"
            id="username"
            label="Username"
            variant="outlined"
            fullWidth
            required
          />
        </Box>
        <Grid item sx={{mb: 3}}>
          <Autocomplete
            id="organization"
            options={security}
            getOptionLabel={(option) => option.label}
            renderInput={(params) => (
              <TextField
                required
                {...params}
                label="Organization"
                variant="outlined"
              />
            )}
          />
        </Grid>
        <Box sx={{mb: 3}}>
          <TextField
            margin="dense"
            id="researcherID"
            label="Researcher ID"
            variant="outlined"
            fullWidth
            required
          />
        </Box>
        <Grid item sx={{mb: 3}}>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DatePicker
              id="contract-start-date"
              variant="inline"
              open={isOpen}
              onClick={() => setIsOpen(true)}
              onOpen={() => setIsOpen(true)}
              onClose={() => setIsOpen(false)}
              autoOk
              required
              format="MM/dd/yyyy"
              value={state.selectedFromDate}
              onChange={handleFromDateChange}
              disablePast
              renderInput={(props) => (
                <TextField
                  {...props}
                  label={'Security clearance expiry'}
                  margin="dense"
                  fullWidth
                  variant="outlined"
                />
              )}
              PopoverProps={{
                'aria-modal': 'true',
              }}
              KeyboardButtonProps={{
                'aria-label': 'Security clearance expiry',
              }}
            />
          </LocalizationProvider>
        </Grid>
        <Typography variant="subtitle1" sx={{mb: 3}}>
          Contact information
        </Typography>
        <Box sx={{mb: 3}}>
          <TextField
            margin="dense"
            id="phoneNumber"
            label="Phone number"
            variant="outlined"
            fullWidth
            required
          />
        </Box>
        <Box sx={{mb: 3}}>
          <TextField
            margin="dense"
            id="email"
            label="Email"
            variant="outlined"
            fullWidth
            required
          />
        </Box>
        <Typography variant="subtitle1" sx={{mb: 3}}>
          Virtual machine details
        </Typography>
        <Box sx={{mb: 3}}>
          <TextField
            margin="dense"
            id="virtualmachinename"
            label="Virtual machine name"
            variant="outlined"
            fullWidth
            required
          />
        </Box>
        <Grid item sx={{mb: 3}}>
          <FormControl component="fieldset" className={classes.fullWidth}>
            <RadioGroup id="virtualmachinename" name="virtualmachinename">
              <FormLabel required component="legend">
                Virtual machine language
              </FormLabel>
              <FormControlLabel
                control={<Radio color="primary" />}
                value="English"
                label="English"
              />
              <FormControlLabel
                control={<Radio color="primary" />}
                value="French"
                label="French"
              />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item>
          <FormControl component="fieldset" className={classes.fullWidth}>
            <FormLabel component="legend" required>
              Required tools
            </FormLabel>
            <FormGroup>
              <Grid container>
                <Grid>
                  <FormControlLabel
                    control={
                      <Checkbox
                        name="Default tools"
                        color="primary"
                        checked
                        InputProps={{readOnly: true}}
                      />
                    }
                    label={
                      <>
                        <Typography variant="body2">
                          {t('Default tools')}
                        </Typography>
                        <Typography variant="caption" component="p">
                          {t(
                              'Adobe Reader DC, Java, LibreOffice, Office 2019, Power BI, ProjectLibre, Python, R, RStudio, RTools, VSCode',
                          )}
                        </Typography>
                      </>
                    }
                  />
                </Grid>
              </Grid>
              <Grid container>
                <Grid>
                  <FormControlLabel
                    control={<Checkbox name="SAS" color="primary" />}
                    label={
                      <>
                        <Typography variant="body2">{t('SAS')}</Typography>
                        <Typography variant="caption" component="p">
                          {t('SAS 9.4, SAS Enterprise Guide')}
                        </Typography>
                      </>
                    }
                  />
                </Grid>
              </Grid>
              <FormControlLabel
                control={<Checkbox color="primary" />}
                label="SPSS"
              />
              <FormControlLabel
                control={<Checkbox color="primary" />}
                label="STATA"
              />
            </FormGroup>
          </FormControl>
        </Grid>
      </div>
    </>
  );
}
