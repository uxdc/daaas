import React from 'react';
import {styled} from '@mui/material/styles';
import {Link as RouterLink} from 'react-router-dom';
import {AppBar, Toolbar, Grid, Button, Typography} from '@mui/material';

const PREFIX = 'RequestToolbar';

const classes = {
  appBar: `${PREFIX}-appBar`,
  headerBtn: `${PREFIX}-headerBtn`,
};

const StyledAppBar = styled(AppBar)(({theme}) => ({
  [`&.${classes.appBar}`]: {
    margin: theme.spacing(0, -3),
    width: 'auto',
    backgroundColor: theme.palette.common.white,
    boxShadow: 'none',
    borderBottom: '1px solid',
    borderBottomColor: theme.palette.divider,
  },

  [`& .${classes.headerBtn}`]: {
    marginRight: theme.spacing(2),
  },
}));

function RequestToolbar(props) {
  return (
    <StyledAppBar position="static" className={classes.appBar} color="default">
      <Toolbar>
        <Grid container justifyContent="space-between">
          <Typography sx={{mt: 1}}>VDL PowerShell Intake Request</Typography>
          <Grid>
            <Button
              className={classes.headerBtn}
              variant="contained"
              color="primary"
              component={RouterLink}
              to="/VDLPowerShellIntake/"
            >
              New request
            </Button>
          </Grid>
        </Grid>
      </Toolbar>
    </StyledAppBar>
  );
}

export default RequestToolbar;
