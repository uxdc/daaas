import React from 'react';
import {styled} from '@mui/material/styles';
import {AppBar, Toolbar} from '@mui/material';
import BrandingStatsCan from '../../../Components/Headers/BrandingStatsCan';
import Language from '../../../Components/Headers/Language';

const PREFIX = 'Header';

const classes = {
  appBar: `${PREFIX}-appBar`,
  toolbar: `${PREFIX}-toolbar`,
  branding: `${PREFIX}-branding`,
  lang: `${PREFIX}-lang`,
  button: `${PREFIX}-button`,
  iconButton: `${PREFIX}-iconButton`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.appBar}`]: {
    zIndex: 1200,
    backgroundColor: theme.palette.common.white,
    color: theme.palette.text.primary,
  },

  [`& .${classes.toolbar}`]: {
    display: 'flex',
    flexWrap: 'wrap',
  },

  [`& .${classes.branding}`]: {
    [theme.breakpoints.down('md')]: {
      flexGrow: 1,
    },
    '& img': {
      height: theme.spacing(3),
    },
  },

  [`& .${classes.lang}`]: {
    marginRight: theme.spacing(1),
    textAlign: 'right',
    flexGrow: 1,
  },

  [`& .${classes.button}`]: {
    'backgroundColor': 'white',
    'boxShadow': 'none',
    '&:hover': {
      backgroundColor: 'white',
      color: '#FFF',
      boxShadow: 'none',
    },
  },

  [`& .${classes.iconButton}`]: {
    marginRight: theme.spacing(-0.5),
  },
}));

export default function DefaultHeader(props) {
  const [state, setState] = React.useState({
    windowWidth: window.innerWidth,
  });

  React.useEffect(() => {
    // Detect screen size
    const handleResize = () =>
      setState({...state, windowWidth: window.innerWidth});
    window.addEventListener('resize', handleResize);
    window.addEventListener('orientationchange', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
      window.removeEventListener('orientationchange', handleResize);
    };
  }, [state]);

  return (
    <Root>
      <AppBar
        className={`${classes.appBar} ${props.flat && classes.flatHeader}`}
      >
        <Toolbar className={classes.toolbar}>
          <div className={classes.branding}>
            <BrandingStatsCan />
          </div>
          <div className={classes.lang}>
            <Language />
          </div>
        </Toolbar>
      </AppBar>
      <Toolbar />
    </Root>
  );
}
