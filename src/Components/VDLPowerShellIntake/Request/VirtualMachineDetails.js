import React from 'react';
import {styled} from '@mui/material/styles';
import {Icon} from '@mdi/react';
import {Card} from '../../CommonComponents/Card';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import {SnackbarDeleteVirtualMachine} from '../../VettingApp/CommonComponents/Snackbars';
import {DialogDelete} from '../../VettingApp/CommonComponents/DialogBox';
import {useTranslation} from 'react-i18next';
import {AddVirtualMachine, EditVirtualMachine} from './MachineDetailsDrawer';
import {green} from '@mui/material/colors';
import {SnackbarEditVirtualMachine} from '../../VettingApp/CommonComponents/Snackbars';
import clsx from 'clsx';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Button from '@mui/material/Button';
import {alpha} from '@mui/material/styles';
import {SnackbarAddVirtualMachine} from '../../VettingApp/CommonComponents/Snackbars';
import {Typography, Divider, Grid, Paper} from '@mui/material';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import {
  mdiSmartCardOutline,
  mdiFileDocumentOutline,
  mdiDomain,
  mdiAccountOutline,
  mdiPhone,
  mdiEmailOutline,
  mdiMonitor,
  mdiTranslate,
  mdiHammerScrewdriver,
  mdiPlus,
} from '@mdi/js';

const PREFIX = 'VirtualMachine';

const classes = {
  addBtn: `${PREFIX}-addBtn`,
  avatar: `${PREFIX}-avatar`,
  powershellSection: `${PREFIX}-powershellSection`,
  powershellHeaderRow: `${PREFIX}-powershellHeaderRow`,
  powershellHeaderRowMarginBottom: `${PREFIX}-powershellHeaderRowMarginBottom`,
  powershellRow: `${PREFIX}-powershellRow`,
  powershellColumn: `${PREFIX}-powershellColumn`,
  widthAuto: `${PREFIX}-widthAuto`,
  root: `${PREFIX}-root`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.addBtn}`]: {
    'marginTop': theme.spacing(-1),
    'borderStyle': 'dashed',
    'width': '100%',
    'borderColor': alpha(theme.palette.common.black, 0.23),
    '&.MuiButton-outlinedPrimary:hover': {
      borderStyle: 'dashed',
    },
  },

  [`& .${classes.avatar}`]: {
    backgroundColor: green[500],
    color: theme.palette.grey[100],
    marginRight: theme.spacing(2),
  },

  [`& .${classes.powershellSection}`]: {
    display: 'flex',
    flexFlow: 'column',
    padding: theme.spacing(3),
    overflowY: 'auto',
  },

  [`& .${classes.powershellHeaderRow}`]: {
    'display': 'flex',
    'margin': theme.spacing(1.5, 0),
    'flexFlow': 'row',
    'height': '100%',
    'justifyContent': 'center',
    'alignItems': 'center',
    '&:first-of-type': {
      marginTop: 0,
    },
  },

  [`& .${classes.powershellHeaderRowMarginBottom}`]: {
    display: 'flex',
    margin: theme.spacing(0),
    flexFlow: 'row',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  [`& .${classes.powershellRow}`]: {
    'display': 'flex',
    'margin': theme.spacing(1.5, 0),
    'flexFlow': 'row',
    'height': '100%',
    'justifyContent': 'center',
    'width': '100%',
    'alignItems': 'center',
    '&:last-child': {
      marginBottom: 0,
    },
  },

  [`& .${classes.powershellColumn}`]: {
    'display': 'flex',
    'flexDirection': 'column',
    'width': '100%',
    'justifyContent': 'center',
    'marginRight': theme.spacing(1),
    'height': '100%',
    '&:last-child': {
      marginRight: 0,
    },
  },

  [`& .${classes.widthAuto}`]: {
    width: 'auto !important',
  },

  [`& .${classes.root}`]: {
    '& .MuiAccordionDetails-root': {
      display: 'block',
    },
    '& .MuiAccordionSummary-content': {
      marginTop: theme.spacing(0),
      marginBottom: theme.spacing(0),
    },
    '& .MuiAccordionSummary-content.Mui-expanded': {
      marginTop: theme.spacing(0),
      marginBottom: theme.spacing(2),
    },
    '& .MuiAccordionSummary-root.Mui-expanded': {
      minHeight: theme.spacing(0),
    },
  },
}));

function VirtualMachine(props) {
  const [state, setState] = React.useState({
    snackbarAddVirtualMachine: false,
    snackbarEditVirtualMachine: false,
    snackbarDelete: false,
    addVirtualMachine: false,
    editVirtualMachine: false,
    dialogDelete: false,
    showCard: false,
  });

  const editVirtualMachine = () => {
    setState({
      ...state,
      snackbarEditVirtualMachine: true,
      editVirtualMachine: false,
    });
  };

  const addVirtualMachine = () => {
    setState({
      ...state,
      snackbarAddVirtualMachine: true,
      addVirtualMachine: false,
      showCard: true,
    });
  };

  const deleteFile = () => {
    setState({
      ...state,
      snackbarDelete: true,
      dialogDelete: false,
      showCard: false,
    });
  };

  const handleClickOpen = (element) => {
    setState({...state, [element]: true});
  };

  const handleClickClose = (element) => {
    setState({...state, [element]: false});
  };

  const {t} = useTranslation();

  return (
    <Root>
      <Grid container sx={{pb: 3, pt: 3}}>
        <Box>
          <Typography variant="h5" sx={{mb: 3}}>
            Virtual machine details
          </Typography>
          <Divider sx={{mb: 3}} />
          <Typography variant="body1" sx={{mb: 3}}>
            All the information required on this section needs to exist
            previously in a Service Request (VDL project and researcher
            information). Re-open the Service Request associated with this
            project if any information is missing.
          </Typography>
          <Typography variant="body2" component="p">
            {t('Add card... *')}
          </Typography>
          <Typography
            variant="caption"
            color="textSecondary"
            component="p"
            sx={{mb: 2}}
          >
            At least one card must be added
          </Typography>
          {state.showCard === true && (
            <Paper className="paper-grey" sx={{mb: 3}} variant="outlined">
              <Card
                error={false}
                variant="outlined"
                primaryButton="Edit"
                secondaryButton="Delete"
                primaryClick={() => handleClickOpen('editVirtualMachine')}
                secondaryClick={() => handleClickOpen('dialogDelete')}
                title="STC-0412-ST Virtual machine name"
                content={
                  <>
                    <div className={classes.root}>
                      <Accordion>
                        <AccordionSummary
                          expandIcon={<ExpandMoreIcon />}
                          aria-controls="panel1a-content"
                          id="panel1a-header"
                        >
                          <div
                            className={classes.powershellHeaderRowMarginBottom}
                          >
                            <div className={classes.powershellColumn}>
                              <Avatar className={classes.avatar}>BB</Avatar>
                            </div>
                            <div className={classes.powershellColumn}>
                              <Typography variant="body1">
                                <Typography>Some email</Typography>
                              </Typography>
                              <Typography variant="body2">
                                <Typography>email@email.com</Typography>
                              </Typography>
                            </div>
                          </div>
                        </AccordionSummary>
                        <AccordionDetails>
                          <Typography variant="h6">
                            {t('Personal information')}
                          </Typography>

                          <div className={classes.powershellRow}>
                            <div
                              className={clsx(
                                  classes.powershellColumn,
                                  classes.widthAuto,
                              )}
                            >
                              <Icon path={mdiFileDocumentOutline} size={1} />
                            </div>
                            <div className={classes.powershellColumn}>
                              <Typography sx={{pl: 1}} variant="body1">
                                {t('Security clearance expiry date')}
                              </Typography>
                              <Typography sx={{pl: 1}} variant="body2">
                                02/23/2021
                              </Typography>
                            </div>
                          </div>
                          <div className={classes.powershellRow}>
                            <div
                              className={clsx(
                                  classes.powershellColumn,
                                  classes.widthAuto,
                              )}
                            >
                              <Icon path={mdiSmartCardOutline} size={1} />
                            </div>
                            <div className={classes.powershellColumn}>
                              <Typography sx={{pl: 1}} variant="body1">
                                Researcher ID
                              </Typography>
                              <Typography sx={{pl: 1}} variant="body2">
                                54674
                              </Typography>
                            </div>
                          </div>
                          <div className={classes.powershellRow}>
                            <div
                              className={clsx(
                                  classes.powershellColumn,
                                  classes.widthAuto,
                              )}
                            >
                              <Icon path={mdiDomain} size={1} />
                            </div>
                            <div className={classes.powershellColumn}>
                              <Typography sx={{pl: 1}} variant="body1">
                                {t('Organization')}
                              </Typography>
                              <Typography sx={{pl: 1}} variant="body2">
                                {t('Statistics Canada')}
                              </Typography>
                            </div>
                          </div>
                          <div className={classes.powershellRow}>
                            <div
                              className={clsx(
                                  classes.powershellColumn,
                                  classes.widthAuto,
                              )}
                            >
                              <Icon path={mdiAccountOutline} size={1} />
                            </div>
                            <div className={classes.powershellColumn}>
                              <Typography sx={{pl: 1}} variant="body1">
                                {t('Username')}
                              </Typography>
                              <Typography sx={{pl: 1}} variant="body2">
                                {t('some.email')}
                              </Typography>
                            </div>
                          </div>
                          <Typography variant="h6">
                            {t('Contact information')}
                          </Typography>
                          <div className={classes.powershellRow}>
                            <div
                              className={clsx(
                                  classes.powershellColumn,
                                  classes.widthAuto,
                              )}
                            >
                              <Icon path={mdiPhone} size={1} />
                            </div>
                            <div className={classes.powershellColumn}>
                              <Typography sx={{pl: 1}} variant="body1">
                                {t('Phone number')}
                              </Typography>
                              <Typography sx={{pl: 1}} variant="body2">
                                555-867-5309
                              </Typography>
                            </div>
                          </div>
                          <div className={classes.powershellRow}>
                            <div
                              className={clsx(
                                  classes.powershellColumn,
                                  classes.widthAuto,
                              )}
                            >
                              <Icon path={mdiEmailOutline} size={1} />
                            </div>
                            <div className={classes.powershellColumn}>
                              <Typography sx={{pl: 1}} variant="body1">
                                {t('Email')}
                              </Typography>
                              <Typography sx={{pl: 1}} variant="body2">
                                {t('email@email.com')}
                              </Typography>
                            </div>
                          </div>
                          <Typography variant="h6">VDL information</Typography>
                          <div className={classes.powershellRow}>
                            <div
                              className={clsx(
                                  classes.powershellColumn,
                                  classes.widthAuto,
                              )}
                            >
                              <Icon path={mdiMonitor} size={1} />
                            </div>
                            <div className={classes.powershellColumn}>
                              <Typography sx={{pl: 1}} variant="body1">
                                {t('Virtual machine name')}
                              </Typography>
                              <Typography sx={{pl: 1}} variant="body2">
                                STC-0412-ST
                              </Typography>
                            </div>
                          </div>
                          <div className={classes.powershellRow}>
                            <div
                              className={clsx(
                                  classes.powershellColumn,
                                  classes.widthAuto,
                              )}
                            >
                              <Icon path={mdiTranslate} size={1} />
                            </div>
                            <div className={classes.powershellColumn}>
                              <Typography sx={{pl: 1}} variant="body1">
                                {t('Virtual machine language')}
                              </Typography>
                              <Typography sx={{pl: 1}} variant="body2">
                                {t('English')}
                              </Typography>
                            </div>
                          </div>
                          <div className={classes.powershellRow}>
                            <div
                              className={clsx(
                                  classes.powershellColumn,
                                  classes.widthAuto,
                              )}
                            >
                              <Icon path={mdiHammerScrewdriver} size={1} />
                            </div>
                            <div className={classes.powershellColumn}>
                              <Typography sx={{pl: 1, mb: 1}} variant="body1">
                                {t('Required tools')}
                              </Typography>
                              <Typography sx={{pl: 1}} variant="body2">
                                {t('Default tools')}
                              </Typography>
                              <Typography sx={{pl: 1, mb: 1}} variant="body2">
                                {t(
                                    '(Adobe Reader DC, Java, LibreOffice, Office 2019, Power BI, ProjectLibre, Python, R, RStudio, RTools, VSCode)',
                                )}
                              </Typography>
                              <Typography sx={{pl: 1}} variant="body2">
                                {t('SAS')}
                              </Typography>
                              <Typography sx={{pl: 1}} variant="body2">
                                {t('(SAS 9.4, SAS Enterprise Guide)')}
                              </Typography>
                            </div>
                          </div>
                        </AccordionDetails>
                      </Accordion>
                    </div>
                  </>
                }
              />
            </Paper>
          )}
          <AddVirtualMachine
            open={state.addVirtualMachine}
            closeDrawer={() => handleClickClose('addVirtualMachine')}
            addVirtualMachine={addVirtualMachine}
          />
          <EditVirtualMachine
            open={state.editVirtualMachine}
            closeDrawer={() => handleClickClose('editVirtualMachine')}
            editVirtualMachine={editVirtualMachine}
          />
          <SnackbarAddVirtualMachine
            open={state.snackbarAddVirtualMachine}
            handleClose={() => handleClickClose('snackbarAddVirtualMachine')}
          />
          <SnackbarEditVirtualMachine
            open={state.snackbarEditVirtualMachine}
            handleClose={() => handleClickClose('snackbarEditVirtualMachine')}
          />
          <Button
            variant="outlined"
            color="primary"
            className={classes.addBtn}
            startIcon={<Icon path={mdiPlus} size={1} />}
            onClick={() => handleClickOpen('addVirtualMachine')}
          >
            {t('Add virtual machine details')}
          </Button>
          <DialogDelete
            submitDialog={deleteFile}
            open={state.dialogDelete}
            toggleDialog={() => handleClickClose('dialogDelete')}
          />
          <SnackbarDeleteVirtualMachine
            open={state.snackbarDelete}
            handleClose={() => handleClickClose('snackbarDelete')}
          />
        </Box>
      </Grid>
    </Root>
  );
}
export default VirtualMachine;
