import React from 'react';
import {styled} from '@mui/material/styles';
import Link from '@mui/material/Link';
import Alert from '@mui/material/Alert';
import RequestToolbar from './RequestToolbar';
import {Button, Typography, Paper, Container} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import Collapse from '@mui/material/Collapse';
import Header from './Header';
import Footer from '../../VettingApp/CommonComponents/Footer';
import {FOOT_H, HEAD_H_XS} from '../../../Theme/constants';

const PREFIX = 'SuccessfulSubmission';

const classes = {
  root: `${PREFIX}-root`,
  inputMargin: `${PREFIX}-inputMargin`,
  divider: `${PREFIX}-divider`,
  paper: `${PREFIX}-paper`,
  main: `${PREFIX}-main`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.root}`]: {
    '& .MuiFormControl-root': {
      width: '100%',
    },
  },

  [`& .${classes.inputMargin}`]: {
    margin: theme.spacing(1, 0),
  },

  [`& .${classes.divider}`]: {
    margin: theme.spacing(1, 0),
  },

  [`& .${classes.paper}`]: {
    maxWidth: '1280px',
    margin: 'auto',
    boxSizing: 'border-box',
    padding: theme.spacing(3),
    marginTop: theme.spacing(3),
    border: '1px solid',
    borderColor: theme.palette.divider,
  },

  [`& .${classes.main}`]: {
    minHeight: `calc(105vh - ${HEAD_H_XS}px - ${FOOT_H}px)`,
    background: theme.palette.grey[100],
    paddingBottom: theme.spacing(6),
  },
}));

function SuccessfulSubmission(props) {
  const [open, setOpen] = React.useState(true);

  return (
    <Root>
      <Header />
      <main className={classes.main} tabIndex="-1">
        <Container maxWidth={false}>
          <RequestToolbar />
          <Paper className={classes.paper}>
            <Collapse in={open} sx={{mb: 3}}>
              <Alert
                action={
                  <Button
                    aria-label="Close"
                    color="inherit"
                    onClick={() => {
                      setOpen(false);
                    }}
                  >
                    <CloseIcon fontSize="inherit" />
                  </Button>
                }
              >
                VDL PowerShell intake request ID-1234 submitted
              </Alert>
            </Collapse>
            <div>
              <Typography variant="body2" sx={{mb: 3}}>
                Your request to create a VDL workspace has been submitted and is
                being processed. You will receive an email once the VDL
                workspace is ready.
              </Typography>
              <Typography variant="body2" sx={{mb: 3}}>
                Your virtual machines will appear in the "DevTest Lab" named as
                "STC-0412-ST". Please be patient while waiting for your virtual
                machines to be ready. They may be visible with the status of
                "Running" but this does not mean they are ready for use.
              </Typography>
              <Typography variant="body2" sx={{mb: 3}}>
                If you encounter any issues, submit a JIRA ticket to the{' '}
                <Link
                  href="https://jirab.statcan.ca/projects/DAZSUPP/summary"
                  underline="always"
                >
                  VDL Jira project.
                </Link>{' '}
                for assistance.
              </Typography>
              <Typography variant="body2">Thank you!</Typography>
            </div>
          </Paper>
        </Container>
      </main>
      <Footer />
    </Root>
  );
}

export default SuccessfulSubmission;
