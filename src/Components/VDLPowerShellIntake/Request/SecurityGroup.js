import React from 'react';
import {styled} from '@mui/material/styles';
import PropTypes from 'prop-types';
import TextField from '@mui/material/TextField';
import useMediaQuery from '@mui/material/useMediaQuery';
import ListSubheader from '@mui/material/ListSubheader';
import {useTheme} from '@mui/material/styles';
import {VariableSizeList} from 'react-window';
import {Typography, Divider, Box} from '@mui/material';
import {VirtualizedAutocomplete} from '../../CommonComponents/VirtualizedAutocomplete';

const PREFIX = 'SecurityGroup';

const classes = {
  listbox: `${PREFIX}-listbox`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')({
  [`& .${classes.listbox}`]: {
    'boxSizing': 'border-box',
    '& ul': {
      padding: 0,
      margin: 0,
    },
  },
});

const LISTBOX_PADDING = 8; // px

function renderRow(props) {
  const {data, index, style} = props;
  return React.cloneElement(data[index], {
    style: {
      ...style,
      top: style.top + LISTBOX_PADDING,
    },
  });
}

const OuterElementContext = React.createContext({});

const OuterElementType = React.forwardRef((props, ref) => {
  const outerProps = React.useContext(OuterElementContext);
  return <div ref={ref} {...props} {...outerProps} />;
});

function useResetCache(data) {
  const ref = React.useRef(null);
  React.useEffect(() => {
    if (ref.current != null) {
      ref.current.resetAfterIndex(0, true);
    }
  }, [data]);
  return ref;
}

// Adapter for react-window
const ListboxComponent = React.forwardRef(function ListboxComponent(
    props,
    ref,
) {
  const {children, ...other} = props;
  const itemData = React.Children.toArray(children);
  const theme = useTheme();
  const smUp = useMediaQuery(theme.breakpoints.up('sm'), {noSsr: true});
  const itemCount = itemData.length;
  const itemSize = smUp ? 36 : 48;

  const getChildSize = (child) => {
    if (React.isValidElement(child) && child.type === ListSubheader) {
      return 48;
    }

    return itemSize;
  };

  const getHeight = () => {
    if (itemCount > 8) {
      return 8 * itemSize;
    }
    return itemData.map(getChildSize).reduce((a, b) => a + b, 0);
  };

  const gridRef = useResetCache(itemCount);

  return (
    <div ref={ref}>
      <OuterElementContext.Provider value={other}>
        <VariableSizeList
          itemData={itemData}
          height={getHeight() + 2 * LISTBOX_PADDING}
          ref={gridRef}
          outerElementType={OuterElementType}
          innerElementType="ul"
          itemSize={(index) => getChildSize(itemData[index])}
          overscanCount={5}
          itemCount={itemCount}
        >
          {renderRow}
        </VariableSizeList>
      </OuterElementContext.Provider>
    </div>
  );
});

ListboxComponent.propTypes = {
  children: PropTypes.node,
};

function random(length) {
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let result = '';

  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(Math.floor(Math.random() * characters.length));
  }

  return result;
}

const OPTIONS = Array.from(new Array(10000))
    .map(() => random(10 + Math.ceil(Math.random() * 20)))
    .sort((a, b) => a.toUpperCase().localeCompare(b.toUpperCase()));

const renderGroup = (params) => [
  <ListSubheader key={params.key} component="div">
    {params.group}
  </ListSubheader>,
  params.children,
];

export default function SecurityGroup() {
  return (
    <Root>
      <Box sx={{pb: 3, pt: 3}}>
        <Typography variant="h5" sx={{mb: 3}}>
          Security group
        </Typography>
        <Divider sx={{mb: 3}} />
        <Typography variant="body1" sx={{mb: 3}}>
          The security group information is included in the VRS project
          onboarding information. External users (non StatCan employees) do not
          automatically get an M365 license. The security group
          "VDL-M365Licensing" only gives users office desktop applications.
        </Typography>
        <VirtualizedAutocomplete
          id="securitygrouup"
          disableListWrap
          label="Security group"
          classes={classes}
          ListboxComponent={ListboxComponent}
          renderGroup={renderGroup}
          options={OPTIONS}
          groupBy={(option) => option[0].toUpperCase()}
          renderInput={(params) => (
            <TextField {...params} variant="outlined" label="Security group" />
          )}
          renderOption={(props, option) => (
            <li {...props}>
              <Typography noWrap>{option}</Typography>
            </li>
          )}
        />
      </Box>
    </Root>
  );
}
