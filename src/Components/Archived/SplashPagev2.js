import React from 'react';
import {styled} from '@mui/material/styles';
import {useTranslation} from 'react-i18next';
import {Link as RouterLink, withRouter} from 'react-router-dom';
import {Button, Divider, Grid, Paper} from '@mui/material';

import {XS_SCREEN} from '../../Theme/constants';
import CenteredFooter from '../Footers/CenteredFooter';

const PREFIX = 'SplashPagev2';

const classes = {
  container: `${PREFIX}-container`,
  splashDivider: `${PREFIX}-splashDivider`,
  splashLogo: `${PREFIX}-splashLogo`,
  splashPaper: `${PREFIX}-splashPaper`,
  splashWrapper: `${PREFIX}-splashWrapper`,
  splashContent: `${PREFIX}-splashContent`,
  splashBtns: `${PREFIX}-splashBtns`,
};

const StyledGrid = styled(Grid)((
    {
      theme,
    },
) => ({
  [`& .${classes.container}`]: {
    margin: theme.spacing(0, 1),
  },

  [`& .${classes.splashDivider}`]: {
    marginTop: theme.spacing(3),
  },

  [`& .${classes.splashLogo}`]: {
    height: '24px',
    [theme.breakpoints.down('sm')]: {
      height: '20px',
    },
  },

  [`& .${classes.splashPaper}`]: {
    marginTop: theme.spacing(6),
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingLeft: theme.spacing(5),
    paddingRight: theme.spacing(5),
    width: '560px',
    boxSizing: 'border-box',
    padding: theme.spacing(6),
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      marginTop: theme.spacing(0),
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(3),
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
      boxShadow: 'none',
    },
  },

  [`&.${classes.splashWrapper}`]: {
    background: theme.palette.grey[100],
    display: 'table',
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      justifyContent: 'flex-start',
      background: theme.palette.common.white,
    },
  },

  [`& .${classes.splashContent}`]: {
    display: 'table-cell',
    verticalAlign: 'middle',
  },

  [`& .${classes.splashBtns}`]: {
    'marginTop': theme.spacing(3),
    'width': '48%',
    '& > .MuiButton-root': {
      width: '100%',
    },
  },
}));

function SplashPage(props) {
  const {t} = useTranslation();

  const [state, setState] = React.useState({
    windowWidth: window.innerWidth,
  });

  const isXsScreen = state.windowWidth < XS_SCREEN;

  React.useEffect(() => {
    document.title = t('DAaaS');
    // Detect screen size
    const handleResize = () =>
      setState({...state, windowWidth: window.innerWidth});
    window.addEventListener('resize', handleResize);
    window.addEventListener('orientationchange', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
      window.removeEventListener('orientationchange', handleResize);
    };
  }, [state, t]);

  return (
    <StyledGrid
      container
      spacing={0}
      direction="column"
      className={classes.splashWrapper}
    >
      <Grid item className={classes.splashContent}>
        <Paper className={classes.splashPaper} elevation={3}>
          <div className={classes.container}>
            <img
              src={process.env.PUBLIC_URL + '/images/sig-blk-en.svg'}
              alt=""
              className={classes.splashLogo}
            />
            <span className="screen-reader-text">
              Government of Canada /{' '}
              <span lang="fr">Gouvernement du Canada</span>
            </span>
          </div>
          <Divider className={classes.splashDivider} />
          <div className={classes.container}>
            <Grid container justifyContent="space-between" alignItems="center">
              <Grid item className={classes.splashBtns}>
                <Button
                  variant="contained"
                  color="primary"
                  component={RouterLink}
                  to="/home"
                >
                  English
                </Button>
              </Grid>
              <Grid item className={classes.splashBtns}>
                <Button
                  variant="contained"
                  color="primary"
                  component={RouterLink}
                  to="/home"
                >
                  Français
                </Button>
              </Grid>
            </Grid>
          </div>
          {isXsScreen && <Divider className={classes.splashDivider} />}
        </Paper>
        <CenteredFooter />
      </Grid>
    </StyledGrid>
  );
}

export default withRouter(SplashPage);
