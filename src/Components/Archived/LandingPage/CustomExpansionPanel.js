import React from 'react';
import {styled} from '@mui/material/styles';
import {
  Typography,
  Accordion,
  AccordionDetails,
  AccordionSummary,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

const PREFIX = 'CustomExpansionPanel';

const classes = {
  root: `${PREFIX}-root`,
  summary: `${PREFIX}-summary`,
  expanded: `${PREFIX}-expanded`,
  details: `${PREFIX}-details`,
};

const StyledAccordion = styled(Accordion)((
    {
      theme,
    },
) => ({
  [`&.${classes.root}`]: {
    '&::before': {
      display: 'none',
    },
    'marginBottom': theme.spacing(3),
    'backgroundColor': 'transparent',
    'boxShadow': 'none',
  },

  [`& .${classes.summary}`]: {
    'borderWidth': '1px',
    'borderStyle': 'solid',
    'borderColor': theme.palette.grey[400],
    'borderRadius': '1.5em',
    '&.Mui-expanded': {
      backgroundColor: theme.palette.common.white,
      minHeight: 0,
      border: 'transparent',
      boxShadow: theme.shadows[5],
    },
  },

  [`& .${classes.expanded}`]: {
    color: theme.palette.primary.main,
  },

  [`& .${classes.details}`]: {
    padding: theme.spacing(2, 4, 2, 4),
  },
}));

export default function CustomExpansionPanel(props) {
  return (
    <StyledAccordion
      className={classes.root}
      expanded={props.expanded}
      onChange={props.onChange}
    >
      <AccordionSummary
        classes={{
          root: classes.summary,
          expanded: classes.expanded,
        }}
        expandIcon={<ExpandMoreIcon />}
        aria-controls={`${props.id}-content`}
        id={`${props.id}-header`}
      >
        <Typography component="h3">{props.summary}</Typography>
      </AccordionSummary>
      <AccordionDetails className={classes.details}>
        <Typography>{props.details}</Typography>
      </AccordionDetails>
    </StyledAccordion>
  );
}
