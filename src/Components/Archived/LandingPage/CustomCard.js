import React from 'react';
import {styled} from '@mui/material/styles';
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
} from '@mui/material';
import {alpha} from '@mui/material/styles';

const PREFIX = 'CustomCard';

const classes = {
  cardAction: `${PREFIX}-cardAction`,
  cardContent: `${PREFIX}-cardContent`,
  cardMedia: `${PREFIX}-cardMedia`,
};

const StyledCard = styled(Card)((
    {
      theme,
    },
) => ({
  [`& .${classes.cardAction}`]: {
    'height': 'inherit',
    '&:hover .MuiCardActionArea-focusHighlight': {
      opacity: 0.1,
    },
  },

  [`& .${classes.cardContent}`]: {
    backgroundColor: alpha(theme.palette.common.white, 0.75),
    boxSizing: 'border-box',
    width: '100%',
    position: 'absolute',
    top: '100%',
    left: 0,
    transform: 'translateY(-100%)',
  },

  [`& .${classes.cardMedia}`]: {
    display: 'block',
    position: 'relative',
    height: '100%',
  },
}));

export default function CustomExpansionPanel(props) {
  return (
    <StyledCard className={props.className}>
      <CardActionArea
        className={classes.cardAction}
        href={props.href}
      >
        <CardMedia
          className={classes.cardMedia}
          component="img"
          alt=""
          image={props.img}
        />
        <CardContent className={classes.cardContent}>
          {props.content}
        </CardContent>
      </CardActionArea>
    </StyledCard>
  );
}
