import {Container, Grid, Typography} from '@mui/material';
import {styled} from '@mui/material/styles';
import React from 'react';

import SearchBar from '../../SearchBar';

const PREFIX = 'Search';

const classes = {
  searchBar: `${PREFIX}-searchBar`,
  section: `${PREFIX}-section`,
  textCenter: `${PREFIX}-textCenter`,
};

const StyledContainer = styled(Container)((
    {
      theme,
    },
) => ({
  [`& .${classes.searchBar}`]: {
    marginBottom: theme.spacing(4),
  },

  [`& .${classes.section}`]: {
    paddingTop: theme.spacing(20),
    paddingBottom: theme.spacing(12),
  },

  [`& .${classes.textCenter}`]: {
    textAlign: 'center',
  },
}));

export default function Search() {
  return (
    <StyledContainer className={`${classes.section} ${classes.textCenter}`}>
      <Typography variant="h1" gutterBottom>Start searching</Typography>
      <Grid container justifyContent="center">
        <Grid item xs={12} sm={10} md={8} className={classes.searchBar}>
          <SearchBar label="Search" placeholder="Start searching"/>
        </Grid>
      </Grid>
      <Grid container justifyContent="center">
        <Grid item xs={12} sm={8} md={6}>
          <Typography variant="body1">
            Your trusted source for Government of Canada data in collaboration
            with provincial and academic sources.
          </Typography>
        </Grid>
      </Grid>
    </StyledContainer>
  );
}
