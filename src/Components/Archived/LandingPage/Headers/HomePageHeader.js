import {AppBar, Button, IconButton, Toolbar} from '@mui/material';
import {styled} from '@mui/material/styles';
import SearchIcon from '@mui/icons-material/Search';
import React from 'react';
import {Link as RouterLink} from 'react-router-dom';

import * as constants from '../../../../Theme/constants';
import Branding from './CommonComponents/Branding';
import ElevationScroll from './CommonComponents/ElevationScroll';
import Language from './CommonComponents/Language';
import NavMenu from './CommonComponents/NavMenu';

const PREFIX = 'HomePageHeader';

const classes = {
  accountOptions: `${PREFIX}-accountOptions`,
  actionButtons: `${PREFIX}-actionButtons`,
  appBar: `${PREFIX}-appBar`,
  branding: `${PREFIX}-branding`,
  nav: `${PREFIX}-nav`,
  navMenu: `${PREFIX}-navMenu`,
  lang: `${PREFIX}-lang`,
  search: `${PREFIX}-search`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.accountOptions}`]: {},

  [`& .${classes.actionButtons}`]: {
    width: '30%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    [theme.breakpoints.down('lg')]: {
      width: 'auto',
      flexGrow: 1,
    },
  },

  [`& .${classes.appBar}`]: {
    'backgroundColor': theme.palette.common.white,
    'color': theme.palette.text.primary,
    '& .MuiToolbar-root': {
      display: 'flex',
      justifyContent: 'space-between',
    },
    '& #search-btn': {
      visibility: 'hidden',
    },
    '&.MuiPaper-elevation4 #search-btn': {
      visibility: 'visible',
    },
    [theme.breakpoints.down('lg')]: {
      '& .MuiToolbar-root': {
        justifyContent: 'flex-start',
      },
    },
  },

  [`& .${classes.branding}`]: {
    'width': '30%',
    '& img': {
      height: theme.spacing(3),
      [theme.breakpoints.down('sm')]: {
        height: theme.spacing(2.5),
        marginTop: theme.spacing(1),
      },
    },
    [theme.breakpoints.down('lg')]: {
      width: 'auto',
    },
  },

  [`& .${classes.nav}`]: {
    'textAlign': 'center',
    'padding': 0,
    'height': theme.spacing(8),
    'display': 'flex',
    'justifyContent': 'center',
    'alignItems': 'center',
    '& li': {
      display: 'inline-block',
    },
    '& a': {
      margin: theme.spacing(0, 2, 0, 2),
    },
  },

  [`& .${classes.navMenu}`]: {},

  [`& .${classes.lang}`]: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.down('sm')]: {
      '& button': {
        paddingRight: 0,
      },
    },
  },

  [`& .${classes.search}`]: {},
}));

export default function HomePageHeader(props) {
  const [state, setState] = React.useState({
    windowWidth: window.innerWidth,
  });
  const isMdScreen = state.windowWidth < constants.MD_SCREEN;

  React.useEffect(() => {
    const handleResize = () =>
      setState({...state, windowWidth: window.innerWidth});
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  });

  return (
    <Root>
      <ElevationScroll {...props}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            {isMdScreen && (
              <nav className={classes.navMenu}>
                <NavMenu />
              </nav>
            )}
            <div className={classes.branding}>
              <Branding />
            </div>
            {!isMdScreen && (
              <nav aria-labelledby="mainmenulabel">
                <span id="mainmenulabel" className="screen-reader-text">
                  Main menu
                </span>
                <ul className={classes.nav}>
                  <li>
                    <Button href="https://www150.statcan.gc.ca/n1/en/type/data?subject_levels=25">
                      Datasets
                    </Button>
                  </li>
                  <li>
                    <Button href="https://www.statcan.gc.ca/eng/interact/datavis">
                      Visualizations
                    </Button>
                  </li>
                  <li>
                    <Button href="#">Community</Button>
                  </li>
                  <li>
                    <Button href="#">Services</Button>
                  </li>
                  <li>
                    <Button href="#">Partners</Button>
                  </li>
                </ul>
              </nav>
            )}
            <div className={classes.actionButtons}>
              <div className={classes.search}>
                <IconButton id="search-btn" color="primary" size="large">
                  <SearchIcon />
                  <span className="screen-reader-text">Search</span>
                </IconButton>
              </div>
              <div className={classes.lang}>
                <Language />
              </div>
              <div className={classes.accountOptions}>
                <Button
                  component={RouterLink}
                  to="/sign-in"
                  variant="outlined"
                  color="primary"
                >
                  Sign&nbsp;in
                </Button>
              </div>
            </div>
          </Toolbar>
        </AppBar>
      </ElevationScroll>
    </Root>
  );
}
