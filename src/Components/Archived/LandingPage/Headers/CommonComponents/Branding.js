import React from 'react';
import {styled} from '@mui/material/styles';
const PREFIX = 'Branding';

const classes = {
  brandImage: `${PREFIX}-brandImage`,
  brandLink: `${PREFIX}-brandLink`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.brandImage}`]: {
    margin: theme.spacing(1.5),
  },

  [`& .${classes.brandLink}`]: {
    '&:focus img': {
      border: '2px solid #0049b3',
      borderRadius: '2px',
    },
  },
}));

export default function Branding() {
  return (
    <Root>
      <a href="https://www.canada.ca/en.html" className={classes.brandLink}>
        <img
          src={process.env.PUBLIC_URL + '/images/sig-blk-en.svg'}
          alt=""
          className={classes.brandImage}
        />
        <span className="screen-reader-text">
          Government of Canada / <span lang="fr">Gouvernement du Canada</span>
        </span>
      </a>
    </Root>
  );
}
