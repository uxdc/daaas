import React from 'react';
import {styled} from '@mui/material/styles';
import {useTranslation} from 'react-i18next';
// import DateFnsUtils from '@date-io/date-fns';
import {
  Button,
  Accordion,
  AccordionDetails,
  AccordionSummary,
  FormControl,
  FormLabel,
  Paper,
  Typography,
  Drawer,
  IconButton,
  TextField,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Icon from '@mdi/react';
import {mdiTune, mdiClose} from '@mdi/js';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import {DatePicker, LocalizationProvider} from '@mui/lab';
import {SM_SCREEN} from '../../../Theme/constants';

import {subjects, surveys} from '../../../Data/fakeData';
import StickySearchableDropdown from '../StickySearchableDropdown';

const PREFIX = 'Filters';

const classes = {
  root: `${PREFIX}-root`,
  filterHeader: `${PREFIX}-filterHeader`,
  closeBtn: `${PREFIX}-closeBtn`,
  dateDetails: `${PREFIX}-dateDetails`,
  datePicker: `${PREFIX}-datePicker`,
  drawer: `${PREFIX}-drawer`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.root}`]: {
    boxShadow: 'none',
    borderRightWidth: '1px',
    borderRightStyle: 'solid',
    borderRightColor: theme.palette.divider,
    height: '100%',
    boxSizing: 'border-box',
    padding: theme.spacing(3, 2, 0, 0),
  },

  [`& .${classes.filterHeader}`]: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 0, 3, 1),
    borderBottomWidth: '1px',
    borderBottomStyle: 'solid',
    borderBottomColor: theme.palette.divider,
  },

  [`& .${classes.closeBtn}`]: {
    flexGrow: 1,
    textAlign: 'right',
  },

  [`& .${classes.dateDetails}`]: {
    flexDirection: 'column',
  },

  [`& .${classes.datePicker}`]: {
    margin: theme.spacing(0, 0, 3, 0),
  },

  [`& .${classes.drawer}`]: {
    '&>.MuiPaper-root': {
      padding: theme.spacing(3, 2, 2, 2),
    },
  },
}));

const Filters = React.forwardRef((props, ref) => {
  const [state, setState] = React.useState({
    windowWidth: window.innerWidth,
  });

  const isSmScreen = state.windowWidth < SM_SCREEN;

  React.useEffect(() => {
    // Detect screen size
    const handleResize = () =>
      setState({...state, windowWidth: window.innerWidth});
    window.addEventListener('resize', handleResize);
    window.addEventListener('orientationchange', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
      window.removeEventListener('orientationchange', handleResize);
    };
  }, [state]);

  return (
    <Root>
      {isSmScreen ? (
        <Drawer
          anchor="left"
          open={props.drawer}
          onClose={props.toggleDrawer(false)}
          className={classes.drawer}
        >
          <FiltersContent
            ref={ref}
            toggleDrawer={props.toggleDrawer}
            isSmScreen={props.isSmScreen}
          />
        </Drawer>
      ) : (
        <Paper className={classes.root}>
          <FiltersContent
            ref={ref}
            toggleDrawer={props.toggleDrawer}
            isSmScreen={props.isSmScreen}
          />
        </Paper>
      )}
    </Root>
  );
});

const FiltersContent = React.forwardRef((props, ref) => {
  const {t} = useTranslation();

  const [state, setState] = React.useState({
    selectedFromDate: null,
    selectedToDate: null,
    selectedDateType: 10,
    checked: [],
  });

  const handleFromDateChange = (date) => {
    setState({...state, selectedFromDate: date});
  };

  const handleToDateChange = (date) => {
    setState({...state, selectedToDate: date});
  };

  const handleClick = () => {
    ref.current.focus();
  };

  return (
    <React.Fragment>
      {!props.isSmScreen && (
        <Button className="screen-reader-text" onClick={handleClick}>
          {t('Skip filters')}
        </Button>
      )}
      <div className={classes.filterHeader}>
        <Icon path={mdiTune} size={1} />
        <Typography variant="h6" component="h2" sx={{ml: 2}}>
          {t('Filters')}
        </Typography>
        {props.isSmScreen && (
          <div className={classes.closeBtn}>
            <IconButton
              onClick={props.toggleDrawer(false)}
              id="filters-close"
              aria-label="Filters panel - close panel"
              size="large"
            >
              <Icon path={mdiClose} size={1} />
            </IconButton>
          </div>
        )}
      </div>
      <StickySearchableDropdown
        id="subjects"
        defaultExpanded={false}
        filterable={true}
        summary={t('Subjects')}
        placeholder={t('Search subjects')}
        list={subjects}
        legendHidden={true}
      />
      <StickySearchableDropdown
        id="sources"
        defaultExpanded={false}
        filterable={true}
        summary={t('Sources')}
        placeholder={t('Search sources')}
        list={surveys}
        legendHidden={true}
      />
      <Accordion>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="subtitle2" component="h3">
            Date
          </Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.dateDetails}>
          <FormControl component="fieldset">
            <FormLabel component="legend" className="screen-reader-text">
              {t('Date')}
            </FormLabel>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                id="start-date"
                className={classes.datePicker}
                // variant="inline"
                // format="MM/dd/yyyy"
                value={state.selectedFromDate}
                maxDate={state.selectedToDate}
                onChange={handleFromDateChange}
                renderInput={(props) => (
                  <TextField
                    margin="dense"
                    label={t('Start date')}
                    variant="outlined"
                  />
                )}
                // PopoverProps={{
                //   'aria-modal': 'true',
                // }}
                OpenPickerButtonProps={{
                  'aria-label': t('select start date'),
                }}
              />
              <DatePicker
                id="end-date"
                // variant="inline"
                // format="MM/dd/yyyy"
                value={state.selectedToDate}
                minDate={state.selectedFromDate}
                onChange={handleToDateChange}
                renderInput={(props) => (
                  <TextField
                    margin="dense"
                    label={t('End date')}
                    variant="outlined"
                  />
                )}
                OpenPickerButtonProps={{
                  'aria-label': t('select end date'),
                }}
              />
            </LocalizationProvider>
          </FormControl>
        </AccordionDetails>
      </Accordion>
    </React.Fragment>
  );
});

export default Filters;
