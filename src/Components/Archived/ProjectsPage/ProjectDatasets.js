import React from 'react';
import {styled} from '@mui/material/styles';
import {useTranslation} from 'react-i18next';
import Icon from '@mdi/react';
import {mdiFileTableOutline} from '@mdi/js';
import {
  Typography,
  IconButton,
  Drawer,
  Divider,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import {HEAD_H} from '../../../Theme/constants';
import {DRAWER_WIDTH} from './index';

const PREFIX = 'ProjectDatasets';

const classes = {
  drawer: `${PREFIX}-drawer`,
  drawerPaper: `${PREFIX}-drawerPaper`,
  drawerHeader: `${PREFIX}-drawerHeader`,
  drawerSection: `${PREFIX}-drawerSection`,
  drawerSectionIndex: `${PREFIX}-drawerSectionIndex`,
  closeBtn: `${PREFIX}-closeBtn`,
};

const StyledDrawer = styled(Drawer)(({theme}) => ({
  [`&.${classes.drawer}`]: {
    boxSizing: 'border-box',
    width: DRAWER_WIDTH,
    flexShrink: 0,
  },

  [`& .${classes.drawerPaper}`]: {
    boxSizing: 'border-box',
    width: DRAWER_WIDTH,
    height: `calc(100vh - 88px - ${HEAD_H}px)`,
    marginTop: `calc(88px + ${HEAD_H}px)`,
    zIndex: 1000,
  },

  [`& .${classes.drawerHeader}`]: {
    'display': 'flex',
    'alignItems': 'center',
    'zIndex': 10,
    'height': '4rem',
    'minHeight': '4rem',
    'position': 'sticky',
    'top': 0,
    'background': 'white',
    'borderBottom': '1px solid rgba(0, 0, 0, 0.12)',
    '& > svg': {
      marginRight: theme.spacing(2),
    },
    'padding': theme.spacing(0, 2),
  },

  [`& .${classes.drawerSection}`]: {
    padding: theme.spacing(3, 2, 0, 2),
  },

  [`& .${classes.drawerSectionIndex}`]: {
    zIndex: -10,
  },

  [`& .${classes.closeBtn}`]: {
    flexGrow: 1,
    textAlign: 'right',
  },
}));

export default function ProjectDatasets(props) {
  const {t} = useTranslation();

  return (
    <StyledDrawer
      className={classes.drawer}
      variant="persistent"
      anchor="right"
      open={props.open}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <div className={classes.drawerHeader}>
        <IconButton
          onClick={props.toggleDatasetsDrawer}
          edge="start"
          id="datasets-back-btn"
          aria-label={t('Datasets panel - Go back to details')}
          size="large"
        >
          <ArrowBackIcon />
        </IconButton>
        <Typography component="h2" variant="h6">
          {t('Datasets')}
        </Typography>
        <div className={classes.closeBtn}>
          <IconButton
            onClick={props.toggleAllDrawers}
            edge="end"
            aria-label={t('Datasets panel - close all panels')}
            size="large"
          >
            <CloseIcon />
          </IconButton>
        </div>
      </div>
      <Divider />
      <div className={classes.drawerSection}>
        <Typography variant="body1">{t('Dataset list')}</Typography>
      </div>
      <div className={classes.drawerSectionIndex}>
        <List dense={true}>
          <ListItem>
            <ListItemIcon>
              <Icon path={mdiFileTableOutline} size={1} />
            </ListItemIcon>
            <ListItemText primary="DatasetOne" />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <Icon path={mdiFileTableOutline} size={1} />
            </ListItemIcon>
            <ListItemText primary="DatasetTwo" />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <Icon path={mdiFileTableOutline} size={1} />
            </ListItemIcon>
            <ListItemText primary="DatasetThree" />
          </ListItem>
        </List>
      </div>
    </StyledDrawer>
  );
}
