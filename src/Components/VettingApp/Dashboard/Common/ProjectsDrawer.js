import React from 'react';
import {styled} from '@mui/material/styles';
import {useTranslation} from 'react-i18next';
import PropTypes from 'prop-types';
import {Typography, Drawer, Tooltip} from '@mui/material';
import TreeView from '@mui/lab/TreeView';
import TreeItem from '@mui/lab/TreeItem';
import FolderOpenIcon from '@mui/icons-material/FolderOpen';
import {
  ThemeProvider,
  StyledEngineProvider,
  alpha,
} from '@mui/material/styles';

const PREFIX = 'ProjectsDrawer';

const classes = {
  drawer: `${PREFIX}-drawer`,
  drawerPaper: `${PREFIX}-drawerPaper`,
  drawerHeader: `${PREFIX}-drawerHeader`,
  drawerSection: `${PREFIX}-drawerSection`,
  drawerSectionIndex: `${PREFIX}-drawerSectionIndex`,
  closeBtn: `${PREFIX}-closeBtn`,
  listIcon: `${PREFIX}-listIcon`,
  root: `${PREFIX}-root`,
  content: `${PREFIX}-content`,
  group: `${PREFIX}-group`,
  expanded: `${PREFIX}-expanded`,
  selected: `${PREFIX}-selected`,
  label: `${PREFIX}-label`,
  labelRoot: `${PREFIX}-labelRoot`,
  labelIcon: `${PREFIX}-labelIcon`,
  labelText: `${PREFIX}-labelText`,
};

const StyledDrawer = styled(Drawer)(({theme}) => ({
  [`&.${classes.drawer}`]: {
    boxSizing: 'border-box',
    width: DRAWER_WIDTH,
    flexShrink: 0,
    zIndex: '10 !important',
  },

  [`& .${classes.drawerPaper}`]: {
    boxSizing: 'border-box',
    width: DRAWER_WIDTH,
    top: 'auto',
    paddingBottom: theme.spacing(2),
    paddingRight: theme.spacing(1.25),
    zIndex: '10 !important',
    overflowX: 'hidden',
  },

  [`& .${classes.drawerHeader}`]: {
    'display': 'flex',
    'alignItems': 'center',
    'zIndex': 10,
    'position': 'sticky',
    'top': 0,
    'background': 'white',
    '& > svg': {
      marginRight: theme.spacing(2),
    },
    'padding': theme.spacing(3, 0, 2, 3),
  },

  [`& .${classes.drawerSection}`]: {
    padding: theme.spacing(3, 2, 0, 2),
    flexShrink: 0,
  },

  [`& .${classes.drawerSectionIndex}`]: {
    zIndex: -10,
  },

  [`& .${classes.closeBtn}`]: {
    flexGrow: 1,
    textAlign: 'right',
  },

  [`& .${classes.listIcon}`]: {
    color: theme.palette.common.black,
  },

  [`& .${classes.root}`]: {
    'color': theme.palette.text.secondary,
    '&:hover > .MuiTreeItem-content': {
      backgroundColor: theme.palette.action.hover,
    },
    '&:focus > .MuiTreeItem-content, & > .Mui-selected.MuiTreeItem-content': {
      backgroundColor: alpha(theme.palette.primary.main, 0.25),
      color: theme.palette.primary.main,
    },
    '&:focus > .MuiTreeItem-content .MuiTreeItem-label, &:hover > .MuiTreeItem-content .MuiTreeItem-label, & > .Mui-selected.MuiTreeItem-content .MuiTreeItem-label': {
      backgroundColor: 'transparent',
    },
  },

  [`& .${classes.content}`]: {
    'padding': 0,
    'color': theme.palette.text.secondary,
    'borderTopRightRadius': theme.spacing(2),
    'borderBottomRightRadius': theme.spacing(2),
    'fontWeight': theme.typography.fontWeightMedium,
    '.Mui-expanded > &': {
      fontWeight: theme.typography.fontWeightRegular,
    },
    '&>.MuiTreeItem-iconContainer': {
      width: 0,
      margin: 0,
    },
  },

  [`& .${classes.group}`]: {
    'marginLeft': 0,
    '& .MuiTreeItem-content': {
      paddingLeft: theme.spacing(2),
    },
  },
  [`& .${classes.label}`]: {
    fontWeight: 'inherit !important',
    color: 'inherit',
    borderTopRightRadius: theme.spacing(2),
    borderBottomRightRadius: theme.spacing(2),
    paddingLeft: 0,
  },

  [`& .${classes.labelRoot}`]: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0.5, 0, 0.5, 0.5),
    paddingLeft: 0,
  },

  [`& .${classes.labelIcon}`]: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(2),
  },

  [`& .${classes.labelText}`]: {
    fontWeight: 'inherit',
    flexGrow: 1,
    maxWidth: '150px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
}));

export const DRAWER_WIDTH = 240;

function StyledTreeItem(props) {
  const {
    labelText,
    labelIcon: LabelIcon,
    labelInfo,
    tooltip,
    color,
    bgColor,
    ...other
  } = props;

  return (
    <TreeItem
      label={
        <StyledEngineProvider injectFirst>
          <ThemeProvider>
            <Tooltip title={labelText} placement="top" arrow>
              <div className={classes.labelRoot}>
                <LabelIcon color="inherit" className={classes.labelIcon} />
                <Typography variant="body2" className={classes.labelText}>
                  {labelText}
                </Typography>
                <Typography variant="caption" color="inherit"></Typography>
              </div>
            </Tooltip>
          </ThemeProvider>
        </StyledEngineProvider>
      }
      style={{
        '--tree-view-color': color,
        '--tree-view-bg-color': bgColor,
      }}
      classes={{
        root: classes.root,
        content: classes.content,
        expanded: classes.expanded,
        selected: classes.selected,
        group: classes.group,
        label: classes.label,
      }}
      {...other}
    />
  );
}

StyledTreeItem.propTypes = {
  bgColor: PropTypes.string,
  color: PropTypes.string,
  labelIcon: PropTypes.elementType.isRequired,
  labelInfo: PropTypes.string,
  labelText: PropTypes.string.isRequired,
};

export default function ProjectsDrawer(props) {
  const {role, open, projectTitle} = props;
  const {t} = useTranslation();

  const projectsArray = () => {
    if (role === 'researcher') {
      return ['20-SSH-UTO-1111', '20-SSH-UTO-1112', '20-SSH-UTO-1113'];
    } else {
      return [
        'All projects',
        '20-SSH-UTO-1111',
        '20-SSH-UTO-1112',
        '20-SSH-UTO-1113',
      ];
    }
  };

  function handleProjectChange(e, node) {
    const num = parseInt(node) + 1;
    const title = document.querySelector(
        '#projects-list li:nth-child(' + num + ') p',
    ).innerHTML;

    projectTitle(title);
  }

  return (
    <StyledDrawer
      className={classes.drawer}
      variant="persistent"
      anchor="left"
      open={open}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <div className={classes.drawerHeader}>
        <Typography variant="subtitle2" component="h2">
          {t('Projects')}
        </Typography>
      </div>
      <TreeView
        id="projects-list"
        className={classes.root}
        defaultSelected={['0']}
        onNodeSelect={handleProjectChange}
      >
        {projectsArray().map((el, index) => (
          <StyledTreeItem
            nodeId={`${index}`}
            key={`${index}`}
            labelText={el}
            labelIcon={FolderOpenIcon}
            tooltip={t(el)}
          />
        ))}
      </TreeView>
    </StyledDrawer>
  );
}
