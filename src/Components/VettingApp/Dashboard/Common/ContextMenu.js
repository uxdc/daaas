import React from 'react';
import {useTranslation} from 'react-i18next';
import {Typography, IconButton} from '@mui/material';
import MenuItem from '@mui/material/MenuItem';
import ListItemText from '@mui/material/ListItemText';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import {Menu} from '../../../CommonComponents/Menu';

import {
  SnackbarDeleteRequest,
  SnackbarReactivateRequest,
  SnackbarSubmitRequest,
  SnackbarAssignLead,
  SnackbarAssignSupport,
  SnackbarUnassign,
} from '../../CommonComponents/Snackbars';
import {
  DialogUpdate,
  DialogDenied,
  DialogApprove,
  DialogAssigneeDetails,
  DialogRequesterDetails,
  DialogNoLead,
  DialogAssignAsLead,
  DialogAssignAsSupport,
  DialogDelete,
  DialogWithdraw,
} from '../../CommonComponents/DialogBox';
import {loggedInUser} from '../../../../Data/fakeData';

// //////////////////////////// Dashboard actions menu
export function ActionsMenu(props) {
  const {
    role,
    statusHead,
    status,
    contextStatusClick,
    contextSummaryClick,
    toggleManageTeamDrawer,
    controls,
    request,
  } = props;
  const {t} = useTranslation();
  const currentUser = loggedInUser();
  let MenuVar;

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [open, setOpen] = React.useState({
    snackbarReopen: false,
    snackbarSubmit: false,
    snackbarDelete: false,
    snackbarAssignLead: false,
    snackbarAssignSupport: false,
    snackbarUnassign: false,
    dialogAssign: false,
    dialogUpdate: false,
    dialogDenied: false,
    dialogApprove: false,
    dialogAssigneeDetails: false,
    dialogRequesterDetails: false,
    dialogNoLeadUnassign: false,
    dialogNoLeadAssignSupport: false,
    dialogAssignAsLead: false,
    dialogAssignAsSupport: false,
    dialogDelete: false,
    dialogWithdraw: false,
  });

  const ariaControls = `actions-menu-${controls}`;

  const handleClick = (e) => {
    setAnchorEl(e.currentTarget);
    contextStatusClick(status);
  };

  const handleClose = (e) => {
    setAnchorEl(null);
  };

  function handleBlur(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setAnchorEl(null);
    }
  }

  const handleSnackbarOpen = (state) => {
    setOpen({...open, [state]: true});
  };

  const handleSnackbarClose = (state) => {
    setOpen({...open, [state]: false});
  };

  const toggleSummary = (e) => {
    handleClose(e);
    contextSummaryClick();
  };
  const contextManageTeam = (e) => {
    handleClose(e);
    toggleManageTeamDrawer(e);
  };

  const toggleDialog = (state, value, e) => {
    setOpen({...open, [state]: value});
    handleClose(e);
  };

  const unassignLead = (e) => {
    setOpen({
      ...open,
      dialogNoLeadUnassign: !open.dialogNoLeadUnassign,
      snackbarUnassign: true,
    });
  };

  const assignSupportNoLead = (e) => {
    setOpen({
      ...open,
      dialogNoLeadAssignSupport: !open.dialogNoLeadAssignSupport,
      snackbarAssignSupport: true,
    });
  };

  const deleteRequest = (e) => {
    e.stopPropagation();
    setOpen({...open, dialogDelete: false, snackbarDelete: true});
  };

  const viewRequestMenuItem = () => {
    return (
      <MenuItem onClick={handleClose} key="0">
        <ListItemText
          primary={<Typography variant="body2">{t('View request')}</Typography>}
        />
      </MenuItem>
    );
  };

  const editMenuItem = () => {
    return (
      <MenuItem onClick={handleClose} key="1">
        <ListItemText
          primary={<Typography variant="body2">{t('Edit')}</Typography>}
        />
      </MenuItem>
    );
  };

  const submitMenuItem = (e) => {
    return (
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          handleClose(e);
          handleSnackbarOpen('snackbarSubmit');
        }}
        key="2"
      >
        <ListItemText
          primary={<Typography variant="body2">{t('Submit')}</Typography>}
        />
      </MenuItem>
    );
  };

  const withdrawMenuItem = (e) => {
    return (
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          handleClose(e);
          toggleDialog('dialogWithdraw', !open.dialogWithdraw, e);
        }}
        key="3"
      >
        <ListItemText
          primary={<Typography variant="body2">{t('Withdraw')}</Typography>}
        />
      </MenuItem>
    );
  };

  const assigneeDetailsMenuItem = () => {
    return (
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          toggleDialog('dialogAssigneeDetails', !open.dialogAssigneeDetails, e);
        }}
        key="4"
      >
        <ListItemText
          primary={
            <Typography variant="body2">{t('Assignee details')}</Typography>
          }
        />
      </MenuItem>
    );
  };

  const requesterDetailsMenuItem = () => {
    return (
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          toggleDialog(
              'dialogRequesterDetails',
              !open.dialogRequesterDetails,
              e,
          );
        }}
        key="5"
      >
        <ListItemText
          primary={
            <Typography variant="body2">{t('Requester details')}</Typography>
          }
        />
      </MenuItem>
    );
  };

  const deleteMenuItem = () => {
    return (
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          toggleDialog('dialogDelete', !open.dialogDelete, e);
        }}
        key="6"
      >
        <ListItemText
          primary={<Typography variant="body2">{t('Delete')}</Typography>}
        />
      </MenuItem>
    );
  };

  const assignAsLeadMenuItem = () => {
    const supports = request.support.includes(currentUser);

    if (supports) {
      // if logged in user IS assigned as support
      return (
        <MenuItem
          onClick={(e) => {
            e.stopPropagation();
            handleClose(e);
            handleSnackbarOpen('snackbarAssignLead');
          }}
          key="7"
        >
          <ListItemText
            primary={
              <Typography variant="body2">{t('Assign me as lead')}</Typography>
            }
          />
        </MenuItem>
      );
    } else if (currentUser !== request.lead) {
      // if logged in user is NOT assigned as lead OR support
      return (
        <MenuItem
          onClick={(e) => {
            e.stopPropagation();
            toggleDialog('dialogAssignAsLead', !open.dialogAssignAsLead, e);
          }}
          key="8"
        >
          <ListItemText
            primary={
              <Typography variant="body2">{t('Assign me as lead')}</Typography>
            }
          />
        </MenuItem>
      );
    }
  };

  const assignAsSupportMenuItem = () => {
    const supports = request.support.includes(currentUser);

    if (currentUser === request.lead) {
      // if logged in user IS assigned as lead
      return (
        <MenuItem
          onClick={(e) => {
            e.stopPropagation();
            toggleDialog(
                'dialogNoLeadAssignSupport',
                !open.dialogNoLeadAssignSupport,
                e,
            );
          }}
          key="9"
        >
          <ListItemText
            primary={
              <Typography variant="body2">
                {t('Assign me as support')}
              </Typography>
            }
          />
        </MenuItem>
      );
    } else if (!supports) {
      // if logged in user is NOT assigned as lead OR support
      return (
        <MenuItem
          onClick={(e) => {
            e.stopPropagation();
            toggleDialog(
                'dialogAssignAsSupport',
                !open.dialogAssignAsSupport,
                e,
            );
          }}
          key="10"
        >
          <ListItemText
            primary={
              <Typography variant="body2">
                {t('Assign me as support')}
              </Typography>
            }
          />
        </MenuItem>
      );
    }
  };

  const startReviewHandler = () => {
    if (currentUser === request.lead) {
      return (
        <MenuItem onClick={handleClose} key="11">
          <ListItemText
            primary={
              <Typography variant="body2">{t('Start review')}</Typography>
            }
          />
        </MenuItem>
      );
    }
  };

  const unassignMenuItem = () => {
    const supports = request.support.includes(currentUser);

    if (currentUser === request.lead) {
      return (
        <MenuItem
          onClick={(e) => {
            e.stopPropagation();
            toggleDialog('dialogNoLeadUnassign', !open.dialogNoLeadUnassign, e);
          }}
          key="12"
        >
          <ListItemText
            primary={
              <Typography variant="body2">{t('Unassign myself')}</Typography>
            }
          />
        </MenuItem>
      );
    } else if (supports) {
      return (
        <MenuItem
          onClick={(e) => {
            handleClose(e);
            handleSnackbarOpen('snackbarUnassign');
          }}
          key="13"
        >
          <ListItemText
            primary={
              <Typography variant="body2">{t('Unassign myself')}</Typography>
            }
          />
        </MenuItem>
      );
    }
  };

  const approveMenuItem = () => {
    if (currentUser === request.lead) {
      return (
        <MenuItem
          onClick={(e) => {
            e.stopPropagation();
            toggleDialog('dialogApprove', !open.dialogApprove, e);
          }}
          key="14"
        >
          <ListItemText
            primary={<Typography variant="body2">{t('Approve')}</Typography>}
          />
        </MenuItem>
      );
    }
  };

  const denyMenuItem = () => {
    if (currentUser === request.lead) {
      return (
        <MenuItem
          onClick={(e) => {
            e.stopPropagation();
            toggleDialog('dialogDenied', !open.dialogDenied, e);
          }}
          key="15"
        >
          <ListItemText
            primary={
              <Typography variant="body2">{t('Don\'t approve')}</Typography>
            }
          />
        </MenuItem>
      );
    }
  };

  const requestChangesMenuItem = () => {
    if (currentUser === request.lead) {
      return (
        <MenuItem
          onClick={(e) => {
            e.stopPropagation();
            toggleDialog('dialogUpdate', !open.dialogUpdate, e);
          }}
          key="16"
        >
          <ListItemText
            primary={
              <Typography variant="body2">{t('Request changes')}</Typography>
            }
          />
        </MenuItem>
      );
    }
  };

  const reactivateMenuItem = () => {
    return (
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          handleSnackbarOpen('snackbarReopen');
          handleClose(e);
        }}
        key="17"
      >
        <ListItemText
          primary={<Typography variant="body2">{t('Reactivate')}</Typography>}
        />
      </MenuItem>
    );
  };

  const manageAssigneesMenuItem = () => {
    return (
      <MenuItem onClick={contextManageTeam} key="18">
        <ListItemText
          primary={
            <Typography variant="body2">{t('Manage assignees')}</Typography>
          }
        />
      </MenuItem>
    );
  };

  const summaryMenuItem = () => {
    return (
      <MenuItem onClick={toggleSummary} key="19">
        <ListItemText
          primary={<Typography variant="body2">{t('Summary')}</Typography>}
        />
      </MenuItem>
    );
  };

  if (role === 'analyst') {
    if (statusHead === 'assigned to me') {
      if (status === 'draft') {
        MenuVar = (
          <Menu
            id={ariaControls}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            handleClose={handleClose}
            handleBlur={handleBlur}
          >
            {[
              viewRequestMenuItem(),
              assignAsLeadMenuItem(),
              assignAsSupportMenuItem(),
              unassignMenuItem(),
              manageAssigneesMenuItem(),
              requesterDetailsMenuItem(),
              summaryMenuItem(),
            ]}
          </Menu>
        );
      } else if (status === 'submitted') {
        MenuVar = (
          <Menu
            id={ariaControls}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            handleClose={handleClose}
            handleBlur={handleBlur}
          >
            {[
              viewRequestMenuItem(),
              startReviewHandler(),
              assignAsLeadMenuItem(),
              assignAsSupportMenuItem(),
              unassignMenuItem(),
              manageAssigneesMenuItem(),
              requesterDetailsMenuItem(),
              summaryMenuItem(),
            ]}
          </Menu>
        );
      } else if (status === 'under review') {
        MenuVar = (
          <Menu
            id={ariaControls}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            handleClose={handleClose}
            handleBlur={handleBlur}
          >
            {[
              viewRequestMenuItem(),
              approveMenuItem(),
              denyMenuItem(),
              requestChangesMenuItem(),
              assignAsLeadMenuItem(),
              assignAsSupportMenuItem(),
              unassignMenuItem(),
              manageAssigneesMenuItem(),
              requesterDetailsMenuItem(),
              summaryMenuItem(),
            ]}
          </Menu>
        );
      } else if (status === 'changes requested') {
        MenuVar = (
          <Menu
            id={ariaControls}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            handleClose={handleClose}
            handleBlur={handleBlur}
          >
            {[
              viewRequestMenuItem(),
              assignAsLeadMenuItem(),
              assignAsSupportMenuItem(),
              unassignMenuItem(),
              manageAssigneesMenuItem(),
              requesterDetailsMenuItem(),
              summaryMenuItem(),
            ]}
          </Menu>
        );
      }
    } else if (statusHead === 'unassigned') {
      MenuVar = (
        <Menu
          id={ariaControls}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          handleClose={handleClose}
          handleBlur={handleBlur}
        >
          {[
            viewRequestMenuItem(),
            assignAsLeadMenuItem(),
            assignAsSupportMenuItem(),
            manageAssigneesMenuItem(),
            requesterDetailsMenuItem(),
            summaryMenuItem(),
          ]}
        </Menu>
      );
    } else if (statusHead === 'active') {
      if (status === 'draft') {
        MenuVar = (
          <Menu
            id={ariaControls}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            handleClose={handleClose}
            handleBlur={handleBlur}
          >
            {[
              viewRequestMenuItem(),
              assignAsLeadMenuItem(),
              assignAsSupportMenuItem(),
              unassignMenuItem(),
              manageAssigneesMenuItem(),
              requesterDetailsMenuItem(),
              summaryMenuItem(),
            ]}
          </Menu>
        );
      } else if (status === 'submitted') {
        MenuVar = (
          <Menu
            id={ariaControls}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            handleClose={handleClose}
            handleBlur={handleBlur}
          >
            {[
              viewRequestMenuItem(),
              startReviewHandler(),
              assignAsLeadMenuItem(),
              assignAsSupportMenuItem(),
              unassignMenuItem(),
              manageAssigneesMenuItem(),
              requesterDetailsMenuItem(),
              summaryMenuItem(),
            ]}
          </Menu>
        );
      } else if (status === 'under review') {
        MenuVar = (
          <Menu
            id={ariaControls}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            handleClose={handleClose}
            handleBlur={handleBlur}
          >
            {[
              viewRequestMenuItem(),
              approveMenuItem(),
              denyMenuItem(),
              requestChangesMenuItem(),
              assignAsLeadMenuItem(),
              assignAsSupportMenuItem(),
              unassignMenuItem(),
              manageAssigneesMenuItem(),
              requesterDetailsMenuItem(),
              summaryMenuItem(),
            ]}
          </Menu>
        );
      } else if (status === 'changes requested') {
        MenuVar = (
          <Menu
            id={ariaControls}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            handleClose={handleClose}
            handleBlur={handleBlur}
          >
            {[
              viewRequestMenuItem(),
              assignAsLeadMenuItem(),
              assignAsSupportMenuItem(),
              manageAssigneesMenuItem(),
              requesterDetailsMenuItem(),
              summaryMenuItem(),
            ]}
          </Menu>
        );
      }
    } else if (statusHead === 'approved') {
      MenuVar = (
        <Menu
          id={ariaControls}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          handleClose={handleClose}
          handleBlur={handleBlur}
        >
          {[
            viewRequestMenuItem(),
            reactivateMenuItem(),
            assigneeDetailsMenuItem(),
            requesterDetailsMenuItem(),
            summaryMenuItem(),
          ]}
        </Menu>
      );
    } else if (statusHead === 'not approved') {
      MenuVar = (
        <Menu
          id={ariaControls}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          handleClose={handleClose}
          handleBlur={handleBlur}
        >
          {[
            viewRequestMenuItem(),
            reactivateMenuItem(),
            assigneeDetailsMenuItem(),
            requesterDetailsMenuItem(),
            summaryMenuItem(),
          ]}
        </Menu>
      );
    }
  } else {
    // ROLE = RESEARCHER
    if (statusHead === 'active') {
      if (status === 'draft') {
        MenuVar = (
          <Menu
            id={ariaControls}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            handleClose={handleClose}
            handleBlur={handleBlur}
          >
            {[
              editMenuItem(),
              submitMenuItem(),
              assigneeDetailsMenuItem(),
              requesterDetailsMenuItem(),
              summaryMenuItem(),
              deleteMenuItem(),
            ]}
          </Menu>
        );
      } else if (status === 'submitted') {
        MenuVar = (
          <Menu
            id={ariaControls}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            handleClose={handleClose}
            handleBlur={handleBlur}
          >
            {[
              viewRequestMenuItem(),
              withdrawMenuItem(),
              assigneeDetailsMenuItem(),
              requesterDetailsMenuItem(),
              summaryMenuItem(),
            ]}
          </Menu>
        );
      } else if (status === 'under review') {
        MenuVar = (
          <Menu
            id={ariaControls}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            handleClose={handleClose}
            handleBlur={handleBlur}
          >
            {[
              viewRequestMenuItem(),
              withdrawMenuItem(),
              assigneeDetailsMenuItem(),
              requesterDetailsMenuItem(),
              summaryMenuItem(),
            ]}
          </Menu>
        );
      } else if (status === 'changes requested') {
        MenuVar = (
          <Menu
            id={ariaControls}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            handleClose={handleClose}
            handleBlur={handleBlur}
          >
            {[
              editMenuItem(),
              submitMenuItem(),
              withdrawMenuItem(),
              assigneeDetailsMenuItem(),
              requesterDetailsMenuItem(),
              summaryMenuItem(),
            ]}
          </Menu>
        );
      }
    } else if (statusHead === 'withdrawn') {
      MenuVar = (
        <Menu
          id={ariaControls}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          handleClose={handleClose}
          handleBlur={handleBlur}
        >
          {[
            viewRequestMenuItem(),
            reactivateMenuItem(),
            assigneeDetailsMenuItem(),
            requesterDetailsMenuItem(),
            summaryMenuItem(),
          ]}
        </Menu>
      );
    } else if (statusHead === 'approved') {
      MenuVar = (
        <Menu
          id={ariaControls}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          handleClose={handleClose}
          handleBlur={handleBlur}
        >
          {[
            viewRequestMenuItem(),
            assigneeDetailsMenuItem(),
            requesterDetailsMenuItem(),
            summaryMenuItem(),
          ]}
        </Menu>
      );
    } else if (statusHead === 'not approved') {
      MenuVar = (
        <Menu
          id={ariaControls}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          handleClose={handleClose}
          handleBlur={handleBlur}
        >
          {[
            viewRequestMenuItem(),
            assigneeDetailsMenuItem(),
            requesterDetailsMenuItem(),
            summaryMenuItem(),
          ]}
        </Menu>
      );
    }
  }

  return (
    <div>
      <IconButton
        onClick={handleClick}
        aria-controls={ariaControls}
        aria-haspopup="true"
        aria-label="Actions menu"
        edge="end"
        onKeyPress={(e) => {
          e.preventDefault();
          e.stopPropagation();
          if (e.key === 'Enter') {
            handleClick(e);
          }
        }}
        size="large"
      >
        <MoreVertIcon />
      </IconButton>
      {MenuVar}

      <SnackbarReactivateRequest
        open={open.snackbarReopen}
        handleClose={() => handleSnackbarClose('snackbarReopen')}
      />
      <SnackbarSubmitRequest
        open={open.snackbarSubmit}
        handleClose={() => handleSnackbarClose('snackbarSubmit')}
      />
      <SnackbarDeleteRequest
        open={open.snackbarDelete}
        handleClose={() => handleSnackbarClose('snackbarDelete')}
      />
      <SnackbarAssignLead
        open={open.snackbarAssignLead}
        handleClose={() => handleSnackbarClose('snackbarAssignLead')}
      />
      <SnackbarAssignSupport
        open={open.snackbarAssignSupport}
        handleClose={() => handleSnackbarClose('snackbarAssignSupport')}
      />
      <SnackbarUnassign
        open={open.snackbarUnassign}
        handleClose={() => handleSnackbarClose('snackbarUnassign')}
      />
      <DialogUpdate
        toggleDialog={(e) =>
          toggleDialog('dialogUpdate', !open.dialogUpdate, e)
        }
        open={open.dialogUpdate}
      />
      <DialogDenied
        toggleDialog={(e) =>
          toggleDialog('dialogDenied', !open.dialogDenied, e)
        }
        open={open.dialogDenied}
      />
      <DialogApprove
        toggleDialog={(e) =>
          toggleDialog('dialogApprove', !open.dialogApprove, e)
        }
        open={open.dialogApprove}
      />
      <DialogWithdraw
        toggleDialog={(e) =>
          toggleDialog('dialogWithdraw', !open.dialogWithdraw, e)
        }
        open={open.dialogWithdraw}
      />
      <DialogRequesterDetails
        toggleDialog={(e) =>
          toggleDialog(
              'dialogRequesterDetails',
              !open.dialogRequesterDetails,
              e,
          )
        }
        open={open.dialogRequesterDetails}
      />
      <DialogAssigneeDetails
        toggleDialog={(e) =>
          toggleDialog('dialogAssigneeDetails', !open.dialogAssigneeDetails, e)
        }
        open={open.dialogAssigneeDetails}
        role={role}
        statusHead={statusHead}
      />
      <DialogNoLead
        toggleDialog={(e) =>
          toggleDialog('dialogNoLeadUnassign', !open.dialogNoLeadUnassign, e)
        }
        open={open.dialogNoLeadUnassign}
        submitDialog={unassignLead}
        snackbar={
          <SnackbarUnassign
            open={open.snackbarUnassign}
            handleClose={() => handleSnackbarClose('snackbarUnassign')}
          />
        }
      />
      <DialogNoLead
        toggleDialog={(e) =>
          toggleDialog(
              'dialogNoLeadAssignSupport',
              !open.dialogNoLeadAssignSupport,
              e,
          )
        }
        open={open.dialogNoLeadAssignSupport}
        submitDialog={assignSupportNoLead}
        snackbar={
          <SnackbarAssignSupport
            open={open.snackbarAssignSupport}
            handleClose={() => handleSnackbarClose('snackbarAssignSupport')}
          />
        }
      />
      <DialogAssignAsLead
        open={open.dialogAssignAsLead}
        toggleDialog={(e) =>
          toggleDialog('dialogAssignAsLead', !open.dialogAssignAsLead, e)
        }
        origin="actionsMenu"
      />
      <DialogAssignAsSupport
        open={open.dialogAssignAsSupport}
        toggleDialog={(e) =>
          toggleDialog('dialogAssignAsSupport', !open.dialogAssignAsSupport, e)
        }
        origin="actionsMenu"
      />
      <DialogDelete
        submitDialog={deleteRequest}
        open={open.dialogDelete}
        toggleDialog={(e) => {
          toggleDialog('dialogDelete', !open.dialogDelete, e);
        }}
        snackbar={
          <SnackbarDeleteRequest
            open={open.snackbarDelete}
            handleClose={() => handleSnackbarClose('snackbarDelete')}
          />
        }
      />
    </div>
  );
}

// //////////////////////////// Analyst role dialog box context menu
export function AnalystMenu(props) {
  const {
    role,
    makeSupport,
    makeLead,
    unassignRequest,
    controls,
    current,
    toggleAssignMeMenu,
  } = props;
  const {t} = useTranslation();
  let MenuVar;

  const [anchorEl, setAnchorEl] = React.useState(null);

  const ariaControls = `context-menu-${controls}`;

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  function handleBlur(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setAnchorEl(null);
    }
  }

  if (role === 'lead') {
    MenuVar = (
      <Menu
        id={ariaControls}
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        handleClose={handleClose}
        handleBlur={handleBlur}
      >
        <MenuItem onClick={makeSupport} key="20">
          <ListItemText
            primary={
              <Typography variant="body2">
                {current ?
                  t('Assign me as support') :
                  t('Assign user as support')}
              </Typography>
            }
          />
        </MenuItem>
        <MenuItem
          onClick={() => {
            unassignRequest();
            toggleAssignMeMenu();
          }}
          key="21"
        >
          <ListItemText
            primary={
              <Typography variant="body2">
                {current ? t('Unassign myself') : t('Unassign user')}
              </Typography>
            }
          />
        </MenuItem>
      </Menu>
    );
  } else if (role === 'support') {
    MenuVar = (
      <Menu
        id={ariaControls}
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        handleClose={handleClose}
        handleBlur={handleBlur}
      >
        <MenuItem onClick={makeLead} key="22">
          <ListItemText
            primary={
              <Typography variant="body2">
                {current ? t('Assign me as lead') : t('Assign user as lead')}
              </Typography>
            }
          />
        </MenuItem>
        <MenuItem
          onClick={() => {
            unassignRequest();
            toggleAssignMeMenu();
          }}
          key="23"
        >
          <ListItemText
            primary={
              <Typography variant="body2">
                {current ? t('Unassign myself') : t('Unassign user')}
              </Typography>
            }
          />
        </MenuItem>
      </Menu>
    );
  }
  return (
    <div>
      <IconButton
        onClick={handleClick}
        aria-controls={ariaControls}
        aria-haspopup="true"
        aria-label="Actions menu"
        edge="end"
        size="large"
      >
        <MoreVertIcon />
      </IconButton>
      {MenuVar}
    </div>
  );
}
