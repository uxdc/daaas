import React from 'react';
import {styled} from '@mui/material/styles';
import {useTranslation} from 'react-i18next';
import {Typography, TableCell, Chip} from '@mui/material';

import {ROW_HEIGHT} from './TableContainerComponent';

const PREFIX = 'AnalystCell';

const classes = {
  tablesCellsFlex: `${PREFIX}-tablesCellsFlex`,
  leadChip: `${PREFIX}-leadChip`,
};

const StyledTableCell = styled(TableCell)(({theme}) => ({
  [`&.${classes.tablesCellsFlex}`]: {
    display: 'flex',
    alignItems: 'center',
    minHeight: `calc(${ROW_HEIGHT}px - ${theme.spacing(2)})`,
  },

  [`& .${classes.leadChip}`]: {
    paddingRight: theme.spacing(1),
    marginRight: theme.spacing(1),
    borderRight: '1px solid',
    borderRightColor: theme.palette.divider,
  },
}));

export default function AnalystCell(props) {
  const {t} = useTranslation();
  const {
    role,
    lead,
    support,
    toggleManageTeamDrawer,
    statusHead,
    clickHandler,
  } = props;
  const extraAnalysts = support.length;

  const handleClick = (e) => {
    e.stopPropagation();
    if (role === 'analyst') {
      if (statusHead === 'approved' || statusHead === 'not approved') {
        clickHandler(e);
      } else {
        toggleManageTeamDrawer(e);
      }
    } else {
      clickHandler(e);
    }
  };

  if (role === 'researcher') {
    if (lead !== '') {
      return (
        <StyledTableCell className={classes.tablesCellsFlex}>
          <Chip label={lead} onClick={handleClick} />
        </StyledTableCell>
      );
    } else {
      return (
        <StyledTableCell className={classes.tablesCellsFlex}>
          <Typography variant="body2" color="textSecondary">
            {t('Unassigned')}
          </Typography>
        </StyledTableCell>
      );
    }
  } else if (role === 'analyst') {
    if (lead !== '' && support.length > 0) {
      return (
        <StyledTableCell className={classes.tablesCellsFlex}>
          <Chip label={lead} onClick={handleClick} sx={{mr: 1}} />
          <Chip
            label={`${extraAnalysts} ${t('support')}`}
            onClick={handleClick}
          />
        </StyledTableCell>
      );
    } else if (lead !== '' && support.length === 0) {
      return (
        <StyledTableCell className={classes.tablesCellsFlex}>
          <Chip label={lead} onClick={handleClick} />
        </StyledTableCell>
      );
    } else if (lead === '' && support.length > 0) {
      return (
        <StyledTableCell className={classes.tablesCellsFlex}>
          <Typography variant="body2" color="textSecondary">
            {t('No lead')}
          </Typography>
          <Chip
            label={`${extraAnalysts} ${t('support')}`}
            onClick={handleClick}
            sx={{ml: 1}}
          />
        </StyledTableCell>
      );
    } else {
      return (
        <StyledTableCell className={classes.tablesCellsFlex}>
          <Typography variant="body2" color="textSecondary">
            {t('Unassigned')}
          </Typography>
        </StyledTableCell>
      );
    }
  }
}
