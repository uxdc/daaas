import React from 'react';
import {styled} from '@mui/material/styles';
import {useTranslation} from 'react-i18next';
import PropTypes from 'prop-types';
import {
  Typography,
  TableCell,
  TableRow,
  TableHead,
  TableSortLabel,
} from '@mui/material';

const PREFIX = 'DashboardTableHead';

const classes = {
  thead: `${PREFIX}-thead`,
  theadNarrow: `${PREFIX}-theadNarrow`,
  visuallyHidden: `${PREFIX}-visuallyHidden`,
  headCell: `${PREFIX}-headCell`,
};

const StyledTableHead = styled(TableHead)(({theme}) => ({
  [`& .${classes.thead}`]: {
    width: '15.7%',
  },

  [`& .${classes.theadNarrow}`]: {
    width: '7%',
  },

  [`& .${classes.visuallyHidden}`]: {
    border: 0,
    clip: 'rect(0, 0, 0, 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },

  [`& .${classes.headCell}`]: {
    paddingTop: [theme.spacing(3), '!important'],
  },
}));

export default function DashboardTableHead(props) {
  const {order, orderBy, onRequestSort, headCells} = props;
  const {t} = useTranslation();

  const [state, setState] = React.useState({
    labelFocus: false,
  });

  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  const labelFocus = () => {
    setState({...state, labelFocus: true});
  };

  const labelBlur = () => {
    setState({...state, labelFocus: false});
  };

  return (
    <StyledTableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align="left"
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
            className={`${classes.headCell} ${
              headCell.narrow ? classes.theadNarrow : classes.thead
            }`}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              onFocus={labelFocus}
              onBlur={labelBlur}
            >
              <Typography noWrap={true} component="span" variant="subtitle2">
                {t(headCell.label)}
                {state.labelFocus && (
                  <span className={classes.visuallyHidden}>
                    {`Sort by ${headCell.label}`}
                  </span>
                )}
              </Typography>
              <span className={classes.visuallyHidden} aria-live="polite">
                {orderBy === headCell.id ?
                  order === 'desc' ?
                    'sorted descending' :
                    'sorted ascending' :
                  null}
              </span>
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </StyledTableHead>
  );
}

DashboardTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};
