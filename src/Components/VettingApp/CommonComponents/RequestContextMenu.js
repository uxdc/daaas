import React from 'react';
import {styled} from '@mui/material/styles';
import {useTranslation} from 'react-i18next';
import {Typography} from '@mui/material';
import IconButton from '@mui/material/IconButton';
import MenuItem from '@mui/material/MenuItem';
import ListItemText from '@mui/material/ListItemText';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import {Menu} from '../../CommonComponents/Menu';

import {
  SnackbarAssignSupport,
  SnackbarDeleteRequest,
  SnackbarUnassign,
} from './Snackbars';
import {
  DialogRequesterDetails,
  DialogAssigneeDetails,
  DialogAssignAsSupport,
  DialogNoLead,
  DialogAssignAsLead,
  DialogDelete,
} from './DialogBox';
import SummaryDrawer from './SummaryDrawer';
import {loggedInUser} from '../../../Data/fakeData';

const PREFIX = 'RequestContextMenu';

const classes = {
  menu: `${PREFIX}-menu`,
};

const Root = styled('div')(({theme}) => ({
  [`&.${classes.menu}`]: {
    paddingLeft: theme.spacing(1),
  },
}));

export function ActionsMenu(props) {
  const {role, status, toggleManageTeamDrawer, controls, request} = props;
  const {t} = useTranslation();
  const currentUser = loggedInUser();
  let MenuVar;

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [open, setOpen] = React.useState({
    snackbarDelete: false,
    snackbarUnassign: false,
    dialogAssign: false,
    dialogAssigneeDetails: false,
    dialogRequesterDetails: false,
    dialogNoLeadAssignSupport: false,
    dialogAssignAsSupport: false,
    dialogNoLeadUnassign: false,
    dialogDelete: false,
    summaryDrawer: false,
  });

  const ariaControls = `actions-menu-${controls}`;

  const handleClick = (e) => {
    e.stopPropagation();
    setAnchorEl(e.currentTarget);
  };

  const handleClose = (e) => {
    e.stopPropagation();
    setAnchorEl(null);
  };

  function handleBlur(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setAnchorEl(null);
    }
  }

  const handleSnackbarOpen = (state) => {
    setOpen({...open, [state]: true});
  };

  const handleSnackbarClose = (state) => {
    setOpen({...open, [state]: false});
  };

  const toggleSummary = (e) => {
    e.stopPropagation();
    handleClose(e);
    setOpen({...open, summaryDrawer: !open.summaryDrawer});
  };

  const contextManageTeam = (e) => {
    e.stopPropagation();
    handleClose(e);
    toggleManageTeamDrawer(e);
  };

  const toggleDialog = (state, value, e) => {
    e.stopPropagation();
    setOpen({...open, [state]: value});
    handleClose(e);
  };

  const assignAsSupport = (e) => {
    setOpen({
      ...open,
      dialogNoLeadAssignSupport: !open.dialogNoLeadAssignSupport,
      snackbarAssignSupport: true,
    });
  };

  const unassignLead = (e) => {
    setOpen({
      ...open,
      dialogNoLeadUnassign: !open.dialogNoLeadUnassign,
      snackbarUnassign: true,
    });
  };

  const deleteRequest = (e) => {
    setOpen({...open, dialogDelete: false, snackbarDelete: true});
  };

  const assigneeDetailsMenuItem = () => {
    if (status === 'approved' || status === 'denied' || role === 'researcher') {
      // if the request is in a resolved state, or user is a researcher
      return (
        <MenuItem
          onClick={(e) => {
            e.stopPropagation();
            toggleDialog(
                'dialogAssigneeDetails',
                !open.dialogAssigneeDetails,
                e,
            );
          }}
          key="0"
        >
          <ListItemText
            primary={
              <Typography variant="body2">{t('Assignee details')}</Typography>
            }
          />
        </MenuItem>
      );
    }
  };

  const requesterDetailsMenuItem = () => {
    return (
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          toggleDialog(
              'dialogRequesterDetails',
              !open.dialogRequesterDetails,
              e,
          );
        }}
        key="1"
      >
        <ListItemText
          primary={
            <Typography variant="body2">{t('Requester details')}</Typography>
          }
        />
      </MenuItem>
    );
  };

  const deleteMenuItem = () => {
    return (
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          handleClose(e);
          toggleDialog('dialogDelete', !open.dialogDelete, e);
        }}
        key="2"
      >
        <ListItemText
          primary={<Typography variant="body2">{t('Delete')}</Typography>}
        />
      </MenuItem>
    );
  };

  const assignAsLeadMenuItem = () => {
    if (request.lead !== currentUser) {
      if (status !== 'approved' && status !== 'denied') {
        // if the request is NOT in a resolved state
        return (
          <MenuItem
            onClick={(e) => {
              e.stopPropagation();
              toggleDialog('dialogAssign', !open.dialogAssign, e);
            }}
            open={open.dialogAssign}
            key="3"
          >
            <ListItemText
              primary={
                <Typography variant="body2">
                  {t('Assign me as lead')}
                </Typography>
              }
            />
          </MenuItem>
        );
      }
    }
  };

  const assignAsSupportMenuItem = () => {
    const supports = request.support.includes(currentUser);

    if (currentUser === request.lead || !supports) {
      // if logged in user IS assigned as lead
      // OR
      // if logged in user is NOT assigned as lead OR support
      if (status !== 'approved' && status !== 'denied') {
        // if the request is NOT in a resolved state
        return (
          <MenuItem
            onClick={(e) => {
              e.stopPropagation();
              toggleDialog(
                  'dialogAssignAsSupport',
                  !open.dialogAssignAsSupport,
                  e,
              );
            }}
            key="4"
          >
            <ListItemText
              primary={
                <Typography variant="body2">
                  {t('Assign me as support')}
                </Typography>
              }
            />
          </MenuItem>
        );
      }
    }
  };

  const unassignMenuItem = () => {
    const supports = request.support.includes(currentUser);
    if (status !== 'approved' && status !== 'denied') {
      // if the request is NOT in a resolved state
      if (currentUser === request.lead) {
        return (
          <MenuItem
            onClick={(e) => {
              e.stopPropagation();
              toggleDialog(
                  'dialogNoLeadUnassign',
                  !open.dialogNoLeadUnassign,
                  e,
              );
            }}
            key="5"
          >
            <ListItemText
              primary={
                <Typography variant="body2">{t('Unassign myself')}</Typography>
              }
            />
          </MenuItem>
        );
      } else if (supports) {
        return (
          <MenuItem
            onClick={(e) => {
              handleClose(e);
              handleSnackbarOpen('snackbarUnassign');
            }}
            key="6"
          >
            <ListItemText
              primary={
                <Typography variant="body2">{t('Unassign myself')}</Typography>
              }
            />
          </MenuItem>
        );
      }
    }
  };

  const manageAssigneesMenuItem = () => {
    if (status !== 'approved' && status !== 'denied') {
      // if the request is NOT in a resolved state
      return (
        <MenuItem onClick={contextManageTeam} key="7">
          <ListItemText
            primary={
              <Typography variant="body2">{t('Manage assignees')}</Typography>
            }
          />
        </MenuItem>
      );
    }
  };

  const summaryMenuItem = () => {
    return (
      <MenuItem onClick={toggleSummary} key="8">
        <ListItemText
          primary={<Typography variant="body2">{t('Summary')}</Typography>}
        />
      </MenuItem>
    );
  };

  if (role === 'analyst') {
    MenuVar = (
      <Menu
        id={ariaControls}
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        handleClose={handleClose}
        handleBlur={handleBlur}
      >
        {[
          assignAsLeadMenuItem(),
          assignAsSupportMenuItem(),
          unassignMenuItem(),
          manageAssigneesMenuItem(),
          assigneeDetailsMenuItem(),
          requesterDetailsMenuItem(),
          summaryMenuItem(),
        ]}
      </Menu>
    );
  } else {
    // ROLE = RESEARCHER
    if (status === 'draft') {
      MenuVar = (
        <Menu
          id={ariaControls}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          handleClose={handleClose}
          handleBlur={handleBlur}
        >
          {[
            assigneeDetailsMenuItem(),
            requesterDetailsMenuItem(),
            summaryMenuItem(),
            deleteMenuItem(),
          ]}
        </Menu>
      );
    } else {
      MenuVar = (
        <Menu
          id={ariaControls}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          handleClose={handleClose}
          handleBlur={handleBlur}
        >
          {[
            assigneeDetailsMenuItem(),
            requesterDetailsMenuItem(),
            summaryMenuItem(),
          ]}
        </Menu>
      );
    }
  }

  return (
    <Root className={classes.menu}>
      <IconButton
        onClick={handleClick}
        aria-controls={ariaControls}
        aria-haspopup="true"
        aria-label="Actions menu"
        edge="end"
        onKeyPress={(e) => {
          e.preventDefault();
          e.stopPropagation();
          if (e.key === 'Enter') {
            handleClick(e);
          }
        }}
        size="large"
      >
        <MoreVertIcon />
      </IconButton>
      {MenuVar}
      {/* Delete request snackbar */}
      <SnackbarDeleteRequest
        open={open.snackbarDelete}
        handleClose={() => handleSnackbarClose('snackbarDelete')}
      />
      {/* Assign as support snackbar */}
      <SnackbarAssignSupport
        open={open.snackbarAssignSupport}
        handleClose={() => handleSnackbarClose('snackbarAssignSupport')}
      />
      {/* Unassign myself snackbar */}
      <SnackbarUnassign
        open={open.snackbarUnassign}
        handleClose={() => handleSnackbarClose('snackbarUnassign')}
      />
      {/* Assign as support dialog */}
      <DialogAssignAsSupport
        toggleDialog={(e) =>
          toggleDialog('dialogAssignAsSupport', !open.dialogAssignAsSupport, e)
        }
        open={open.dialogAssignAsSupport}
      />
      {/* Assign lead as support warning dialog */}
      <DialogNoLead
        toggleDialog={(e) =>
          toggleDialog(
              'dialogNoLeadAssignSupport',
              !open.dialogNoLeadAssignSupport,
              e,
          )
        }
        open={open.dialogNoLeadAssignSupport}
        submitDialog={(e) => assignAsSupport(e)}
        snackbar={
          <SnackbarAssignSupport
            open={open.snackbarAssignSupport}
            handleClose={() => handleSnackbarClose('snackbarAssignSupport')}
          />
        }
      />
      {/* Unassign lead warning dialog */}
      <DialogNoLead
        toggleDialog={(e) =>
          toggleDialog('dialogNoLeadUnassign', !open.dialogNoLeadUnassign, e)
        }
        open={open.dialogNoLeadUnassign}
        submitDialog={(e) => unassignLead(e)}
        snackbar={
          <SnackbarUnassign
            open={open.snackbarUnassign}
            handleClose={() => handleSnackbarClose('snackbarUnassign')}
          />
        }
      />
      {/* Assign as lead dialog */}
      <DialogAssignAsLead
        toggleDialog={(e) =>
          toggleDialog('dialogAssign', !open.dialogAssign, e)
        }
        open={open.dialogAssign}
      />
      {/* Assignee details dialog */}
      <DialogAssigneeDetails
        toggleDialog={(e) =>
          toggleDialog('dialogAssigneeDetails', !open.dialogAssigneeDetails, e)
        }
        open={open.dialogAssigneeDetails}
        role={role}
        statusHead={status}
      />
      {/* Requester details dialog */}
      <DialogRequesterDetails
        toggleDialog={(e) =>
          toggleDialog(
              'dialogRequesterDetails',
              !open.dialogRequesterDetails,
              e,
          )
        }
        open={open.dialogRequesterDetails}
      />
      {/* Delete request dialog */}
      <DialogDelete
        submitDialog={deleteRequest}
        open={open.dialogDelete}
        toggleDialog={(e) => {
          toggleDialog('dialogDelete', !open.dialogDelete, e);
        }}
        snackbar={
          <SnackbarDeleteRequest
            open={open.snackbarDelete}
            handleClose={() => handleSnackbarClose('snackbarDelete')}
          />
        }
      />
      {/* Summary drawer */}
      <SummaryDrawer
        open={open.summaryDrawer}
        clickHandler={toggleSummary}
        status={status}
      />
    </Root>
  );
}
