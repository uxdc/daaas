import * as React from 'react';
import {styled} from '@mui/material/styles';
import {IconButton} from '@mui/material';
import Alert from '@mui/material/Alert';
import Collapse from '@mui/material/Collapse';
import CloseIcon from '@mui/icons-material/Close';

const PREFIX = 'CutCopyPasteAlert';

const classes = {
  alert: `${PREFIX}-alert`,
};

const StyledCollapse = styled(Collapse)(({theme}) => ({
  [`& .${classes.alert}`]: {
    '& .MuiAlert-action': {
      alignItems: 'start',
    },
  },
}));

export default function CutCopyPasteAlert() {
  const [open, setOpen] = React.useState(true);

  return (
    <StyledCollapse in={open}>
      <Alert
        severity="warning"
        sx={{mb: 3}}
        className={classes.alert}
        action={
          <IconButton
            aria-label="Close"
            onClick={() => {
              setOpen(false);
            }}
            size="large"
          >
            <CloseIcon fontSize="small" />
          </IconButton>
        }
      >
        Cut, copy and paste functionality has been disabled on text fields for
        security purposes.
      </Alert>
    </StyledCollapse>
  );
}
