import React from 'react';
import {styled} from '@mui/material/styles';
import {
  DialogSaveBeforeLeaving,
  DialogWithdraw,
} from '../CommonComponents/DialogBox';
import {
  AppBar,
  Button,
  Toolbar,
  IconButton,
  Typography,
  Grid,
  Divider,
} from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import {Menu} from '../../CommonComponents/Menu';
import MenuItem from '@mui/material/MenuItem';
import Icon from '@mdi/react';
import {
  mdiContentSave,
  mdiEmailEditOutline,
  mdiInboxArrowDown,
  mdiMenuDown,
  mdiPlaySpeed,
  mdiProgressWrench,
  mdiRestore,
  mdiUndo,
} from '@mdi/js';
import {
  SnackbarApproveRequest,
  SnackbarDenyRequest,
  SnackbarReactivateRequest,
  SnackbarSaveRequest,
  SnackbarStartReview,
  SnackbarSubmitRequest,
} from './Snackbars';
import {DialogDenied, DialogUpdate, DialogApprove} from './DialogBox';
import {ActionsMenu} from './RequestContextMenu';
import {loggedInUser} from '../../../Data/fakeData';

const PREFIX = 'RequestToolbar';

const classes = {
  appBar: `${PREFIX}-appBar`,
  gridContainer: `${PREFIX}-gridContainer`,
  title: `${PREFIX}-title`,
  headerBtn: `${PREFIX}-headerBtn`,
  actions: `${PREFIX}-actions`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.appBar}`]: {
    justifyContent: 'center',
    width: 'auto',
    minHeight: theme.spacing(8),
    backgroundColor: theme.palette.common.white,
    boxShadow: 'none',
    borderBottom: '1px solid',
    borderBottomColor: theme.palette.divider,
    top: 'auto',
    left: 0,
  },

  [`& .${classes.gridContainer}`]: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
  },

  [`& .${classes.title}`]: {
    paddingLeft: theme.spacing(1),
  },

  [`& .${classes.headerBtn}`]: {
    marginRight: theme.spacing(2),
  },

  [`& .${classes.actions}`]: {
    display: 'flex',
    alignItems: 'center',
  },
}));

function RequestToolbar(props) {
  const currentUser = loggedInUser();
  const {status, role, assignees} = {...props};

  const [anchorEl, setAnchorEl] = React.useState(null);

  const [open, setOpen] = React.useState({
    dialogApprove: false,
    dialogUpdate: false,
    dialogDenied: false,
    dialogWithdraw: false,
    dialogSave: false,
    snackbarReactivate: false,
    snackbarSubmit: false,
    snackbarSave: false,
    snackbarDeny: false,
    snackbarApprove: false,
    snackbarStart: false,
    snackbarWithdraw: false,
  });

  const handleClickOpen = (state) => {
    setOpen({...open, [state]: true});
  };

  const handleClickClose = (state) => {
    setOpen({...open, [state]: false});
  };

  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  function handleBlur(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setAnchorEl(null);
    }
  }

  const reactivateButton = () => {
    if (
      status === 'approved' ||
      status === 'not approved' ||
      status === 'withdrawn'
    ) {
      return (
        <Button
          className={classes.headerBtn}
          variant="text"
          color="primary"
          startIcon={<Icon path={mdiRestore} size={1} />}
          onClick={() => handleClickOpen('snackbarReactivate')}
        >
          Reactivate
        </Button>
      );
    }
  };

  const requestChangesButton = () => {
    if (status === 'under review') {
      return (
        <Button
          variant="outlined"
          color="primary"
          className={classes.headerBtn}
          startIcon={<Icon path={mdiEmailEditOutline} size={1} />}
          onClick={() => handleClickOpen('dialogUpdate')}
        >
          Request changes
        </Button>
      );
    }
  };

  const resolveButton = () => {
    if (status === 'under review') {
      return (
        <>
          <Button
            className={classes.headerBtn}
            aria-controls="resolve-menu"
            aria-haspopup="true"
            onClick={handleMenuOpen}
            variant="contained"
            color="primary"
            startIcon={<Icon path={mdiProgressWrench} size={1} />}
            endIcon={<Icon path={mdiMenuDown} size={1} />}
          >
            Resolve
          </Button>
          <Menu
            id="resolve-menu"
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            handleClose={handleMenuClose}
            handleBlur={handleBlur}
          >
            <MenuItem onClick={() => handleClickOpen('dialogApprove')}>
              <Typography variant="body2">Approve</Typography>
            </MenuItem>
            <MenuItem onClick={() => handleClickOpen('dialogDenied')}>
              <Typography variant="body2">Don't approve</Typography>
            </MenuItem>
          </Menu>
        </>
      );
    }
  };

  const saveButton = () => {
    if (status === 'draft' || status === 'changes requested') {
      return (
        <Button
          variant="outlined"
          color="primary"
          className={classes.headerBtn}
          startIcon={<Icon path={mdiContentSave} size={1} />}
          onClick={() => handleClickOpen('snackbarSave')}
        >
          Save
        </Button>
      );
    }
  };

  const startReviewButton = () => {
    if (status === 'submitted') {
      return (
        <Button
          className={classes.headerBtn}
          variant="contained"
          color="primary"
          startIcon={<Icon path={mdiPlaySpeed} size={1} />}
          onClick={() => handleClickOpen('snackbarStart')}
        >
          Start review
        </Button>
      );
    }
  };

  const submitButton = () => {
    if (status === 'draft' || status === 'changes requested') {
      return (
        <Button
          className={classes.headerBtn}
          variant="contained"
          color="primary"
          startIcon={<Icon path={mdiInboxArrowDown} size={1} />}
          onClick={() => handleClickOpen('snackbarSubmit')}
        >
          Submit
        </Button>
      );
    }
  };

  const withdrawButton = () => {
    if (
      status !== 'withdrawn' &&
      status !== 'approved' &&
      status !== 'not approved' &&
      status !== 'draft'
    ) {
      return (
        <Button
          className={classes.headerBtn}
          variant="text"
          color="primary"
          startIcon={<Icon path={mdiUndo} size={1} />}
          onClick={() => handleClickOpen('dialogWithdraw')}
        >
          Withdraw
        </Button>
      );
    }
  };

  return (
    <Root>
      <AppBar className={classes.appBar} color="default" position="static">
        <Toolbar disableGutters>
          <Grid
            container
            justifyContent="space-between"
            className={classes.gridContainer}
          >
            <Grid item>
              <IconButton
                edge="start"
                className={classes.menuButton}
                aria-label="Back to dashboard"
                onClick={() => handleClickOpen('dialogSave')}
                size="large"
              >
                <ArrowBackIcon />
              </IconButton>
              <Typography
                variant="body2"
                component="span"
                className={classes.title}
              >
                Dashboard
              </Typography>
            </Grid>
            <Grid item className={classes.actions}>
              {role === 'analyst' && (
                <>
                  {reactivateButton()}
                  {assignees.lead === currentUser && (
                    <>
                      {requestChangesButton()}
                      {resolveButton()}
                      {startReviewButton()}
                    </>
                  )}
                </>
              )}
              {role === 'researcher' && (
                <>
                  {withdrawButton()}
                  {saveButton()}
                  {submitButton()}
                  {status === 'withdrawn' && reactivateButton()}
                </>
              )}
              <Divider orientation="vertical" flexItem />
              <ActionsMenu
                toggleManageTeamDrawer={props.toggleManageTeamDrawer}
                status={props.status}
                role={props.role}
                request={props.assignees}
              />
            </Grid>
          </Grid>
          {/* Request changes dialog */}
          <DialogUpdate
            toggleDialog={() => handleClickClose('dialogUpdate')}
            open={open.dialogUpdate}
          />
          {/* Approve request dialog */}
          <DialogApprove
            toggleDialog={() => handleClickClose('dialogApprove')}
            open={open.dialogApprove}
          />
          {/* Deny request dialog */}
          <DialogDenied
            toggleDialog={() => handleClickClose('dialogDenied')}
            open={open.dialogDenied}
          />
          {/* Withdraw request dialog */}
          <DialogWithdraw
            toggleDialog={() => handleClickClose('dialogWithdraw')}
            open={open.dialogWithdraw}
          />
          {/* Save before leaving dialog */}
          <DialogSaveBeforeLeaving
            toggleDialog={() => handleClickClose('dialogSave')}
            open={open.dialogSave}
          />

          {/* Approve request snackbar */}
          <SnackbarApproveRequest
            open={open.snackbarApprove}
            handleClose={() => handleClickClose('snackbarApprove')}
          />
          {/* Deny request snackbar */}
          <SnackbarDenyRequest
            open={open.snackbarDeny}
            handleClose={() => handleClickClose('snackbarDeny')}
          />
          {/* Save request snackbar */}
          <SnackbarSaveRequest
            open={open.snackbarSave}
            handleClose={() => handleClickClose('snackbarSave')}
          />
          {/* Start review snackbar */}
          <SnackbarStartReview
            open={open.snackbarStart}
            handleClose={() => handleClickClose('snackbarStart')}
          />
          {/* Reactivate request snackbar */}
          <SnackbarReactivateRequest
            open={open.snackbarReactivate}
            handleClose={() => handleClickClose('snackbarReactivate')}
          />
          {/* Submit request snackbar */}
          <SnackbarSubmitRequest
            open={open.snackbarSubmit}
            handleClose={() => handleClickClose('snackbarSubmit')}
          />
        </Toolbar>
      </AppBar>
    </Root>
  );
}

export default RequestToolbar;
