import React from 'react';
import {styled} from '@mui/material/styles';
import {useTranslation} from 'react-i18next';
import {alpha} from '@mui/material/styles';
import {mdiInformationOutline} from '@mdi/js';
import {
  Divider,
  Typography,
  IconButton,
  Button,
  FormControl,
  FormLabel,
  FormControlLabel,
  RadioGroup,
  Radio,
  Tooltip,
  Grid,
  Collapse,
  Paper,
  Box,
} from '@mui/material';
import Alert from '@mui/material/Alert';
import {Card} from '../../../CommonComponents/Card';
import {OutputFile} from './ModifyFile';
import CloseIcon from '@mui/icons-material/Close';
import Icon from '@mdi/react';
import {mdiChevronRight, mdiPlus} from '@mdi/js';
import {
  SnackbarAddOutputFile,
  SnackbarAddSupportFile,
  SnackbarDeleteOutputFile,
  SnackbarDeleteSupportFile,
  SnackbarUpdateOutputFile,
  SnackbarUpdateSupportFile,
} from '../Snackbars';
import {DialogDelete, DialogFileSupport} from '../DialogBox';

const PREFIX = 'FilesList';

const classes = {
  addBtn: `${PREFIX}-addBtn`,
  drawer: `${PREFIX}-drawer`,
  alert: `${PREFIX}-alert`,
  legendInfoIcon: `${PREFIX}-legendInfoIcon`,
  filePath: `${PREFIX}-filePath`,
  filePathItem: `${PREFIX}-filePathItem`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.addBtn}`]: {
    'marginTop': theme.spacing(-1),
    'borderStyle': 'dashed',
    'width': '100%',
    'borderColor': alpha(theme.palette.common.black, 0.23),
    '&.MuiButton-outlinedPrimary:hover': {
      borderStyle: 'dashed',
    },
  },
  [`& .${classes.alert}`]: {
    '& .MuiAlert-action': {
      alignItems: 'start',
    },
  },

  [`& .${classes.legendInfoIcon}`]: {
    marginTop: theme.spacing(-1.25),
    marginLeft: theme.spacing(1),
  },

  [`& .${classes.filePath}`]: {
    display: 'flex',
    flexFlow: 'wrap',
    alignItems: 'flex-end',
  },

  [`& .${classes.filePathItem}`]: {
    display: 'flex',
    alignItems: 'center',
  },
}));

// FAKE DATA
const outputFiles = [
  {
    name: 'Output file for vetting',
    sheet: '{SheetName}',
    path: [
      '{ProjectFolderName}',
      '{RequestFolderName}',
      '{FolderName}',
      '{FolderName}',
      '{OutputFileName}.xls',
    ],
    numErrors: 0,
  },
];

const syntaxFiles = [
  {
    name: 'Syntax or log file',
    path: [
      '{ProjectFolderName}',
      '{RequestFolderName}',
      '{FolderName}',
      '{FolderName}',
      '{SyntaxFileName}.doc',
    ],
    notes:
      'Notes section to include any details regarding the syntax file added. This will be helpful for the Researcher/Analyst during the vetting process.',
    numErrors: 0,
  },
];

const variableFiles = [
  {
    name: 'Variable list and/or variable descriptions',
    path: [
      '{ProjectFolderName}',
      '{RequestFolderName}',
      '{FolderName}',
      '{FolderName}',
      '{VariableFileName}.doc',
    ],
    notes:
      'Notes section to include any details regarding the variable file added. This will be helpful for the Researcher/Analyst during the vetting process.',
    numErrors: 0,
  },
];

const additionalFiles = [];

function FilesList(props) {
  const [open, setOpen] = React.useState({
    snackbarAddSupportFile: false,
    snackbarUpdateSupportFile: false,
    snackbarCreate: false,
    snackbarUpdate: false,
    snackbarOutputDelete: false,
    snackbarSupportDelete: false,

    outputFile: false,
    dialogOutputDelete: false,
    dialogSupportDelete: false,
    dialogAddSupportFile: false,
    dialogUpdateSupportFile: false,
    alert: true,
  });

  const toggleDrawer = (event, drawer) => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }
    setOpen({...open, outputFile: !open.outputFile, drawerType: drawer});
  };

  const handleClickOpen = (state) => {
    setOpen({...open, [state]: true});
  };

  const handleClickClose = (state) => {
    setOpen({...open, [state]: false});
  };

  const createFile = () => {
    setOpen({...open, outputFile: false});
    // DELAY ADDED SO SNACKBAR WILL BE ANNOUNCED AFTER DRAWER CLOSES
    setTimeout(function() {
      setOpen({...open, snackbarCreate: true, outputFile: false});
    }, 10);
  };

  const updateFile = () => {
    setOpen({...open, outputFile: false});
    // DELAY ADDED SO SNACKBAR WILL BE ANNOUNCED AFTER DRAWER CLOSES
    setTimeout(function() {
      setOpen({...open, snackbarUpdate: true, outputFile: false});
    }, 10);
  };

  const deleteOutputFile = () => {
    setOpen({...open, snackbarOutputDelete: true, dialogOutputDelete: false});
  };

  const deleteSupportFile = () => {
    setOpen({
      ...open,
      snackbarSupportDelete: true,
      dialogSupportDelete: false,
    });
  };

  const submitDialog = (e, dialog) => {
    e.preventDefault();
    e.stopPropagation();
    if (dialog === 'add') {
      setOpen({...open, dialogAddSupportFile: false});
      setTimeout(function() {
        setOpen({
          ...open,
          dialogAddSupportFile: false,
          snackbarAddSupportFile: true,
        });
      }, 500);
    } else if (dialog === 'edit') {
      setOpen({...open, dialogUpdateSupportFile: false});
      setTimeout(function() {
        setOpen({
          ...open,
          dialogUpdateSupportFile: false,
          snackbarUpdateSupportFile: true,
        });
      }, 500);
    }
  };

  return (
    <Root>
      <Typography component="h2" variant="h6" sx={{mb: 3}}>
        Output files for vetting
      </Typography>
      <Typography component="p" variant="body1" sx={{mb: 3}}>
        Please add and prepare your files for vetting. You will not have access
        to your files from within this request and are simply adding the file
        path to help Analysts locate the file on your virtual machine. If your
        request is approved you will be granted access to your files outside of
        your secure environment.
      </Typography>
      <Divider sx={{mb: 3}} />
      <Typography component="h3" variant="subtitle2" sx={{mb: 3}}>
        Screening questions
      </Typography>
      <Collapse in={open.alert}>
        <Alert
          role="note"
          severity="info"
          className={classes.alert}
          sx={{mb: 3}}
          action={
            <IconButton
              aria-label="Close"
              onClick={() => {
                handleClickClose('alert');
              }}
              size="large"
            >
              <CloseIcon fontSize="small" />
            </IconButton>
          }
        >
          <Typography component="p" variant="body2" sx={{mb: 2}}>
            Please consider the following guidelines:
          </Typography>
          <Box sx={{mb: 2}}>
            <ul>
              <li>
                <Typography variant="body2">
                  Check your output against the vetting guidelines.
                </Typography>
              </li>
              <li>
                <Typography variant="body2">
                  Delete values you do not need released at this time.
                </Typography>
              </li>
            </ul>
          </Box>
          <Typography component="p" variant="body2">
            This request will be stored as part of the request record.
          </Typography>
        </Alert>
      </Collapse>
      <Box sx={{mb: 2}}>
        <FormControl component="fieldset" required>
          <FormLabel component="legend">
            Is the requested output consistent with the approved proposal for
            this project?
            <span className="screen-reader-text">required</span>
          </FormLabel>
          <RadioGroup id="approvedProposal" name="approvedProposal">
            <FormControlLabel
              value="Yes"
              control={<Radio color="primary" />}
              label="Yes"
            />
            <FormControlLabel
              value="No"
              control={<Radio color="primary" />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Box>
      <Box sx={{mb: 2}}>
        <FormControl component="fieldset" required>
          <FormLabel component="legend">
            Have you checked the vetting rules to determine if there are
            geographical, institutional, household size and/or population
            requirements for your output?
            <span className="screen-reader-text">required</span>
          </FormLabel>
          <RadioGroup id="vettingRules" name="vettingRules">
            <FormControlLabel
              value="Yes"
              control={<Radio color="primary" />}
              label="Yes"
            />
            <FormControlLabel
              value="No"
              control={<Radio color="primary" />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Box>
      <Grid container>
        <Grid xs item>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend">
                Is the requested output your final output?
                <span className="screen-reader-text">required</span>
              </FormLabel>
              <RadioGroup id="finalOutput" name="finalOutput">
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Box>
        </Grid>
        <Grid component="span" className={classes.legendInfoIcon} item>
          <Tooltip
            title="If no, future vetting release requests under this contract may be restricted due to residual disclosure. You are strongly encouraged to consult with your Analyst."
            arrow="true"
          >
            <IconButton aria-label="More information" edge="end">
              <Icon path={mdiInformationOutline} size={1} />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
      <Divider sx={{mb: 3}} />
      {/* OUTPUT FILES */}
      <Grid container>
        <Grid xs item>
          <Typography component="h3" variant="subtitle2" sx={{mb: 3}}>
            Output files
          </Typography>
        </Grid>
        <Grid component="span" className={classes.legendInfoIcon} item>
          <Tooltip
            title="Add the files containing your statistical output that you would like to have vetted and released from the secure environment."
            arrow="true"
          >
            <IconButton aria-label="More information" edge="end">
              <Icon path={mdiInformationOutline} size={1} />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
      <Typography id="output-file-header" variant="body2">
        Add output file for vetting... <span aria-hidden="true">*</span>
        <span className="screen-reader-text">required</span>
      </Typography>
      <Typography
        variant="caption"
        color="textSecondary"
        component="p"
        sx={{mb: 2}}
      >
        At least one file must be added
      </Typography>
      <Paper className="paper-grey" sx={{mb: 3}} variant="outlined">
        {outputFiles.length > 0 ? (
          outputFiles.map((file, index) => (
            <OutputFileCard
              key={`output-file-${index}`}
              file={file}
              index={index}
              role={props.role}
              toggleDrawer={toggleDrawer}
              handleClickOpen={handleClickOpen}
            />
          ))
        ) : (
          <Typography variant="body2" component="p" color="textSecondary">
            No output files added
          </Typography>
        )}
      </Paper>
      {props.role === 'researcher' && (
        <Button
          aria-describedby="output-file-header"
          variant="outlined"
          color="primary"
          startIcon={<Icon path={mdiPlus} size={1} />}
          fullWidth={true}
          className={classes.addBtn}
          onClick={(e) => toggleDrawer(e, 'addFile', true)}
        >
          Add output file
        </Button>
      )}
      <Divider sx={{mb: 3, mt: 3}} />
      {/* MANDATORY SUPPORTING FILES */}
      <Grid container>
        <Grid xs item>
          <Typography component="h3" variant="subtitle2" sx={{mb: 3}}>
            Mandatory supporting files
          </Typography>
        </Grid>
        <Grid component="span" className={classes.legendInfoIcon} item>
          <Tooltip
            title="These files are to support the vetting request and will not be released."
            arrow="true"
          >
            <IconButton aria-label="More information" edge="end">
              <Icon path={mdiInformationOutline} size={1} />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
      <Typography id="syntax-file-header" variant="body2">
        Add syntax or log file... <span aria-hidden="true">*</span>
        <span className="screen-reader-text">required</span>
      </Typography>
      <Typography
        variant="caption"
        color="textSecondary"
        component="p"
        sx={{mb: 2}}
      >
        At least one file must be added
      </Typography>
      <Paper className="paper-grey" sx={{mb: 3}} variant="outlined">
        {syntaxFiles.length > 0 ? (
          syntaxFiles.map((file, index) => (
            <SupportingFileCard
              key={`syntax-file-${index}`}
              file={file}
              index={index}
              role={props.role}
              handleClickOpen={handleClickOpen}
            />
          ))
        ) : (
          <Typography variant="body2" component="p" color="textSecondary">
            No support files added
          </Typography>
        )}
      </Paper>
      {props.role === 'researcher' && (
        <Button
          aria-describedby="syntax-file-header"
          variant="outlined"
          color="primary"
          startIcon={<Icon path={mdiPlus} size={1} />}
          fullWidth={true}
          className={classes.addBtn}
          onClick={(e) => handleClickOpen('dialogAddSupportFile')}
        >
          Add support file
        </Button>
      )}
      <Grid container sx={{mt: 3}}>
        <Grid xs item>
          <Typography id="var-file-header" variant="body2">
            Add variable list and/or variable descriptions...{' '}
            <span aria-hidden="true">*</span>
            <span className="screen-reader-text">required</span>
          </Typography>
          <Typography
            variant="caption"
            color="textSecondary"
            component="p"
            sx={{mb: 2}}
          >
            At least one file must be added
          </Typography>
        </Grid>
        <Grid component="span" className={classes.legendInfoIcon} item>
          <Tooltip
            title="Include information for derived or recoded variables (e.g. syntax, codebook, description)."
            arrow="true"
          >
            <IconButton aria-label="More information" edge="end">
              <Icon path={mdiInformationOutline} size={1} />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
      <Paper className="paper-grey" sx={{mb: 3}} variant="outlined">
        {variableFiles.length > 0 ? (
          variableFiles.map((file, index) => (
            <SupportingFileCard
              key={`var-file-${index}`}
              file={file}
              index={index}
              role={props.role}
              handleClickOpen={handleClickOpen}
            />
          ))
        ) : (
          <Typography variant="body2" component="p" color="textSecondary">
            No support files added
          </Typography>
        )}
      </Paper>
      {props.role === 'researcher' && (
        <Button
          aria-describedby="var-file-header"
          variant="outlined"
          color="primary"
          startIcon={<Icon path={mdiPlus} size={1} />}
          fullWidth={true}
          className={classes.addBtn}
          onClick={(e) => handleClickOpen('dialogAddSupportFile')}
        >
          Add support file
        </Button>
      )}
      <Divider sx={{mb: 3, mt: 3}} />
      <Typography component="h3" variant="subtitle2" sx={{mb: 3}}>
        Additional supporting files
      </Typography>
      <Typography id="additional-file-header" variant="body2" sx={{mb: 2}}>
        Add additional support file...
      </Typography>
      <Paper className="paper-grey" sx={{mb: 3}} variant="outlined">
        {additionalFiles.length > 0 ? (
          additionalFiles.map((file, index) => (
            <SupportingFileCard
              key={`additional-file-${index}`}
              file={file}
              index={index}
              role={props.role}
              handleClickOpen={handleClickOpen}
            />
          ))
        ) : (
          <Typography variant="caption" component="p" color="textSecondary">
            No support files added
          </Typography>
        )}
      </Paper>
      {props.role === 'researcher' && (
        <Button
          aria-describedby="additional-file-header"
          variant="outlined"
          color="primary"
          startIcon={<Icon path={mdiPlus} size={1} />}
          fullWidth={true}
          onClick={(e) => handleClickOpen('dialogAddSupportFile')}
          className={classes.addBtn}
          sx={{mb: 3}}
        >
          Add support file
        </Button>
      )}
      {/* Output file drawer */}
      <OutputFile
        toggleDrawer={toggleDrawer}
        updateFile={updateFile}
        createFile={createFile}
        handleClickOpen={handleClickOpen}
        drawerType={open.drawerType}
        outputFile={open.outputFile}
      />
      {/* Add supporting file dialog */}
      <DialogFileSupport
        submitDialog={(e) => {
          submitDialog(e, 'add');
        }}
        toggleDialog={() => handleClickClose('dialogAddSupportFile')}
        open={open.dialogAddSupportFile}
        fileFunction="add"
        snackbar={
          <SnackbarAddSupportFile
            open={open.snackbarAddSupportFile}
            handleClose={() => handleClickClose('snackbarAddSupportFile')}
          />
        }
      />
      {/* Edit supporting file dialog */}
      <DialogFileSupport
        submitDialog={(e) => {
          submitDialog(e, 'edit');
        }}
        toggleDialog={() => handleClickClose('dialogUpdateSupportFile')}
        open={open.dialogUpdateSupportFile}
        fileFunction="edit"
        snackbar={
          <SnackbarUpdateSupportFile
            open={open.snackbarUpdateSupportFile}
            handleClose={() => handleClickClose('snackbarUpdateSupportFile')}
          />
        }
      />
      {/* Delete output file dialog */}
      <DialogDelete
        submitDialog={deleteOutputFile}
        open={open.dialogOutputDelete}
        toggleDialog={() => handleClickClose('dialogOutputDelete')}
        snackbar={
          <SnackbarDeleteOutputFile
            open={open.snackbarOutputDelete}
            handleClose={() => handleClickClose('snackbarOutputDelete')}
          />
        }
      />
      {/* Delete supporting file dialog */}
      <DialogDelete
        submitDialog={deleteSupportFile}
        open={open.dialogSupportDelete}
        toggleDialog={() => handleClickClose('dialogSupportDelete')}
        snackbar={
          <SnackbarDeleteSupportFile
            open={open.snackbarSupportDelete}
            handleClose={() => handleClickClose('snackbarSupportDelete')}
          />
        }
      />
      {/* Create output file snackbar */}
      <SnackbarAddOutputFile
        open={open.snackbarCreate}
        handleClose={() => handleClickClose('snackbarCreate')}
      />
      {/* Update output file snackbar */}
      <SnackbarUpdateOutputFile
        open={open.snackbarUpdate}
        handleClose={() => handleClickClose('snackbarUpdate')}
      />
      {/* Delete output file snackbar */}
      <SnackbarDeleteOutputFile
        open={open.snackbarOutputDelete}
        handleClose={() => handleClickClose('snackbarOutputDelete')}
      />
      {/* Delete supporting file snackbar */}
      <SnackbarDeleteSupportFile
        open={open.snackbarSupportDelete}
        handleClose={() => handleClickClose('snackbarSupportDelete')}
      />
    </Root>
  );
}
export default FilesList;

function OutputFileCard(props) {
  const {t} = useTranslation();
  const {file, index, role, toggleDrawer, handleClickOpen} = props;
  return (
    <Card
      title={`${t('File')} ${index + 1} · ${file.name}`}
      titleComponent="h4"
      error={file.numErrors > 0}
      totalErrors={file.numErrors}
      primaryButton={role === 'researcher' ? 'Edit' : 'Review'}
      secondaryButton={role === 'researcher' ? 'Delete' : ''}
      primaryClick={
        role === 'researcher' ?
          (e) => toggleDrawer(e, 'editFile', true) :
          (e) => toggleDrawer(e, 'viewFile', true)
      }
      secondaryClick={() => handleClickOpen('dialogOutputDelete')}
      content={
        <>
          <Typography variant="caption" component="p" color="textSecondary">
            {t('File path')}
          </Typography>
          <Box className={classes.filePath} sx={{mb: 2}}>
            {file.path.map((dir, index) => {
              const last = index === file.path.length - 1;
              return (
                <div
                  key={`output-path-${index}`}
                  className={classes.filePathItem}
                >
                  <Typography
                    variant="body2"
                    component="p"
                    sx={!last ? {mr: 1} : {}}
                  >
                    {dir}
                  </Typography>
                  {!last && (
                    <Box sx={{mr: 1}}>
                      <Icon path={mdiChevronRight} size={0.5} />
                    </Box>
                  )}
                </div>
              );
            })}
          </Box>
          <Typography variant="caption" color="textSecondary" component="p">
            Sheet name
          </Typography>
          <Typography variant="body2">{file.sheet}</Typography>
        </>
      }
    />
  );
}

function SupportingFileCard(props) {
  const {t} = useTranslation();
  const {file, index, role, handleClickOpen} = props;
  return (
    <Card
      title={`${t('File')} ${index + 1} · ${file.name}`}
      titleComponent="h4"
      error={file.numErrors > 0}
      totalErrors={file.numErrors}
      primaryButton={role === 'researcher' ? 'Edit' : ''}
      secondaryButton={role === 'researcher' ? 'Delete' : ''}
      primaryClick={() => handleClickOpen('dialogUpdateSupportFile')}
      secondaryClick={() => handleClickOpen('dialogSupportDelete')}
      content={
        <>
          <Typography variant="caption" component="p" color="textSecondary">
            {t('File path')}
          </Typography>
          <Box className={classes.filePath} sx={{mb: 2}}>
            {file.path.map((dir, index) => {
              const last = index === file.path.length - 1;
              return (
                <div
                  key={`support-path-${index}`}
                  className={classes.filePathItem}
                >
                  <Typography
                    variant="body2"
                    component="p"
                    sx={!last ? {mr: 1} : {}}
                  >
                    {dir}
                  </Typography>
                  {!last && (
                    <Box sx={{mr: 1}}>
                      <Icon path={mdiChevronRight} size={0.5} />
                    </Box>
                  )}
                </div>
              );
            })}
          </Box>
          <Typography variant="caption" component="p" color="textSecondary">
            {t('Notes')}
          </Typography>
          <Typography variant="body2" component="p">
            {file.notes}
          </Typography>
        </>
      }
    />
  );
}
