import React from 'react';
import {styled} from '@mui/material/styles';
import {mdiInformationOutline} from '@mdi/js';
import {useTranslation} from 'react-i18next';
import {alpha} from '@mui/material/styles';
import clsx from 'clsx';
import {
  FormControl,
  FormHelperText,
  TextField,
  FormLabel,
  RadioGroup,
  FormGroup,
  FormControlLabel,
  Radio,
  Checkbox,
  Typography,
  Button,
  IconButton,
  Link,
  Divider,
  Paper,
  Grid,
  Tooltip,
  Box,
  Drawer,
} from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import CloseIcon from '@mui/icons-material/Close';
import Icon from '@mdi/react';
import {mdiChevronRight, mdiPlus} from '@mdi/js';
import {
  DialogOutputMethodHelp,
  DialogFileSupport,
  DialogDelete,
} from '../DialogBox';
import {
  SnackbarAddSupportFile,
  SnackbarDeleteSupportFile,
  SnackbarUpdateSupportFile,
} from '../Snackbars';
import {Card} from '../../../../Components/CommonComponents/Card';

const PREFIX = 'ModifyFile';

const classes = {
  drawerHeader: `${PREFIX}-drawerHeader`,
  drawerContent: `${PREFIX}-drawerContent`,
  footer: `${PREFIX}-footer`,
  addBtn: `${PREFIX}-addBtn`,
  mt2: `${PREFIX}-mt2`,
  autocompleteOption: `${PREFIX}-autocompleteOption`,
  filePath: `${PREFIX}-filePath`,
  filePathItem: `${PREFIX}-filePathItem`,
  indentedSection: `${PREFIX}-indentedSection`,
  legendInfoIcon: `${PREFIX}-legendInfoIcon`,
  textFieldInfoIcon: `${PREFIX}-textFieldInfoIcon`,
};

const StyledDrawer = styled(Drawer)(({theme}) => ({
  '& .MuiDrawer-paper.MuiDrawer-paper': {
    width: '100%',
    maxWidth: theme.spacing(75),
    boxSizing: 'border-box',
  },
  [`& .${classes.drawerHeader}`]: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: theme.palette.common.white,
    margin: theme.spacing(0, -3, 3, 0),
    boxShadow: theme.shadows[0],
    borderBottom: '1px solid',
    borderBottomColor: theme.palette.divider,
    position: 'fixed',
    top: 0,
    zIndex: 500,
    width: '100%',
    maxWidth: theme.spacing(75),
    boxSizing: 'border-box',
    padding: theme.spacing(1.5, 3),
  },

  [`& .${classes.drawerContent}`]: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(8),
    padding: theme.spacing(3),
    overflowY: 'auto',
    overflowX: 'hidden',
    height: '100%',
  },

  [`& .${classes.footer}`]: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginRight: theme.spacing(-3),
    padding: theme.spacing(1.75, 3),
    borderTop: '1px solid',
    borderTopColor: theme.palette.divider,
    position: 'fixed',
    bottom: 0,
    width: '100%',
    maxWidth: theme.spacing(75),
    boxSizing: 'border-box',
    backgroundColor: theme.palette.common.white,
    zIndex: 500,
  },

  [`& .${classes.addBtn}`]: {
    'marginTop': theme.spacing(-1),
    'borderStyle': 'dashed',
    'width': '100%',
    'borderColor': alpha(theme.palette.common.black, 0.23),
    '&.MuiButton-outlinedPrimary:hover': {
      borderStyle: 'dashed',
    },
  },

  [`& .${classes.mt2}`]: {
    marginTop: theme.spacing(2),
  },
  [`& .${classes.autocompleteOption}`]: {
    whiteSpace: 'normal',
    wordBreak: 'break-all',
  },

  [`& .${classes.filePath}`]: {
    display: 'flex',
    flexFlow: 'wrap',
    alignItems: 'flex-end',
  },

  [`& .${classes.filePathItem}`]: {
    display: 'flex',
    alignItems: 'center',
  },

  [`& .${classes.indentedSection}`]: {
    display: 'inline-block',
    width: '100%',
    boxSizing: 'border-box',
    paddingTop: theme.spacing(3),
    paddingLeft: theme.spacing(3),
    borderLeft: '1px solid',
    borderLeftColor: theme.palette.divider,
  },

  [`& .${classes.legendInfoIcon}`]: {
    marginTop: theme.spacing(-1.25),
    marginLeft: theme.spacing(1),
  },

  [`& .${classes.textFieldInfoIcon}`]: {
    paddingLeft: theme.spacing(1),
  },
}));

const outputMethods = {
  '1.Descriptive': [
    'ANOVA',
    'Correlation matrix',
    'Cross-tabular analysis',
    'Distributions',
    'Frequencies',
    'Kurtosis',
    'Means',
    'Medians',
    'Modes',
    'Percentages',
    'Quartiles',
    'Ranges',
    'Ratios',
    'Regression models with only one independent variable',
    'Skewness',
    'Standard deviations',
    'Totals',
    'Variances',
  ],
  '2.Scaling': ['Factor Analysis'],
  '3.Graphs': ['Histograms'],
  '4.Multivariable regression analysis': [
    'Logistic Regression',
    'OLS',
    'Poisson',
    'Probit',
    'Tobit',
  ],
  '5.Complex modeling': [
    'Event History Analysis',
    'Fixed Effects Models',
    'Growth Analysis',
    'Hierarchical Linear Modeling',
    'Random Effects Models',
    'Simultaneous-Equations Models',
    'Structural equation modeling',
    'Survival Analysis',
  ],
  '6.Other': [],
};

const outputMethodsTerms = [];
for (const [key, value] of Object.entries(outputMethods)) {
  if (value.length > 0) {
    for (const term of value) {
      outputMethodsTerms.push({method: key, term: term});
    }
  }
}

export function OutputFile(props) {
  return (
    <StyledDrawer
      anchor="right"
      open={props.outputFile}
      className={classes.drawer}
    >
      <div className={classes.drawerHeader}>
        <Typography variant="h6" component="h2" className={classes.title}>
          {props.drawerType === 'addFile' ?
            'Add output file' :
            props.drawerType === 'editFile' ?
            'Edit output file' :
            'Review output file'}
        </Typography>
        <IconButton
          aria-label="Close"
          className={classes.margin}
          edge="end"
          onClick={(e) => props.toggleDrawer(e, props.drawerType)}
          size="large"
        >
          <CloseIcon />
        </IconButton>
      </div>
      <div className={classes.drawerContent}>
        <AddFileForm {...props} />
      </div>
      {props.drawerType === 'addFile' && (
        <div className={classes.footer}>
          <Button
            sx={{mr: 2}}
            variant="outlined"
            color="primary"
            onClick={(e) => props.toggleDrawer(e, props.drawerType)}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={props.createFile}
          >
            Add
          </Button>
        </div>
      )}
      {props.drawerType === 'editFile' && (
        <div className={classes.footer}>
          <Button
            sx={{mr: 2}}
            variant="outlined"
            color="primary"
            onClick={(e) => props.toggleDrawer(e, props.drawerType)}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={props.updateFile}
          >
            Update
          </Button>
        </div>
      )}
      {props.drawerType === 'viewFile' && (
        <div className={classes.footer}>
          <Button
            variant="contained"
            color="primary"
            onClick={(e) => props.toggleDrawer(e, props.drawerType)}
          >
            Close
          </Button>
        </div>
      )}
    </StyledDrawer>
  );
}

function AddFileForm(props) {
  const {t} = useTranslation();

  const [state, setState] = React.useState({
    researcher: () => {
      if (props.drawerType === 'addFile' || props.drawerType === 'editFile') {
        return true;
      } else {
        return false;
      }
    },
    includeWeightVariable: null,
    linkedData: null,
    descriptiveStats: null,
    covariance: null,
    dollarIncluded: null,
    equivalentDescriptiveStats: null,
    matrixContinuous: null,
    matrixDichotomous: null,
    matrixCorrelated: null,
    roundingOutput: null,
    snackbarSupportDelete: false,
    snackbarAddSupportFile: false,
    snackbarUpdateSupportFile: false,
    dialogOutputMethodHelp: false,
    dialogSupportDelete: false,
    dialogAddSupportFile: false,
    dialogUpdateSupportFile: false,
    path: null,
    sheetname: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
      helperText: 'For spreadsheets only. Add a file for each sheet.',
    },
    survey: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
      helperText: 'Seperate by semicolon (e.g. LFS 2012; LFS 2011; CCHS 2009)',
    },
    outputMethodDesc: false,
    outputMethodScale: false,
    outputMethodGraph: false,
    outputMethodMulti: false,
    outputMethodModel: false,
    outputMethodOther: false,
    outputMethodDescription: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
      helperText: '',
    },
    weightvariable: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
      helperText: '',
    },
    sample: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
      helperText: '(e.g. Males 50 years of age or older)',
    },
    geography: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
      helperText: '(e.g. National, provincial, municipal)',
    },
    linkage: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
      helperText: '(e.g. Person based, record based, matching geographies)',
    },
    rounding: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
      helperText: '',
    },
  });

  const handleClickOpen = (element) => {
    setState({...state, [element]: true});
  };

  const handleClickClose = (element) => {
    setState({...state, [element]: false});
  };

  const handleRadioChange = (event) => {
    const name = event.target.name;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  const handleChbxChange = (event) => {
    setState({...state, [event.target.name]: event.target.checked});
  };

  const deleteSupportFile = () => {
    setState({
      ...state,
      snackbarSupportDelete: true,
      dialogSupportDelete: false,
    });
  };

  const submitDialog = (e, dialog) => {
    e.preventDefault();
    e.stopPropagation();
    if (dialog === 'add') {
      setState({...state, dialogAddSupportFile: false});
      setTimeout(function() {
        setState({
          ...state,
          dialogAddSupportFile: false,
          snackbarAddSupportFile: true,
        });
      }, 500);
    } else if (dialog === 'edit') {
      setState({...state, dialogUpdateSupportFile: false});
      setTimeout(function() {
        setState({
          ...state,
          dialogUpdateSupportFile: false,
          snackbarUpdateSupportFile: true,
        });
      }, 500);
    }
  };

  return (
    <>
      <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
        {t('Select output file for vetting')}
      </Typography>
      <Autocomplete
        sx={{mb: 3}}
        componentsProps={{
          clearIndicator: {tabIndex: 0},
        }}
        classes={{
          option: classes.autocompleteOption,
        }}
        id="filePath"
        options={[
          'spreadsheet1.xls',
          'spreadsheet2.doc',
          'spreadsheet3.xls',
          'Project1\\VettingRequests\\User_Name\\Files for output\\a_really_long_path_name_spreadsheet4.xls',
        ]}
        renderInput={(params) => (
          <TextField
            {...params}
            multiline
            minRows="1"
            label="File path"
            required={true}
            variant="outlined"
          />
        )}
        value={state.path}
        onChange={(event, newValue) => {
          setState({...state, path: newValue});
        }}
      />
      {Boolean(state.path) && (
        <Box className={classes.indentedSection} sx={{mb: 3}}>
          <Box sx={{mb: 3}}>
            <TextField
              id="sheet-name"
              label={t('Sheet name')}
              fullWidth
              margin="dense"
              variant="outlined"
              required
              autoComplete="off"
            />
          </Box>
        </Box>
      )}
      <Divider sx={{mb: 3}} />
      <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
        {t('Output details')}
      </Typography>
      <Box sx={{mb: 2}}>
        <FormControl
          component="fieldset"
          required
          aria-describedby="output-meth-helper"
        >
          <FormLabel component="legend" id="output-meth-legend">
            {t('Output method')}{' '}
            <span className="screen-reader-text">required</span>
          </FormLabel>
          <FormHelperText id="output-meth-helper">
            {t('Need help?')}{' '}
            <Link
              underline="always"
              variant="caption"
              color="inherit"
              onClick={() => handleClickOpen('dialogOutputMethodHelp')}
              component="button"
            >
              {t('Learn about output methods')}
            </Link>
          </FormHelperText>
          <FormGroup role="group" aria-labelledby="output-meth-legend">
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  checked={state.outputMethodDesc}
                  onChange={handleChbxChange}
                  name="outputMethodDesc"
                />
              }
              label={
                <>
                  <Typography variant="body2" component="span">
                    Descriptive
                  </Typography>
                </>
              }
            />
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  checked={state.outputMethodScale}
                  onChange={handleChbxChange}
                  name="outputMethodScale"
                />
              }
              label={
                <>
                  <Typography variant="body2" component="span">
                    Scaling
                  </Typography>
                </>
              }
            />
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  checked={state.outputMethodGraph}
                  onChange={handleChbxChange}
                  name="outputMethodGraph"
                />
              }
              label={
                <>
                  <Typography variant="body2" component="span">
                    Graphs
                  </Typography>
                </>
              }
            />
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  checked={state.outputMethodMulti}
                  onChange={handleChbxChange}
                  name="outputMethodMulti"
                />
              }
              label={
                <>
                  <Typography variant="body2" component="span">
                    Multivariable regression analysis
                  </Typography>
                </>
              }
            />
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  checked={state.outputMethodModel}
                  onChange={handleChbxChange}
                  name="outputMethodModel"
                />
              }
              label={
                <>
                  <Typography variant="body2" component="span">
                    Complex modeling
                  </Typography>
                </>
              }
            />
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  checked={state.outputMethodOther}
                  onChange={handleChbxChange}
                  name="outputMethodOther"
                />
              }
              label={
                <>
                  <Typography variant="body2" component="span">
                    Other
                  </Typography>
                </>
              }
            />
          </FormGroup>
        </FormControl>
      </Box>
      {state.outputMethodOther && (
        <Box className={classes.indentedSection} sx={{mb: 3}}>
          <Box sx={{mb: 3}}>
            <TextField
              margin="dense"
              id="outputMethodDescription"
              label={t('Output method description')}
              variant="outlined"
              fullWidth
              required
              multiline
              minRows="3"
              defaultValue={state.outputMethodDescription.text}
              error={Boolean(state.outputMethodDescription.errorText)}
              helperText={state.outputMethodDescription.errorText}
              autoComplete="off"
            />
          </Box>
        </Box>
      )}
      <Box sx={{mb: 3}}>
        <TextField
          margin="dense"
          id="datasetName"
          label={t('Surveys/Datasets and cycles')}
          variant="outlined"
          fullWidth
          required
          defaultValue={state.survey.text}
          error={Boolean(state.survey.errorText)}
          helperText={state.survey.helperText}
          autoComplete="off"
        />
      </Box>
      <Grid sx={{mb: 3}} container>
        <Grid xs item>
          <TextField
            margin="dense"
            id="sample-input"
            label={t('Sample, sub-sample, inclusion/exclusion')}
            variant="outlined"
            fullWidth
            required
            defaultValue={state.sample.text}
            error={Boolean(state.sample.errorText)}
            helperText={state.sample.helperText}
            autoComplete="off"
          />
        </Grid>
        <Grid component="span" className={classes.textFieldInfoIcon} item>
          <Tooltip
            title="Describe each of the different samples, sub-samples, or inclusions/exclusions used to produce your output."
            arrow="true"
          >
            <IconButton aria-label="More information" edge="end">
              <Icon path={mdiInformationOutline} size={1} />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
      <Box sx={{mb: 3}}>
        <TextField
          margin="dense"
          id="geography-input"
          label={t('Geography level')}
          variant="outlined"
          fullWidth
          required
          defaultValue={state.geography.text}
          error={Boolean(state.geography.errorText)}
          helperText={state.geography.helperText}
          autoComplete="off"
        />
      </Box>
      <Divider sx={{mb: 3}} />
      <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
        {t('Weighted variable')}
      </Typography>
      <Box sx={{mb: 2}}>
        <FormControl component="fieldset" required>
          <FormLabel component="legend">
            {t('Does this output include a weight variable?')}
            <span className="screen-reader-text">{t('required')}</span>
          </FormLabel>
          <RadioGroup
            id="includeWeightVariable"
            value={state.includeWeightVariable}
            name="includeWeightVariable"
            onChange={handleRadioChange}
          >
            <FormControlLabel
              value="Yes"
              control={<Radio color="primary" />}
              label="Yes"
            />
            <FormControlLabel
              value="No"
              control={<Radio color="primary" />}
              label="No"
            />
          </RadioGroup>
          <FormHelperText></FormHelperText>
        </FormControl>
      </Box>
      {state.includeWeightVariable === 'Yes' && (
        <Box className={classes.indentedSection} sx={{mb: 3}}>
          <Box sx={{mb: 3}}>
            <TextField
              margin="dense"
              id="weightVariableName"
              label={t('Weighted variable name')}
              variant="outlined"
              required
              fullWidth
              defaultValue={state.weightvariable.text}
              error={Boolean(state.weightvariable.errorText)}
              helperText={state.weightvariable.errorText}
              autoComplete="off"
            />
          </Box>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend">
                {t('Was the weighted variable scaled/normalized?')}
                <span className="screen-reader-text">{t('required')}</span>
              </FormLabel>
              <RadioGroup id="weightVariableType">
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
              <FormHelperText></FormHelperText>
            </FormControl>
          </Box>
        </Box>
      )}
      <Divider sx={{mb: 3}} />
      <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
        {t('Linked data')}
      </Typography>
      <Box sx={{mb: 2}}>
        <FormControl component="fieldset" required>
          <FormLabel component="legend">
            {t('Is linked data used?')}
            <span className="screen-reader-text">{t('required')}</span>
          </FormLabel>
          <RadioGroup
            id="linkedData"
            value={state.linkedData}
            name="linkedData"
            onChange={handleRadioChange}
          >
            <FormControlLabel
              value="Yes"
              control={<Radio color="primary" />}
              label="Yes"
            />
            <FormControlLabel
              value="No"
              control={<Radio color="primary" />}
              label="No"
            />
          </RadioGroup>
          <FormHelperText></FormHelperText>
        </FormControl>
      </Box>
      {state.linkedData === 'Yes' && (
        <Box className={classes.indentedSection} sx={{mb: 3}}>
          <Box sx={{mb: 3}}>
            <TextField
              margin="dense"
              id="linkageDescription"
              label={t('Linkage method')}
              variant="outlined"
              fullWidth
              required
              defaultValue={state.linkage.text}
              error={Boolean(state.linkage.errorText)}
              helperText={state.linkage.helperText}
              autoComplete="off"
            />
          </Box>
        </Box>
      )}
      <Divider sx={{mb: 3}} />
      <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
        {t('Financial variables')}
      </Typography>
      <Grid container>
        <Grid xs item>
          <Box sx={{mb: 2}}>
            <FormControl
              component="fieldset"
              required
              error={Boolean(props.errors)}
            >
              <FormLabel component="legend" className={classes.tooltipLabel}>
                {t(
                    'Are variables related to income, earnings, tax and/or dollar values included?',
                )}
                <span className="screen-reader-text">{t('required')}</span>
              </FormLabel>
              <RadioGroup
                id="dollarIncluded"
                value={state.dollarIncluded}
                name="dollarIncluded"
                onChange={handleRadioChange}
              >
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
              <FormHelperText></FormHelperText>
            </FormControl>
          </Box>
        </Grid>
        <Grid component="span" className={classes.legendInfoIcon} item>
          <Tooltip
            title="If yes, check the vetting guidelines and requirements for these kinds of variables. Consult your Analyst if needed."
            arrow="true"
          >
            <IconButton aria-label="More information" edge="end">
              <Icon path={mdiInformationOutline} size={1} />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
      {state.dollarIncluded === 'Yes' && (
        <Box className={classes.indentedSection} sx={{mb: 3}}>
          <Typography id="counts-file-header" variant="body2" component="p">
            {t('Add unweighted supporting sample counts... ')}
            <span aria-hidden="true">*</span>
            <span className="screen-reader-text">{t('required')}</span>
          </Typography>
          <Typography
            variant="caption"
            color="textSecondary"
            component="p"
            sx={{mb: 2}}
          >
            {t('At least one file must be added')}
          </Typography>
          {state.researcher() ? (
            <>
              <Paper
                sx={{mb: 3}}
                className={clsx('paper-grey', classes.cardContainer)}
                variant="outlined"
              >
                <Card
                  title={t('File 1 · Unweighted supporting sample counts')}
                  titleComponent="h4"
                  error={false}
                  primaryButton={t('Edit')}
                  secondaryButton={t('Delete')}
                  primaryClick={() =>
                    handleClickOpen('dialogUpdateSupportFile')
                  }
                  secondaryClick={() => handleClickOpen('dialogSupportDelete')}
                  content={
                    <>
                      <Typography
                        variant="caption"
                        component="p"
                        color="textSecondary"
                      >
                        {t('File path')}
                      </Typography>
                      <Box className={classes.filePath} sx={{mb: 2}}>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'Project folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'Request folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'First level folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'Second level folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography variant="body2" component="p">
                            {'Suporting file name example.doc'}
                          </Typography>
                        </div>
                      </Box>
                      <Typography
                        variant="caption"
                        component="p"
                        color="textSecondary"
                      >
                        {t('Notes')}
                      </Typography>
                      <Typography variant="body2" component="p">
                        {t(
                            'Notes section to include any details regarding the syntax file added. This will be helpful for the Researcher/Analyst during the vetting process.',
                        )}
                      </Typography>
                    </>
                  }
                />
                <Card
                  title={t('File 2 · Unweighted supporting sample counts')}
                  titleComponent="h4"
                  error={false}
                  primaryButton={t('Edit')}
                  secondaryButton={t('Delete')}
                  primaryClick={() =>
                    handleClickOpen('dialogUpdateSupportFile')
                  }
                  secondaryClick={() => handleClickOpen('dialogSupportDelete')}
                  content={
                    <>
                      <Typography
                        variant="caption"
                        component="p"
                        color="textSecondary"
                      >
                        {t('File path')}
                      </Typography>
                      <Box className={classes.filePath} sx={{mb: 2}}>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'Project folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'Request folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'First level folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'Second level folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography variant="body2" component="p">
                            {'Suporting file name example.doc'}
                          </Typography>
                        </div>
                      </Box>
                      <Typography
                        variant="caption"
                        component="p"
                        color="textSecondary"
                      >
                        {t('Notes')}
                      </Typography>
                      <Typography variant="body2" component="p">
                        {t(
                            'Notes section to include any details regarding the syntax file added. This will be helpful for the Researcher/Analyst during the vetting process.',
                        )}
                      </Typography>
                    </>
                  }
                />
              </Paper>
            </>
          ) : (
            <>
              <Paper
                sx={{mb: 3}}
                className={clsx('paper-grey', classes.cardContainer)}
                variant="outlined"
              >
                <Card
                  title={t('File 1 · Unweighted supporting sample counts')}
                  titleComponent="h4"
                  error={false}
                  content={
                    <>
                      <Typography
                        variant="caption"
                        component="p"
                        color="textSecondary"
                      >
                        {t('File path')}
                      </Typography>
                      <Box className={classes.filePath} sx={{mb: 2}}>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'Project folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'Request folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'First level folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'Second level folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography variant="body2" component="p">
                            {'Suporting file name example.doc'}
                          </Typography>
                        </div>
                      </Box>
                      <Typography
                        variant="caption"
                        component="p"
                        color="textSecondary"
                      >
                        {t('Notes')}
                      </Typography>
                      <Typography variant="body2" component="p">
                        {t(
                            'Notes section to include any details regarding the syntax file added. This will be helpful for the Researcher/Analyst during the vetting process.',
                        )}
                      </Typography>
                    </>
                  }
                />
                <Card
                  title={t('File 2 · Unweighted supporting sample counts')}
                  titleComponent="h4"
                  error={false}
                  content={
                    <>
                      <Typography
                        variant="caption"
                        component="p"
                        color="textSecondary"
                      >
                        {t('File path')}
                      </Typography>
                      <Box className={classes.filePath} sx={{mb: 2}}>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'Project folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'Request folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'First level folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography
                            variant="body2"
                            component="p"
                            sx={{mr: 1}}
                          >
                            {'Second level folder example'}
                          </Typography>
                          <Box sx={{mr: 1}}>
                            <Icon path={mdiChevronRight} size={0.5} />
                          </Box>
                        </div>
                        <div className={classes.filePathItem}>
                          <Typography variant="body2" component="p">
                            {'Suporting file name example.doc'}
                          </Typography>
                        </div>
                      </Box>
                      <Typography
                        variant="caption"
                        component="p"
                        color="textSecondary"
                      >
                        {t('Notes')}
                      </Typography>
                      <Typography variant="body2" component="p">
                        {t(
                            'Notes section to include any details regarding the syntax file added. This will be helpful for the Researcher/Analyst during the vetting process.',
                        )}
                      </Typography>
                    </>
                  }
                />
              </Paper>
            </>
          )}
          {state.researcher() && (
            <Button
              aria-describedby="counts-file-header"
              variant="outlined"
              color="primary"
              startIcon={<Icon path={mdiPlus} size={1} />}
              onClick={() => handleClickOpen('dialogAddSupportFile')}
              className={classes.addBtn}
              sx={{mb: 3}}
            >
              {t('Add support file')}
            </Button>
          )}
          <Typography id="var-syntax-file-header" variant="body2" component="p">
            {t(
                'Add variable creation syntax, analysis syntax and vetting test syntax... ',
            )}
            <span aria-hidden="true">*</span>
            <span className="screen-reader-text">{t('required')}</span>
          </Typography>
          <Typography
            variant="caption"
            color="textSecondary"
            component="p"
            sx={{mb: 2}}
          >
            {t('At least one file must be added')}
          </Typography>
          <Paper
            sx={{mb: 3}}
            className={clsx('paper-grey', classes.cardContainer)}
            variant="outlined"
          >
            <Typography variant="caption" component="p" color="textSecondary">
              No support files added
            </Typography>
          </Paper>
          {state.researcher() && (
            <Button
              aria-describedby="var-syntax-file-header"
              variant="outlined"
              color="primary"
              startIcon={<Icon path={mdiPlus} size={1} />}
              onClick={() => handleClickOpen('dialogAddSupportFile')}
              className={classes.addBtn}
              sx={{mb: 3}}
            >
              {t('Add support file')}
            </Button>
          )}
          <Typography id="results-file-header" variant="body2" component="p">
            {t('Add vetting test results... ')}
            <span aria-hidden="true">*</span>
            <span className="screen-reader-text">{t('required')}</span>
          </Typography>
          <Typography
            variant="caption"
            color="textSecondary"
            component="p"
            sx={{mb: 2}}
          >
            {t('At least one file must be added')}
          </Typography>
          <Paper
            sx={{mb: 3}}
            className={clsx('paper-grey', classes.cardContainer)}
            variant="outlined"
          >
            <Typography variant="caption" component="p" color="textSecondary">
              No support files added
            </Typography>
          </Paper>
          {state.researcher() && (
            <Button
              aria-describedby="results-file-header"
              variant="outlined"
              color="primary"
              startIcon={<Icon path={mdiPlus} size={1} />}
              onClick={() => handleClickOpen('dialogAddSupportFile')}
              className={classes.addBtn}
              sx={{mb: 3}}
            >
              {t('Add support file')}
            </Button>
          )}
        </Box>
      )}
      <Divider sx={{mb: 3}} />
      <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
        {t('Descriptive statistics')}
      </Typography>
      <Box sx={{mb: 2}}>
        <FormControl component="fieldset" required>
          <FormLabel component="legend">
            {t('Does the request include descriptive statistics?')}
            <span className="screen-reader-text">{t('required')}</span>
          </FormLabel>
          <RadioGroup
            id="descriptiveStats"
            value={state.descriptiveStats}
            name="descriptiveStats"
            onChange={handleRadioChange}
          >
            <FormControlLabel
              value="Yes"
              control={<Radio color="primary" />}
              label="Yes"
            />
            <FormControlLabel
              value="No"
              control={<Radio color="primary" />}
              label="No"
            />
          </RadioGroup>
          <FormHelperText></FormHelperText>
        </FormControl>
      </Box>
      {state.descriptiveStats === 'Yes' && (
        <Box className={classes.indentedSection} sx={{mb: 3}}>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend" className={classes.lineHeight}>
                {t(
                    'Is the output clearly labelled (tables have a title and variables/categories are labelled)?',
                )}
                <span className="screen-reader-text">{t('required')}</span>
              </FormLabel>
              <RadioGroup id="outpuLabelled">
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
              <FormHelperText></FormHelperText>
            </FormControl>
          </Box>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend">
                {t('Are minimum cell sizes met as per the rules for the data?')}
                <span className="screen-reader-text">{t('required')}</span>
              </FormLabel>
              <RadioGroup id="minimumCellSizes">
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
              <FormHelperText></FormHelperText>
            </FormControl>
          </Box>
          <Grid container>
            <Grid xs item>
              <Typography
                id="support-doc-file-header"
                variant="body2"
                component="p"
              >
                {t(
                    'Add correct supporting documentation (see vetting rules)...  ',
                )}
                <span aria-hidden="true">*</span>
                <span className="screen-reader-text">{t('required')}</span>
              </Typography>
              <Typography
                variant="caption"
                color="textSecondary"
                component="p"
                sx={{mb: 2}}
              >
                {t('At least one file must be added')}
              </Typography>
            </Grid>
            <Grid component="span" className={classes.legendInfoIcon} item>
              <Tooltip
                title="(e.g. counts are unweighted, weighted, weighted and rounded)"
                arrow="true"
              >
                <IconButton aria-label="More information" edge="end">
                  <Icon path={mdiInformationOutline} size={1} />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
          <Paper
            sx={{mb: 3}}
            className={clsx('paper-grey', classes.cardContainer)}
            variant="outlined"
          >
            <Typography variant="caption" component="p" color="textSecondary">
              No support files added
            </Typography>
          </Paper>
          {state.researcher() && (
            <Button
              aria-describedby="support-doc-file-header"
              variant="outlined"
              color="primary"
              startIcon={<Icon path={mdiPlus} size={1} />}
              onClick={() => handleClickOpen('dialogAddSupportFile')}
              className={classes.addBtn}
              sx={{mb: 3}}
            >
              {t('Add support file')}
            </Button>
          )}
        </Box>
      )}
      <Grid container>
        <Grid xs item>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend" className={classes.tooltipLabel}>
                {t(
                    'Does this request include model output or graphs that are equivalent to a descriptive statistics?',
                )}
                <span className="screen-reader-text">{t('required')}</span>
              </FormLabel>
              <RadioGroup
                id="equivalentDescriptiveStats"
                value={state.equivalentDescriptiveStats}
                name="equivalentDescriptiveStats"
                onChange={handleRadioChange}
              >
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
              <FormHelperText></FormHelperText>
            </FormControl>
          </Box>
        </Grid>
        <Grid component="span" className={classes.legendInfoIcon} item>
          <Tooltip
            title="(e.g. Model with a single independent variable, a model with all possible interactions, histograms)"
            arrow="true"
          >
            <IconButton aria-label="More information" edge="end">
              <Icon path={mdiInformationOutline} size={1} />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
      {state.equivalentDescriptiveStats === 'Yes' && (
        <Box className={classes.indentedSection} sx={{mb: 3}}>
          <Typography id="freq-table-file-header" variant="body2" component="p">
            {t('Add unweighted frequency table for respondent counts... ')}
            <span aria-hidden="true">*</span>
            <span className="screen-reader-text">{t('required')}</span>
          </Typography>
          <Typography
            variant="caption"
            color="textSecondary"
            component="p"
            sx={{mb: 2}}
          >
            {t('At least one file must be added')}
          </Typography>
          <Paper
            sx={{mb: 3}}
            className={clsx('paper-grey', classes.cardContainer)}
            variant="outlined"
          >
            <Typography variant="caption" component="p" color="textSecondary">
              No support files added
            </Typography>
          </Paper>
          {state.researcher() && (
            <Button
              aria-describedby="freq-table-file-header"
              variant="outlined"
              color="primary"
              startIcon={<Icon path={mdiPlus} size={1} />}
              onClick={() => handleClickOpen('dialogAddSupportFile')}
              className={classes.addBtn}
              sx={{mb: 3}}
            >
              {t('Add support file')}
            </Button>
          )}
        </Box>
      )}
      <Divider sx={{mb: 3}} />
      <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
        {t('Correlation/Covariance matrix')}
      </Typography>
      <Box sx={{mb: 2}}>
        <FormControl component="fieldset" required>
          <FormLabel component="legend">
            {t('Does this output include a correlation or covariance matrix?')}
            <span className="screen-reader-text">{t('required')}</span>
          </FormLabel>
          <RadioGroup
            id="includeMatrix"
            value={state.covariance}
            name="covariance"
            onChange={handleRadioChange}
          >
            <FormControlLabel
              value="Yes"
              control={<Radio color="primary" />}
              label="Yes"
            />
            <FormControlLabel
              value="No"
              control={<Radio color="primary" />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Box>
      {state.covariance === 'Yes' && (
        <Box className={classes.indentedSection} sx={{pb: 3, mb: 3}}>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend">
                {t('Does the matrix include continuous variables?')}
                <span className="screen-reader-text">{t('required')}</span>
              </FormLabel>
              <RadioGroup
                id="matrixContinuous"
                value={state.matrixContinuous}
                name="matrixContinuous"
                onChange={handleRadioChange}
              >
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Box>
          {state.matrixContinuous === 'Yes' && (
            <Box className={classes.indentedSection} sx={{mb: 3}}>
              <Typography
                id="sample-size-file-header"
                variant="body2"
                component="p"
              >
                {t('Add unweighted sample size... ')}
                <span aria-hidden="true">*</span>
                <span className="screen-reader-text">{t('required')}</span>
              </Typography>
              <Typography
                variant="caption"
                color="textSecondary"
                component="p"
                sx={{mb: 2}}
              >
                {t('At least one file must be added')}
              </Typography>
              <Paper
                sx={{mb: 3}}
                className={clsx('paper-grey', classes.cardContainer)}
                variant="outlined"
              >
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  No support files added
                </Typography>
              </Paper>
              {state.researcher() && (
                <Button
                  aria-describedby="sample-size-file-header"
                  variant="outlined"
                  color="primary"
                  startIcon={<Icon path={mdiPlus} size={1} />}
                  onClick={() => handleClickOpen('dialogAddSupportFile')}
                  className={classes.addBtn}
                  sx={{mb: 3}}
                >
                  {t('Add support file')}
                </Button>
              )}
            </Box>
          )}
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend">
                {t('Does the matrix inclue dichotomous variables?')}
                <span className="screen-reader-text">{t('required')}</span>
              </FormLabel>
              <RadioGroup
                id="matrixDichotomous"
                value={state.matrixDichotomous}
                name="matrixDichotomous"
                onChange={handleRadioChange}
              >
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Box>
          {state.matrixDichotomous === 'Yes' && (
            <Box className={classes.indentedSection} sx={{mb: 3}}>
              <Typography
                id="cross-tab-file-header"
                variant="body2"
                component="p"
              >
                {t('Add unweighted cross-tabulation table... ')}
                <span aria-hidden="true">*</span>
                <span className="screen-reader-text">{t('required')}</span>
              </Typography>
              <Typography
                variant="caption"
                color="textSecondary"
                component="p"
                sx={{mb: 2}}
              >
                {t('At least one file must be added')}
              </Typography>
              <Paper
                sx={{mb: 3}}
                className={clsx('paper-grey', classes.cardContainer)}
                variant="outlined"
              >
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  No support files added
                </Typography>
              </Paper>
              {state.researcher() && (
                <Button
                  aria-describedby="cross-tab-file-header"
                  variant="outlined"
                  color="primary"
                  startIcon={<Icon path={mdiPlus} size={1} />}
                  onClick={() => handleClickOpen('dialogAddSupportFile')}
                  className={classes.addBtn}
                  sx={{mb: 3}}
                >
                  {t('Add support file')}
                </Button>
              )}
            </Box>
          )}
          <Box sx={{mb: -1}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend" className={classes.lineHeight}>
                {t(
                    'Does the matrix include a dichotomous variable correlated with continuous variable?',
                )}
                <span className="screen-reader-text">{t('required')}</span>
              </FormLabel>
              <RadioGroup
                id="matrixCorrelated"
                value={state.matrixCorrelated}
                name="matrixCorrelated"
                onChange={handleRadioChange}
              >
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Box>
          {state.matrixCorrelated === 'Yes' && (
            <Box className={classes.indentedSection} sx={{mt: 3}}>
              <Typography
                id="sub-total-file-header"
                variant="body2"
                component="p"
              >
                {t(
                    'Add unweighted sub-totals for the categories of the dichotomous variable correlated with a continuous variable... ',
                )}
                <span aria-hidden="true">*</span>
                <span className="screen-reader-text">{t('required')}</span>
              </Typography>
              <Typography
                variant="caption"
                color="textSecondary"
                component="p"
                sx={{mb: 2}}
              >
                {t('At least one file must be added')}
              </Typography>
              <Paper
                sx={{mb: 3}}
                className={clsx('paper-grey', classes.cardContainer)}
                variant="outlined"
              >
                <Typography
                  variant="caption"
                  component="p"
                  color="textSecondary"
                >
                  No support files added
                </Typography>
              </Paper>
              {state.researcher() && (
                <Button
                  aria-describedby="sub-total-file-header"
                  variant="outlined"
                  color="primary"
                  startIcon={<Icon path={mdiPlus} size={1} />}
                  onClick={() => handleClickOpen('dialogAddSupportFile')}
                  className={classes.addBtn}
                  sx={{mb: 3}}
                >
                  {t('Add support file')}
                </Button>
              )}
            </Box>
          )}
        </Box>
      )}
      <Divider sx={{mb: 3}} />
      <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
        {t('Rounding')}
      </Typography>
      <Grid container>
        <Grid xs item>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend">
                {t('Is rounding of output required for this vetting request?')}
                <span className="screen-reader-text">{t('required')}</span>
              </FormLabel>
              <RadioGroup
                id="roundingOutput"
                value={state.roundingOutput}
                name="roundingOutput"
                onChange={handleRadioChange}
              >
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
              <FormHelperText></FormHelperText>
            </FormControl>
          </Box>
        </Grid>
        <Grid component="span" className={classes.legendInfoIcon} item>
          <Tooltip
            title="If yes, ensure that any forced rounding to zero is shown."
            arrow="true"
          >
            <IconButton aria-label="More information" edge="end">
              <Icon path={mdiInformationOutline} size={1} />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
      {state.roundingOutput === 'Yes' && (
        <Box className={classes.indentedSection} sx={{mb: 3}}>
          <Box sx={{mb: 3}}>
            <TextField
              margin="dense"
              id="roundingDesc"
              label={t('Rounding approach description')}
              variant="outlined"
              fullWidth
              required
              multiline
              minRows="3"
              defaultValue={state.rounding.text}
              error={Boolean(state.rounding.errorText)}
              helperText={state.rounding.errorText}
              autoComplete="off"
            />
          </Box>
          <Typography id="unrounded-file-header" variant="body2" component="p">
            {t('Add unrounded version of this output... ')}
            <span aria-hidden="true">*</span>
            <span className="screen-reader-text">{t('required')}</span>
          </Typography>
          <Typography
            variant="caption"
            color="textSecondary"
            component="p"
            sx={{mb: 2}}
          >
            {t('At least one file must be added')}
          </Typography>
          <Paper
            sx={{mb: 3}}
            className={clsx('paper-grey', classes.cardContainer)}
            variant="outlined"
          >
            <Typography variant="caption" component="p" color="textSecondary">
              No support files added
            </Typography>
          </Paper>
          {state.researcher() && (
            <Button
              aria-describedby="unrounded-file-header"
              variant="outlined"
              color="primary"
              startIcon={<Icon path={mdiPlus} size={1} />}
              onClick={() => handleClickOpen('dialogAddSupportFile')}
              className={classes.addBtn}
              sx={{mb: 3}}
            >
              {t('Add support file')}
            </Button>
          )}
        </Box>
      )}
      <Divider sx={{mb: 3}} />
      <Typography component="h3" variant="subtitle2" sx={{mb: 3}}>
        Additional supporting files
      </Typography>
      <Typography
        id="output-support-file-header"
        variant="body2"
        sx={{mb: 2}}
      >
        Add additional support file...
      </Typography>
      <Paper className="paper-grey" variant="outlined">
        <Typography variant="caption" component="p" color="textSecondary">
          No support files added
        </Typography>
      </Paper>
      {state.researcher() && (
        <Button
          aria-describedby="output-support-file-header"
          variant="outlined"
          color="primary"
          startIcon={<Icon path={mdiPlus} size={1} />}
          onClick={() => handleClickOpen('dialogAddSupportFile')}
          className={`${classes.addBtn} ${classes.mt2}`}
        >
          {t('Add support file')}
        </Button>
      )}
      {/* Output method help dialog */}
      <DialogOutputMethodHelp
        toggleDialog={() => handleClickClose('dialogOutputMethodHelp')}
        open={state.dialogOutputMethodHelp}
      />
      {/* Add supporting file dialog */}
      <DialogFileSupport
        submitDialog={(e) => {
          submitDialog(e, 'add');
        }}
        toggleDialog={() => handleClickClose('dialogAddSupportFile')}
        open={state.dialogAddSupportFile}
        fileFunction="add"
        snackbar={
          <SnackbarAddSupportFile
            open={state.snackbarAddSupportFile}
            handleClose={() => handleClickClose('snackbarAddSupportFile')}
          />
        }
      />
      {/* Edit supporting file dialog */}
      <DialogFileSupport
        submitDialog={(e) => {
          submitDialog(e, 'edit');
        }}
        toggleDialog={() => handleClickClose('dialogUpdateSupportFile')}
        open={state.dialogUpdateSupportFile}
        fileFunction="edit"
        snackbar={
          <SnackbarUpdateSupportFile
            open={state.snackbarUpdateSupportFile}
            handleClose={() => handleClickClose('snackbarUpdateSupportFile')}
          />
        }
      />
      {/* Delete supporting file dialog */}
      <DialogDelete
        submitDialog={deleteSupportFile}
        open={state.dialogSupportDelete}
        toggleDialog={() => handleClickClose('dialogSupportDelete')}
        snackbar={
          <SnackbarDeleteSupportFile
            open={state.snackbarSupportDelete}
            handleClose={() => handleClickClose('snackbarSupportDelete')}
          />
        }
      />
      {/* Delete supporting file snackbar */}
      <SnackbarDeleteSupportFile
        open={state.snackbarSupportDelete}
        handleClose={() => handleClickClose('snackbarSupportDelete')}
      />
    </>
  );
}
