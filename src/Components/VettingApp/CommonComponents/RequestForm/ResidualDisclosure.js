import React from 'react';
import {styled} from '@mui/material/styles';
import {alpha} from '@mui/material/styles';
import {mdiInformationOutline} from '@mdi/js';
import {useTranslation} from 'react-i18next';
import {
  Typography,
  Divider,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  Button,
  TextField,
  Tooltip,
  Grid,
  Paper,
  IconButton,
  Box,
} from '@mui/material';
import Icon from '@mdi/react';
import {mdiPlus} from '@mdi/js';
import {SnackbarAddSupportFile} from '../Snackbars';
import {DialogFileSupport} from '../DialogBox';
import clsx from 'clsx';

const PREFIX = 'ResidualDisclosure';

const classes = {
  emphasisBox: `${PREFIX}-emphasisBox`,
  indentedSection: `${PREFIX}-indentedSection`,
  textFieldInfoIcon: `${PREFIX}-textFieldInfoIcon`,
  legendInfoIcon: `${PREFIX}-legendInfoIcon`,
  addBtn: `${PREFIX}-addBtn`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('div')(({theme}) => ({
  [`& .${classes.emphasisBox}`]: {
    background: theme.palette.grey[100],
    padding: theme.spacing(2),
    marginBottom: theme.spacing(3),
    borderLeftStyle: 'solid',
    borderLeftWidth: '4px',
    borderLeftColor: theme.palette.primary.main,
    borderTopRightRadius: theme.shape.borderRadius,
    borderBottomRightRadius: theme.shape.borderRadius,
  },

  [`& .${classes.indentedSection}`]: {
    display: 'inline-block',
    width: '100%',
    boxSizing: 'border-box',
    paddingLeft: theme.spacing(3),
    paddingTop: theme.spacing(3),
    borderLeft: '1px solid',
    borderLeftColor: theme.palette.divider,
  },

  [`& .${classes.textFieldInfoIcon}`]: {
    paddingLeft: theme.spacing(1),
  },

  [`& .${classes.legendInfoIcon}`]: {
    marginTop: theme.spacing(-1.25),
    marginLeft: theme.spacing(1),
  },

  [`& .${classes.addBtn}`]: {
    'marginTop': theme.spacing(-1),
    'borderStyle': 'dashed',
    'width': '100%',
    'borderColor': alpha(theme.palette.common.black, 0.23),
    '&.MuiButton-outlinedPrimary:hover': {
      borderStyle: 'dashed',
    },
  },
}));

function ResidualDisclosure(props) {
  const [state, setState] = React.useState({
    subset: false,
    versionpreviouslyReleased: false,
    subsetAddFile: false,
    subsetChanges: '',
    subsetNotes: '',
    subsetNotes2: '',
    subsetNotes3: '',
    addFile: false,
    changes: '',
    date: '',
    notes: '',
    notes2: '',
    notes3: '',
    snackbarAddFile: false,
    snackbarDelete: false,
    dialogDelete: false,
    dialogFileSupport: false,
  });

  const initial = {
    // blank object used to reset state
    subsetChanges: {
      text: '',
      errorText: '',
      helperText: '',
      invalid: '',
      commands: '',
    },
    changes: {
      text: '',
      errorText: '',
      helperText: '',
      invalid: '',
      commands: '',
    },
    subsetNotes: {
      text: '',
      errorText: '',
      helperText: '',
      invalid: '',
      commands: '',
    },
    subsetNotes2: {
      text: '',
      errorText: '',
      helperText: '',
      invalid: '',
      commands: '',
    },
    subsetNotes3: {
      text: '',
      errorText: '',
      helperText: '',
      invalid: '',
      commands: '',
    },
    date: {
      text: '',
      errorText: '',
      helperText: '',
      invalid: '',
      commands: '',
    },
    notes: {
      text: '',
      errorText: '',
      helperText: '',
      invalid: '',
      commands: '',
    },
    notes2: {
      text: '',
      errorText: '',
      helperText: '',
      invalid: '',
      commands: '',
    },
    notes3: {
      text: '',
      errorText: '',
      helperText: '',
      invalid: '',
      commands: '',
    },
  };

  const handleRadioChange = (event) => {
    const name = event.target.name;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  const disableCutCopyPaste = (e, command, value) => {
    // display error if user tries to cut/copy/paste
    let msg;
    e.preventDefault();
    switch (command) {
      case 'cut':
        msg = t('Cut has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
            helperText: msg,
          },
        });
        break;
      case 'copy':
        msg = t('Copy has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
            helperText: msg,
          },
        });
        break;
      case 'paste':
        msg = t('Paste has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
            helperText: msg,
          },
        });
        break;
      default:
        break;
    }
  };

  const toggleHelperText = (value) => {
    if (state[value].commands === state[value].errorText) {
      if (Boolean(state[value].invalid)) {
        // set error text back to invalid error
        setState({
          ...state,
          [value]: {
            ...state[value],
            helperText: state[value].invalid,
          },
        });
      } else {
        // clear error text if no invalid error exists
        setState({
          ...state,
          [value]: {
            ...state[value],
            helperText: initial[value].helperText,
            errorText: initial[value].errorText,
          },
        });
      }
    }
  };

  const handleAddFile = () => {
    setState({
      ...state,
      addFile: true,
      snackbarAddFile: true,
      dialogFileSupport: false,
    });
  };

  const handleClickOpen = (element) => {
    setState({...state, [element]: true});
  };

  const handleClickClose = (element) => {
    setState({...state, [element]: false});
  };

  const {t} = useTranslation();

  return (
    <Root>
      <Typography component="h2" variant="h6" sx={{mb: 3}}>
        Residual disclosure
      </Typography>
      <Typography sx={{mb: 3}}>
        A re-release of the same output after slight modifications greatly
        increases the risk of residual disclosure. Statistics Canada strongly
        recommends that researchers submit as few versions of the output as
        possible.
      </Typography>
      <Divider sx={{mb: 3}} />
      <Grid container>
        <Grid xs item>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset">
              <FormLabel component="legend" required>
                For this request, have you subsetted any variables, where one or
                more variables is a subset of another?
                <span className="screen-reader-text">required</span>
              </FormLabel>
              <RadioGroup
                id="subset"
                onChange={handleRadioChange}
                value={state.subset}
                name="subset"
              >
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Box>
        </Grid>
        <Grid component="span" className={classes.legendInfoIcon} item>
          <Tooltip
            title="(e.g. had depression in the past 5 years and had depression in the previous year; had a chronic condition and had a respiratory chronic condition)"
            arrow="true"
          >
            <IconButton aria-label="More information" edge="end">
              <Icon path={mdiInformationOutline} size={1} />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
      {state.subset === 'Yes' && (
        <Box className={classes.indentedSection} sx={{mb: 3}}>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend">
                Compared to the other subset, have you changed the sub-sample of
                population of interest?
                <span className="screen-reader-text">required</span>
              </FormLabel>
              <RadioGroup id="subsetSubSample" name="subsetSubSample">
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Box>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend">
                Compared to the other subset, have you dropped individual cases
                or outliers?
                <span className="screen-reader-text">required</span>
              </FormLabel>
              <RadioGroup id="subsetDropped" name="subsetDropped">
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Box>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend">
                Compared to the other subset, have you imputed the missing data?
                <span className="screen-reader-text">required</span>
              </FormLabel>
              <RadioGroup id="subsetImputed" name="subsetImputed">
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Box>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend">
                Compared to the other subset, have you recoded or modified any
                variables even slightly?
                <span className="screen-reader-text">required</span>
              </FormLabel>
              <RadioGroup id="subsetRecoded" name="subsetRecoded">
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Box>
          <Box sx={{mb: 3}}>
            <TextField
              margin="dense"
              id="subsetChanges"
              label="List subsetted variables"
              multiline
              variant="outlined"
              fullWidth
              required
              onCut={(e) => disableCutCopyPaste(e, 'cut', 'subsetChanges')}
              onCopy={(e) => disableCutCopyPaste(e, 'copy', 'subsetChanges')}
              onPaste={(e) => disableCutCopyPaste(e, 'paste', 'subsetChanges')}
              onClick={() => toggleHelperText('subsetChanges')}
              onBlur={() => toggleHelperText('subsetChanges')}
              onFocus={() => toggleHelperText('subsetChanges')}
              value={state.subsetChanges.text}
              error={Boolean(state.subsetChanges.errorText)}
              helperText={state.subsetChanges.errorText}
              autoComplete="off"
            />
          </Box>
          <Typography id="subset-table-header" component="p" variant="body2">
            Add residual table (see vetting orientation)...
            <span aria-hidden="true">*</span>
            <span className="screen-reader-text">required</span>
          </Typography>
          <Typography
            component="p"
            variant="caption"
            color="textSecondary"
            sx={{mb: 2}}
          >
            At least one file must be added
          </Typography>
          <Paper
            sx={{mb: 3}}
            className={clsx('paper-grey', classes.cardContainer)}
            variant="outlined"
          >
            <Typography variant="caption" component="p" color="textSecondary">
              No support files added
            </Typography>
          </Paper>
          {props.role === 'researcher' && (
            <Button
              aria-describedby="subset-table-header"
              variant="outlined"
              color="primary"
              startIcon={<Icon path={mdiPlus} size={1} />}
              fullWidth={true}
              className={classes.addBtn}
              sx={{mb: 3}}
              onClick={() => handleClickOpen('dialogFileSupport')}
            >
              Add support file
            </Button>
          )}
        </Box>
      )}
      <Box sx={{mb: 2}}>
        <FormControl component="fieldset">
          <FormLabel component="legend" required>
            Has a version of this output, in part or in whole, been previously
            released for this project?
            <span className="screen-reader-text">required</span>
          </FormLabel>
          <RadioGroup
            id="versionpreviouslyReleased"
            onChange={handleRadioChange}
            value={state.versionpreviouslyReleased}
            name="versionpreviouslyReleased"
          >
            <FormControlLabel
              value="Yes"
              control={<Radio color="primary" />}
              label="Yes"
            />
            <FormControlLabel
              value="No"
              control={<Radio color="primary" />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Box>
      {state.versionpreviouslyReleased === 'Yes' && (
        <Box className={classes.indentedSection} sx={{mb: 3}}>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend" required>
                Compared to the other output, have you changed the sub-sample of
                population of interest?
                <span className="screen-reader-text">required</span>
              </FormLabel>
              <RadioGroup id="subSample" name="subSample">
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Box>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend" required>
                Compared to the other output, have you dropped individual cases
                or outliers?
                <span className="screen-reader-text">required</span>
              </FormLabel>
              <RadioGroup id="Droppped">
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Box>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend" required>
                Compared to the other output, have you imputed the missing data?
                <span className="screen-reader-text">required</span>
              </FormLabel>
              <RadioGroup id="Imputed" name="Imputed">
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Box>
          <Box sx={{mb: 2}}>
            <FormControl component="fieldset" required>
              <FormLabel component="legend" required>
                Compared to the other output, have you recoded or modified any
                variables even slightly?
                <span className="screen-reader-text">required</span>
              </FormLabel>
              <RadioGroup id="Recoded" name="Recoded">
                <FormControlLabel
                  value="Yes"
                  control={<Radio color="primary" />}
                  label="Yes"
                />
                <FormControlLabel
                  value="No"
                  control={<Radio color="primary" />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Box>
          <Box sx={{mb: 3}}>
            <TextField
              margin="dense"
              id="changes"
              label="Explanation of changes"
              multiline
              variant="outlined"
              fullWidth
              required
              onCut={(e) => disableCutCopyPaste(e, 'cut', 'changes')}
              onCopy={(e) => disableCutCopyPaste(e, 'copy', 'changes')}
              onPaste={(e) => disableCutCopyPaste(e, 'paste', 'changes')}
              onClick={() => toggleHelperText('changes')}
              onBlur={() => toggleHelperText('changes')}
              onFocus={() => toggleHelperText('changes')}
              value={state.changes.text}
              error={Boolean(state.changes.errorText)}
              helperText={state.changes.errorText}
              autoComplete="off"
            />
          </Box>
          <Grid component="span" sx={{mb: 3}} container>
            <Grid component="span" xs item>
              <TextField
                margin="dense"
                id="relatedDate2"
                label="Related request date"
                variant="outlined"
                fullWidth
                required
                onCut={(e) => disableCutCopyPaste(e, 'cut', 'date')}
                onCopy={(e) => disableCutCopyPaste(e, 'copy', 'date')}
                onPaste={(e) => disableCutCopyPaste(e, 'paste', 'date')}
                onClick={() => toggleHelperText('date')}
                onBlur={() => toggleHelperText('date')}
                onFocus={() => toggleHelperText('date')}
                value={state.date.text}
                error={Boolean(state.date.errorText)}
                helperText={state.date.helperText}
                autoComplete="off"
              />
            </Grid>
            <Grid component="span" className={classes.textFieldInfoIcon} item>
              <Tooltip
                title="Indicate the date(s) of the previous vetting requests related to this request.)"
                arrow="true"
              >
                <IconButton aria-label="More information" edge="end">
                  <Icon path={mdiInformationOutline} size={1} />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
          <Typography id="prev-table-header" component="p" variant="body2">
            Add residual table (see vetting orientation)...
            <span aria-hidden="true">*</span>
            <span className="screen-reader-text">required</span>
          </Typography>
          <Typography
            component="p"
            variant="caption"
            color="textSecondary"
            sx={{mb: 2}}
          >
            At least one file must be added
          </Typography>
          <Paper
            sx={{mb: 3}}
            className={clsx('paper-grey', classes.cardContainer)}
            variant="outlined"
          >
            <Typography variant="caption" component="p" color="textSecondary">
              No support files added
            </Typography>
          </Paper>
          {props.role === 'researcher' && (
            <Button
              aria-describedby="prev-table-header"
              variant="outlined"
              color="primary"
              startIcon={<Icon path={mdiPlus} size={1} />}
              fullWidth={true}
              className={classes.addBtn}
              sx={{mb: 3}}
              onClick={() => handleClickOpen('dialogFileSupport')}
            >
              Add support file
            </Button>
          )}

          <Typography id="prev-syntax-header" component="p" variant="body2">
            Add both sets of syntax with changes highlighted...
            <span aria-hidden="true">*</span>
            <span className="screen-reader-text">required</span>
          </Typography>
          <Typography
            component="p"
            variant="caption"
            color="textSecondary"
            sx={{mb: 2}}
          >
            At least one file must be added
          </Typography>
          <Paper
            sx={{mb: 3}}
            className={clsx('paper-grey', classes.cardContainer)}
            variant="outlined"
          >
            <Typography variant="caption" component="p" color="textSecondary">
              No support files added
            </Typography>
          </Paper>
          {props.role === 'researcher' && (
            <Button
              aria-describedby="prev-syntax-header"
              variant="outlined"
              color="primary"
              startIcon={<Icon path={mdiPlus} size={1} />}
              fullWidth={true}
              className={classes.addBtn}
              sx={{mb: 3}}
              onClick={() => handleClickOpen('dialogFileSupport')}
            >
              Add support file
            </Button>
          )}
        </Box>
      )}
      {/* Add file snackbar */}
      <SnackbarAddSupportFile
        open={state.snackbarAddFile}
        handleClose={() => handleClickClose('snackbarAddFile')}
      />
      <DialogFileSupport
        submitDialog={handleAddFile}
        toggleDialog={() => handleClickClose('dialogFileSupport')}
        open={state.dialogFileSupport}
        fileFunction="add"
      />
    </Root>
  );
}
export default ResidualDisclosure;
