import React from 'react';
import {styled} from '@mui/material/styles';
const PREFIX = 'BrandingStatCan';

const classes = {
  brandImageGroup: `${PREFIX}-brandImageGroup`,
  brandImage: `${PREFIX}-brandImage`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled('span')(({theme}) => ({
  [`& .${classes.brandImageGroup}`]: {
    display: 'flex',
    height: '100%',
    alignItems: 'center',
  },

  [`& .${classes.brandImage}`]: {
    'maxHeight': '100%',
    'height': theme.spacing(2.5),
    'width': 'auto',
    '&:not(:last-child)': {
      marginRight: theme.spacing(2.5),
    },
  },
}));

export function BrandingStatCanEn() {
  return (
    <Root>
      <div className={classes.brandImageGroup}>
        <img
          src={process.env.PUBLIC_URL + '/images/StatisticsCanadaLogoFlag.svg'}
          alt="Statistics Canada"
          className={classes.brandImage}
        />
        <img
          src={
            process.env.PUBLIC_URL + '/images/StatisticsCanadaLogoTextEN.svg'
          }
          alt=""
          className={classes.brandImage}
        />
        <img
          src={
            process.env.PUBLIC_URL + '/images/StatisticsCanadaLogoTextFR.svg'
          }
          alt=""
          className={classes.brandImage}
        />
      </div>
    </Root>
  );
}

export function BrandingStatCanFr() {
  return (
    <Root>
      <img
        src={process.env.PUBLIC_URL + '/images/sig-blk-fr.svg'}
        alt="Statistique Canada"
        className={classes.brandImage}
      />
    </Root>
  );
}
