/* eslint-disable jsx-a11y/no-autofocus */
import React from 'react';
import {styled} from '@mui/material/styles';
import clsx from 'clsx';
import Grid from '@mui/material/Grid';
import {useTranslation} from 'react-i18next';
import {useHistory, useLocation} from 'react-router-dom';
import {green} from '@mui/material/colors';
import CloseIcon from '@mui/icons-material/Close';
import {
  Typography,
  TextField,
  FormControl,
  Avatar,
  Button,
  Divider,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  IconButton,
  InputLabel,
  FormLabel,
  FormGroup,
  Checkbox,
  Select,
  MenuItem,
  FormControlLabel,
  FormHelperText,
  Box,
} from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import {Alert} from '@mui/material';
import {Dialog as CustomDialog} from '../../CommonComponents/Dialog';
import NumberFormat from 'react-number-format';

import {
  SnackbarApproveRequest,
  SnackbarAssignLead,
  SnackbarAssignSupport,
  SnackbarChangeRequest,
  SnackbarDenyRequest,
  SnackbarWithdrawRequest,
} from './Snackbars';

const PREFIX = 'DialogBox';

const classes = {
  root: `${PREFIX}-root`,
  avatar: `${PREFIX}-avatar`,
  vettingContainerTitle: `${PREFIX}-vettingContainerTitle`,
  vettingSection: `${PREFIX}-vettingSection`,
  vettingRow: `${PREFIX}-vettingRow`,
  vettingColumn: `${PREFIX}-vettingColumn`,
  vettingText: `${PREFIX}-vettingText`,
  footerBtns: `${PREFIX}-footerBtns`,
  textField: `${PREFIX}-textField`,
  widthAuto: `${PREFIX}-widthAuto`,
  alignStart: `${PREFIX}-alignStart`,
  divider: `${PREFIX}-divider`,
  autocompleteOption: `${PREFIX}-autocompleteOption`,
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const StyledDialog = styled(Dialog)(({theme}) => ({
  '& .MuiFormControl-root': {
    width: '100%',
  },
  '& .MuiFormControlLabel-root': {
    'marginLeft': '0px',
    'flexBasis': '50%',
    'height': '100%',
    '&:last-child': {
      marginRight: '0px',
    },
    '& .MuiTextField-root': {
      width: '100% !important',
    },
  },
  '& .Mui-error ~.MuiFormHelperText-root, & .Mui-error + label': {
    color: theme.palette.error.main,
  },
  '& .MuiInputBase-input:not(.MuiInputBase-inputMultiline)': {
    height: '100%',
  },
  '& .MuiDialogTitle-root': {
    padding: theme.spacing(1.5, 3),
  },
  '& .MuiSelect-select': {
    paddingTop: 0,
    paddingBottom: 0,
  },
  '& .MuiDialog-paperWidthSm .MuiOutlinedInput-inputMultiline': {
    maxHeight: 'none',
  },

  [`& .${classes.avatar}`]: {
    backgroundColor: green[500],
    color: theme.palette.grey[100],
  },

  [`& .${classes.vettingContainerTitle}`]: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  [`& .${classes.vettingSection}`]: {
    padding: theme.spacing(3),
  },

  [`& .${classes.vettingRow}`]: {
    'display': 'flex',
    'padding': theme.spacing(1.5, 0),
    'flexFlow': 'row',
    'height': 'auto',
    'justifyContent': 'center',
    'width': '100%',
    'alignItems': 'center',
    '&:first-of-type': {
      paddingTop: 0,
    },
    '&:last-child': {
      paddingBottom: 0,
    },
  },

  [`& .${classes.vettingColumn}`]: {
    'display': 'flex',
    'flexDirection': 'column',
    'width': '100%',
    'justifyContent': 'center',
    'marginRight': theme.spacing(1),
    'height': '100%',
    '&:last-child': {
      marginRight: 0,
    },
  },

  [`& .${classes.vettingText}`]: {
    paddingLeft: theme.spacing(1),
  },

  [`& .${classes.footerBtns}`]: {
    marginLeft: theme.spacing(2),
  },

  [`& .${classes.textField}`]: {
    width: '100%',
    padding: 0,
  },

  [`& .${classes.widthAuto}`]: {
    width: 'auto !important',
  },

  [`& .${classes.alignStart}`]: {
    alignItems: 'start',
  },

  [`& .${classes.divider}`]: {
    marginTop: theme.spacing(1.5),
    marginBottom: theme.spacing(1.5),
  },

  [`& .${classes.autocompleteOption}`]: {
    whiteSpace: 'normal',
    wordBreak: 'break-all',
  },
}));

// ////////////////////////////////////////// REQUESTER DETAILS
export function DialogRequesterDetails(props) {
  const {open, toggleDialog} = props;
  const {t} = useTranslation();

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={toggleDialog}
        aria-labelledby="dashboard-dialog-title"
        open={open}
        className={classes.root}
        scroll="paper"
        onClick={handleClick}
      >
        <DialogTitle id="dashboard-dialog-title" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              Requester details
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <div className={classes.vettingSection}>
            <div className={classes.vettingRow}>
              <div className={clsx(classes.vettingColumn, classes.widthAuto)}>
                <Avatar className={classes.avatar}>BB</Avatar>
              </div>
              <div className={classes.vettingColumn}>
                <Typography className={classes.vettingText} variant="body2">
                  Bill Brian
                </Typography>
                <Typography className={classes.vettingText} variant="body2">
                  brian.bill@cloud.statcan.ca
                </Typography>
                <Typography className={classes.vettingText} variant="body2">
                  +1 (999) 999 9999
                </Typography>
              </div>
            </div>
          </div>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button variant="contained" color="primary" onClick={toggleDialog}>
            {t('Close')}
          </Button>
        </DialogActions>
      </StyledDialog>
    </React.Fragment>
  );
}

// ////////////////////////////////////////// ASSIGNEE DETAILS
export function DialogAssigneeDetails(props) {
  const {open, toggleDialog, role, statusHead} = props;
  const {t} = useTranslation();

  const handleClick = (e) => {
    e.stopPropagation();
  };

  const resolved = () => {
    if (role === 'analyst') {
      if (statusHead === 'approved' || statusHead === 'denied') {
        return true;
      }
    } else {
      return false;
    }
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={toggleDialog}
        aria-labelledby="dashboard-dialog-title"
        open={open}
        className={classes.root}
        scroll="paper"
        onClick={handleClick}
      >
        <DialogTitle id="dashboard-dialog-title" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              Assignee Details
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          {!resolved() && (
            <div className={classes.vettingSection}>
              <div className={classes.vettingRow}>
                <div className={clsx(classes.vettingColumn, classes.widthAuto)}>
                  <Avatar className={classes.avatar}>AA</Avatar>
                </div>
                <div className={classes.vettingColumn}>
                  <Typography className={classes.vettingText} variant="body2">
                    Alfie Allen
                  </Typography>
                  <Typography className={classes.vettingText} variant="body2">
                    alfie.allen@cloud.statcan.ca
                  </Typography>
                  <Typography className={classes.vettingText} variant="body2">
                    +1 (999) 999 9999
                  </Typography>
                </div>
              </div>
            </div>
          )}
          {resolved() && (
            <div className={classes.vettingSection}>
              <div className={classes.vettingRow}>
                <div className={classes.vettingColumn}>
                  <Typography variant="subtitle1" component="h3">
                    {t('Assignees')}
                  </Typography>
                </div>
              </div>
              <div className={classes.vettingRow}>
                <div className={classes.vettingColumn}>
                  <Typography variant="subtitle2" component="h4">
                    {t('Lead')}
                  </Typography>
                </div>
              </div>
              <div className={classes.vettingRow}>
                <div className={clsx(classes.vettingColumn, classes.widthAuto)}>
                  <Avatar className={classes.avatar}>BB</Avatar>
                </div>
                <div className={classes.vettingColumn}>
                  <Typography className={classes.vettingText} variant="body2">
                    Bill Brian
                  </Typography>
                  <Typography className={classes.vettingText} variant="body2">
                    brian.bill@cloud.statcan.ca
                  </Typography>
                  <Typography className={classes.vettingText} variant="body2">
                    +1 (999) 999 9999
                  </Typography>
                </div>
              </div>
              <div className={classes.vettingRow}>
                <div className={classes.vettingColumn}>
                  <Typography variant="subtitle2" component="h4">
                    {t('Support')}
                  </Typography>
                </div>
              </div>
              <div className={classes.vettingRow}>
                <div className={clsx(classes.vettingColumn, classes.widthAuto)}>
                  <Avatar className={classes.avatar}>CC</Avatar>
                </div>
                <div className={classes.vettingColumn}>
                  <Typography className={classes.vettingText} variant="body2">
                    Charlie Cox
                  </Typography>
                  <Typography className={classes.vettingText} variant="body2">
                    charlie.cox@cloud.statcan.ca
                  </Typography>
                  <Typography className={classes.vettingText} variant="body2">
                    +1 (999) 999 9999
                  </Typography>
                </div>
              </div>
              <div className={classes.vettingRow}>
                <div className={clsx(classes.vettingColumn, classes.widthAuto)}>
                  <Avatar className={classes.avatar}>DD</Avatar>
                </div>
                <div className={classes.vettingColumn}>
                  <Typography className={classes.vettingText} variant="body2">
                    Darren Darrelson
                  </Typography>
                  <Typography className={classes.vettingText} variant="body2">
                    darren.darrelson@cloud.statcan.ca
                  </Typography>
                  <Typography className={classes.vettingText} variant="body2">
                    +1 (999) 999 9999
                  </Typography>
                </div>
              </div>
            </div>
          )}
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button variant="contained" color="primary" onClick={toggleDialog}>
            {t('Close')}
          </Button>
        </DialogActions>
      </StyledDialog>
    </React.Fragment>
  );
}

// ////////////////////////////////////////// WITHDRAW REQUEST
export function DialogWithdraw(props) {
  const {t} = useTranslation();
  const [snackbar, setSnackbar] = React.useState(false);
  const [state, setState] = React.useState({
    withdrawReason: {
      text: '',
      errorText: '',
      helperText: '',
      invalid: '',
    },
  });

  const initial = {
    // blank object used to reset state
    withdrawReason: {
      text: '',
      errorText: '',
      helperText: '',
      invalid: '',
    },
  };

  const handleSelectChange = (event) => {
    const name = event.target.name;
    setState({
      ...state,
      [name]: {
        text: event.target.value,
        errorText: '',
        helperText: '',
        invalid: '',
      },
    });
  };

  const snackbarClose = () => {
    setSnackbar(false);
  };

  const validateForm = () => {
    let isError = false;
    if (state.withdrawReason.text.trim() === '') {
      isError = true;
      state.withdrawReason.invalid = t('Select a withdraw reason.');
      state.withdrawReason.errorText = t('Select a withdraw reason.');
      state.withdrawReason.helperText = t('Select a withdraw reason.');
    }

    if (isError) {
      setState({
        ...state,
      });
    }

    return isError;
  };

  const submitForm = (e) => {
    e.stopPropagation();
    e.preventDefault();
    const err = validateForm();
    if (!err) {
      // if no errors exist, submit the form and reset the inputs
      props.toggleDialog(e);
      setSnackbar(!snackbar);
      setState({...initial});
    } else {
      for (const property in state) {
        // focus on the first input that has an error on submit
        if (state[property].invalid) {
          switch (property) {
            case 'comments':
              document.getElementById('comments-input').focus();
              break;
            default:
              break;
          }
          break;
        }
      }
    }
  };

  const content = () => (
    <>
      <FormControl
        sx={{m: 0}}
        variant="outlined"
        error={Boolean(state.withdrawReason.errorText)}
        margin="dense"
        required
      >
        <InputLabel id="withdrawReason-label">
          {t('Withdraw reason')}
          <span className="screen-reader-text">{t('required')}</span>
        </InputLabel>
        <Select
          id="withdrawReason"
          labelId="withdrawReason-label"
          name="withdrawReason"
          label={t('Withdraw reason') + ' *'}
          onChange={handleSelectChange}
          value={state.withdrawReason.text}
        >
          <MenuItem value="makeChanges">Need to make changes</MenuItem>
          <MenuItem value="notNeeded">No longer required</MenuItem>
          <MenuItem value="other">Other</MenuItem>
        </Select>
      </FormControl>
      {Boolean(state.withdrawReason.errorText) && (
        <FormHelperText
          error={Boolean(state.withdrawReason.errorText)}
          variant="outlined"
          margin="dense"
        >
          {state.withdrawReason.helperText}
        </FormHelperText>
      )}
      <SnackbarWithdrawRequest open={snackbar} handleClose={snackbarClose} />
    </>
  );

  return (
    <>
      <CustomDialog
        id="withdraw-dialog"
        open={props.open}
        title={t('Withdraw request')}
        content={content()}
        primaryButton={t('Withdraw')}
        secondaryButton={t('Cancel')}
        handlePrimaryClick={submitForm}
        handleSecondaryClick={(e) => props.toggleDialog(e)}
        toggleDialog={(e) => props.toggleDialog(e)}
      />

      <SnackbarWithdrawRequest open={snackbar} handleClose={snackbarClose} />
    </>
  );
}

// ////////////////////////////////////////// SAVE BEFORE LEAVING
export function DialogSaveBeforeLeaving(props) {
  const {t} = useTranslation();
  const {toggleDialog, open} = props;

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={toggleDialog}
        aria-labelledby="dashboard-dialog-title"
        aria-describedby="dashboard-dialog-description"
        open={open}
        className={classes.root}
        scroll="paper"
        onClick={handleClick}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
          }
        }}
      >
        <DialogTitle id="dashboard-dialog-title" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              {t('Save before leaving?')}
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              onKeyPress={(e) => {
                e.preventDefault();
                e.stopPropagation();
                if (e.key === 'Enter') {
                  toggleDialog(e);
                }
              }}
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <div className={classes.vettingSection}>
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <Alert
                  id="dashboard-dialog-description"
                  severity="warning"
                  className={classes.alert}
                  role="note"
                >
                  {t('If you don\'t save, your changes will be lost.')}
                </Alert>
              </div>
            </div>
          </div>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Grid container justifyContent="space-between">
            <Grid item>
              <Button
                color="primary"
                onClick={toggleDialog}
                className={clsx(classes.footerBtn, 'MuiIconButton-edgeStart')}
                onKeyPress={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  if (e.key === 'Enter') {
                    toggleDialog(e);
                  }
                }}
              >
                {t('Don\'t save')}
              </Button>
            </Grid>
            <Grid item>
              <Button
                variant="outlined"
                color="primary"
                onClick={toggleDialog}
                className={classes.footerBtns}
                onKeyPress={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  if (e.key === 'Enter') {
                    toggleDialog(e);
                  }
                }}
              >
                {t('Cancel')}
              </Button>
              <Button
                autoFocus
                variant="contained"
                color="primary"
                onClick={toggleDialog}
                className={classes.footerBtns}
                onKeyPress={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  if (e.key === 'Enter') {
                    toggleDialog(e);
                  }
                }}
              >
                {t('Save')}
              </Button>
            </Grid>
          </Grid>
        </DialogActions>
      </StyledDialog>
    </React.Fragment>
  );
}

// ////////////////////////////////////////// REQUEST CHANGES
export function DialogUpdate(props) {
  const {t} = useTranslation();
  const {toggleDialog, open} = props;
  const [snackbar, setSnackbar] = React.useState(false);

  const SnackbarClose = () => {
    setSnackbar(false);
  };

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={(e) => {
          toggleDialog(e);
        }}
        aria-labelledby="dashboard-dialog-title"
        open={open}
        className={classes.root}
        scroll="paper"
        onClick={handleClick}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
          }
        }}
      >
        <DialogTitle id="dashboard-dialog-title" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              {t('Request changes')}
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              onKeyPress={(e) => {
                e.preventDefault();
                e.stopPropagation();
                if (e.key === 'Enter') {
                  toggleDialog(e);
                }
              }}
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <div className={classes.vettingSection}>
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <Typography variant="subtitle2" component="h3">
                  What you need to do...
                </Typography>
              </div>
            </div>
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <ul>
                  <li>
                    <Typography variant="body2" gutterBottom>
                      Click the "Request changes" button to notify the requester
                      that changes will need to be made to the request.
                    </Typography>
                  </li>
                  <li>
                    <Typography variant="body2">
                      Contact the requester on a secure communication channel
                      indicating what changes need to be made.
                    </Typography>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button
            variant="outlined"
            color="primary"
            onClick={(e) => {
              toggleDialog(e);
            }}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                toggleDialog(e);
              }
            }}
          >
            {t('Cancel')}
          </Button>
          <Button
            autoFocus
            type="submit"
            variant="contained"
            color="primary"
            className={classes.footerBtns}
            onClick={(e) => {
              toggleDialog(e);
              setTimeout(function() {
                setSnackbar(true);
              }, 250);
            }}
            onKeyPress={(e) => {
              e.stopPropagation();
              if (e.key === 'Enter') {
                toggleDialog(e);
                setSnackbar(true);
              }
            }}
          >
            {t('Request changes')}
          </Button>
        </DialogActions>
        <SnackbarChangeRequest open={snackbar} handleClose={SnackbarClose} />
      </StyledDialog>
      <SnackbarChangeRequest open={snackbar} handleClose={SnackbarClose} />
    </React.Fragment>
  );
}

// ////////////////////////////////////////// GET SUPPORT FAB
export function DialogGetSupportFab(props) {
  const {t} = useTranslation();
  const {submitDialog, toggleDialog, open} = props;

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={toggleDialog}
        aria-labelledby="get-support-dialog-title"
        open={open}
        className={classes.root}
        disableBackdropClick
        scroll="paper"
        onClick={handleClick}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
          }
        }}
      >
        <DialogTitle id="get-support-dialog-title" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              {t('Get support')}
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              onKeyPress={(e) => {
                e.preventDefault();
                e.stopPropagation();
                if (e.key === 'Enter') {
                  toggleDialog(e);
                }
              }}
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <div className={classes.vettingSection}>
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <Typography variant="subtitle2" component="h3">
                  Do you need help with something?
                </Typography>
              </div>
            </div>
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <Typography variant="body2">
                  Click the "Get support" button and one of our Analysts will
                  contact you to resolve the issue. We aim to make contact
                  within 1 to 2 business days.
                </Typography>
              </div>
            </div>
          </div>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button
            variant="outlined"
            color="primary"
            onClick={toggleDialog}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                toggleDialog(e);
              }
            }}
          >
            {t('Cancel')}
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            onClick={submitDialog}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                submitDialog(e);
              }
            }}
          >
            {t('Get support')}
          </Button>
        </DialogActions>
        {props.snackbar ? props.snackbar : null}
      </StyledDialog>
    </React.Fragment>
  );
}

// ////////////////////////////////////////// GET SUPPORT (FORM ONLY)
export function DialogFormGetSupportFab(props) {
  const {t} = useTranslation();
  const {submitDialog, toggleDialog, open} = props;
  const [state, setState] = React.useState({
    step1: false,
    step2: false,
    step3: false,
    step4: false,
    other: false,
  });

  const handleChbxChange = (event) => {
    setState({...state, [event.target.name]: event.target.checked});
  };

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={(e) => {
          toggleDialog(e);
        }}
        aria-labelledby="get-support-form-dialog-title"
        open={open}
        className={classes.root}
        scroll="paper"
        onClick={handleClick}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
          }
        }}
      >
        <DialogTitle id="get-support-form-dialog-title" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              {t('Get support')}
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              onKeyPress={(e) => {
                e.preventDefault();
                e.stopPropagation();
                if (e.key === 'Enter') {
                  toggleDialog(e);
                }
              }}
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <div className={classes.vettingSection}>
            <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
              Do you need help with something?
            </Typography>
            <Box sx={{mb: 2}}>
              <FormControl
                component="fieldset"
                required
                aria-describedby="support-helperText"
              >
                <FormLabel component="legend">
                  Select the steps you need help with.
                  <span className="screen-reader-text">{t('required')}</span>
                </FormLabel>
                <FormHelperText id="support-helperText">
                  Select all that apply
                </FormHelperText>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={state.step1}
                        onChange={handleChbxChange}
                        name="step1"
                        color="primary"
                      />
                    }
                    label="Step 1 - Requester details"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={state.step2}
                        onChange={handleChbxChange}
                        name="step2"
                        color="primary"
                      />
                    }
                    label="Step 2 - Output details"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={state.step3}
                        onChange={handleChbxChange}
                        name="step3"
                        color="primary"
                      />
                    }
                    label="Step 3 - Residual disclosure"
                  />
                  <FormControlLabel
                    sx={{mb: 2}}
                    control={
                      <Checkbox
                        checked={state.step4}
                        onChange={handleChbxChange}
                        name="step4"
                        color="primary"
                      />
                    }
                    label="Step 4 - Additional information"
                  />
                  <Divider sx={{mb: 2}} />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={state.other}
                        onChange={handleChbxChange}
                        name="other"
                        color="primary"
                      />
                    }
                    label="I need help with something else"
                  />
                </FormGroup>
              </FormControl>
            </Box>
            <Typography variant="body2">
              Click the "Get support" button and one of our Analysts will
              contact you to resolve the issue. We aim to make contact within 1
              to 2 business days.
            </Typography>
          </div>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button
            variant="outlined"
            color="primary"
            onClick={toggleDialog}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                toggleDialog(e);
              }
            }}
          >
            {t('Cancel')}
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            onClick={submitDialog}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                submitDialog(e);
              }
            }}
          >
            {t('Get support')}
          </Button>
        </DialogActions>
        {props.snackbar ? props.snackbar : null}
      </StyledDialog>
      {props.snackbar ? props.snackbar : null}
    </React.Fragment>
  );
}

// ////////////////////////////////////////// DENY REQUEST
export function DialogDenied(props) {
  const {toggleDialog, open} = props;
  const {t} = useTranslation();
  const [snackbar, setSnackbar] = React.useState(false);
  const initial = {
    // blank object used to reset state
    hours: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
    minutes: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
    reason: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
  };
  const [state, setState] = React.useState({
    hours: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
    minutes: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
    reason: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
  });

  const handleChange = (e, val) => {
    const comment = e.target.value;
    setState({
      ...state,
      [val]: {
        // updates state with text from input
        ...state[val],
        text: comment,
      },
    });

    if (e.target.value && state[val].errorText) {
      // if input text is valid, clear errors
      setState({
        ...state,
        [val]: {
          ...state[val],
          text: comment,
          errorText: '',
          invalid: '',
          commands: '',
        },
      });
    }
  };

  const SnackbarClose = () => {
    setSnackbar(false);
  };

  const validateForm = () => {
    let isError = false;
    if (state.hours.text.trim() === '') {
      isError = true;
      state.hours.invalid = t('Enter total hours.');
      state.hours.errorText = t('Enter total hours.');
    }
    if (state.minutes.text.trim() === '' || state.minutes.text.trim() === '.') {
      isError = true;
      state.minutes.invalid = t('Enter total minutes.');
      state.minutes.errorText = t('Enter total minutes.');
    }
    if (state.reason.text.trim() === '') {
      isError = true;
      state.reason.invalid = t('Select a reason.');
      state.reason.errorText = t('Select a reason.');
    }

    if (isError) {
      setState({
        ...state,
      });
    }

    return isError;
  };

  const submitForm = (e) => {
    e.stopPropagation();
    e.preventDefault();
    const err = validateForm();
    if (!err) {
      // if no errors exist, submit the form, toggle snackbar, and reset the inputs
      toggleDialog(e);
      setSnackbar(!snackbar);
      setState({...initial});
    } else {
      for (const property in state) {
        // focus on the first input that has an error on submit
        if (state[property].invalid) {
          switch (property) {
            case 'hours':
              document.getElementById('hours-input').focus();
              break;
            case 'minutes':
              document.getElementById('minutes-input').focus();
              break;
            case 'reason':
              document.getElementById('reason-select-label').focus();
              break;
            case 'comments':
              document.getElementById('comments-input').focus();
              break;
            default:
              break;
          }
          break;
        }
      }
    }
  };

  const disableCutCopyPaste = (e, command, value) => {
    // display error if user tries to cut/copy/paste
    let msg;
    e.preventDefault();
    switch (command) {
      case 'cut':
        msg = t('Cut has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      case 'copy':
        msg = t('Copy has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      case 'paste':
        msg = t('Paste has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      default:
        break;
    }
  };

  const toggleHelperText = (value) => {
    if (state[value].commands === state[value].errorText) {
      if (Boolean(state[value].invalid)) {
        // set error text back to invalid error
        setState({
          ...state,
          [value]: {
            ...state[value],
            errorText: state[value].invalid,
          },
        });
      } else {
        // clear error text if no invalid error exists
        setState({
          ...state,
          [value]: {
            ...state[value],
            errorText: '',
          },
        });
      }
    }
  };

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={(e) => {
          setState({...initial});
          toggleDialog(e);
        }}
        aria-labelledby="dashboard-dialog-title"
        open={open}
        className={classes.root}
        scroll="paper"
        onClick={handleClick}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
          }
        }}
      >
        <DialogTitle id="dashboard-dialog-title" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              {t('Don\'t approve request')}
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              onKeyPress={(e) => {
                e.preventDefault();
                e.stopPropagation();
                if (e.key === 'Enter') {
                  toggleDialog(e);
                }
              }}
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <form onSubmit={submitForm} noValidate id="deny-form">
            <div className={classes.vettingSection}>
              <div className={classes.vettingRow}>
                <div className={classes.vettingColumn}>
                  <Typography variant="subtitle2" component="h3">
                    {t('Billable hours')}
                  </Typography>
                </div>
              </div>
              <div className={clsx(classes.vettingRow, classes.alignStart)}>
                <div className={classes.vettingColumn}>
                  <FormControl variant="outlined">
                    <NumberFormat
                      id="hours-input"
                      label={t('Hours')}
                      value={state.hours.text}
                      customInput={TextField}
                      type="text"
                      variant="outlined"
                      error={Boolean(state.hours.errorText)}
                      helperText={state.hours.errorText}
                      required
                      onCut={(e) => disableCutCopyPaste(e, 'cut', 'hours')}
                      onCopy={(e) => disableCutCopyPaste(e, 'copy', 'hours')}
                      onPaste={(e) => disableCutCopyPaste(e, 'paste', 'hours')}
                      onChange={(e) => handleChange(e, 'hours')}
                      onClick={() => toggleHelperText('hours')}
                      onBlur={() => toggleHelperText('hours')}
                      onFocus={() => toggleHelperText('hours')}
                      autoComplete="off"
                    />
                  </FormControl>
                </div>
                <div className={classes.vettingColumn}>
                  <FormControl variant="outlined">
                    <NumberFormat
                      id="minutes-input"
                      label={t('Minutes')}
                      value={state.minutes.text}
                      customInput={TextField}
                      type="text"
                      variant="outlined"
                      isAllowed={(values) => {
                        const {formattedValue, floatValue} = values;
                        return formattedValue === '' || floatValue <= 60;
                      }}
                      error={Boolean(state.minutes.errorText)}
                      helperText={state.minutes.errorText}
                      required
                      onCut={(e) => disableCutCopyPaste(e, 'cut', 'minutes')}
                      onCopy={(e) => disableCutCopyPaste(e, 'copy', 'minutes')}
                      onPaste={(e) =>
                        disableCutCopyPaste(e, 'paste', 'minutes')
                      }
                      onChange={(e) => handleChange(e, 'minutes')}
                      onClick={() => toggleHelperText('minutes')}
                      onBlur={() => toggleHelperText('minutes')}
                      onFocus={() => toggleHelperText('minutes')}
                      autoComplete="off"
                    />
                  </FormControl>
                </div>
              </div>

              <div className={classes.vettingRow}>
                <div className={classes.vettingColumn}>
                  <FormControl
                    sx={{m: 0}}
                    margin="dense"
                    required
                    variant="outlined"
                    fullWidth
                  >
                    <InputLabel id="reason-select-label">
                      {t('Reason')}
                      <span className="screen-reader-text">
                        {t('required')}
                      </span>
                    </InputLabel>
                    <Select
                      labelId="reason-select-label"
                      label={t('Reason') + ' *'}
                      value={state.reason.text}
                      placeholder={t('Select an option')}
                      onChange={(e) => handleChange(e, 'reason')}
                      error={Boolean(state.reason.errorText)}
                    >
                      <MenuItem value="">None</MenuItem>
                      <MenuItem value="Non-SSI project">
                        {t('Non-SSI project')}
                      </MenuItem>
                      <MenuItem value="Confidential requirements are not met">
                        {t('Confidential requirements are not met')}
                      </MenuItem>
                      <MenuItem value="Request is missing information">
                        {t('Request is missing information')}
                      </MenuItem>
                      <MenuItem value="Output file(s) are not in line with the project proposal">
                        {t(
                            'Output file(s) are not in line with the project proposal',
                        )}
                      </MenuItem>
                      <MenuItem value="Other">{t('Other')}</MenuItem>
                    </Select>
                    <FormHelperText>{state.reason.errorText}</FormHelperText>
                  </FormControl>
                </div>
              </div>
            </div>
          </form>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button
            variant="outlined"
            color="primary"
            onClick={(e) => {
              setState({...initial});
              toggleDialog(e);
            }}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                setState({...initial});
                toggleDialog(e);
              }
            }}
          >
            {t('Cancel')}
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            className={classes.footerBtns}
            form="deny-form"
            onKeyPress={(e) => {
              e.stopPropagation();
              if (e.key === 'Enter') {
                submitForm(e);
              }
            }}
          >
            {t('Don\'t approve')}
          </Button>
        </DialogActions>
        <SnackbarDenyRequest open={snackbar} handleClose={SnackbarClose} />
      </StyledDialog>
      <SnackbarDenyRequest open={snackbar} handleClose={SnackbarClose} />
    </React.Fragment>
  );
}

// ////////////////////////////////////////// APPROVE REQUEST
export function DialogApprove(props) {
  const {t} = useTranslation();
  const {toggleDialog, open} = props;
  const [snackbar, setSnackbar] = React.useState(false);
  const initial = {
    // blank object used to reset state
    hours: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
    minutes: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
  };
  const [state, setState] = React.useState({
    hours: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
    minutes: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
  });

  const handleChange = (e, val) => {
    const comment = e.target.value;
    setState({
      ...state,
      [val]: {
        // updates state with text from input
        ...state[val],
        text: comment,
      },
    });

    if (e.target.value && state[val].errorText) {
      // if input text is valid, clear error
      setState({
        ...state,
        [val]: {
          ...state[val],
          text: comment,
          errorText: '',
          invalid: '',
          commands: '',
        },
      });
    }
  };

  const SnackbarClose = () => {
    setSnackbar(false);
  };

  const validateForm = () => {
    let isError = false;
    if (state.hours.text.trim() === '') {
      isError = true;
      state.hours.invalid = t('Enter total hours.');
      state.hours.errorText = t('Enter total hours.');
    }
    if (state.minutes.text.trim() === '' || state.minutes.text.trim() === '.') {
      isError = true;
      state.minutes.invalid = t('Enter total minutes.');
      state.minutes.errorText = t('Enter total minutes.');
    }

    if (isError) {
      setState({
        ...state,
      });
    }

    return isError;
  };

  const submitForm = (e) => {
    e.stopPropagation();
    e.preventDefault();
    const err = validateForm();
    if (!err) {
      // if no errors exist, submit the form, toggle snackbar, and reset the inputs
      toggleDialog(e);
      setSnackbar(!snackbar);
      setState({...initial});
    } else {
      for (const property in state) {
        // focus on the first input that has an error on submit
        if (state[property].invalid) {
          switch (property) {
            case 'hours':
              document.getElementById('hours-input').focus();
              break;
            case 'minutes':
              document.getElementById('minutes-input').focus();
              break;
            default:
              break;
          }
          break;
        }
      }
    }
  };

  const disableCutCopyPaste = (e, command, value) => {
    // display error if user tries to cut/copy/paste
    let msg;
    e.preventDefault();
    switch (command) {
      case 'cut':
        msg = t('Cut has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      case 'copy':
        msg = t('Copy has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      case 'paste':
        msg = t('Paste has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      default:
        break;
    }
  };

  const toggleHelperText = (value) => {
    if (state[value].commands === state[value].errorText) {
      if (Boolean(state[value].invalid)) {
        // set error text back to invalid error
        setState({
          ...state,
          [value]: {
            ...state[value],
            errorText: state[value].invalid,
          },
        });
      } else {
        // clear error text if no invalid error exists
        setState({
          ...state,
          [value]: {
            ...state[value],
            errorText: '',
          },
        });
      }
    }
  };

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={toggleDialog}
        aria-labelledby="dashboard-dialog-title"
        open={open}
        className={classes.root}
        scroll="paper"
        onClick={handleClick}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
          }
        }}
      >
        <DialogTitle id="dashboard-dialog-title" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              {t('Approve request')}
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              onKeyPress={(e) => {
                e.preventDefault();
                e.stopPropagation();
                if (e.key === 'Enter') {
                  toggleDialog(e);
                }
              }}
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <form onSubmit={submitForm} noValidate id="approve-form">
            <div className={classes.vettingSection}>
              <div className={classes.vettingRow}>
                <div className={classes.vettingColumn}>
                  <Typography variant="subtitle2" component="h3">
                    {t('Billable hours')}
                  </Typography>
                </div>
              </div>
              <div className={clsx(classes.vettingRow, classes.alignStart)}>
                <div className={classes.vettingColumn}>
                  <FormControl variant="outlined">
                    <NumberFormat
                      id="hours-input"
                      label={t('Hours')}
                      value={state.hours.text}
                      customInput={TextField}
                      type="text"
                      variant="outlined"
                      error={Boolean(state.hours.errorText)}
                      helperText={state.hours.errorText}
                      required
                      onCut={(e) => disableCutCopyPaste(e, 'cut', 'hours')}
                      onCopy={(e) => disableCutCopyPaste(e, 'copy', 'hours')}
                      onPaste={(e) => disableCutCopyPaste(e, 'paste', 'hours')}
                      onChange={(e) => handleChange(e, 'hours')}
                      onClick={() => toggleHelperText('hours')}
                      onBlur={() => toggleHelperText('hours')}
                      onFocus={() => toggleHelperText('hours')}
                      autoComplete="off"
                    />
                  </FormControl>
                </div>
                <div className={classes.vettingColumn}>
                  <FormControl variant="outlined">
                    <NumberFormat
                      id="minutes-input"
                      label={t('Minutes')}
                      value={state.minutes.text}
                      customInput={TextField}
                      type="text"
                      variant="outlined"
                      isAllowed={(values) => {
                        const {formattedValue, floatValue} = values;
                        return formattedValue === '' || floatValue <= 60;
                      }}
                      error={Boolean(state.minutes.errorText)}
                      helperText={state.minutes.errorText}
                      required
                      onCut={(e) => disableCutCopyPaste(e, 'cut', 'minutes')}
                      onCopy={(e) => disableCutCopyPaste(e, 'copy', 'minutes')}
                      onPaste={(e) =>
                        disableCutCopyPaste(e, 'paste', 'minutes')
                      }
                      onChange={(e) => handleChange(e, 'minutes')}
                      onClick={() => toggleHelperText('minutes')}
                      onBlur={() => toggleHelperText('minutes')}
                      onFocus={() => toggleHelperText('minutes')}
                      autoComplete="off"
                    />
                  </FormControl>
                </div>
              </div>
            </div>
          </form>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button
            variant="outlined"
            color="primary"
            onClick={(e) => {
              setState({...initial});
              toggleDialog(e);
            }}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                setState({...initial});
                toggleDialog(e);
              }
            }}
          >
            {t('Cancel')}
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            className={classes.footerBtns}
            form="approve-form"
            onKeyPress={(e) => {
              e.stopPropagation();
              if (e.key === 'Enter') {
                submitForm(e);
              }
            }}
          >
            {t('Approve')}
          </Button>
        </DialogActions>
        <SnackbarApproveRequest open={snackbar} handleClose={SnackbarClose} />
      </StyledDialog>
      <SnackbarApproveRequest open={snackbar} handleClose={SnackbarClose} />
    </React.Fragment>
  );
}

// ////////////////////////////////////////// NEW REQUEST TITLE
export function DialogNewRequestTitle(props) {
  const {t} = useTranslation();
  const history = useHistory();
  const location = useLocation();
  const {toggleDialog, open, role} = props;
  const initial = {
    // blank object used to reset state
    name: {
      text: 'Untitled request',
      errorText: '',
      invalid: '',
      commands: '',
    },
    path: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
  };
  const [state, setState] = React.useState({
    name: {
      text: 'Untitled request',
      errorText: '',
      invalid: '',
      commands: '',
    },
    path: null,
    from: location.pathname,
  });

  const handleChange = (e, val) => {
    const comment = e.target.value;
    setState({
      ...state,
      [val]: {
        // updates state with text from input
        ...state[val],
        text: comment,
      },
    });

    if (e.target.value && state.name.errorText) {
      // if input text is valid, clear error
      setState({
        ...state,
        [val]: {
          ...state[val],
          text: comment,
          errorText: '',
          invalid: '',
          commands: '',
        },
      });
    }
  };

  const validateForm = () => {
    let isError = false;
    if (state.name.text.trim() === '') {
      isError = true;
      state.name.invalid = t('Enter a title.');
      state.name.errorText = t('Enter a title.');
    }

    if (isError) {
      setState({
        ...state,
      });
    }

    return isError;
  };

  const submitForm = (e) => {
    e.preventDefault();
    const err = validateForm();
    if (!err) {
      // if no errors exist, submit the form and reset the inputs
      toggleDialog();
      setState({...initial});

      if (role === 'researcher') {
        history.push({
          pathname: '/vetting-app/request-researcher',
          state,
        });
      }
    } else {
      for (const property in state) {
        // focus on the first input that has an error on submit
        if (state[property].invalid) {
          switch (property) {
            case 'title':
              document.getElementById('title-input').focus();
              break;
            default:
              break;
          }
          break;
        }
      }
    }
  };

  const disableCutCopyPaste = (e, command, value) => {
    // display error if user tries to cut/copy/paste
    let msg;
    e.preventDefault();
    switch (command) {
      case 'cut':
        msg = t('Cut has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      case 'copy':
        msg = t('Copy has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      case 'paste':
        msg = t('Paste has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      default:
        break;
    }
  };

  const toggleHelperText = (value) => {
    if (state[value].commands === state[value].errorText) {
      if (Boolean(state[value].invalid)) {
        // set error text back to invalid error
        setState({
          ...state,
          [value]: {
            ...state[value],
            errorText: state[value].invalid,
          },
        });
      } else {
        // clear error text if no invalid error exists
        setState({
          ...state,
          [value]: {
            ...state[value],
            errorText: '',
          },
        });
      }
    }
    if (!state[value].text) {
      // if field is empty, set field to "untitled request"
      setState({
        ...state,
        [value]: {
          ...state[value],
          text: initial[value].text,
        },
      });
    }
  };

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={() => {
          setState({...initial});
          toggleDialog();
        }}
        aria-labelledby="dashboard-dialog-title"
        open={open}
        className={classes.root}
        scroll="paper"
        onClick={handleClick}
        maxWidth="md"
      >
        <DialogTitle id="dashboard-dialog-title" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              {t('New vetting request')}
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <form onSubmit={submitForm} noValidate id="new-form">
            <div className={classes.vettingSection}>
              <div className={classes.vettingRow}>
                <div className={classes.vettingColumn}>
                  <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
                    {t('Enter the details below to start your request...')}
                  </Typography>
                  <Typography component="p" variant="body2">
                    {t(
                        'Select the request folder associated with this request. If the request folder can not be found, please add it to the shared drive of your virtual machine. You will be required to place all associated files in this folder.',
                    )}
                  </Typography>
                </div>
              </div>
              <div className={classes.vettingRow}>
                <div className={classes.vettingColumn}>
                  <Autocomplete
                    sx={{m: 0}}
                    componentsProps={{
                      clearIndicator: {tabIndex: 0},
                    }}
                    classes={{
                      option: classes.autocompleteOption,
                    }}
                    id="request-folder-path"
                    options={[
                      'Project folder / Shared folder / Request folder one',
                      'Project folder / Shared folder / Request folder two',
                      'Project folder / Shared folder / Request folder three',
                      'Project folder / Shared folder / VettingRequests / User_Name / Files for output / A really long folder name',
                    ]}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        multiline
                        minRows="1"
                        label="Request folder path"
                        required={true}
                        variant="outlined"
                      />
                    )}
                    value={state.path}
                    onChange={(event, newValue) => {
                      setState({...state, path: newValue});
                    }}
                  />
                </div>
              </div>
              <div className={classes.vettingRow}>
                <div className={classes.vettingColumn}>
                  <FormControl variant="outlined">
                    <TextField
                      id="name-input"
                      label={t('Request name')}
                      aria-label={t('Request name')}
                      value={state.name.text}
                      error={Boolean(state.name.errorText)}
                      helperText={state.name.errorText}
                      onCut={(e) => disableCutCopyPaste(e, 'cut', 'name')}
                      onCopy={(e) => disableCutCopyPaste(e, 'copy', 'name')}
                      onPaste={(e) => disableCutCopyPaste(e, 'paste', 'name')}
                      onChange={(e) => handleChange(e, 'name')}
                      onClick={() => toggleHelperText('name')}
                      onBlur={() => toggleHelperText('name')}
                      autoComplete="off"
                    />
                  </FormControl>
                </div>
              </div>
            </div>
          </form>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button
            variant="outlined"
            color="primary"
            onClick={() => {
              setState({...initial});
              toggleDialog();
            }}
            className={classes.footerBtns}
          >
            {t('Cancel')}
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            className={classes.footerBtns}
            form="new-form"
          >
            {t('Create')}
          </Button>
        </DialogActions>
      </StyledDialog>
    </React.Fragment>
  );
}

// ////////////////////////////////////////// NO LEAD ASSIGNED
export function DialogNoLead(props) {
  const {t} = useTranslation();
  const {submitDialog, toggleDialog, open} = props;

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={toggleDialog}
        aria-labelledby="dashboard-dialog-title"
        aria-describedby="dashboard-dialog-description"
        open={open}
        className={classes.root}
        scroll="paper"
        onClick={handleClick}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
          }
        }}
      >
        <DialogTitle id="dashboard-dialog-title" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              {t('Continue with no lead?')}
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              onKeyPress={(e) => {
                e.preventDefault();
                e.stopPropagation();
                if (e.key === 'Enter') {
                  toggleDialog(e);
                }
              }}
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <div className={classes.vettingSection}>
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <Alert
                  id="dashboard-dialog-description"
                  severity="warning"
                  className={classes.alert}
                  role="note"
                >
                  {t(
                      'If you continue, the request will have no lead and the requester will be notified of the change.',
                  )}
                </Alert>
              </div>
            </div>
          </div>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button
            variant="outlined"
            color="primary"
            onClick={toggleDialog}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                toggleDialog(e);
              }
            }}
          >
            {t('Cancel')}
          </Button>
          <Button
            autoFocus
            variant="contained"
            color="primary"
            onClick={submitDialog}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                submitDialog(e);
              }
            }}
          >
            {t('Continue')}
          </Button>
        </DialogActions>
        {props.snackbar ? props.snackbar : null}
      </StyledDialog>
    </React.Fragment>
  );
}

// ////////////////////////////////////////// ASSIGN AS LEAD
export function DialogAssignAsLead(props) {
  const {t} = useTranslation();
  const {toggleDialog, open, handleAssignMeAs, origin} = props;
  const [snackbar, setSnackbar] = React.useState({
    snackbarAssignLead: false,
  });
  const initial = {
    // blank object used to reset state
    phone: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
  };
  const [state, setState] = React.useState({
    phone: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
  });

  const phoneExp = /^[+][1]\s\([0-9]{3}\)\s[0-9]{3}\s[0-9]{4}/;

  const handleSnackbarOpen = (state) => {
    setSnackbar({...snackbar, [state]: true});
  };

  const handleSnackbarClose = (state) => {
    setSnackbar({...snackbar, [state]: false});
  };

  const handleChange = (e, val) => {
    const comment = e.target.value.trim();
    setState({
      ...state,
      [val]: {
        // updates state with text from input
        ...state[val],
        text: comment,
      },
    });

    if (e.target.value.match(phoneExp) && state[val].errorText) {
      // if input text is valid, clear error
      setState({
        ...state,
        [val]: {
          ...state[val],
          text: comment,
          errorText: '',
          invalid: '',
          commands: '',
        },
      });
    }
  };

  const validateForm = () => {
    let isError = false;
    if (!state.phone.text.match(phoneExp)) {
      isError = true;
      state.phone.invalid = t('Enter phone number.');
      state.phone.errorText = t('Enter phone number.');
    }

    if (isError) {
      setState({
        ...state,
      });
    }

    return isError;
  };

  const submitForm = (e) => {
    e.preventDefault();
    const err = validateForm();
    if (!err) {
      // if no errors exist, submit the form and reset the inputs
      toggleDialog(e);
      setState({...initial});
      handleSnackbarOpen('snackbarAssignLead');
      if (origin === 'manageTeamDrawer') {
        handleAssignMeAs(state.phone.text, 'lead');
      }
    } else {
      for (const property in state) {
        // focus on the first input that has an error on submit
        if (state[property].invalid) {
          switch (property) {
            case 'phone':
              document.getElementById('phone-input').focus();
              break;
            default:
              break;
          }
          break;
        }
      }
    }
  };

  const disableCutCopyPaste = (e, command, value) => {
    // display error if user tries to cut/copy/paste
    let msg;
    e.preventDefault();
    switch (command) {
      case 'cut':
        msg = t('Cut has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      case 'copy':
        msg = t('Copy has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      case 'paste':
        msg = t('Paste has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      default:
        break;
    }
  };

  const toggleHelperText = (value) => {
    if (state[value].commands === state[value].errorText) {
      if (Boolean(state[value].invalid)) {
        // set error text back to invalid error
        setState({
          ...state,
          [value]: {
            ...state[value],
            errorText: state[value].invalid,
          },
        });
      } else {
        // clear error text if no invalid error exists
        setState({
          ...state,
          [value]: {
            ...state[value],
            errorText: '',
          },
        });
      }
    }
  };

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={() => {
          setState({...initial});
          toggleDialog();
        }}
        aria-labelledby="dashboard-dialog-title"
        open={open}
        className={classes.root}
        scroll="paper"
        onClick={handleClick}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
          }
        }}
      >
        <DialogTitle id="dashboard-dialog-title" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              {t('Assign me as lead')}
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              onKeyPress={(e) => {
                e.preventDefault();
                e.stopPropagation();
                if (e.key === 'Enter') {
                  toggleDialog(e);
                }
              }}
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <form onSubmit={submitForm} noValidate id="assign-form">
            <div className={classes.vettingSection}>
              <div className={classes.vettingRow}>
                <div className={classes.vettingColumn}>
                  <Typography variant="body2">
                    {t(
                        'To assign yourself to the request you must first enter a secure phone number',
                    )}
                  </Typography>
                </div>
              </div>
              <div className={classes.vettingRow}>
                <div className={classes.vettingColumn}>
                  <FormControl variant="outlined" className={classes.textField}>
                    <NumberFormat
                      id="phone-input"
                      label={t('Phone number')}
                      value={state.phone.text}
                      customInput={TextField}
                      type="text"
                      variant="outlined"
                      format="+1 (###) ### ####"
                      mask="_"
                      allowEmptyFormatting
                      autoComplete="tel-national"
                      error={Boolean(state.phone.errorText)}
                      helperText={state.phone.errorText}
                      required
                      onCut={(e) => disableCutCopyPaste(e, 'cut', 'phone')}
                      onCopy={(e) => disableCutCopyPaste(e, 'copy', 'phone')}
                      onPaste={(e) => disableCutCopyPaste(e, 'paste', 'phone')}
                      onChange={(e) => handleChange(e, 'phone')}
                      onClick={() => toggleHelperText('phone')}
                      onBlur={() => toggleHelperText('phone')}
                      onFocus={() => toggleHelperText('phone')}
                    />
                  </FormControl>
                </div>
              </div>
            </div>
          </form>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button
            variant="outlined"
            color="primary"
            onClick={(e) => {
              setState({...initial});
              toggleDialog(e);
            }}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                toggleDialog(e);
              }
            }}
          >
            {t('Cancel')}
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            className={classes.footerBtns}
            form="assign-form"
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                submitForm(e);
              }
            }}
          >
            {t('Assign')}
          </Button>
        </DialogActions>
        <SnackbarAssignLead
          open={snackbar.snackbarAssignLead}
          handleClose={() => handleSnackbarClose('snackbarAssignLead')}
        />
      </StyledDialog>
      <SnackbarAssignLead
        open={snackbar.snackbarAssignLead}
        handleClose={() => handleSnackbarClose('snackbarAssignLead')}
      />
    </React.Fragment>
  );
}

// ////////////////////////////////////////// ASSIGN AS SUPPORT
export function DialogAssignAsSupport(props) {
  const {t} = useTranslation();
  const {toggleDialog, open, handleAssignMeAs, origin} = props;
  const [snackbar, setSnackbar] = React.useState({
    snackbarAssignSupport: false,
  });
  const initial = {
    // blank object used to reset state
    phone: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
  };
  const [state, setState] = React.useState({
    phone: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
    },
  });

  const phoneExp = /^[+][1]\s\([0-9]{3}\)\s[0-9]{3}\s[0-9]{4}/;

  const handleSnackbarOpen = (state) => {
    setSnackbar({...snackbar, [state]: true});
  };

  const handleSnackbarClose = (state) => {
    setSnackbar({...snackbar, [state]: false});
  };

  const handleChange = (e, val) => {
    const comment = e.target.value.trim();
    setState({
      ...state,
      [val]: {
        // updates state with text from input
        ...state[val],
        text: comment,
      },
    });

    if (e.target.value.match(phoneExp) && state[val].errorText) {
      // if input text is valid, clear error
      setState({
        ...state,
        [val]: {
          ...state[val],
          text: comment,
          errorText: '',
          invalid: '',
          commands: '',
        },
      });
    }
  };

  const validateForm = () => {
    let isError = false;
    if (!state.phone.text.match(phoneExp)) {
      isError = true;
      state.phone.invalid = t('Enter phone number.');
      state.phone.errorText = t('Enter phone number.');
    }

    if (isError) {
      setState({
        ...state,
      });
    }

    return isError;
  };

  const submitForm = (e) => {
    e.preventDefault();
    const err = validateForm();
    if (!err) {
      // if no errors exist, submit the form and reset the inputs
      toggleDialog(e);
      setState({...initial});
      handleSnackbarOpen('snackbarAssignSupport');
      if (origin === 'manageTeamDrawer') {
        handleAssignMeAs(state.phone.text, 'support');
      }
    } else {
      for (const property in state) {
        // focus on the first input that has an error on submit
        if (state[property].invalid) {
          switch (property) {
            case 'phone':
              document.getElementById('phone-input').focus();
              break;
            default:
              break;
          }
          break;
        }
      }
    }
  };

  const disableCutCopyPaste = (e, command, value) => {
    // display error if user tries to cut/copy/paste
    let msg;
    e.preventDefault();
    switch (command) {
      case 'cut':
        msg = t('Cut has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      case 'copy':
        msg = t('Copy has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      case 'paste':
        msg = t('Paste has been disabled for security purposes.');
        setState({
          ...state,
          [value]: {
            ...state[value],
            commands: msg,
            errorText: msg,
          },
        });
        break;
      default:
        break;
    }
  };

  const toggleHelperText = (value) => {
    if (state[value].commands === state[value].errorText) {
      if (Boolean(state[value].invalid)) {
        // set error text back to invalid error
        setState({
          ...state,
          [value]: {
            ...state[value],
            errorText: state[value].invalid,
          },
        });
      } else {
        // clear error text if no invalid error exists
        setState({
          ...state,
          [value]: {
            ...state[value],
            errorText: '',
          },
        });
      }
    }
  };

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={() => {
          setState({...initial});
          toggleDialog();
        }}
        aria-labelledby="dashboard-dialog-title"
        open={open}
        className={classes.root}
        scroll="paper"
        onClick={handleClick}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
          }
        }}
      >
        <DialogTitle id="dashboard-dialog-title" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              {t('Assign me as support')}
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              onKeyPress={(e) => {
                e.preventDefault();
                e.stopPropagation();
                if (e.key === 'Enter') {
                  toggleDialog(e);
                }
              }}
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <form onSubmit={submitForm} noValidate id="assign-form">
            <div className={classes.vettingSection}>
              <div className={classes.vettingRow}>
                <div className={classes.vettingColumn}>
                  <Typography variant="body2">
                    {t(
                        'To assign yourself to the request you must first enter a secure phone number',
                    )}
                  </Typography>
                </div>
              </div>
              <div className={classes.vettingRow}>
                <div className={classes.vettingColumn}>
                  <FormControl variant="outlined" className={classes.textField}>
                    <NumberFormat
                      id="phone-input"
                      label={t('Phone number')}
                      value={state.phone.text}
                      customInput={TextField}
                      type="text"
                      variant="outlined"
                      format="+1 (###) ### ####"
                      mask="_"
                      allowEmptyFormatting
                      autoComplete="tel-national"
                      error={Boolean(state.phone.errorText)}
                      helperText={state.phone.errorText}
                      required
                      onCut={(e) => disableCutCopyPaste(e, 'cut', 'phone')}
                      onCopy={(e) => disableCutCopyPaste(e, 'copy', 'phone')}
                      onPaste={(e) => disableCutCopyPaste(e, 'paste', 'phone')}
                      onChange={(e) => handleChange(e, 'phone')}
                      onClick={() => toggleHelperText('phone')}
                      onBlur={() => toggleHelperText('phone')}
                      onFocus={() => toggleHelperText('phone')}
                    />
                  </FormControl>
                </div>
              </div>
            </div>
          </form>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button
            variant="outlined"
            color="primary"
            onClick={(e) => {
              setState({...initial});
              toggleDialog(e);
            }}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                toggleDialog(e);
              }
            }}
          >
            {t('Cancel')}
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            className={classes.footerBtns}
            form="assign-form"
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                submitForm(e);
              }
            }}
          >
            {t('Assign')}
          </Button>
        </DialogActions>
        <SnackbarAssignSupport
          open={snackbar.snackbarAssignSupport}
          handleClose={() => handleSnackbarClose('snackbarAssignSupport')}
        />
      </StyledDialog>
      <SnackbarAssignSupport
        open={snackbar.snackbarAssignSupport}
        handleClose={() => handleSnackbarClose('snackbarAssignSupport')}
      />
    </React.Fragment>
  );
}

// ////////////////////////////////////////// DELETE
export function DialogDelete(props) {
  const {t} = useTranslation();
  const {submitDialog, toggleDialog, open} = props;

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={toggleDialog}
        aria-labelledby="dashboard-dialog-title"
        aria-describedby="dashboard-dialog-description"
        open={open}
        className={classes.root}
        scroll="paper"
        onClick={handleClick}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
          }
        }}
      >
        <DialogTitle id="dashboard-dialog-title" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              {t('Delete permanently?')}
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              onKeyPress={(e) => {
                e.preventDefault();
                e.stopPropagation();
                if (e.key === 'Enter') {
                  toggleDialog(e);
                }
              }}
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <div className={classes.vettingSection}>
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <Alert
                  id="dashboard-dialog-description"
                  severity="warning"
                  className={classes.alert}
                  role="note"
                >
                  {t('If you delete this item, it cannot be undone.')}
                </Alert>
              </div>
            </div>
          </div>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button
            variant="outlined"
            color="primary"
            onClick={toggleDialog}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                toggleDialog(e);
              }
            }}
          >
            {t('Cancel')}
          </Button>
          <Button
            autoFocus
            variant="contained"
            color="primary"
            onClick={submitDialog}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                submitDialog(e);
              }
            }}
          >
            {t('Delete')}
          </Button>
        </DialogActions>
        {props.snackbar ? props.snackbar : null}
      </StyledDialog>
    </React.Fragment>
  );
}

// ////////////////////////////////////////// OUTPUT METHOD HELP
export function DialogOutputMethodHelp(props) {
  const {t} = useTranslation();
  const {toggleDialog, open} = props;

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={(e) => {
          toggleDialog(e);
        }}
        aria-labelledby="dialog-output-method-help"
        open={open}
        className={classes.root}
        scroll="paper"
        onClick={handleClick}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
          }
        }}
      >
        <DialogTitle id="dialog-output-method-help" component="div">
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              {t('Output method help')}
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label={t('Close')}
              onKeyPress={(e) => {
                e.preventDefault();
                e.stopPropagation();
                if (e.key === 'Enter') {
                  toggleDialog(e);
                }
              }}
              size="large"
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <div className={classes.vettingSection}>
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
                  {t('Descriptive')}
                </Typography>
                <Typography variant="body2" component="p">
                  {t(
                      'Examples include: ANOVA, correlation matrix, cross-tabular analysis, distributions, frequencies, kurtosis, means, medians, modes, percentages, quartiles, ranges, ratios, regression models with only one independent variable, skewness, standard deviations, totals, variances',
                  )}
                </Typography>
              </div>
            </div>
            <Divider className={classes.divider} />
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
                  {t('Scaling')}
                </Typography>
                <Typography variant="body2" component="p">
                  {t(
                      'Examples include: Likert scale, factor analysis, reliability analysis, latent class analysis, guttman scale, multidimension scaling',
                  )}
                </Typography>
              </div>
            </div>
            <Divider className={classes.divider} />
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
                  {t('Graphs')}
                </Typography>
                <Typography variant="body2" component="p">
                  {t('Examples include: Histograms')}
                </Typography>
              </div>
            </div>
            <Divider className={classes.divider} />
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
                  {t('Multivariable regression analysis')}
                </Typography>
                <Typography variant="body2" component="p">
                  {t('Examples include: OLS, logistic, probit, tobit, poisson')}
                </Typography>
              </div>
            </div>
            <Divider className={classes.divider} />
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <Typography variant="subtitle2" component="h3" sx={{mb: 3}}>
                  {t('Complex modeling')}
                </Typography>
                <Typography variant="body2" component="p">
                  {t(
                      'Examples include: Structural equation modeling, hierarchical linear modeling, growth analysis, survival analysis, event history analysis, simultaneous-equations models, fixed effects models, random effects models',
                  )}
                </Typography>
              </div>
            </div>
          </div>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            className={classes.footerBtns}
            onClick={(e) => {
              toggleDialog(e);
            }}
            onKeyPress={(e) => {
              e.stopPropagation();
              if (e.key === 'Enter') {
                toggleDialog(e);
              }
            }}
          >
            {t('Close')}
          </Button>
        </DialogActions>
      </StyledDialog>
    </React.Fragment>
  );
}

// ////////////////////////////////////////// FILE SUPPORT
export function DialogFileSupport(props) {
  const {t} = useTranslation();
  const {submitDialog, toggleDialog, open, fileFunction} = props;
  const [state, setState] = React.useState({
    path: null,
    fileNotes: {
      text: '',
      errorText: '',
      invalid: '',
      commands: '',
      helperText: '',
    },
  });

  const handleClick = (e) => {
    e.stopPropagation();
  };

  const handleChange = (event) => {
    const name = event.target.name;
    const val = event.target.value;
    setState({
      ...state,
      [name]: {
        ...state[name],
        text: val,
      },
    });
  };

  const handleTitle = () => {
    if (fileFunction === 'view') {
      return t('View file for support');
    } else if (fileFunction === 'edit') {
      return t('Edit support file');
    } else {
      return t('Add support file');
    }
  };

  const handlePrimaryButton = () => {
    if (fileFunction === 'edit') {
      return t('Update');
    } else if (fileFunction === 'add') {
      return t('Add');
    } else {
      return t('Close');
    }
  };

  return (
    <React.Fragment>
      <StyledDialog
        onClose={toggleDialog}
        aria-labelledby="dashboard-dialog-title"
        aria-describedby="dashboard-dialog-description"
        open={open}
        className={classes.root}
        disableBackdropClick
        scroll="paper"
        onClick={handleClick}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
          }
        }}
      >
        <DialogTitle id="dashboard-dialog-title" disableTypography>
          <div className={classes.vettingContainerTitle}>
            <Typography variant="h6" component="h2">
              {t(handleTitle())}
            </Typography>
            <IconButton
              id="dialog-close"
              onClick={toggleDialog}
              edge="end"
              aria-label="Close"
              onKeyPress={(e) => {
                e.preventDefault();
                e.stopPropagation();
                if (e.key === 'Enter') {
                  toggleDialog(e);
                }
              }}
            >
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <div className={classes.vettingSection}>
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <Typography variant="subtitle2" component="h3">
                  {t('Select {supportFileType}')}
                </Typography>
              </div>
            </div>
            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <Autocomplete
                  classes={{
                    option: classes.autocompleteOption,
                  }}
                  componentsProps={{
                    clearIndicator: {tabIndex: 0},
                  }}
                  id="request-folder-path"
                  options={[
                    'spreadsheet1.xls',
                    'spreadsheet2.doc',
                    'spreadsheet3.xls',
                    'Project1\\VettingRequests\\User_Name\\Files for output\\a_really_long_path_name_spreadsheet4.xls',
                  ]}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      multiline
                      minRows="1"
                      label="File path"
                      required={true}
                      variant="outlined"
                    />
                  )}
                  value={state.path}
                  onChange={(event, newValue) => {
                    setState({...state, path: newValue});
                  }}
                />
              </div>
            </div>

            <div className={classes.vettingRow}>
              <div className={classes.vettingColumn}>
                <TextField
                  className={classes.inputMargin}
                  margin="dense"
                  id="fileNotes"
                  name="fileNotes"
                  label={t('Notes')}
                  variant="outlined"
                  fullWidth
                  required
                  multiline
                  minRows="3"
                  onChange={handleChange}
                  value={state.fileNotes.text}
                  error={Boolean(state.fileNotes.errorText)}
                  helperText={state.fileNotes.errorText}
                  autoComplete="off"
                />
              </div>
            </div>
          </div>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button
            variant="outlined"
            color="primary"
            onClick={toggleDialog}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                toggleDialog(e);
              }
            }}
          >
            {t('Cancel')}
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={submitDialog}
            className={classes.footerBtns}
            onKeyPress={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (e.key === 'Enter') {
                submitDialog(e);
              }
            }}
          >
            {t(handlePrimaryButton())}
          </Button>
        </DialogActions>
        {props.snackbar ? props.snackbar : null}
      </StyledDialog>
      {props.snackbar ? props.snackbar : null}
    </React.Fragment>
  );
}
