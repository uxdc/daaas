import React from 'react';
import {styled} from '@mui/material/styles';
import Fab from '@mui/material/Fab';
import Email from '@mui/icons-material/Email';
import {
  DialogFormGetSupportFab,
  DialogGetSupportFab,
} from '../CommonComponents/DialogBox';
import {SnackbarSupportFab} from './Snackbars';

const PREFIX = 'Support';

const classes = {
  fab: `${PREFIX}-fab`,
  extendedIcon: `${PREFIX}-extendedIcon`,
};

const Root = styled('div')(({theme}) => ({
  [`& .${classes.fab}`]: {
    position: 'fixed',
    bottom: theme.spacing(3),
    right: theme.spacing(3),
  },

  [`& .${classes.extendedIcon}`]: {
    marginRight: theme.spacing(1),
  },
}));

export default function FloatingSupportButton(props) {
  const [open, setOpen] = React.useState({
    dialogSupportFab: false,
    dialogFormSupport: false,
    snackbarSupportFab: false,
  });

  const handleClickOpen = (state) => {
    setOpen({...open, [state]: true});
  };

  const handleClickClose = (state) => {
    setOpen({...open, [state]: false});
  };

  const submitDialog = (e, dialog) => {
    e.stopPropagation();
    setOpen({...open, [dialog]: false});
    setTimeout(function() {
      setOpen({...open, snackbarSupportFab: true, [dialog]: false});
    }, 500);
  };

  return (
    <Root className={classes.root}>
      <Fab
        color="primary"
        variant="extended"
        onClick={
          props.form ?
            () => handleClickOpen('dialogFormSupport') :
            () => handleClickOpen('dialogSupportFab')
        }
        className={classes.fab}
      >
        <Email className={classes.extendedIcon} />
        Get support
      </Fab>
      <DialogGetSupportFab
        toggleDialog={() => handleClickClose('dialogSupportFab')}
        submitDialog={(e) => {
          submitDialog(e, 'dialogSupportFab');
        }}
        open={open.dialogSupportFab}
        snackbar={
          <SnackbarSupportFab
            open={open.snackbarSupportFab}
            handleClose={() => handleClickClose('snackbarSupportFab')}
          />
        }
      />
      <DialogFormGetSupportFab
        toggleDialog={() => handleClickClose('dialogFormSupport')}
        submitDialog={(e) => {
          submitDialog(e, 'dialogFormSupport');
        }}
        open={open.dialogFormSupport}
        snackbar={
          <SnackbarSupportFab
            open={open.snackbarSupportFab}
            handleClose={() => handleClickClose('snackbarSupportFab')}
          />
        }
      />
    </Root>
  );
}
