import React from 'react';
import {styled} from '@mui/material/styles';
import clsx from 'clsx';
import {
  DialogFormGetSupportFab,
  DialogGetSupportFab,
} from '../CommonComponents/DialogBox';
import {
  AppBar,
  Toolbar,
  MenuItem,
  ListItemText,
  Typography,
  Grid,
  Link,
  ListItemIcon,
  Divider,
  Button,
  Avatar,
  IconButton,
} from '@mui/material';
import Icon from '@mdi/react';
import {mdiHelpCircle, mdiMenuDown, mdiOpenInNew} from '@mdi/js';
import {BrandingStatCanEn} from './BrandingStatCan';
import Language from '../../../Components/Headers/Language';
import MenuIcon from '@mui/icons-material/Menu';
import {Menu} from '../../../Components/CommonComponents/Menu';
import {useTranslation} from 'react-i18next';
import {SM_SCREEN} from '../../../Theme/constants';
import {Alert} from '../../../Components/CommonComponents/Alert';

const PREFIX = 'Header';

const classes = {
  appBar: `${PREFIX}-appBar`,
  toolbar: `${PREFIX}-toolbar`,
  statisticsCanadaLogoHeight: `${PREFIX}-statisticsCanadaLogoHeight`,
  verticalDividerHeight: `${PREFIX}-verticalDividerHeight`,
  branding: `${PREFIX}-branding`,
  lang: `${PREFIX}-lang`,
  verticalDivider: `${PREFIX}-verticalDivider`,
  menuIcon: `${PREFIX}-menuIcon`,
};

const Root = styled('div')(({theme}) => ({
  position: 'sticky',
  top: 0,
  zIndex: 1200,

  [`& .${classes.appBar}`]: {
    backgroundColor: theme.palette.common.white,
    zIndex: 1100,
  },

  [`& .${classes.toolbar}`]: {
    padding: 0,
  },

  [`& .${classes.statisticsCanadaLogoHeight}`]: {
    height: theme.spacing(3),
    maxWidth: '100%',
  },

  [`& .${classes.verticalDividerHeight}`]: {
    height: theme.spacing(5),
  },

  [`& .${classes.branding}`]: {
    [theme.breakpoints.down('md')]: {
      flexGrow: 1,
    },
    '& img': {
      height: theme.spacing(3),
    },
  },

  [`& .${classes.lang}`]: {
    paddingLeft: theme.spacing(1),
  },

  [`& .${classes.verticalDivider}`]: {
    margin: theme.spacing(2),
  },
  [`& .${classes.menuIcon}`]: {
    marginLeft: theme.spacing(1),
  },
}));

function ShowMenuButton(props) {
  if (props.role === 'analyst' || props.role === 'researcher') {
    return (
      <IconButton
        onClick={props.clickHandler}
        sx={{mr: 1}}
        edge="start"
        aria-label={props.open ? 'Hide project list' : 'Show project list'}
        size="large"
      >
        <MenuIcon />
      </IconButton>
    );
  } else {
    return false;
  }
}

export default function DefaultHeader(props) {
  const {t} = useTranslation();

  const [state, setState] = React.useState({
    windowWidth: window.innerWidth,
    role: props.role,
  });

  const [open, setOpen] = React.useState({
    dialogSupportFab: false,
    dialogFormSupport: false,
    alert: JSON.parse(window.sessionStorage.getItem('alert') || true),
  });

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [anchorElHelp, setAnchorElHelp] = React.useState(null);

  const isSmScreen = state.windowWidth < SM_SCREEN;

  const handleClickOpen = (state) => {
    setOpen({...open, [state]: true});
  };

  const handleClickClose = (state) => {
    setOpen({...open, [state]: false});
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClickHelp = (event) => {
    setAnchorElHelp(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleCloseHelp = () => {
    setAnchorElHelp(null);
  };

  const handleDismissAlert = () => {
    window.sessionStorage.setItem('alert', JSON.stringify(false));
    setOpen({...open, alert: false});
  };

  function handleBlur(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setAnchorEl(null);
    }
  }

  function handleBlurHelp(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setAnchorElHelp(null);
    }
  }

  const menuItemSignOut = () => {
    return (
      <MenuItem onClick={handleClose} key="1">
        <ListItemText
          primary={<Typography variant="body2">{t('Sign out')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItemHandbook = () => {
    const ref = React.createRef();
    return (
      <MenuItem
        onClick={() => {
          handleCloseHelp();
          ref.current.click();
        }}
        key="2"
      >
        <Link ref={ref} href="#" target="_blank" underline="hover">
          <ListItemText
            primary={
              <Typography variant="body2">
                {t('Vetting handbook (Word, 461KB)')}
              </Typography>
            }
          />
          <ListItemIcon>
            <Icon path={mdiOpenInNew} size={1} className={classes.menuIcon} />
            <span className="screen-reader-text">Opens in new tab</span>
          </ListItemIcon>
        </Link>
      </MenuItem>
    );
  };

  const menuItemDocumentation = () => {
    const ref = React.createRef();
    return (
      <MenuItem
        divider
        onClick={() => {
          handleCloseHelp();
          ref.current.click();
        }}
        key="3"
      >
        <Link ref={ref} href="#" target="_blank" underline="hover">
          <ListItemText
            primary={
              <Typography variant="body2">
                {t('Training documentation (PDF, 3MB)')}
              </Typography>
            }
          />
          <ListItemIcon>
            <Icon path={mdiOpenInNew} size={1} className={classes.menuIcon} />
            <span className="screen-reader-text">Opens in new tab</span>
          </ListItemIcon>
        </Link>
      </MenuItem>
    );
  };

  const menuItemsSupport = () => {
    return (
      <MenuItem
        onClick={
          props.source === 'dashboard' ?
            () => {
              handleClickOpen('dialogSupportFab');
              handleCloseHelp();
            } :
            () => {
              handleClickOpen('dialogFormSupport');
              handleCloseHelp();
            }
        }
        key="5"
      >
        <ListItemText
          primary={<Typography variant="body2">{t('Get support')}</Typography>}
        />
      </MenuItem>
    );
  };

  const menuItems = () => {
    return [
      menuItemHandbook(),
      menuItemDocumentation(),

      menuItemsSupport(true),
    ];
  };

  React.useEffect(() => {
    // Detect screen size
    const handleResize = () =>
      setState({...state, windowWidth: window.innerWidth});
    window.addEventListener('resize', handleResize);
    window.addEventListener('orientationchange', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
      window.removeEventListener('orientationchange', handleResize);
    };
  }, [state]);

  return (
    <Root>
      <AppBar className={classes.appBar} position="sticky">
        <Alert
          severity="info"
          role="note"
          message="This is a beta version. This application functions best when used with a computer or laptop."
          handleClose={handleDismissAlert}
          open={open.alert}
          banner
        />
        <Toolbar>
          <Grid container>
            <Grid item sm={12} md>
              <Toolbar className={classes.toolbar}>
                <ShowMenuButton
                  clickHandler={props.clickHandler}
                  role={props.role}
                  open={props.open}
                />
                <div className={classes.statisticsCanadaLogoHeight}>
                  <BrandingStatCanEn />
                </div>
                <Divider
                  orientation="vertical"
                  className={classes.verticalDivider}
                  flexItem
                />
                <Typography variant="body2" component="h1" color="textPrimary">
                  Vetting Management
                </Typography>
              </Toolbar>
            </Grid>
            <Grid item>
              <Toolbar className={classes.toolbar}>
                <nav>
                  <Button
                    color="default"
                    aria-controls="menu-help"
                    aria-haspopup="true"
                    onClick={handleClickHelp}
                    startIcon={<Icon path={mdiHelpCircle} size={1} />}
                    endIcon={<Icon path={mdiMenuDown} size={1} />}
                    sx={{mr: 1}}
                    className={clsx({
                      'edge-start': isSmScreen,
                    })}
                  >
                    Help
                  </Button>
                  <Menu
                    id="menu-help"
                    anchorEl={anchorElHelp}
                    keepMounted
                    open={Boolean(anchorElHelp)}
                    handleClose={handleCloseHelp}
                    handleBlur={handleBlurHelp}
                    anchorOrigin={{vertical: 'bottom', horizontal: 'center'}}
                    transformOrigin={{vertical: 'top', horizontal: 'center'}}
                  >
                    {menuItems()}
                  </Menu>
                </nav>
                <Divider
                  sx={{mr: 1}}
                  orientation="vertical"
                  className={classes.verticalDividerHeight}
                />
                <Language />
                <IconButton
                  onClick={handleClick}
                  aria-controls="avatar-button-menu"
                  aria-haspopup="true"
                  aria-label="Account menu"
                  size="large"
                >
                  <Avatar className="avatar-purple" component="span">
                    AZ
                  </Avatar>
                </IconButton>
                <Menu
                  id="avatar-button-menu"
                  open={Boolean(anchorEl)}
                  anchorEl={anchorEl}
                  handleClose={handleClose}
                  handleBlur={handleBlur}
                >
                  {menuItemSignOut()}
                </Menu>
              </Toolbar>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      {props.children}
      <DialogGetSupportFab
        toggleDialog={() => handleClickClose('dialogSupportFab')}
        open={open.dialogSupportFab}
      />
      <DialogFormGetSupportFab
        toggleDialog={() => handleClickClose('dialogFormSupport')}
        open={open.dialogFormSupport}
      />
    </Root>
  );
}
