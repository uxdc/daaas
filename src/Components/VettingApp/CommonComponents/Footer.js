import React from 'react';
import {styled} from '@mui/material/styles';
import clsx from 'clsx';
import {useTranslation} from 'react-i18next';
import moment from 'moment';
import Typography from '@mui/material/Typography';

import {DRAWER_WIDTH} from '../Dashboard/Common/ProjectsDrawer';

const PREFIX = 'Footer';

const classes = {
  footer: `${PREFIX}-footer`,
  footerContainer: `${PREFIX}-footerContainer`,
  footerBtn: `${PREFIX}-footerBtn`,
  canadaLogo: `${PREFIX}-canadaLogo`,
  langBtnContainer: `${PREFIX}-langBtnContainer`,
  contentShift: `${PREFIX}-contentShift`,
};

const Root = styled('footer')((
    {
      theme,
    },
) => ({
  [`&.${classes.footer}`]: {
    height: 'auto',
    display: 'flex',
    flexFlow: 'column',
    justifyContent: 'space-between',
    background: theme.palette.grey[100],
  },

  [`& .${classes.footerContainer}`]: {
    borderTop: '1px solid',
    borderTopColor: theme.palette.divider,
    margin: theme.spacing(0, 3, 0, 3),
    padding: theme.spacing(3, 0),
  },

  [`& .${classes.footerBtn}`]: {
    color: theme.palette.text.secondary,
    margin: theme.spacing(0, 1, 0, 0),
  },

  [`& .${classes.canadaLogo}`]: {
    height: '24px',
    marginBottom: theme.spacing(3),
  },

  [`& .${classes.langBtnContainer}`]: {
    marginRight: theme.spacing(1),
    borderRightWidth: '1px',
    borderRightStyle: 'solid',
    borderRightColor: theme.palette.divider,
  },

  [`&.${classes.contentShift}`]: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: `calc(${DRAWER_WIDTH}px)`,
  },
}));

const date = moment().format('YYYY-MM-DD');

const Footer = React.forwardRef((props, ref) => {
  const {t} = useTranslation();


  return (
    <Root
      className={clsx(classes.root, classes.footer, {
        [classes.contentShift]: props.open,
      })}
      ref={ref}
      tabIndex="-1"
    >
      <div className={classes.footerContainer}>
        {/** ******** LEAVING THESE LINKS IN COMMENTS FOR FUTURE USE **********/}

        {/* <ul className="list-horizontal">
          <li>
            <div className={classes.langBtnContainer}>
              <span className="screen-reader-text">
                {t('Language selection')}
              </span>
              <Button
                id="test"
                className={clsx(classes.footerBtn, 'MuiIconButton-edgeStart')}
              >
                <span lang="fr">Français</span>
              </Button>
            </div>
          </li>
          <li>
            <Button className={classes.footerBtn}>
              {t('Terms and conditions')}
            </Button>
          </li>
          <li>
            <Button className={classes.footerBtn}>{t('Privacy')}</Button>
          </li>
        </ul> */}
        <div className={classes.canadaLogo}>
          <img
            src={process.env.PUBLIC_URL + '/images/wmms.svg'}
            alt={t('Symbol of the Government of Canada')}
          />
        </div>
        <Typography variant="body2" color="textSecondary" component="p">
          Date modified: {date}
        </Typography>
      </div>
    </Root>
  );
});

export default Footer;
