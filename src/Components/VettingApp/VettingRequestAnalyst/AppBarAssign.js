import React from 'react';
import {styled} from '@mui/material/styles';
import clsx from 'clsx';
import {Grid, Chip, Typography, Divider} from '@mui/material';
import {mdiInboxArrowDown} from '@mdi/js';
import Icon from '@mdi/react';

const PREFIX = 'AppBarUnAssign';

const classes = {
  title: `${PREFIX}-title`,
  upperCase: `${PREFIX}-upperCase`,
  assignee: `${PREFIX}-assignee`,
  icongrey: `${PREFIX}-icongrey`,
  formVerticalDivider: `${PREFIX}-formVerticalDivider`,
  headerBtn: `${PREFIX}-headerBtn`,
  gridDetails: `${PREFIX}-gridDetails`,
  alignCenter: `${PREFIX}-alignCenter`,
  details: `${PREFIX}-details`,
  requestFormHeader: `${PREFIX}-requestFormHeader`,
};

const StyledGrid = styled(Grid)(({theme}) => ({
  [`& .${classes.title}`]: {
    flexGrow: 1,
  },

  [`& .${classes.upperCase}`]: {
    textTransform: 'uppercase',
  },

  [`& .${classes.assignee}`]: {
    display: 'flex',
    alignItems: 'center',
  },

  [`& .${classes.icongrey}`]: {
    marginLeft: theme.spacing(1),
  },

  [`& .${classes.formVerticalDivider}`]: {
    margin: theme.spacing(0, 2, 0, 2),
  },

  [`& .${classes.headerBtn}`]: {
    marginLeft: theme.spacing(1),
  },

  [`& .${classes.gridDetails}`]: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'flex-start',
      marginTop: theme.spacing(3),
    },
  },

  [`& .${classes.alignCenter}`]: {
    display: 'flex',
    alignItems: 'center',
  },

  [`& .${classes.details}`]: {
    height: theme.spacing(4),
    display: 'flex',
    alignItems: 'center',
  },

  [`&.${classes.requestFormHeader}`]: {
    display: 'flex',
    flexFlow: 'row',
    alignItems: 'start',
    [theme.breakpoints.down('md')]: {
      flexFlow: 'column',
    },
  },
}));

function Assignee(props) {
  const {toggleManageTeamDrawer, lead, support} = props;
  // No analysts assigned
  if (lead === '' && support.length === 0) {
    return (
      <div className={classes.details}>
        <Typography variant="body2" color="textSecondary">
          Unassigned
        </Typography>
      </div>
    );
    // Only lead analyst assigned
  } else if (lead !== '' && support.length === 0) {
    return (
      <div className={classes.details}>
        <Chip
          label={lead}
          onClick={(e) => {
            e.stopPropagation();
            toggleManageTeamDrawer(e);
          }}
          aria-describedby="roleAssigneeLabel"
        />
      </div>
    );
    // Only support analysts assigned
  } else if (lead === '' && support.length !== 0) {
    return (
      <div className={classes.details}>
        <Typography variant="body2" color="textSecondary">
          Unassigned
        </Typography>
      </div>
    );
    // Both load and support analysts assigned
  } else {
    return (
      <div className={classes.details}>
        <Chip
          label={lead}
          onClick={(e) => {
            e.stopPropagation();
            toggleManageTeamDrawer(e);
          }}
          sx={{mr: 1}}
          aria-describedby="roleAssigneeLabel"
        />
        <Chip
          label={`${support.length} support`}
          onClick={(e) => {
            e.stopPropagation();
            toggleManageTeamDrawer(e);
          }}
          aria-describedby="roleAssigneeLabel"
        />
      </div>
    );
  }
}

function AppBarUnAssign(props) {
  return (
    <StyledGrid container className={classes.requestFormHeader}>
      <Grid item className={classes.title}>
        <Typography variant="caption" component="p">
          Project 20-SSH-UTO-1111 · Request 0101-000000
        </Typography>
        <Typography variant="h5" component="h2">
          {props.title}
        </Typography>
      </Grid>
      <Grid item className={classes.gridDetails}>
        <Divider
          sx={{display: {xs: 'none', md: 'block'}}}
          orientation="vertical"
          className={classes.formVerticalDivider}
          flexItem
        />
        <Grid item className={classes.alignCenter}>
          <div>
            <Typography variant="caption" component="p">
              Status
            </Typography>
            <Grid className={clsx(classes.alignCenter, classes.details)}>
              <Icon path={mdiInboxArrowDown} size={1} />
              <Typography
                variant="body2"
                component="p"
                className={classes.icongrey}
              >
                Submitted
              </Typography>
            </Grid>
          </div>
          <Divider
            orientation="vertical"
            className={classes.formVerticalDivider}
            flexItem
          />
          <Grid item>
            <div>
              <Typography
                variant="caption"
                component="p"
                id="roleRequesterLabel"
              >
                Requester
              </Typography>
              <div className={classes.details}>
                <Chip
                  label="Steve Rogers"
                  aria-describedby="roleRequesterLabel"
                  onClick={props.toggleRequesterDetails}
                />
              </div>
            </div>
          </Grid>
        </Grid>
        <Divider
          orientation="vertical"
          className={classes.formVerticalDivider}
          flexItem
        />
        <Grid item className={classes.assignee}>
          <div>
            <Typography variant="caption" component="p" id="roleAssigneeLabel">
              Assignee
            </Typography>
            <Assignee {...props} />
          </div>
        </Grid>
      </Grid>
    </StyledGrid>
  );
}

export default AppBarUnAssign;
