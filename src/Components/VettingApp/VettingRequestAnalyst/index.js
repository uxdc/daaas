import React, {useEffect} from 'react';
import {styled} from '@mui/material/styles';
import {
  Paper,
  Container,
  Grid,
  Button,
  Step,
  Stepper,
  StepButton,
  Typography,
  Divider,
  StepLabel,
} from '@mui/material';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ResearcherInfo from '../CommonComponents/RequestForm/ResearcherInfo';
import FilesList from '../CommonComponents/RequestForm/FilesList';
import ResidualDisclosure from '../CommonComponents/RequestForm/ResidualDisclosure';
import AdditionalInfo from '../CommonComponents/RequestForm/Additionalnfo';
import RequestToolbar from '../CommonComponents/RequestToolbar';
import AppBarAssign from './AppBarAssign';
import Header from '../CommonComponents/Header';
import Footer from '../CommonComponents/Footer';
import BypassBlocks from '../../CommonComponents/BypassBlocks';
import FloatingSupportButton from '../CommonComponents/Support';
import CutCopyPasteAlert from '../CommonComponents/CutCopyPasteAlert';
import {SnackbarSubmitRequest} from '../CommonComponents/Snackbars';
import ManageTeamDrawer from '../CommonComponents/ManageTeamDrawer';
import {DialogRequesterDetails} from '../CommonComponents/DialogBox';

const PREFIX = 'VettingRequestAnalyst';

const classes = {
  main: `${PREFIX}-main`,
  paper: `${PREFIX}-paper`,
  maxWidth640: `${PREFIX}-maxWidth640`,
  title: `${PREFIX}-title`,
  stepperContainer: `${PREFIX}-stepperContainer`,
  stepperNextBtn: `${PREFIX}-stepperNextBtn`,
  stepperBackBtn: `${PREFIX}-stepperBackBtn`,
  stepperText: `${PREFIX}-stepperText`,
};

const Root = styled('div')(({theme}) => ({
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column',

  [`& .${classes.main}`]: {
    background: theme.palette.grey[100],
    paddingBottom: theme.spacing(6),
    flex: 1,
  },

  [`& .${classes.paper}`]: {
    maxWidth: '1280px',
    margin: 'auto',
    boxSizing: 'border-box',
    padding: theme.spacing(6),
    marginTop: theme.spacing(3),
  },

  [`& .${classes.maxWidth640}`]: {
    maxWidth: '640px',
  },

  [`& .${classes.title}`]: {
    flexGrow: 1,
  },

  [`& .${classes.stepperContainer}`]: {
    'display': 'flex',
    '& .MuiStepper-root': {
      flexGrow: 1,
      padding: 0,
    },
  },

  [`& .${classes.stepperNextBtn}`]: {
    marginLeft: theme.spacing(6),
  },

  [`& .${classes.stepperBackBtn}`]: {
    marginRight: theme.spacing(6),
  },

  [`& .${classes.stepperText}`]: {
    margin: 0,
    textAlign: 'left',
  },
}));

function getSteps() {
  return [
    'Request details',
    'Output files for vetting',
    'Residual disclosure',
    'Additional information',
  ];
}

function VettingRequestAnalyst(props) {
  useEffect(() => {
    document.title =
      'Request 0101-000000 • Vetting Management • Data Analytics as a Service • Statistics Canada';
  }, []);

  const [open, setOpen] = React.useState({
    manageTeamDrawer: false,
    dialogRequesterDetails: false,
  });

  const [state, setState] = React.useState({
    activeStep: 0,
    title: 'Untitled request',
    errors: [0, 4, 0, 0],
    lead: props.lead,
    support: props.support,
  });

  const [openSnackbar, setOpenSnackbar] = React.useState({
    snackbarSubmitted: false,
  });

  const toggleManageTeamDrawer = () => {
    setOpen({...open, manageTeamDrawer: !open.manageTeamDrawer});
  };

  const toggleRequesterDetails = () => {
    setOpen({...open, dialogRequesterDetails: !open.dialogRequesterDetails});
  };

  const refStep0 = React.useRef(null);
  const refStep1 = React.useRef(null);
  const refStep2 = React.useRef(null);
  const refStep3 = React.useRef(null);
  const mainRef = React.createRef();
  const footerRef = React.createRef();
  const steps = getSteps();

  const getStepRef = (step) => {
    switch (step) {
      case 0:
        return refStep0;
      case 1:
        return refStep1;
      case 2:
        return refStep2;
      case 3:
        return refStep3;
      default:
        return null;
    }
  };

  const handleNext = () => {
    window.scrollTo(0, 0);
    const prevActiveStep = state.activeStep;
    setState({...state, activeStep: prevActiveStep + 1});
    getStepRef(prevActiveStep + 1).current.focus();
  };

  const handleBack = () => {
    window.scrollTo(0, 0);
    const prevActiveStep = state.activeStep;
    setState({...state, activeStep: prevActiveStep - 1});
    getStepRef(prevActiveStep).current.focus();
  };

  const handleStep = (step) => {
    window.scrollTo(0, 0);
    setState({...state, activeStep: step});
  };

  const snackbarHandleClick = (state) => {
    setOpenSnackbar({...openSnackbar, [state]: true});
  };

  const snackbarHandleClose = (state) => {
    setOpenSnackbar({...openSnackbar, [state]: false});
  };

  const getStepContent = (step) => {
    switch (step) {
      case 0:
        return <ResearcherInfo handleTitleChange={handleTitleChange} />;
      case 1:
        return <FilesList role="analyst" />;
      case 2:
        return <ResidualDisclosure />;
      case 3:
        return <AdditionalInfo />;
      default:
        return 'Unknown step';
    }
  };

  const handleTitleChange = (e) => {
    const title = e.target.value;
    if (title !== '') {
      setState({...state, title: e.target.value});
    } else {
      setState({...state, title: 'Untitled request'});
    }
  };

  const isStepFailed = (step) => {
    return state.errors[step] !== 0;
  };

  return (
    <Root>
      <BypassBlocks ref={{main: mainRef, footer: footerRef}} />
      <Header>
        <RequestToolbar
          role="researcher"
          status="draft"
          assignees={{
            lead: '',
            support: [],
          }}
        />
      </Header>
      <main className={classes.main} ref={mainRef} tabIndex="-1">
        <Container maxWidth={false}>
          <Paper variant="outlined" className={classes.paper}>
            <Grid container alignItems="center">
              <AppBarAssign
                title={state.title}
                lead={state.lead}
                support={state.support}
                toggleManageTeamDrawer={toggleManageTeamDrawer}
                toggleRequesterDetails={toggleRequesterDetails}
              />
            </Grid>
            <Divider sx={{mb: 3, mt: 3}} />
            <div className={classes.stepperContainer}>
              {state.activeStep !== 0 && (
                <Button
                  onClick={handleBack}
                  className={classes.stepperBackBtn}
                  startIcon={<ArrowBackIosIcon />}
                >
                  Back
                </Button>
              )}
              <Stepper nonLinear activeStep={state.activeStep}>
                {steps.map((label, index) => {
                  const labelProps = {};
                  const buttonProps = {};
                  if (isStepFailed(index)) {
                    labelProps.error = true;
                    buttonProps.optional = (
                      <Typography
                        className={classes.stepperText}
                        variant="body2"
                        color="error"
                        component="span"
                      >
                        {state.errors[index]}{' '}
                        {state.errors[index] === 1 ? 'error' : 'errors'}
                      </Typography>
                    );
                  }
                  return (
                    <Step key={label} ref={getStepRef(index)} tabIndex="-1">
                      <StepButton
                        {...buttonProps}
                        onClick={() => handleStep(index)}
                      >
                        <StepLabel
                          {...labelProps}
                          className={classes.stepperText}
                        >
                          <span className="screen-reader-text">{`Step ${index +
                            1}: `}</span>
                          {label}
                        </StepLabel>
                      </StepButton>
                    </Step>
                  );
                })}
              </Stepper>
              {state.activeStep !== getSteps().length - 1 && (
                <Button
                  onClick={handleNext}
                  className={classes.stepperNextBtn}
                  endIcon={<ArrowForwardIosIcon />}
                >
                  Next
                </Button>
              )}
            </div>
            <Divider sx={{mb: 3, mt: 3}} />
            <CutCopyPasteAlert />
            <Grid justifyContent="center" container>
              <Grid className={classes.maxWidth640} container>
                <Grid item>{getStepContent(state.activeStep)}</Grid>
                <Grid xs={12} item>
                  <Divider sx={{mb: 3}} />
                </Grid>
                <Grid justifyContent="flex-end" container>
                  <Grid item>
                    {state.activeStep !== 0 && (
                      <Button
                        variant="outlined"
                        color="primary"
                        sx={{mr: 2}}
                        onClick={handleBack}
                      >
                        Back
                      </Button>
                    )}
                    {state.activeStep === getSteps().length - 1 ? (
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={() => snackbarHandleClick('snackbarSubmitted')}
                      >
                        Submit
                      </Button>
                    ) : (
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={handleNext}
                      >
                        Next
                      </Button>
                    )}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </Container>
      </main>
      <FloatingSupportButton form />
      <ManageTeamDrawer
        open={open.manageTeamDrawer}
        clickHandler={toggleManageTeamDrawer}
        toggleManageTeamDrawer={toggleManageTeamDrawer}
      />
      <DialogRequesterDetails
        open={open.dialogRequesterDetails}
        toggleDialog={toggleRequesterDetails}
      />
      <SnackbarSubmitRequest
        open={openSnackbar.snackbarSubmitted}
        handleClose={() => snackbarHandleClose('snackbarSubmitted')}
      />
      <Footer ref={footerRef} />
    </Root>
  );
}
export default VettingRequestAnalyst;
