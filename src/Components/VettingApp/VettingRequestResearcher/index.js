import React, {useEffect} from 'react';
import {styled} from '@mui/material/styles';
import clsx from 'clsx';
import {
  Paper,
  Container,
  Grid,
  Button,
  Step,
  Stepper,
  StepButton,
  StepLabel,
  Typography,
  Divider,
  Chip,
} from '@mui/material';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import Icon from '@mdi/react';
import {mdiFileEditOutline} from '@mdi/js';
import ResearcherInfo from '../CommonComponents/RequestForm/ResearcherInfo';
import FilesList from '../CommonComponents/RequestForm/FilesList';
import ResidualDisclosure from '../CommonComponents/RequestForm/ResidualDisclosure';
import AdditionalInfo from '../CommonComponents/RequestForm/Additionalnfo';
import FloatingSupportButton from '../CommonComponents/Support';
import Header from '../CommonComponents/Header';
import Footer from '../CommonComponents/Footer';
import BypassBlocks from '../../CommonComponents/BypassBlocks';
import CutCopyPasteAlert from '../CommonComponents/CutCopyPasteAlert';
import {SnackbarSubmitRequest} from '../CommonComponents/Snackbars';
import RequestToolbar from '../CommonComponents/RequestToolbar';
import {DialogRequesterDetails} from '../CommonComponents/DialogBox';

const PREFIX = 'VettingRequestResearcher';

const classes = {
  main: `${PREFIX}-main`,
  paper: `${PREFIX}-paper`,
  maxWidth640: `${PREFIX}-maxWidth640`,
  title: `${PREFIX}-title`,
  icongrey: `${PREFIX}-icongrey`,
  formVerticalDivider: `${PREFIX}-formVerticalDivider`,
  stepperContainer: `${PREFIX}-stepperContainer`,
  stepperNextBtn: `${PREFIX}-stepperNextBtn`,
  stepperBackBtn: `${PREFIX}-stepperBackBtn`,
  stepperText: `${PREFIX}-stepperText`,
  gridDetails: `${PREFIX}-gridDetails`,
  alignCenter: `${PREFIX}-alignCenter`,
  details: `${PREFIX}-details`,
  requestFormHeader: `${PREFIX}-requestFormHeader`,
};

const Root = styled('div')(({theme}) => ({
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column',

  [`& .${classes.main}`]: {
    background: theme.palette.grey[100],
    paddingBottom: theme.spacing(6),
    flex: 1,
  },

  [`& .${classes.paper}`]: {
    maxWidth: '1280px',
    margin: 'auto',
    boxSizing: 'border-box',
    padding: theme.spacing(6),
    marginTop: theme.spacing(3),
  },

  [`& .${classes.maxWidth640}`]: {
    maxWidth: '640px',
  },

  [`& .${classes.title}`]: {
    flexGrow: 1,
  },

  [`& .${classes.icongrey}`]: {
    marginLeft: theme.spacing(1),
  },

  [`& .${classes.formVerticalDivider}`]: {
    margin: theme.spacing(0, 2, 0, 2),
  },

  [`& .${classes.stepperContainer}`]: {
    'display': 'flex',
    '& .MuiStepper-root': {
      flexGrow: 1,
      padding: 0,
    },
  },

  [`& .${classes.stepperNextBtn}`]: {
    marginLeft: theme.spacing(6),
  },

  [`& .${classes.stepperBackBtn}`]: {
    marginRight: theme.spacing(6),
  },

  [`& .${classes.stepperText}`]: {
    margin: 0,
    textAlign: 'left',
  },

  [`& .${classes.gridDetails}`]: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'flex-start',
      marginTop: theme.spacing(3),
    },
  },

  [`& .${classes.alignCenter}`]: {
    display: 'flex',
    alignItems: 'center',
  },

  [`& .${classes.details}`]: {
    height: theme.spacing(4),
    display: 'flex',
    alignItems: 'center',
  },

  [`& .${classes.requestFormHeader}`]: {
    display: 'flex',
    flexFlow: 'row',
    alignItems: 'start',
    [theme.breakpoints.down('md')]: {
      flexFlow: 'column',
    },
  },
}));

function getSteps() {
  return [
    'Request details',
    'Output files for vetting',
    'Residual disclosure',
    'Additional information',
  ];
}

function VettingRequestResearcher(props) {
  useEffect(() => {
    document.title =
      'Request 0101-000000 • Vetting Management • Data Analytics as a Service • Statistics Canada';
  }, []);

  const [state, setState] = React.useState({
    activeStep: 0,
    open: false,
    errors: [0, 4, 0, 0],
    title: 'Untitled request',
    dialogRequesterDetails: false,
  });

  const [openSnackbar, setOpenSnackbar] = React.useState(false);

  const refStep0 = React.useRef(null);
  const refStep1 = React.useRef(null);
  const refStep2 = React.useRef(null);
  const refStep3 = React.useRef(null);
  const mainRef = React.createRef();
  const footerRef = React.createRef();
  const steps = getSteps();

  const getStepRef = (step) => {
    switch (step) {
      case 0:
        return refStep0;
      case 1:
        return refStep1;
      case 2:
        return refStep2;
      case 3:
        return refStep3;
      default:
        return null;
    }
  };

  const handleNext = () => {
    window.scrollTo(0, 0);
    const prevActiveStep = state.activeStep;
    setState({...state, activeStep: prevActiveStep + 1});
    getStepRef(prevActiveStep + 1).current.focus();
  };

  const handleBack = () => {
    window.scrollTo(0, 0);
    const prevActiveStep = state.activeStep;
    setState({...state, activeStep: prevActiveStep - 1});
    getStepRef(prevActiveStep - 1).current.focus();
  };

  const handleStep = (step) => {
    window.scrollTo(0, 0);
    setState({...state, activeStep: step});
  };

  const snackbarhandleClose = () => {
    setOpenSnackbar(false);
  };

  const handleClick = () => {
    setOpenSnackbar(true);
  };

  const getStepContent = (step) => {
    switch (step) {
      case 0:
        return (
          <ResearcherInfo
            handleTitleChange={handleTitleChange}
            handleFieldOnBlur={handleFieldOnBlur}
            title={state.title}
          />
        );
      case 1:
        return <FilesList role="researcher" />;
      case 2:
        return <ResidualDisclosure role="researcher" />;
      case 3:
        return <AdditionalInfo />;
      default:
        return 'Unknown step';
    }
  };

  const handleTitleChange = (e) => {
    const title = e.target.value;
    if (title !== '') {
      setState({...state, title: e.target.value});
    } else {
      setState({...state, title: 'Untitled request'});
    }
  };

  const handleFieldOnBlur = (e) => {
    const defaultTitle = 'Untitled request';

    if (e.target.value === '') {
      // if field is empty, set field to "untitled request"
      setState({
        ...state,
        title: function() {
          return defaultTitle;
        },
      });
    }
  };

  const isStepFailed = (step) => {
    return state.errors[step] !== 0;
  };

  const toggleRequesterDetails = () => {
    setState({
      ...state,
      dialogRequesterDetails: !state.dialogRequesterDetails,
    });
  };

  return (
    <Root>
      <BypassBlocks ref={{main: mainRef, footer: footerRef}} />
      <Header>
        <RequestToolbar
          role="researcher"
          status="draft"
          assignees={{
            lead: '',
            support: [],
          }}
        />
      </Header>
      <main className={classes.main} ref={mainRef} tabIndex="-1">
        <Container maxWidth={false}>
          <Paper variant="outlined" className={classes.paper}>
            <Grid container className={classes.requestFormHeader}>
              <Grid item className={classes.title}>
                <Typography variant="caption" component="p">
                  Project 20-SSH-UTO-1111 · Request 0101-000000
                </Typography>
                <Typography variant="h5" component="h2">
                  {state.title}
                </Typography>
              </Grid>
              <Grid item className={classes.gridDetails}>
                <Divider
                  sx={{display: {xs: 'none', md: 'block'}}}
                  orientation="vertical"
                  className={classes.formVerticalDivider}
                  flexItem
                />
                <Grid item className={classes.alignCenter}>
                  <div>
                    <Typography variant="caption" component="p">
                      Status
                    </Typography>
                    <Grid
                      className={clsx(classes.alignCenter, classes.details)}
                    >
                      <Icon path={mdiFileEditOutline} size={1} />
                      <Typography
                        variant="body2"
                        component="p"
                        className={classes.icongrey}
                      >
                        Draft
                      </Typography>
                    </Grid>
                  </div>
                </Grid>
                <Divider
                  orientation="vertical"
                  className={classes.formVerticalDivider}
                  flexItem
                />
                <Grid item>
                  <div>
                    <Typography
                      variant="caption"
                      component="p"
                      id="roleRequesterLabel"
                    >
                      Requester
                    </Typography>
                    <div className={classes.details}>
                      <Chip
                        label="Steve Rogers"
                        aria-describedby="roleRequesterLabel"
                        onClick={toggleRequesterDetails}
                      />
                    </div>
                  </div>
                </Grid>
                <Divider
                  orientation="vertical"
                  className={classes.formVerticalDivider}
                  flexItem
                />
                <Grid item className={classes.assignee}>
                  <div>
                    <Typography
                      variant="caption"
                      component="p"
                      id="roleAssigneeLabel"
                    >
                      Assignee
                    </Typography>
                    <div className={classes.details} aria-label="assignee">
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                        className={classes.details}
                      >
                        Unassigned
                      </Typography>
                    </div>
                  </div>
                </Grid>
              </Grid>
            </Grid>
            <Divider sx={{mb: 3, mt: 3}} />
            <div className={classes.stepperContainer}>
              {state.activeStep !== 0 && (
                <Button
                  onClick={handleBack}
                  className={classes.stepperBackBtn}
                  startIcon={<ArrowBackIosIcon />}
                >
                  Back
                </Button>
              )}
              <Stepper nonLinear activeStep={state.activeStep}>
                {steps.map((label, index) => {
                  const labelProps = {};
                  const buttonProps = {};
                  if (isStepFailed(index)) {
                    labelProps.error = true;
                    buttonProps.optional = (
                      <Typography
                        className={classes.stepperText}
                        variant="body2"
                        color="error"
                        component="span"
                      >
                        {state.errors[index]}{' '}
                        {state.errors[index] === 1 ? 'error' : 'errors'}
                      </Typography>
                    );
                  }
                  return (
                    <Step key={label} ref={getStepRef(index)} tabIndex="-1">
                      <StepButton
                        {...buttonProps}
                        onClick={() => handleStep(index)}
                      >
                        <StepLabel
                          {...labelProps}
                          className={classes.stepperText}
                        >
                          <span className="screen-reader-text">{`Step ${index +
                            1}: `}</span>
                          {label}
                        </StepLabel>
                      </StepButton>
                    </Step>
                  );
                })}
              </Stepper>
              {state.activeStep !== getSteps().length - 1 && (
                <Button
                  onClick={handleNext}
                  className={classes.stepperNextBtn}
                  endIcon={<ArrowForwardIosIcon />}
                >
                  Next
                </Button>
              )}
            </div>
            <Divider sx={{mb: 3, mt: 3}} />
            <CutCopyPasteAlert />
            <Grid justifyContent="center" container>
              <Grid className={classes.maxWidth640} container>
                <Grid item>{getStepContent(state.activeStep)}</Grid>
                <Grid xs={12} item>
                  <Divider sx={{mb: 3}} />
                </Grid>
                <Grid justifyContent="flex-end" container>
                  <Grid item>
                    {state.activeStep !== 0 && (
                      <Button
                        variant="outlined"
                        color="primary"
                        sx={{mr: 2}}
                        onClick={handleBack}
                      >
                        Back
                      </Button>
                    )}
                    {state.activeStep === getSteps().length - 1 ? (
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={handleClick}
                      >
                        Submit
                      </Button>
                    ) : (
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={handleNext}
                      >
                        Next
                      </Button>
                    )}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </Container>
      </main>
      <FloatingSupportButton form />
      <DialogRequesterDetails
        open={state.dialogRequesterDetails}
        toggleDialog={toggleRequesterDetails}
      />
      {/* Submit request snackbar */}
      <SnackbarSubmitRequest
        open={openSnackbar}
        handleClose={snackbarhandleClose}
      />
      <Footer ref={footerRef} />
    </Root>
  );
}
export default VettingRequestResearcher;
