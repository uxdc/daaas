import {createTheme} from '@mui/material';
import {alpha} from '@mui/material/styles';
import {lighten} from '@mui/material/styles';

export const theme = createTheme({
  // PALETTE
  palette: {
    mode: 'light',
    primary: {
      main: '#1A73E8',
      light: '#d4e5ff',
      dark: '#1765CC',
    },
    secondary: {
      main: '#34dbc5',
      light: '#77fff8',
      dark: '#00a995',
    },
    default: {
      main: '#5F6368',
      hover: '#54575C',
    },
    error: {
      main: '#E91B0C',
    },
    buttons: {
      default: '#5F6368',
      defaultHover: '#54575C',
    },
  },
  shadows: [
    'none',
    '0px 2px 1px -1px rgba(117,117,117,0.2),0px 1px 1px 0px rgba(117,117,117,0.14),0px 1px 3px 0px rgba(117,117,117,0.12)',
    '0px 3px 1px -2px rgba(117,117,117,0.2),0px 2px 2px 0px rgba(117,117,117,0.14),0px 1px 5px 0px rgba(117,117,117,0.12)',
    '0px 3px 3px -2px rgba(117,117,117,0.2),0px 3px 4px 0px rgba(117,117,117,0.14),0px 1px 8px 0px rgba(117,117,117,0.12)',
    '0px 2px 4px -1px rgba(117,117,117,0.2),0px 4px 5px 0px rgba(117,117,117,0.14),0px 1px 10px 0px rgba(117,117,117,0.12)',
    '0px 3px 5px -1px rgba(117,117,117,0.2),0px 5px 8px 0px rgba(117,117,117,0.14),0px 1px 14px 0px rgba(117,117,117,0.12)',
    '0px 3px 5px -1px rgba(117,117,117,0.2),0px 6px 10px 0px rgba(117,117,117,0.14),0px 1px 18px 0px rgba(117,117,117,0.12)',
    '0px 4px 5px -2px rgba(117,117,117,0.2),0px 7px 10px 1px rgba(117,117,117,0.14),0px 2px 16px 1px rgba(117,117,117,0.12)',
    '0px 5px 5px -3px rgba(117,117,117,0.2),0px 8px 10px 1px rgba(117,117,117,0.14),0px 3px 14px 2px rgba(117,117,117,0.12)',
    '0px 5px 6px -3px rgba(117,117,117,0.2),0px 9px 12px 1px rgba(117,117,117,0.14),0px 3px 16px 2px rgba(117,117,117,0.12)',
    '0px 6px 6px -3px rgba(117,117,117,0.2),0px 10px 14px 1px rgba(117,117,117,0.14),0px 4px 18px 3px rgba(117,117,117,0.12)',
    '0px 6px 7px -4px rgba(117,117,117,0.2),0px 11px 15px 1px rgba(117,117,117,0.14),0px 4px 20px 3px rgba(117,117,117,0.12)',
    '0px 7px 8px -4px rgba(117,117,117,0.2),0px 12px 17px 2px rgba(117,117,117,0.14),0px 5px 22px 4px rgba(117,117,117,0.12)',
    '0px 7px 8px -4px rgba(117,117,117,0.2),0px 13px 19px 2px rgba(117,117,117,0.14),0px 5px 24px 4px rgba(117,117,117,0.12)',
    '0px 7px 9px -4px rgba(117,117,117,0.2),0px 14px 21px 2px rgba(117,117,117,0.14),0px 5px 26px 4px rgba(117,117,117,0.12)',
    '0px 8px 9px -5px rgba(117,117,117,0.2),0px 15px 22px 2px rgba(117,117,117,0.14),0px 6px 28px 5px rgba(117,117,117,0.12)',
    '0px 8px 10px -5px rgba(117,117,117,0.2),0px 16px 24px 2px rgba(117,117,117,0.14),0px 6px 30px 5px rgba(117,117,117,0.12)',
    '0px 8px 11px -5px rgba(117,117,117,0.2),0px 17px 26px 2px rgba(117,117,117,0.14),0px 6px 32px 5px rgba(117,117,117,0.12)',
    '0px 9px 11px -5px rgba(117,117,117,0.2),0px 18px 28px 2px rgba(117,117,117,0.14),0px 7px 34px 6px rgba(117,117,117,0.12)',
    '0px 9px 12px -6px rgba(117,117,117,0.2),0px 19px 29px 2px rgba(117,117,117,0.14),0px 7px 36px 6px rgba(117,117,117,0.12)',
    '0px 10px 13px -6px rgba(117,117,117,0.2),0px 20px 31px 3px rgba(117,117,117,0.14),0px 8px 38px 7px rgba(117,117,117,0.12)',
    '0px 10px 13px -6px rgba(117,117,117,0.2),0px 21px 33px 3px rgba(117,117,117,0.14),0px 8px 40px 7px rgba(117,117,117,0.12)',
    '0px 10px 14px -6px rgba(117,117,117,0.2),0px 22px 35px 3px rgba(117,117,117,0.14),0px 8px 42px 7px rgba(117,117,117,0.12)',
    '0px 11px 14px -7px rgba(117,117,117,0.2),0px 23px 36px 3px rgba(117,117,117,0.14),0px 9px 44px 8px rgba(117,117,117,0.12)',
    '0px 11px 15px -7px rgba(117,117,117,0.2),0px 24px 38px 3px rgba(117,117,117,0.14),0px 9px 46px 8px rgba(117,117,117,0.12)',
  ],
  typography: {
    h1: {
      letterSpacing: '-0.01562em',
    },
    h2: {
      letterSpacing: '-0.00833em',
    },
    h3: {
      letterSpacing: '0em',
    },
    h4: {
      letterSpacing: '0.00735em',
    },
    h5: {
      letterSpacing: '0em',
    },
    h6: {
      letterSpacing: '0.0075em',
    },
    subtitle1: {
      letterSpacing: '0.00938em',
    },
    subtitle2: {
      letterSpacing: '0.00714em',
    },
    body1: {
      letterSpacing: '0.00938em',
    },
    body2: {
      letterSpacing: '0.01071em',
    },
    button: {
      letterSpacing: '0.02857em',
    },
    caption: {
      letterSpacing: '0.03333em',
    },
    overline: {
      letterSpacing: '0.08333em',
    },
  },
});

theme.components = {
  MuiButton: {
    defaultProps: {
      disableFocusRipple: true,
    },
    variants: [
      {
        props: {variant: 'text', color: 'default'},
        style: {
          'color': theme.palette.default.main,
          '& .MuiButton-startIcon': {
            color: theme.palette.default.main,
            marginLeft: theme.spacing(0),
          },
          '& .MuiButton-endIcon': {
            color: theme.palette.default.main,
            marginRight: theme.spacing(0),
          },
          '&:hover': {
            'backgroundColor': alpha(theme.palette.default.main, 0.12),
            'color': theme.palette.default.hover,
            '& .MuiButton-startIcon': {
              color: theme.palette.default.hover,
            },
            '& .MuiButton-endIcon': {
              color: theme.palette.default.hover,
            },
          },
          '&.Mui-focusVisible': {
            'backgroundColor': alpha(theme.palette.default.main, 0.12),
            'color': theme.palette.default.hover,
            '& .MuiButton-startIcon': {
              color: theme.palette.default.hover,
            },
            '& .MuiButton-endIcon': {
              color: theme.palette.default.hover,
            },
          },
        },
      },
    ],
    styleOverrides: {
      root: {
        textTransform: 'none',
      },
      endIcon: {
        marginRight: theme.spacing(-1),
      },
      startIcon: {
        marginLeft: theme.spacing(-1),
      },
      // contained
      contained: {
        'boxShadow': theme.shadows[0],
        // contained disabled
        '&.Mui-disabled': {
          color: alpha(theme.palette.common.black, 0.4),
          backgroundColor: alpha(theme.palette.common.black, 0.08),
        },
      },
      // contained primary
      containedPrimary: {
        '&:hover': {
          backgroundColor: theme.palette.primary.dark,
          boxShadow: theme.shadows[0],
        },
        '&.Mui-focusVisible': {
          backgroundColor: theme.palette.primary.dark,
          boxShadow: theme.shadows[0],
        },
      },
      // outlined
      outlined: {
        // outlined disabled
        '&.Mui-disabled': {
          color: alpha(theme.palette.common.black, 0.4),
          borderColor: alpha(theme.palette.common.black, 0.12),
        },
      },
      // outlined primary
      outlinedPrimary: {
        'borderColor': alpha(theme.palette.primary.main, 0.4),
        '&:hover': {
          color: theme.palette.primary.dark,
          borderColor: alpha(theme.palette.primary.main, 0.4),
          backgroundColor: alpha(theme.palette.primary.main, 0.12),
        },
        '&.Mui-focusVisible': {
          color: theme.palette.primary.dark,
          backgroundColor: alpha(theme.palette.primary.main, 0.12),
        },
      },
      // text primary
      text: {
        '& .MuiButton-startIcon': {
          color: theme.palette.primary.main,
          marginLeft: theme.spacing(0),
        },
        '& .MuiButton-endIcon': {
          color: theme.palette.primary.main,
          marginRight: theme.spacing(0),
        },
        '&:hover': {
          'color': theme.palette.primary.dark,
          'backgroundColor': alpha(theme.palette.primary.main, 0.12),
          '& .MuiButton-startIcon': {
            color: theme.palette.primary.dark,
          },
          '& .MuiButton-endIcon': {
            color: theme.palette.primary.dark,
          },
        },
        '&.Mui-focusVisible': {
          'color': theme.palette.primary.dark,
          'backgroundColor': alpha(theme.palette.primary.main, 0.12),
          '& .MuiButton-startIcon': {
            color: theme.palette.primary.dark,
          },
          '& .MuiButton-endIcon': {
            color: theme.palette.primary.dark,
          },
        },
        // text disabled
        '&.Mui-disabled': {
          'color': alpha(theme.palette.common.black, 0.4),
          '& .MuiButton-startIcon': {
            color: alpha(theme.palette.common.black, 0.4),
          },
          '& .MuiButton-endIcon': {
            color: alpha(theme.palette.common.black, 0.4),
          },
        },
      },
    },
  },
  MuiIconButton: {
    defaultProps: {
      disableFocusRipple: true,
    },
    styleOverrides: {
      edgeStart: {
        marginLeft: theme.spacing(-1),
      },
      edgeEnd: {
        marginRight: theme.spacing(-1),
      },
      root: {
        'color': theme.palette.default.main,
        'padding': theme.spacing(1),
        '&:hover': {
          backgroundColor: alpha(theme.palette.default.main, 0.12),
          color: theme.palette.default.hover,
        },
        '&.Mui-focusVisible': {
          backgroundColor: alpha(theme.palette.default.main, 0.12),
          color: theme.palette.default.hover,
        },
        // disabled icon
        '&.Mui-disabled': {
          color: alpha(theme.palette.common.black, 0.4),
        },
      },
      // primary icon
      colorPrimary: {
        'color': theme.palette.primary.main,
        '&:hover': {
          color: theme.palette.primary.dark,
          backgroundColor: alpha(theme.palette.primary.main, 0.12),
        },
        '&.Mui-focusVisible': {
          color: theme.palette.primary.dark,
          backgroundColor: alpha(theme.palette.primary.main, 0.12),
        },
      },
    },
  },
  MuiTouchRipple: {
    styleOverrides: {
      'rippleVisible': {
        opacity: 0.2,
        animation: `$enter 550ms ${theme.transitions.easing.easeInOut}`,
      },
      '@keyframes enter': {
        '0%': {
          transform: 'scale(0)',
          opacity: 0.1,
        },
        '100%': {
          transform: 'scale(1)',
          opacity: 0.2,
        },
      },
    },
  },
  MuiFormControl: {
    defaultProps: {
      fullWidth: true,
      margin: 'dense',
      variant: 'outlined',
    },
  },
  MuiTextField: {
    defaultProps: {
      margin: 'dense',
      variant: 'outlined',
      maxRows: 100,
    },
  },
  MuiTooltip: {
    styleOverrides: {
      tooltip: {
        backgroundColor: lighten(theme.palette.common.black, 0.071),
        fontSize: theme.typography.caption.fontSize,
        letterSpacing: theme.typography.caption.letterSpacing,
        lineHeight: theme.typography.caption.lineHeight,
        fontWeight: theme.typography.caption.fontWeight,
      },
      arrow: {
        color: lighten(theme.palette.common.black, 0.071),
      },
      tooltipPlacementLeft: {
        margin: theme.spacing(0, 2),
        [theme.breakpoints.up('sm')]: {
          margin: theme.spacing(0, 2),
        },
      },
      tooltipPlacementRight: {
        margin: theme.spacing(0, 2),
        [theme.breakpoints.up('sm')]: {
          margin: theme.spacing(0, 2),
        },
      },
      tooltipPlacementTop: {
        margin: theme.spacing(2, 0),
        [theme.breakpoints.up('sm')]: {
          margin: theme.spacing(2, 0),
        },
      },
      tooltipPlacementBottom: {
        margin: theme.spacing(2, 0),
        [theme.breakpoints.up('sm')]: {
          margin: theme.spacing(2, 0),
        },
      },
    },
  },
};

export const darkTheme = createTheme({
  ...theme,
  palette: {
    ...theme.palette,
    type: 'dark',
    background: {
      paper: '#173048',
    },
    text: {
      primary: theme.palette.common.white,
      secondary: alpha(theme.palette.common.white, 0.7),
    },
    divider: alpha(theme.palette.common.white, 0.12),
  },
  components: {
    ...theme.components,
    MuiButton: {
      ...theme.components.MuiButton,
      styleOverrides: {
        ...theme.components.MuiButton.styleOverrides,
        contained: {
          ...theme.components.MuiButton.contained,
          // contained disabled
          '&.Mui-disabled': {
            color: alpha(theme.palette.common.white, 0.4),
            backgroundColor: alpha(theme.palette.common.white, 0.08),
          },
        },
        // outlined
        outlinedPrimary: {
          ...theme.components.MuiButton.outlined,
          'color': theme.palette.common.white,
          'borderColor': alpha(theme.palette.common.white, 0.4),
          '&:hover': {
            backgroundColor: alpha(theme.palette.common.white, 0.12),
            borderColor: alpha(theme.palette.common.white, 0.4),
          },
          '&.Mui-focusVisible': {
            backgroundColor: alpha(theme.palette.common.white, 0.12),
            borderColor: alpha(theme.palette.common.white, 0.4),
          },
          // outlined disabled
          '&.Mui-disabled': {
            color: alpha(theme.palette.common.white, 0.4),
            borderColor: alpha(theme.palette.common.white, 0.12),
          },
        },
        // text
        text: {
          ...theme.components.MuiButton.text,
          'color': theme.palette.common.white,
          '& .MuiButton-startIcon': {
            color: theme.palette.common.white,
            marginLeft: theme.spacing(0),
          },
          '& .MuiButton-endIcon': {
            color: theme.palette.common.white,
            marginRight: theme.spacing(0),
          },
          '&:hover': {
            backgroundColor: alpha(theme.palette.common.white, 0.12),
          },
          '&.Mui-focusVisible': {
            backgroundColor: alpha(theme.palette.common.white, 0.12),
          },
          // text disabled
          '&.Mui-disabled': {
            'color': alpha(theme.palette.common.white, 0.4),
            '& .MuiButton-startIcon': {
              color: alpha(theme.palette.common.white, 0.4),
            },
            '& .MuiButton-endIcon': {
              color: alpha(theme.palette.common.white, 0.4),
            },
          },
        },
      },
    },
  },
});
