import {theme} from './theme';

export const XS_SCREEN = theme.breakpoints.values.sm;
export const SM_SCREEN = theme.breakpoints.values.md;
export const MD_SCREEN = theme.breakpoints.values.lg;
export const LG_SCREEN = theme.breakpoints.values.xl;

export const HEAD_H = 64;
export const HEAD_H_XS = 112;
export const FOOT_H = 117;
