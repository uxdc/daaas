import {alpha, darken} from '@mui/material/styles';
import {deepOrange, deepPurple, green} from '@mui/material/colors';
import {theme} from './theme';

export const styles = {
  // ************* Overrides **************

  // Buttons
  '.MuiButtonBase-root.MuiTab-root, .MuiButtonBase-root.MuiFab-root': {
    textTransform: 'none',
  },
  '.MuiButton-text': {
    '&.edge-start': {
      marginLeft: theme.spacing(-1),
    },
    '&.edge-end': {
      marginRight: theme.spacing(-1),
    },
  },
  '.MuiButton-root': {
    '&.MuiButton-containedPrimary': {
      '&:focus': {
        outline: '1px dashed',
        outlineColor: alpha(theme.palette.common.black, 0.87),
        outlineOffset: '1px',
      },
    },
  },

  // Avatar button
  '.MuiIconButton-root > .MuiAvatar-root': {
    width: theme.spacing(4),
    height: theme.spacing(4),
    fontSize: theme.typography.caption.fontSize,
    fontWeight: theme.typography.caption.fontWeight,
    letterSpacing: theme.typography.caption.letterSpacing,
    margin: theme.spacing(-0.5),
  },

  '.MuiIconButton-root.Mui-disabled > .MuiAvatar-root': {
    color: alpha(theme.palette.common.black, 0.26),
    backgroundColor: alpha(theme.palette.common.black, 0.08),
  },

  // Alerts
  '.MuiAlert-root.MuiAlert-standardError .MuiAlert-icon': {
    color: 'inherit',
  },
  '.MuiAlert-root.MuiAlert-standardWarning .MuiAlert-icon': {
    color: 'inherit',
  },
  '.MuiAlert-root.MuiAlert-standardInfo .MuiAlert-icon': {
    color: 'inherit',
  },
  '.MuiAlert-root.MuiAlert-standardSuccess .MuiAlert-icon': {
    color: 'inherit',
  },
  '.MuiAlert-root .MuiAlert-action': {
    paddingTop: 0,
  },
  '.MuiAlert-root .MuiAlert-action .MuiIconButton-root': {
    color: 'inherit',
  },
  '.MuiAlert-standardError .MuiAlert-action .MuiIconButton-root:hover': {
    backgroundColor: alpha(darken(theme.palette.error.main, 0.6), 0.12),
    color: darken(darken(theme.palette.error.main, 0.6), 0.12),
  },
  '.MuiAlert-standardError .MuiAlert-action .MuiIconButton-root:focus': {
    backgroundColor: alpha(darken(theme.palette.error.main, 0.6), 0.12),
    color: darken(darken(theme.palette.error.main, 0.6), 0.12),
  },
  '.MuiAlert-standardWarning .MuiAlert-action .MuiIconButton-root:hover': {
    backgroundColor: alpha(darken(theme.palette.warning.main, 0.6), 0.12),
    color: darken(darken(theme.palette.warning.main, 0.6), 0.12),
  },
  '.MuiAlert-standardWarning .MuiAlert-action .MuiIconButton-root:focus': {
    backgroundColor: alpha(darken(theme.palette.warning.main, 0.6), 0.12),
    color: darken(darken(theme.palette.warning.main, 0.6), 0.12),
  },
  '.MuiAlert-standardInfo .MuiAlert-action .MuiIconButton-root:hover': {
    backgroundColor: alpha(darken(theme.palette.info.main, 0.6), 0.12),
    color: darken(darken(theme.palette.info.main, 0.6), 0.12),
  },
  '.MuiAlert-standardInfo .MuiAlert-action .MuiIconButton-root:focus': {
    backgroundColor: alpha(darken(theme.palette.info.main, 0.6), 0.12),
    color: darken(darken(theme.palette.info.main, 0.6), 0.12),
  },
  '.MuiAlert-standardSuccess .MuiAlert-action .MuiIconButton-root:hover': {
    backgroundColor: alpha(darken(theme.palette.success.main, 0.6), 0.12),
    color: darken(darken(theme.palette.success.main, 0.6), 0.12),
  },
  '.MuiAlert-standardSuccess .MuiAlert-action .MuiIconButton-root:focus': {
    backgroundColor: alpha(darken(theme.palette.success.main, 0.6), 0.12),
    color: darken(darken(theme.palette.success.main, 0.6), 0.12),
  },
  '.MuiAlert-action .MuiIconButton-root:hover': {
    backgroundColor: alpha(theme.palette.common.black, 0.12),
  },
  '.MuiAlert-action .MuiIconButton-root:focus': {
    backgroundColor: alpha(theme.palette.common.black, 0.12),
  },

  // Accordions
  '.MuiPaper-root.MuiAccordion-root': {
    'borderRadius': 0,
    '&:last-of-type': {
      borderRadius: 0,
    },
    '&:first-of-type': {
      borderRadius: 0,
    },
    '&::before': {
      display: 'none',
    },
    '&.Mui-expanded': {
      margin: 0,
    },
    'boxShadow': 'none',
    'borderBottom': '1px solid',
    'borderBottomColor': theme.palette.divider,
    '&:last-child': {
      borderBottom: 'none',
    },
    '& .MuiButtonBase-root.MuiAccordionSummary-root': {
      'padding': 0,
      '& > .MuiAccordionSummary-content, .MuiAccordionSummary-content.Mui-expanded': {
        margin: theme.spacing(3, 0),
      },
    },
    '& .MuiAccordionDetails-root': {
      padding: theme.spacing(0, 0, 3, 0),
    },
  },

  // Textfields
  '.MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline': {
    borderColor: 'rgba(0, 0, 0, 0.42)',
  },
  // Dense
  '.MuiFormControl-root.MuiFormControl-marginDense': {
    'margin': theme.spacing(0, 0, 0, 0),
    // Outlined label
    '& .MuiInputLabel-outlined': {
      'fontSize': theme.typography.body2.fontSize,
      'letterSpacing': theme.typography.body2.letterSpacing,
      'lineHeight': '1.429em',
      'transform': 'translate(14px, 10px)',
      '&.MuiInputLabel-shrink': {
        'transform': 'translate(14px, -6px) ',
        'letterSpacing': theme.typography.caption.letterSpacing,
        'fontSize': theme.typography.caption.fontSize,
        'lineHeight': 1,
        '&+ .MuiOutlinedInput-root': {
          '& > fieldset > legend ': {
            fontSize: theme.typography.caption.fontSize,
            letterSpacing: theme.typography.caption.letterSpacing,
          },
        },
      },
    },
    // Outlined input
    '& .MuiOutlinedInput-root': {
      'fontSize': theme.typography.body2.fontSize,
      'letterSpacing': theme.typography.body2.letterSpacing,
      '& input.MuiOutlinedInput-input': {
        paddingTop: theme.spacing(1.25),
        paddingBottom: theme.spacing(1.25),
        height: '1.429em',
      },
      // Select
      '& .MuiSelect-select': {
        minHeight: '1.429em',
        height: '1.429em',
        paddingTop: theme.spacing(1.25),
        paddingBottom: theme.spacing(1.25),
      },
      '& .MuiSelect-icon': {
        color: theme.palette.buttons.default,
      },
      // Input adornments
      '& .MuiInputAdornment-root': {
        color: theme.palette.grey[500],
      },
    },
    // Outlined multiline input
    '& .MuiInputBase-multiline': {
      paddingTop: theme.spacing(1.25),
      paddingBottom: theme.spacing(1.25),
    },
  },
  // Autocomplete
  '.MuiAutocomplete-root': {
    // Dense
    '& .MuiFormControl-root.MuiFormControl-marginDense': {
      // Outlined input
      '& .MuiOutlinedInput-root': {
        'paddingTop': theme.spacing(0),
        'paddingBottom': theme.spacing(0),
        'paddingLeft': theme.spacing(0),
        'paddingRight': theme.spacing(0),
        '& .MuiAutocomplete-input': {
          paddingTop: theme.spacing(1.25),
          paddingBottom: theme.spacing(1.25),
          paddingLeft: theme.spacing(1.75),
          paddingRight: theme.spacing(6),
        },
        '& .MuiAutocomplete-endAdornment': {
          'right': theme.spacing(1),
          'top': 'calc(50% - 16px)',
          '& .MuiAutocomplete-clearIndicator': {
            padding: theme.spacing(0.75),
          },
          '& .MuiAutocomplete-popupIndicator': {
            padding: theme.spacing(0.5),
          },
        },
        '& .MuiAutocomplete-clearIndicator': {
          visibility: 'visible',
        },
      },
    },
  },
  '.MuiAutocomplete-root.MuiAutocomplete-hasPopupIcon.MuiAutocomplete-hasClearIcon': {
    // Dense
    '& .MuiFormControl-root.MuiFormControl-marginDense': {
      // Outlined input
      '& .MuiOutlinedInput-root': {
        'paddingRight': theme.spacing(0),
        '& .MuiAutocomplete-input': {
          'paddingRight': theme.spacing(10),
          '&[readonly]': {
            paddingRight: theme.spacing(1.75),
          },
        },
      },
    },
  },
  // Radio buttons & checkboxes
  'legend.MuiFormLabel-root:not(.MuiInputLabel-root)': {
    lineHeight: theme.typography.body2.lineHeight,
    fontSize: theme.typography.body2.fontSize,
    letterSpacing: theme.typography.body2.letterSpacing,
    color: theme.palette.text.primary,
    marginBottom: theme.spacing(1),
  },
  'legend.MuiFormLabel-root:not(.MuiInputLabel-root) + .MuiFormHelperText-root, .MuiCheckbox-root + .MuiFormControlLabel-label > .MuiFormHelperText-root': {
    marginTop: theme.spacing(-1),
    marginBottom: theme.spacing(1),
    marginLeft: 0,
    marginRight: 0,
  },
  'legend.MuiFormLabel-root:not(.MuiInputLabel-root).Mui-focused': {
    color: theme.palette.text.primary,
  },
  'legend.MuiFormLabel-root:not(.MuiInputLabel-root).Mui-error': {
    'color': theme.palette.error.main,
    '& +.MuiFormGroup-root': {
      '& .MuiRadio-root, & .MuiCheckbox-root': {
        color: `${theme.palette.error.main} !important`,
      },
    },
  },
  '.MuiFormHelperText-root.Mui-error': {
    '& +.MuiFormGroup-root': {
      '& .MuiRadio-root, & .MuiCheckbox-root': {
        color: `${theme.palette.error.main} !important`,
      },
    },
  },
  '.MuiFormGroup-root': {
    '& .MuiFormControlLabel-root': {
      'marginLeft': theme.spacing(-1),
      'marginRight': theme.spacing(0),
      'alignItems': 'start',
      '&:not(.Mui-disabled)': {
        '&:hover': {
          '& .MuiRadio-colorPrimary, & .MuiCheckbox-colorPrimary': {
            color: theme.palette.primary.dark,
            backgroundColor: alpha(theme.palette.primary.main, 0.12),
          },
        },
      },
    },
    '& .MuiFormControlLabel-label': {
      'fontSize': theme.typography.body2.fontSize,
      'letterSpacing': theme.typography.body2.letterSpacing,
      'padding': theme.spacing(1.25, 0, 1, 1),
      '& .MuiFormHelperText-root': {
        marginTop: theme.spacing(0),
      },
    },
  },
  '.MuiButtonBase-root.MuiRadio-root, .MuiButtonBase-root.MuiCheckbox-root': {
    color: theme.palette.buttons.default,
    padding: theme.spacing(1),
  },

  // Fix for bug: extra space under text field with long label in dialogs
  '.MuiOutlinedInput-root > fieldset > legend > span': {
    display: 'none',
  },
  '.MuiInputLabel-outlined.MuiInputLabel-shrink + .MuiOutlinedInput-root': {
    '& > fieldset > legend > span': {
      display: 'inline-block',
    },
  },
  // Fix for bug: fieldset overflowing container in IE11
  'fieldset.MuiFormControl-root': {
    maxWidth: '100%',
  },

  // Links
  '.MuiLink-root:focus': {
    outline: '1px dashed',
  },

  // Bullet lists
  'li::marker': {
    fontFamily: 'Roboto sans-serif',
  },

  // Lists & Menus
  '.MuiButtonBase-root.MuiMenuItem-root': {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    minHeight: theme.spacing(0),
  },
  '.MuiButtonBase-root.MuiMenuItem-root > a': {
    padding: theme.spacing(1, 2),
    margin: theme.spacing(-1, -2),
    display: 'flex',
    width: '100%',
    overflow: 'hidden',
    color: 'inherit',
    textDecoration: 'none !important',
  },
  '.MuiButtonBase-root.MuiMenuItem-root .MuiListItemText-root': {
    marginTop: theme.spacing(0),
    marginBottom: theme.spacing(0),
  },
  '.MuiButtonBase-root.MuiMenuItem-root .MuiListItemIcon-root': {
    minWidth: theme.spacing(0),
    marginTop: '-2px',
    marginBottom: '-2px',
  },
  '.MuiButtonBase-root.MuiMenuItem-root .MuiListItemText-root + .MuiListItemIcon-root': {
    marginLeft: theme.spacing(1),
  },
  '.MuiButtonBase-root.MuiMenuItem-root .MuiListItemIcon-root + .MuiListItemText-root': {
    marginLeft: theme.spacing(1),
  },

  // Datepickers // TODO: check these when datepickers are fixed
  '.MuiPickersToolbarText-toolbarTxt': {
    color: theme.palette.common.white,
  },
  '.MuiPickersCalendarHeader-dayLabel': {
    color: theme.palette.grey[600],
  },
  '.MuiPickersCalendarHeader-transitionContainer': {
    height: '1.5em',
  },

  // Tables
  'th.MuiTableCell-root, td.MuiTableCell-root': {
    padding: theme.spacing(1),
  },
  'th.MuiTableCell-stickyHeader': {
    backgroundColor: 'white',
  },
  /*
   Commented out for now, will revisit at a later date to address tabbing vs hover bg color
   '.MuiTableRow-root.Mui-selected, .MuiTableRow-root.Mui-selected:hover': {
      backgroundColor: fade(theme.palette.primary.main),
    },
    },*/
  '.MuiTableSortLabel-root:focus .MuiTableSortLabel-icon': {
    opacity: 0.5,
  },
  '.MuiTableCell-head': {
    '& .MuiButtonBase-root': {
      '&:focus': {
        outline: '1px dashed',
        outlineColor: alpha(theme.palette.common.black, 0.87),
        outlineOffset: '1px',
      },
    },
  },

  // Typography
  '.MuiTypography-root.MuiTypography-gutterBottom': {
    marginBottom: '0.5em',
  },

  // Pagination
  '.MuiButtonBase-root.MuiPaginationItem-root': {
    margin: '0px',
  },
  '.MuiButtonBase-root.MuiPaginationItem-page': {
    height: '2.5rem',
    minWidth: '2.5rem',
    borderRadius: '24px',
  },

  // Dialogs
  '.MuiDialog-paperWidthSm, .MuiDialog-paperWidthMd': {
    'width': 'calc(100% - 64px)',
    '& .MuiDialogContent-root': {
      padding: 0,
    },
    '& .MuiDialogActions-spacing': {
      'padding': theme.spacing(1.75, 3),
      '&> :not(:first-of-type)': {
        marginLeft: theme.spacing(2),
      },
    },
  },

  // *********** Custom styles **************
  '.MuiAvatar-root.avatar-orange': {
    color: theme.palette.getContrastText(deepOrange[500]),
    backgroundColor: deepOrange[500],
  },
  '.MuiAvatar-root.avatar-green': {
    color: theme.palette.getContrastText(green[800]),
    backgroundColor: green[800],
  },
  '.MuiAvatar-root.avatar-purple': {
    color: theme.palette.getContrastText(deepPurple[500]),
    backgroundColor: deepPurple[500],
  },
  'ul.list-horizontal': {
    'display': 'flex',
    'flexWrap': 'wrap',
    'alignItems': 'center',
    'padding': 0,
    '& li': {
      display: 'inline-block',
    },
  },
  '.MuiPaper-root.paper-grey': {
    padding: theme.spacing(2),
    backgroundColor: theme.palette.grey[100],
    borderColor: alpha(theme.palette.common.black, 0.08),
  },
  '.resp-iframe-container': {
    position: 'relative',
    overflow: 'hidden',
    paddingTop: '56.25%',
  },
  'iframe.resp-iframe': {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    border: 0,
  },
  '*, div, span, button, h1, h2, h3, h4, h5, h6, p, legend, label': {
    '&.screen-reader-text': {
      position: 'fixed',
      left: '-10000px',
      top: 'auto',
      width: '1px',
      height: '1px',
      overflow: 'hidden',
    },
  },
  '.row': {
    'display': 'flex',
    'margin': theme.spacing(1.5, 0),
    'flexFlow': 'row',
    'height': 'auto',
    'justifyContent': 'space-between',
    'width': '100%',
    'alignItems': 'center',
    '&:first-of-type': {
      marginTop: 0,
    },
    '&:last-child': {
      marginBottom: 0,
    },
  },
  '.column': {
    'display': 'flex',
    'flexDirection': 'column',
    'width': '100%',
    'justifyContent': 'center',
    'marginRight': theme.spacing(1),
    'height': '100%',
    '&:last-child': {
      marginRight: 0,
    },
  },
  '.border-top': {
    borderTop: '1px solid',
    borderTopColor: theme.palette.divider,
  },
  '.border-bottom': {
    borderBottom: '1px solid',
    borderBottomColor: theme.palette.divider,
  },
};
