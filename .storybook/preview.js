import React from 'react';

import {addDecorator} from '@storybook/react';
import {ThemeProvider, StyledEngineProvider} from '@mui/material/styles';
import {ThemeProvider as Emotion10ThemeProvider} from 'emotion-theming';

import {styles} from '../src/Theme/globalStyles';
import {theme} from '../src/Theme/theme';
import GlobalStyles from '@mui/material/GlobalStyles';
import '../src/styles/styles.css';

const globalStyles = <GlobalStyles styles={styles} />;

addDecorator((story) => {
  return (
    <StyledEngineProvider injectFirst>
      <Emotion10ThemeProvider theme={theme}>
        <ThemeProvider theme={theme}>
          {globalStyles}
          {story()}
        </ThemeProvider>
      </Emotion10ThemeProvider>
    </StyledEngineProvider>
  );
});

export const parameters = {
  actions: {argTypesRegex: '^on[A-Z].*'},
  previewTabs: {
    docs: {
      hidden: false,
    },
  },
};
